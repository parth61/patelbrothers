<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Visitors extends Model
{
    protected  $table = 'tbl_visitors';
    protected $fillable=['visitor_tag','utm_source','utm_medium','utm_campaign','utm_content','utm_term','utm_initial_referrer','utm_last_referrer','utm_landing_page','utm_visits','browser_name','browser_version','browser_platform','ip_address','created_at','updated_at'];
    
     public static function getvisitors($data)
    {
        
        $query = DB::table('tbl_visitors as tv')->select('tv.*');
         
        if (array_key_exists('visitor_tag', $data) && isset($data['visitor_tag'])) {
            $query = $query->where('tv.visitor_tag', '=' ,$data['visitor_tag']);
        }

        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tv.visitor_id', 'ASC')->get();
                            
        return $result;
    }
}
