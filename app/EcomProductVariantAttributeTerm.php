<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductVariantAttributeTerm extends Model
{
    protected $table = 'tbl_ecom_product_variant_attribute_term';
    protected $fillable=['attribute_term_id','variant_product_id',
    'created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    public static function getproductvariantattributeterm($data)
    {
        
        $query = DB::table('tbl_ecom_product_variant_attribute_term as provariantattrterm')->select('provariantattrterm.*');
         
        if (array_key_exists('variant_product_id', $data) && isset($data['variant_product_id'])) {
            $query = $query->where('provariantattrterm.variant_product_id', '=' ,$data['variant_product_id']);
        }
        if (array_key_exists('attribute_term_id', $data) && isset($data['attribute_term_id'])) {
            $query = $query->where('provariantattrterm.attribute_term_id', '=' ,$data['attribute_term_id']);
        }
    
        if (array_key_exists('product_variant_attribute_term_id', $data) && isset($data['product_variant_attribute_term_id'])) {
            $query = $query->where('provariantattrterm.product_variant_attribute_term_id', '=' ,$data['product_variant_attribute_term_id']);
        }   
    
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('provariantattrterm.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('provariantattrterm.status', '=' ,1);
        }
    
        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('provariantattrterm.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('provariantattrterm.product_variant_attribute_term_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

}  
