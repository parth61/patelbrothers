<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class GeneralSetting extends Model
{
    Protected $table = 'tbl_generalsetting';
    protected $fillable=['site_title','tag_line','time_zone','date_format','search_engine_visibility','site_id',
    'created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getgeneralsetting($data)
    {
        
        $query = DB::table('tbl_generalsetting as gs')->select('gs.*');

         
        if (array_key_exists('generalsetting_id', $data) && isset($data['generalsetting_id'])) {
            $query = $query->where('gs.generalsetting_id', '=' ,$data['generalsetting_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('gs.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('gs.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('gs.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('gs.'.$data['sortby'], $data['sorttype']);
        }
                    
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
