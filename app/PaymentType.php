<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PaymentType extends Model
{
    protected $table = 'tbl_paymenttype';
    protected $fillable=['lead_remarks','user_id','project_id','sales_executive_id','touchpoint_id','source_id','lead_status_primary','lead_status_seconday','lead_status_tertiary','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getPaymentType($data)
    {
        
         $query = DB::table('tbl_paymenttype as tpt')->select('tpt.*');
         $query = $query->where('tpt.status', '=' ,1);             
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tpt.paymenttype_id', 'ASC')->get();
                            
         return $result;
    }
}
