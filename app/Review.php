<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Review extends Model
{
    protected $table = 'tbl_review';
    protected $fillable=['product_id','review','user_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getreview($data)
    {
        
        $query = DB::table('tbl_review as rev')->select('rev.*');
         
        if (array_key_exists('amenities_id', $data) && isset($data['amenities_id'])) {
            $query = $query->where('am.amenities_id', '=' ,$data['amenities_id']);
        }
                          
        $query = $query->where('am.status', '=' ,1);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('am.amenities_id', 'ASC')->get();
                            
        return $result;
    }
}
