<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Regions extends Model
{
    protected $table = 'tbl_regions';
    protected $fillable=['region','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getregion($data)
    {
        
         $query = DB::table('tbl_regions as tb')->select('tb.*');
         
         if (array_key_exists('region_id', $data) && isset($data['region_id'])) {
            $query = $query->where('tb.region_id', '=' ,$data['region_id']);
           }
                          
         $query = $query->where('tb.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tb.region_id', 'ASC')->get();
                            
         return $result;
    }
}
