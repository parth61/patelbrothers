<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    protected $table = 'tbl_amenities';
    protected $fillable=['amenities_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getamenities($data)
    {
        
        $query = DB::table('tbl_amenities as am')->select('am.*');
         
        if (array_key_exists('amenities_id', $data) && isset($data['amenities_id'])) {
            $query = $query->where('am.amenities_id', '=' ,$data['amenities_id']);
        }
                          
        $query = $query->where('am.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('am.amenities_id', 'ASC')->get();
                            
        return $result;
    }
}
