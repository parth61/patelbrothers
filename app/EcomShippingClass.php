<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomShippingClass extends Model
{
    protected $table = 'tbl_ecom_shipping_class';
    protected $fillable=['shipping_class_name','slug_id','shipping_class_description','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getshippingclass($data)
    {
        
        $query = DB::table('tbl_ecom_shipping_class as shipping_class')
        ->select('shipping_class.*','shipping_class.slug_name as shipping_class_slug');
         
         
        if (array_key_exists('shipping_class_id', $data) && isset($data['shipping_class_id'])) {
            $query = $query->where('shipping_class.shipping_class_id', '=' ,$data['shipping_class_id']);
        }
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('shipping_class.slug_name', '=' ,$data['slug_name']);
        }
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('shipping_class.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('shipping_class.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('shipping_class.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)){
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('shipping_class.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('shipping_class.shipping_class_id', 'DESC');
        }
                          
        $result = $query->get();
                            
        return $result;
    }
}
