<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomTempOrder extends Model
{
     protected $table = 'tbl_ecom_temp_order';
    protected $fillable=['user_id','coupon_id','shipping_id','site_id','order_amount','order_tax','order_total','order_discount','order_shipping_charge','order_grandtotal','order_status','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getorder($data)
    {
        
        $query = DB::table('tbl_ecom_temp_order as ord')->select('ord.*','us.*','cp.coupon_code','cp.coupon_description','cp.discount_type','shipp.shipping_method_id','shipp.shipping_rate')
        ->leftJoin('tbl_user as us', 'us.user_id', '=', 'ord.user_id')
        ->leftJoin('tbl_ecom_shipping as shipp', 'shipp.shipping_id', '=', 'ord.shipping_id')
        ->leftJoin('tbl_ecom_coupon as cp', 'cp.coupon_id', '=', 'ord.coupon_id');
         
        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('ord.order_id', '=' ,$data['order_id']);
        }

        if (array_key_exists('order_guid', $data) && isset($data['order_guid'])) {
            $query = $query->where('ord.order_guid', '=' ,$data['order_guid']);
        }

        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('ord.user_id', '=' ,$data['user_id']);
        }
               
        if (array_key_exists('shipping_id', $data) && isset($data['shipping_id'])) {
            $query = $query->where('ord.shipping_id', '=' ,$data['shipping_id']);
        }

        if (array_key_exists('coupon_id', $data) && isset($data['coupon_id'])) {
            $query = $query->where('ord.coupon_id', '=' ,$data['coupon_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ord.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ord.status', '!=' ,0);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ord.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('from', $data) && isset($data['from']) && $data['from'] == "customer") {
            $query = $query->where('ord.order_status', '!=' ,'pending');
        }

        if (array_key_exists('from_date', $data) && isset($data['from_date']) && array_key_exists('to_date', $data) && isset($data['to_date'])) {
            $query = $query->whereBetween(DB::raw('DATE(ord.created_at)'), array($data['from_date'], $data['to_date']));
        }

        if (array_key_exists('order_status', $data) && isset($data['order_status'])) {
            $query = $query->where('ord.order_status', '=' , $data['order_status']);
        }

        $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ord.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('ord.order_id', 'DESC');
        }

                          
        $result = $query->get();
                            
        return $result;
    }
}
