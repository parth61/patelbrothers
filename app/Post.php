<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Post extends Model
{
    protected $table = 'tbl_post';
    protected $fillable=['post_id','post_title','post_short_description','slug_name','post_body','post_featured_img','site_id',
    'postcategory_id','seo_id','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getpost($data)
    {
        
        $query = DB::table('tbl_post as po')
        ->select('po.*','po.slug_name as post_slug','adm.admin_firstname as cb_admin_firstname','adm.admin_lastname as cb_admin_lastname',
        'admin.admin_firstname as ub_admin_firstname','admin.admin_lastname as ub_admin_lastname','admin.admin_slug',
        'med.media_file as post_media_file','seo.seo_description','seo.seo_keywords','seo.seo_image','seo.seo_type','seo.seo_for','seo.seo_for_id','seo.seo_title','seo.seo_canonical','medi.media_file as seo_media_file')
        ->leftJoin('tbl_media as med','med.media_id','=','po.post_featured_img')
        ->leftJoin('tbl_admin as adm','adm.admin_id','=','po.created_by')
        ->leftJoin('tbl_admin as admin','admin.admin_id','=','po.updated_by')
        ->leftJoin('tbl_seo as seo','seo.seo_id','=','po.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');


        
        
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('po.slug_name', '=' ,$data['slug_name']);
        }
        if (array_key_exists('author', $data) && isset($data['author'])) {
            $query = $query->where('po.updated_by', '=' ,$data['author']);
        }
        if (array_key_exists('admin_slug', $data) && isset($data['admin_slug'])) {
            $query = $query->where('admin.admin_slug', '=' ,$data['admin_slug']);
        }
       
         
        
        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('po.post_id', '=' ,$data['post_id']);
        }
       

      


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('po.status', '=' ,$data['status']);
        } 
        else
        {
            $query = $query->where('po.status', '!=' ,0);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('po.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('po.'.$data['sortby'], $data['sorttype']);
        }
                        
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('order_by', $data) && isset($data['order_by'])) {
            $query = $query->orderBy('po.post_id',$data['order_by'] );
        }
        
        // $query = DB::table('tbl_postcategory as postcategory')
        // ->where('slug_name','residential-news');
        // if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
        //     $query = $query->where('postcategory.slug_name', '=' ,$data['slug_name']);
        // }
        $result = $query->get();
                            
        return $result;
    }
    public static function getpostbycategory($data)
    {
        
        $query = DB::table('tbl_relation_post_category as rel')
        ->select('rel.postcategory_id','rel.relation_post_category_id','rel.post_id',
        'po.*','po.slug_name as post_slug','adm.admin_firstname as cb_admin_firstname','adm.admin_lastname as cb_admin_lastname',
        'admin.admin_firstname as ub_admin_firstname','admin.admin_lastname as ub_admin_lastname','admin.admin_slug',
        'med.media_file as post_media_file','seo.seo_description','seo.seo_keywords','seo.seo_image','seo.seo_type','seo.seo_for','seo.seo_for_id','seo.seo_title','seo.seo_canonical','medi.media_file as seo_media_file'
        ,'postcategory.slug_name as categoryslug_name','postcategory.postcategory_name')
        ->leftJoin('tbl_post as po','po.post_id','=','rel.post_id')
        ->leftJoin('tbl_postcategory as postcategory','postcategory.postcategory_id','=','rel.postcategory_id')
        ->leftJoin('tbl_media as med','med.media_id','=','po.post_featured_img')
        ->leftJoin('tbl_admin as adm','adm.admin_id','=','po.created_by')
        ->leftJoin('tbl_admin as admin','admin.admin_id','=','po.updated_by')
        ->leftJoin('tbl_seo as seo','seo.seo_id','=','po.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');


        
        
        if (array_key_exists('post_slug', $data) && isset($data['post_slug'])) {
            $query = $query->where('po.slug_name', '=' ,$data['post_slug']);
        }
        if (array_key_exists('categoryslug_name', $data) && isset($data['categoryslug_name'])) {
            $query = $query->where('postcategory.slug_name', '=' ,$data['categoryslug_name']);
        }
        if (array_key_exists('postcategory_id', $data) && isset($data['postcategory_id'])) {
            $query = $query->where('postcategory.postcategory_id', '=' ,$data['postcategory_id']);
        }
        if (array_key_exists('admin_slug', $data) && isset($data['admin_slug'])) {
            $query = $query->where('admin.admin_slug', '=' ,$data['admin_slug']);
        }
       
        if (array_key_exists('author', $data) && isset($data['author'])) {
            $query = $query->where('po.updated_by', '=' ,$data['author']);
        }
         
        
        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('po.post_id', '=' ,$data['post_id']);
        }
       

      


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('po.status', '=' ,$data['status']);
        } 
        else
        {
            $query = $query->where('po.status', '!=' ,0);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('po.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('po.'.$data['sortby'], $data['sorttype']);
        }
                        
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('order_by', $data) && isset($data['order_by'])) {
            $query = $query->orderBy('po.post_id',$data['order_by'] );
        }
        
        // $query = DB::table('tbl_postcategory as postcategory')
        // ->where('slug_name','residential-news');
        // if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
        //     $query = $query->where('postcategory.slug_name', '=' ,$data['slug_name']);
        // }
        $result = $query->get();
                            
        return $result;
    }

    public static function getpostbytags($data)
    {
        
        $query = DB::table('tbl_relation_post_tag as rel')
        ->select('rel.relation_post_tag_id','rel.posttag_id','rel.post_id',
        'po.*','po.slug_name as post_slug','adm.admin_firstname as cb_admin_firstname','adm.admin_lastname as cb_admin_lastname','admin.admin_slug',
        'admin.admin_firstname as ub_admin_firstname','admin.admin_lastname as ub_admin_lastname',
        'med.media_file as post_media_file','seo.seo_description','seo.seo_keywords','seo.seo_image','seo.seo_type','seo.seo_for','seo.seo_for_id','seo.seo_title','seo.seo_canonical','medi.media_file as seo_media_file'
        ,'posttag.slug_name as posttagslug_name','posttag.slug_name as posttagslug_name','posttag.posttag_name')
        ->leftJoin('tbl_post as po','po.post_id','=','rel.post_id')
        ->leftJoin('tbl_posttag as posttag','posttag.posttag_id','=','rel.posttag_id')
        ->leftJoin('tbl_media as med','med.media_id','=','po.post_featured_img')
        ->leftJoin('tbl_admin as adm','adm.admin_id','=','po.created_by')
        ->leftJoin('tbl_admin as admin','admin.admin_id','=','po.updated_by')
        ->leftJoin('tbl_seo as seo','seo.seo_id','=','po.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');


        
        
        if (array_key_exists('post_slug', $data) && isset($data['post_slug'])) {
            $query = $query->where('po.slug_name', '=' ,$data['post_slug']);
        }
        if (array_key_exists('categoryslug_name', $data) && isset($data['categoryslug_name'])) {
            $query = $query->where('posttag.slug_name', '=' ,$data['categoryslug_name']);
        }
        if (array_key_exists('relation_post_tag_id', $data) && isset($data['relation_post_tag_id'])) {
            $query = $query->where('posttag.relation_post_tag_id', '=' ,$data['relation_post_tag_id']);
        }
        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('po.post_id', '=' ,$data['post_id']);
        }
        if (array_key_exists('author', $data) && isset($data['author'])) {
            $query = $query->where('po.updated_by', '=' ,$data['author']);
        }
        if (array_key_exists('author', $data) && isset($data['author'])) {
            $query = $query->where('po.updated_by', '=' ,$data['author']);
        }
        if (array_key_exists('admin_slug', $data) && isset($data['admin_slug'])) {
            $query = $query->where('admin.admin_slug', '=' ,$data['admin_slug']);
        }
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('po.status', '=' ,$data['status']);
        } 
        else
        {
            $query = $query->where('po.status', '!=' ,0);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('po.site_id', '=' ,$data['site_id']);
        }
        if (array_key_exists('posttag_name', $data) && isset($data['posttag_name']) ) {
            $query = $query->where('posttag.posttag_name', 'LIKE' ,'%'.$data['posttag_name'].'%');
            
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('po.'.$data['sortby'], $data['sorttype']);
        }
                        
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('order_by', $data) && isset($data['order_by'])) {
            $query = $query->orderBy('po.post_id',$data['order_by'] );
        }
        
        
        // $query = DB::table('tbl_postcategory as postcategory')
        // ->where('slug_name','residential-news');
        // if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
        //     $query = $query->where('postcategory.slug_name', '=' ,$data['slug_name']);
        // }
        $result = $query->get();
                            
        return $result;
    }
    
}
