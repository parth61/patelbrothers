<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Legaldocuments extends Model
{
    protected $table = 'tbl_legal_document_type';
    protected $fillable=['legal_document_type','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getlegaldocuments($data)
    {
        
         $query = DB::table('tbl_legal_document_type as b')->select('b.*');
         
         if (array_key_exists('legal_document_type_id', $data) && isset($data['legal_document_type_id'])) {
            $query = $query->where('b.legal_document_type_id', '=' ,$data['legal_document_type_id']);
           }
                          
         $query = $query->where('b.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.legal_document_type_id', 'ASC')->get();
                            
         return $result;
    }
}
