<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Leadstatussecondary extends Model
{
    protected  $table = 'tbl_lead_status_secondary';
    protected $fillable=['lead_status_primary_id','lead_status_primary_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getleadsecondary($data)
    {
        
        $query = DB::table('tbl_lead_status_secondary as s')->select('s.*','c.lead_status_primary_name')
        ->leftJoin('tbl_lead_status_primary as c', 'c.lead_status_primary_id', '=', 's.lead_status_primary_id');
         
        if (array_key_exists('lead_status_primary_id', $data) && isset($data['lead_status_primary_id'])) {
            $query = $query->where('s.lead_status_primary_id', '=' ,$data['lead_status_primary_id']);
        }

        if (array_key_exists('lead_status_secondary_id', $data) && isset($data['lead_status_secondary_id'])) {
            $query = $query->where('s.lead_status_secondary_id', '=' ,$data['lead_status_secondary_id']);
        }
                          
        $query = $query->where('s.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('s.lead_status_secondary_id', 'ASC')->get();
                            
        return $result;
    }
}
