<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class RelationPostTag extends Model
{
    protected $table = 'tbl_relation_post_tag';
    protected $fillable=['relation_post_tag_id','posttag_id','post_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getrelationposttag($data)
    {
        
        $query = DB::table('tbl_relation_post_tag as rpt')->select('rpt.*');
         
        if (array_key_exists('relation_post_tag_id', $data) && isset($data['relation_post_tag_id'])) {
            $query = $query->where('rpt.relation_post_tag_id', '=' ,$data['relation_post_tag_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('rpt.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('rpt.status', '=' ,1);
        }

        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('rpt.post_id', '=' ,$data['post_id']);
        }
        if (array_key_exists('posttag_id', $data) && isset($data['posttag_id'])) {
            $query = $query->where('rpt.posttag_id', '=' ,$data['posttag_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('rpt.'.$data['sortby'], $data['sorttype']);
        }           
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
