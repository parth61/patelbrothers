<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TrackingCode extends Model
{
    protected $table = 'tbl_tracking_codes';
    protected $fillable=['tracking_googleanalytics','tracking_facebook_pixelcode','tracking_googleadwords','tracking_googleadwords_label_phone','tracking_googleadwords_label_conversation','trackingl_facebook_pixelid','tracking_taboolaid','tracking_colombia_pixel','tracking_colombia_pixel_conversation','tracking_colombia_pixel_phoneconversation','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function gettrackingcode($data)
    {
        
        $query = DB::table('tbl_tracking_codes as ttc')->select('ttc.*');

         
        if (array_key_exists('tracking_code_id', $data) && isset($data['tracking_code_id'])) {
            $query = $query->where('ttc.tracking_code_id', '=' ,$data['tracking_code_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ttc.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('ttc.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ttc.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ttc.'.$data['sortby'], $data['sorttype']);
        }
                    
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
