<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Leadsourceprimary extends Model
{
    protected  $table = 'tbl_lead_source_primary';
    protected $fillable=['lead_source_primary_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getleadsourceprimary($data)
    {
        
         $query = DB::table('tbl_lead_source_primary as c')->select('c.*');
         
         if (array_key_exists('lead_source_primary_id', $data) && isset($data['lead_source_primary_id'])) {
            $query = $query->where('c.lead_source_primary_id', '=' ,$data['lead_source_primary_id']);
           }
                          
         $query = $query->where('c.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.lead_source_primary_id', 'ASC')->get();
                            
         return $result;
    }
}
