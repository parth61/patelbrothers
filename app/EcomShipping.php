<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomShipping extends Model
{
    protected $table = 'tbl_ecom_shipping';
    protected $fillable=['site_id','shipping_method_id','shipping_class_id','shipping_rate','shipping_city','shipping_state','shipping_country','shipping_pincode','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getshippings($data)
    {
        
        $query = DB::table('tbl_ecom_shipping as sp')->select('sp.*','sm.shipping_method','sm.shipping_method_id','sc.shipping_class_name','sc.shipping_class_id')
        ->leftJoin('tbl_ecom_shipping_methods as sm', 'sm.shipping_method_id', '=', 'sp.shipping_method_id')
        ->leftJoin('tbl_ecom_shipping_class as sc', 'sc.shipping_class_id', '=', 'sp.shipping_class_id');
         
        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('sp.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('shipping_method_id', $data) && isset($data['shipping_method_id'])) {
            $query = $query->where('sp.shipping_method_id', '=' ,$data['shipping_method_id']);
        }

        if (array_key_exists('shipping_class_id', $data) && isset($data['shipping_class_id'])) {
            $query = $query->where('sp.shipping_class_id', '=' ,$data['shipping_class_id']);
        }

        if (array_key_exists('shipping_city', $data) && isset($data['shipping_city'])) {
            $query = $query->where('sp.shipping_city', '=' ,$data['shipping_city']);
        }

        if (array_key_exists('shipping_state', $data) && isset($data['shipping_state'])) {
            $query = $query->where('sp.shipping_state', '=' ,$data['shipping_state']);
        }

        if (array_key_exists('shipping_country', $data) && isset($data['shipping_country'])) {
            $query = $query->where('sp.shipping_country', '=' ,$data['shipping_country']);
        }

        if (array_key_exists('shipping_pincode', $data) && isset($data['shipping_pincode'])) {
            $query = $query->where('sp.shipping_pincode', '=' ,$data['shipping_pincode']);
        }
        
        $query = $query->where('sp.status', '=' ,1)->where('sm.status', '=' ,1)->where('sc.status', '=' ,1);

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)){
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
    
        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('sm.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('sp.shipping_id', 'DESC');
        }
                          
        $result = $query->get();
                            
        return $result;
    }

    public function getshippingforsiteid($shipping_class_id, $shipping_method_id, $site_id, $userdata)
    {
        
        /* Location Based Shipping Methods */

        /* Get Applied Tax */
        $appliedshipping = DB::table('tbl_ecom_shipping as ship')->select('ship.*')
            ->whereIn('ship.shipping_country', [$userdata->country,'*'])
            ->whereIn('ship.shipping_state', [$userdata->state,'*'])
            ->whereIn('ship.shipping_city', [$userdata->city,'*'])
            ->whereIn('ship.shipping_pincode', [$userdata->pincode,'*'])
            ->where('ship.site_id', $site_id)
            ->where('ship.shipping_method_id', 2)
            ->where('ship.shipping_class_id', 1)
            ->where('ship.status', 1)
            ->orderBy('ship.shipping_rate', 'asc')
            ->offset(0)->limit(1)
            ->get();

        return $appliedshipping;

        /* Previous logic

        $query = DB::table('tbl_ecom_shipping as sp')->select('sp.*','sm.shipping_method','sm.shipping_method_id','sc.shipping_class_name','sc.shipping_class_id')
        ->leftJoin('tbl_ecom_shipping_methods as sm', 'sm.shipping_method_id', '=', 'sp.shipping_method_id')
        ->leftJoin('tbl_ecom_shipping_class as sc', 'sc.shipping_class_id', '=', 'sp.shipping_class_id');
         
        $query = $query->where('sp.site_id', '=' ,$site_id);

        $query = $query->where('sp.shipping_class_id', '=' ,$shipping_class_id);

        $query = $query->where('sp.status', '=' ,1);
                          
        $result = $query->first();
                            
        return $result;

        */
    }
}
