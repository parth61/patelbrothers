<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class UserJourney extends Model
{
    protected $table = 'tbl_user_journey';
    protected $fillable=['user_id','user_page_visit','site_id','browser_name','browser_version','browser_platform','ip_address'];

    public static function getamenities($data)
    {
        
         $query = DB::table('tbl_user_journey as uj')->select('uj.*')
         ->leftJoin('tbl_user as u', 'u.user_id', '=', 'u.user_id');
         
        if (array_key_exists('user_journey_id', $data) && isset($data['user_journey_id'])) {
            $query = $query->where('uj.user_journey_id', '=' ,$data['user_journey_id']);
        }
           
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('uj.site_id', '=' ,$data['site_id']);
        } 
        
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('uj.user_id', '=' ,$data['user_id']);
        }
                          
         $query = $query->where('uj.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('uj.user_journey_id', 'DESC')->get();
                            
         return $result;
    }
}
