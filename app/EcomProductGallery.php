<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductGallery extends Model
{
    protected $table = 'tbl_ecom_product_gallery';
    protected $fillable=['product_gallery_id','product_id','product_image','isfeatured','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getproductgallery($data)
    {
        
        $query = DB::table('tbl_ecom_product_gallery as pg')->select('pg.*','med.media_file as product_image_media_file')
        ->leftJoin('tbl_media as med','med.media_id','=','pg.product_image');
         
        if (array_key_exists('product_gallery_id', $data) && isset($data['product_gallery_id'])) {
            $query = $query->where('pg.product_gallery_id', '=' ,$data['product_gallery_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('pg.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('isfeatured', $data) && isset($data['isfeatured'])) {
            $query = $query->where('pg.isfeatured', '=' ,$data['isfeatured']);
        }             
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pg.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pg.status', '=' ,1);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('pg.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('pg.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('pg.product_gallery_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
