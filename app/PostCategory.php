<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class PostCategory extends Model
{
    protected $table = 'tbl_postcategory';
    protected $fillable=['postcategory_name','slug_name','postcategory_description','site_id',
    'created_by','updated_by','seo_id','parentcategory_id','browser_name','browser_version','browser_platform','ip_address'];

    public static function getpostcategory($data)
    {
        
        $query = DB::table('tbl_postcategory as pc')->select('pc.*',
        'pc.slug_name as postcategory_slug','seo.*','medi.media_file as seo_media_file')
        ->leftJoin('tbl_seo as seo','seo.seo_id','=','pc.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');

        if (array_key_exists('postcategory_id', $data) && isset($data['postcategory_id'])) {
            $query = $query->where('pc.postcategory_id', '=' ,$data['postcategory_id']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('pc.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pc.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pc.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('pc.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('pc.'.$data['sortby'], $data['sorttype']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
                    
        $result = $query->get();
                            
        return $result;
    }
}
