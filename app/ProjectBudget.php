<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectBudget extends Model
{
    protected $table = 'tbl_project_budget';
    protected $fillable=['project_id','budget_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getprojectbudget($data)
    {
        
         $query = DB::table('tbl_project_budget as pb')
         ->select('pb.*','b.budget_min_amount','b.budget_max_amount')
         ->leftJoin('tbl_budget as b','b.budget_id','=','pb.budget_id');
         
           if (array_key_exists('project_budget_id', $data) && isset($data['project_budget_id'])) {
            $query = $query->where('pb.project_budget_id', '=' ,$data['project_budget_id']);
           }
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('pb.project_id', '=' ,$data['project_id']);
           }
           if (array_key_exists('budget_id', $data) && isset($data['budget_id'])) {
            $query = $query->where('pb.budget_id', '=' ,$data['budget_id']);
           }
                          
         $query = $query->where('pb.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('pb.project_budget_id', 'ASC')->get();
                            
         return $result;
    }
    public static function getprojectbudgetid($data)
    {
        
         $query = DB::table('tbl_project_budget as pb')
         ->select('pb.budget_id');
        
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('pb.project_id', '=' ,$data['project_id']);
           }
          $query = $query->where('pb.status', '=' ,1);
                          
         $result = $query->get();
                            
         return $result;
    }
   
}
