<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Package extends Model
{
    protected  $table = 'tbl_package';
    protected $fillable=['package_id','package_name','slug_id','slug_name','package_description','package_projects','package_images','package_executives','browser_name','browser_version','browser_platform','ip_address','created_at','updated_at'];
    
     public static function getPackage($data)
    {
        
        $query = DB::table('tbl_package as pa')->select('pa.*');
         
        if (array_key_exists('package_id', $data) && isset($data['package_id'])) {
            $query = $query->where('pa.package_id', '=' ,$data['package_id']);
        }

        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('pa.package_id', 'ASC')->get();
                            
        return $result;
    }
}
