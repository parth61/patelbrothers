<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomTag extends Model
{
    protected $table = 'tbl_ecom_tag';
    protected $fillable=['tag_name','slug_id','tag_description','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function gettag($data)
    {
        
        $query = DB::table('tbl_ecom_tag as tag')->select('tag.*','tag.slug_name as tag_slug');
         
        if (array_key_exists('tag_id', $data) && isset($data['tag_id'])) {
            $query = $query->where('tag.tag_id', '=' ,$data['tag_id']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('tag.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('tag.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('tag.status', '=' ,1);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('tag.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('tag.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('tag.tag_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
