<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomCart extends Model
{
    protected $table = 'tbl_ecom_cart';
    protected $fillable=['product_id','user_id','attribute_id','attribute_term_id','cart_quantity','cart_amount','cart_tax','cart_total','isbuynow','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getcart($data)
    {
        
        /*$query = DB::table('tbl_ecom_cart as ct')->select('ct.*','pro.*','us.*')*/
        $query = DB::table('tbl_ecom_cart as ct')->select('ct.*','pro.product_name','pro.product_id','pro.product_short_description',
            'pro.product_actual_price','pro.product_sale_price','pro.start_date','pro.end_date','pro.stock_status','pro.product_stock','pro.is_stockmanagement','pro.product_height','pro.product_length','pro.product_weight','pro.product_wide','pro.slug_name','pro.has_variation','pro.is_variation','pro.product_type')
        ->leftJoin('tbl_ecom_product as pro', 'pro.product_id', '=', 'ct.product_id');
         
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('ct.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('ct.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('visitor_tag', $data) && isset($data['visitor_tag'])) {
            $query = $query->where('ct.visitor_tag', '=' ,$data['visitor_tag']);
        }

        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('ct.attribute_id', '=' ,$data['attribute_id']);
        }
           
        if (array_key_exists('attribute_term_id', $data) && isset($data['attribute_term_id'])) {
            $query = $query->where('ct.attribute_term_id', '=' ,$data['attribute_term_id']);
        } 
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ct.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ct.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ct.status', '=' ,1);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ct.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('ct.cart_id', 'DESC');
        }

                          
        $result = $query->get();

        foreach($result as $item){
            $price = $item->product_actual_price;
            if($item->product_sale_price != null && $item->product_sale_price != 0){
                $currentDate = date('Y-m-d');
                $currentDate = date('Y-m-d', strtotime($currentDate));   
                $startDate = date('Y-m-d', strtotime($item->start_date));
                $endDate = date('Y-m-d', strtotime($item->end_date));   
                if (($currentDate >= $startDate) && ($currentDate <= $endDate)){   
                    $price = $item->product_sale_price;
                }
            }
            $item->cart_amount = $price;
            $item->cart_total = floatval($price)*intval($item->cart_quantity);
        }
                            
        return $result;
    }

    public static function getremindercart($data)
    {
        
        /*$query = DB::table('tbl_ecom_cart as ct')->select('ct.*','pro.*','us.*')*/
        $query = DB::table('tbl_ecom_cart as ct')->select('ct.*','pro.product_name','pro.product_id','pro.product_short_description',
            'pro.product_actual_price','pro.product_sale_price','pro.start_date','pro.end_date','pro.stock_status','pro.product_stock','pro.is_stockmanagement','pro.product_height','pro.product_length','pro.product_weight','pro.product_wide','pro.slug_name','pro.has_variation','pro.is_variation','pro.product_type','medi.media_file as product_image_media_file')
        ->leftJoin('tbl_ecom_product as pro', 'pro.product_id', '=', 'ct.product_id')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
         
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('ct.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('ct.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('visitor_tag', $data) && isset($data['visitor_tag'])) {
            $query = $query->where('ct.visitor_tag', '=' ,$data['visitor_tag']);
        }

        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('ct.attribute_id', '=' ,$data['attribute_id']);
        }
           
        if (array_key_exists('attribute_term_id', $data) && isset($data['attribute_term_id'])) {
            $query = $query->where('ct.attribute_term_id', '=' ,$data['attribute_term_id']);
        } 
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ct.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ct.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ct.status', '=' ,1);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ct.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('ct.cart_id', 'DESC');
        }

                          
        $result = $query->get();

        foreach($result as $item){
            $price = $item->product_actual_price;
            if($item->product_sale_price != null && $item->product_sale_price != 0){
                $currentDate = date('Y-m-d');
                $currentDate = date('Y-m-d', strtotime($currentDate));   
                $startDate = date('Y-m-d', strtotime($item->start_date));
                $endDate = date('Y-m-d', strtotime($item->end_date));   
                if (($currentDate >= $startDate) && ($currentDate <= $endDate)){   
                    $price = $item->product_sale_price;
                }
            }
            $item->cart_amount = $price;
            $item->cart_total = floatval($price)*intval($item->cart_quantity);
        }
                            
        return $result;
    }

    public static function syncCartFromVisitorTag($user_id, $visitor_tag)
    {
        
        $result = DB::table('tbl_ecom_cart as ct')->where('visitor_tag', $visitor_tag)->update(
            array(
                "user_id" => $user_id
            )
        );

        return $result;
    }

    public static function getremindercart($data)
    {
        
        /*$query = DB::table('tbl_ecom_cart as ct')->select('ct.*','pro.*','us.*')*/
        $query = DB::table('tbl_ecom_cart as ct')->select('ct.*','pro.product_name','pro.product_id','pro.product_short_description',
            'pro.product_actual_price','pro.product_sale_price','pro.start_date','pro.end_date','pro.stock_status','pro.product_stock','pro.is_stockmanagement','pro.product_height','pro.product_length','pro.product_weight','pro.product_wide','pro.slug_name','pro.has_variation','pro.is_variation','pro.product_type','medi.media_file as product_image_media_file')
        ->leftJoin('tbl_ecom_product as pro', 'pro.product_id', '=', 'ct.product_id')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
         
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('ct.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('ct.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('visitor_tag', $data) && isset($data['visitor_tag'])) {
            $query = $query->where('ct.visitor_tag', '=' ,$data['visitor_tag']);
        }

        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('ct.attribute_id', '=' ,$data['attribute_id']);
        }
           
        if (array_key_exists('attribute_term_id', $data) && isset($data['attribute_term_id'])) {
            $query = $query->where('ct.attribute_term_id', '=' ,$data['attribute_term_id']);
        } 
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ct.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ct.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ct.status', '=' ,1);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ct.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('ct.cart_id', 'DESC');
        }

                          
        $result = $query->get();

        foreach($result as $item){
            $price = $item->product_actual_price;
            if($item->product_sale_price != null && $item->product_sale_price != 0){
                $currentDate = date('Y-m-d');
                $currentDate = date('Y-m-d', strtotime($currentDate));   
                $startDate = date('Y-m-d', strtotime($item->start_date));
                $endDate = date('Y-m-d', strtotime($item->end_date));   
                if (($currentDate >= $startDate) && ($currentDate <= $endDate)){   
                    $price = $item->product_sale_price;
                }
            }
            $item->cart_amount = $price;
            $item->cart_total = floatval($price)*intval($item->cart_quantity);
        }
                            
        return $result;
    }
}
