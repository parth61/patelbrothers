<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomCoupon extends Model
{
    protected $table = 'tbl_ecom_coupon';
    protected $fillable=['coupon_code','coupon_description','coupon_amount','discount_type',
    'coupon_quantity','coupon_type','coupon_startdate','coupon_enddate',
    'coupon_min_amount','coupon_max_amount','usage_limit_coupon','usage_limit_user','is_freeshipping',
    'is_visible','is_individual_use','excluded_sale_item','status','site_id',
    'created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address','coupon_title','coupon_subtitle','coupon_image','is_visible_offer'];

    public  function getcoupon($data)
    {
        
        $query = DB::table('tbl_ecom_coupon as cp')->select('cp.*','med.media_file  as coupon_media_path')
        ->leftJoin('tbl_media as med','med.media_id','=','cp.coupon_image');
         
        if (array_key_exists('coupon_id', $data) && isset($data['coupon_id'])) {
            $query = $query->where('cp.coupon_id', '=' ,$data['coupon_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('cp.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('cp.status', '!=' ,0);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('cp.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('cp.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('cp.coupon_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
