<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class RelationPostCategory extends Model
{
    protected $table = 'tbl_relation_post_category';
    protected $fillable=['relation_post_category_id','postcategory_id','post_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getrelationpostcategory($data)
    {
        
        $query = DB::table('tbl_relation_post_category as rpc')->select('rpc.*');
         
        if (array_key_exists('relation_post_category_id', $data) && isset($data['relation_post_category_id'])) {
            $query = $query->where('rpc.relation_post_category_id', '=' ,$data['relation_post_category_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('rpc.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('rpc.status', '=' ,1);
        }

        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('rpc.post_id', '=' ,$data['post_id']);
        }

        if (array_key_exists('postcategory_id', $data) && isset($data['postcategory_id'])) {
            $query = $query->where('rpc.postcategory_id', '=' ,$data['postcategory_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('rpc.'.$data['sortby'], $data['sorttype']);
        }           
      //  $query = $query->where('ads.', '=' ,1);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
