<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Source extends Model
{
 
    protected $table = 'tbl_source';
    protected $fillable=['primary_source','secondary_source','tertiary_source','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getsource($data)
    {
        
         $query = DB::table('tbl_source as source')->select('source.*');
         
         if (array_key_exists('source_id', $data) && isset($data['source_id'])) {
            $query = $query->where('source.source_id', '=' ,$data['source_id']);
           }
                          
         $query = $query->where('source.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('source.source_id', 'ASC')->get();
                            
         return $result;
    }
}
