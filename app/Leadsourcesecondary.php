<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Leadsourcesecondary extends Model
{
    protected  $table = 'tbl_lead_source_secondary';
    protected $fillable=['lead_source_primary_id','lead_source_secondary_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getleadsourcesecondary($data)
    {
        
        $query = DB::table('tbl_lead_source_secondary as s')->select('s.*','c.lead_source_primary_name')
        ->leftJoin('tbl_lead_source_primary as c', 'c.lead_source_primary_id', '=', 's.lead_source_primary_id');
         
        if (array_key_exists('lead_source_primary_id', $data) && isset($data['lead_source_primary_id'])) {
            $query = $query->where('s.lead_source_primary_id', '=' ,$data['lead_source_primary_id']);
        }

        if (array_key_exists('lead_source_secondary_id', $data) && isset($data['lead_source_secondary_id'])) {
            $query = $query->where('s.lead_source_secondary_id', '=' ,$data['lead_source_secondary_id']);
        }
                          
        $query = $query->where('s.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('s.lead_source_secondary_id', 'ASC')->get();
                            
        return $result;
    }
}
