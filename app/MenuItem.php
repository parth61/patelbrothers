<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class MenuItem extends Model
{
    protected $table = 'tbl_menuitem';
    protected $fillable=['menuitem_id','menuitem_title','menu_id','menuitem_target','menuitem_url','menuitem_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getmenu($data)
    {
        
        $query = DB::table('tbl_menuitem as menitem')
        ->select('men.*');
         
        if (array_key_exists('menuitem_id', $data) && isset($data['menuitem_id'])) {
            $query = $query->where('menitem.menuitem_id', '=' ,$data['menuitem_id']);
        }

        if (array_key_exists('menu_id', $data) && isset($data['menu_id'])) {
            $query = $query->where('menitem.menu_id', '=' ,$data['menu_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('menitem.status', '=' ,$data['status']);
        } else
        {
            $query = $query->where('menitem.status', '=' ,1);
        }


        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('menitem.'.$data['sortby'], $data['sorttype']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
                                      
        $result = $query->get();
                            
        return $result;
    }
}
