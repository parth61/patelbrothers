<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Location extends Model
{
   protected $table = 'tbl_location';
    protected $fillable=['location_name','created_by','updated_by','location_slug','location_description','location_featured_image','location_latitude','location_longitude','browser_name','browser_version','browser_platform','ip_address'];

    public static function getlocation($data)
    {
        
         $query = DB::table('tbl_location as l')->select('l.*');
         
         if (array_key_exists('location_id', $data) && isset($data['location_id'])) {
            $query = $query->where('l.location_id', '=' ,$data['location_id']);
           }
                          
         $query = $query->where('l.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('l.location_name', 'ASC')->get();
                            
         return $result;
    }
	
	public static function getlocationwhichhasproject($data)
    {
        
         $query = DB::table('tbl_project as tp')->select('l.*')
			->select('l.location_id', 'l.location_name', 'l.location_latitude', 'l.location_longitude', DB::raw('count(*) as totalproject'))
			->leftJoin('tbl_location as l','l.location_id','=','tp.location_id');
         
         if (array_key_exists('location_id', $data) && isset($data['location_id'])) {
            $query = $query->where('l.location_id', '=' ,$data['location_id']);
           }
                          
         $query = $query->where('tp.isvisible', '=' ,1)->where('tp.status', '=' ,1)->where('l.status', '=' ,1)->groupby('tp.location_id');;
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('l.location_name', 'ASC')->get();
                            
         return $result;
    }

    
    public static function  getlocationwithhighestproject($data)
    {
        
         $query = DB::table('tbl_project as pro')
         ->select('l.*', DB::raw('count(*) as totalproject'))
         ->leftJoin('tbl_location as l','l.location_id','=','pro.location_id')
         ->whereNotNull('pro.location_id')
         ->groupby('pro.location_id');
         
         if (array_key_exists('location_id', $data) && isset($data['location_id'])) {
            $query = $query->where('l.location_id', '=' ,$data['location_id']);
           }
                          
         $query = $query->where('pro.status', '=' ,1);
                          
         $result = $query->orderBy('totalproject', 'DESC')->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
   
}
