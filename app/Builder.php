<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Builder extends Model
{
    protected $table = 'tbl_builder';
    protected $fillable=['builder_phone_code','builder_phone_iso','builder_name','builder_logo','builder_address','builder_featured_img','builder_website','package_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    //
      public static function getbuilder($data)
    {
        
         $query = DB::table('tbl_builder as b')->select('b.*','p.package_name','p.package_slug','p.package_description','p.package_projects','p.package_images','p.package_executives')
                 ->leftJoin('tbl_package as p', 'b.package_id', '=', 'p.package_id');
         
         if (array_key_exists('builder_id', $data) && isset($data['builder_id'])) {
            $query = $query->where('b.builder_id', '=' ,$data['builder_id']);
           }
                          
         $query = $query->where('b.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.builder_name', 'ASC')->get();
                            
         return $result;
    }
}
