<?php

namespace App\Http\Middleware;

use Closure;

class CheckBuilderSession {

    public function handle($request, Closure $next) {
        if (!$request->session()->exists('builderdata')) {
            // user value cannot be found in session
            return redirect('/');
        }

        return $next($request);
    }

}

?>
