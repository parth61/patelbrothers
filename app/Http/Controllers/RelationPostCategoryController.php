<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\RelationPostCategory;
class RelationPostCategoryController extends Controller
{
    public function addrelationpostcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "post_id"=>"required",
            "postcategory_id"=>"required"
         
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $relationpostcategory = new RelationPostCategory();
            $relationpostcategory->post_id = $request->input('post_id');
            $relationpostcategory->postcategory_id = $request->input('postcategory_id');
            $relationpostcategory->status = $request->input('status');
            $relationpostcategory->created_by = $request->input('user_id');
            $relationpostcategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $relationpostcategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $relationpostcategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $relationpostcategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $relationpostcategory->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "relationpostcategory Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallrelationpostcategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relationpostcategory = new RelationPostCategory();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('relation_post_category_id') && $request->input('relation_post_category_id') != "") {
                $data['relation_post_category_id'] = $request->input('relation_post_category_id');
            }
            if ($request->has('post_id') && $request->input('post_id') != "") {
                $data['post_id'] = $request->input('post_id');
            }
            if ($request->has('postcategory_id') && $request->input('postcategory_id') != "") {
                $data['postcategory_id'] = $request->input('postcategory_id');
            }
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $relationpostcategory = $relationpostcategory->getrelationpostcategory($data);
            if($relationpostcategory)
            {
                
                return response()->json(['status' => 200, 'count' => count($relationpostcategory), 'data' => $relationpostcategory]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updaterelationpostcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "relation_post_category_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relationpostcategory = new RelationPostCategory();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['relation_post_category_id','user_id','accesstoken']);
            $result = $relationpostcategory->where('relation_post_category_id', $request->input('relation_post_category_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "relationpostcategory Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleterelationpostcategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "relation_post_category_id" => "required",
                    "accesstoken" => "required",
                    "user_id" => "required|numeric|exists:tbl_user,user_id",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relationpostcategory = new RelationPostCategory();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['relation_post_category_id','user_id','accesstoken']);
            $result = $relationpostcategory->where('relation_post_category_id', $request->input('relation_post_category_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
