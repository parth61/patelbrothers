<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomTaxClass;
use Carbon\Carbon;



class EcomTaxClassController extends Controller
{
    public function addtaxclass(Request $request) {
        
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",
            "tax_class_name"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxclass = new EcomTaxClass();
            $taxclass->tax_class_name = $request->input('tax_class_name');
            $taxclass->created_by = $request->input('user_id');
            $taxclass->site_id = $request->input('site_id');
            $taxclass->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $taxclass->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $taxclass->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $taxclass->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $taxclass->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "taxclass Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalltaxclass(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxclass = new EcomTaxClass();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('tax_class_id') && $request->input('tax_class_id') != "") {
                $data['tax_class_id'] = $request->input('tax_class_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            $taxclass = $taxclass->gettaxclass($data);

            return response()->json(['status' => 200, 'count' => count($taxclass), 'data' => $taxclass]);
        }

    }

    public function updatetaxclass(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "tax_class_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxclass = new EcomTaxClass();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tax_class_id','user_id','accesstoken']);
            $result = $taxclass->where('tax_class_id', $request->input('tax_class_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "taxclass Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletetaxclass(Request $request) {

        $valid = Validator::make($request->all(), [
                    "tax_class_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxclass = new EcomTaxClass();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tax_class_id','user_id','accesstoken']);
            $result = $taxclass->where('tax_class_id', $request->input('tax_class_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
?>