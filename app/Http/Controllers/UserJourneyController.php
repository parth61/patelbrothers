<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\UserJourney;

class UserJourneyController extends Controller
{
    public function adduserjourney(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
            "site_id" => "required",
            "user_id" => "required",
            "user_page_visit" => "required"        
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $userjourney = new UserJourney();
            $userjourney->user_id = $request->input('user_id');
            $userjourney->user_page_visit = $request->input('user_page_visit');
            $userjourney->site_id = $request->input('site_id');
            $userjourney->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $userjourney->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $userjourney->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $userjourney->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $userjourney->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "userjourney Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalluserjourney(Request $request) {

        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $userjourney = new UserJourney();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('user_journey_id') && $request->input('user_journey_id') != "") {
                $data['user_journey_id'] = $request->input('user_journey_id');
            }
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            $userjourney = $userjourney->getuserjourney($data);

            return response()->json(['status' => 200, 'count' => count($userjourney), 'data' => $userjourney]);
        }
    }

    public function updateuserjourney(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "user_journey_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $userjourney = new UserJourney();
            $newrequest = $request->except(['user_journey_id','user_id']);
            $result = $userjourney->where('user_journey_id', $request->input('user_journey_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "userjourney Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteuserjourney(Request $request) {
        $valid = Validator::make($request->all(), [
                    "user_journey_id" => "required|numeric",
                  
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $userjourney = new UserJourney();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['user_journey_id']);
            $result = $userjourney->where('user_journey_id', $request->input('user_journey_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
