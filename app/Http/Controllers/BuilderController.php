<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Builder;
use DB;
use Session;
use Illuminate\Support\Facades\File;
class BuilderController extends Controller {

    //
    public function addbuilder(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "builder_name" => "required",
                    "builder_email" => "required|unique:tbl_builder,builder_email",
                    "builder_password" => "required",
                    "builder_logo" => "required",
                    "builder_address" => "required",
                    "builder_featured_img" => "required",
                    "builder_website" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
           /*  if ($request->hasFile('builder_logo')) {
                    
                $files = $request->file('builder_logo');
               
                $filename =$files->getClientOriginalName();
                $imgpath1= fileupload($filename, $files);
                }  */

             
          /*   if ($request->hasFile('builder_featured_img')) {
                    
                $files = $request->file('builder_featured_img');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileupload($filename, $files);
                }  */
                $imgpath=null;
                $imgpath1=null;

                //BASE 64 IMAGE UPLOAD
                $builder_logo_base64= $request->input('builder_logo');
                if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
                {
                    $imgpath=fileupload($builder_logo_base64);
                }
                $builder_featured_img_base64= $request->input('builder_featured_img');
                if($builder_featured_img_base64!="" && $builder_featured_img_base64!=null && $builder_featured_img_base64!="null")
                {
                    $imgpath1=fileupload($builder_featured_img_base64);
                }
            $builder->builder_name = $request->input('builder_name');
            $builder->builder_email = $request->input('builder_email');
            $builder->builder_password = md5($request->input('builder_password'));
            $builder->builder_logo = $imgpath1;
            $builder->builder_address = $request->input('builder_address');
            $builder->builder_featured_img = $imgpath;
            $builder->builder_description = $request->input('builder_description');
            $builder->builder_phone = $request->input('builder_phone');
            $builder->builder_phone_code = $request->input('builder_phone_code');
            $builder->builder_phone_iso = $request->input('builder_phone_iso');
            $builder->builder_website = $request->input('builder_website');
            $builder->package_id = $request->input('package_id');
            $builder->created_by = $request->input('created_by');
            $builder->updated_by = $request->input('updated_by');
            $builder->guid = generateAccessToken(8);
            $builder->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $builder->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $builder->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $builder->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $builder->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Builder added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallbuilder(Request $request) {
      $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('builder_id') && $request->input('builder_id') != "") {
                $data['builder_id'] = $request->input('builder_id');

                $projectcountlist = DB::table('tbl_project as tp')->select('tp.builder_id', DB::raw('count(*) as total'))
                ->leftJoin('tbl_builder as tb','tb.builder_id','=','tp.builder_id')
                ->where('tp.status', 1)
                ->where('tp.builder_id', $request->input('builder_id'))
                ->groupBy('tp.builder_id')
                ->get();

                $salesexecutivecountlist = DB::table('tbl_sales_executive as ts')->select('ts.builder_id', DB::raw('count(*) as total'))
                ->leftJoin('tbl_builder as tb','tb.builder_id','=','ts.builder_id')
                ->where('ts.status', 1)
                ->where('ts.builder_id', $request->input('builder_id'))
                ->groupBy('ts.builder_id')
                ->get();

                $leadcountlist = DB::table('tbl_leads as tl')->select('tb.builder_id', DB::raw('count(*) as total'))
                ->leftJoin('tbl_project as tp','tp.project_id','=','tl.project_id')
                ->leftJoin('tbl_builder as tb','tb.builder_id','=','tp.builder_id')
                ->where('tp.status', 1)
                ->where('tb.builder_id', $request->input('builder_id'))
                ->groupBy('tp.builder_id')
                ->get();

            }else{
                $projectcountlist = DB::table('tbl_project as tp')->select('tp.builder_id', DB::raw('count(*) as total'))
                ->leftJoin('tbl_builder as tb','tb.builder_id','=','tp.builder_id')
                ->where('tp.status', 1)
                ->groupBy('tp.builder_id')
                ->get();

                $salesexecutivecountlist = DB::table('tbl_sales_executive as ts')->select('ts.builder_id', DB::raw('count(*) as total'))
                ->leftJoin('tbl_builder as tb','tb.builder_id','=','ts.builder_id')
                ->where('ts.status', 1)
                ->groupBy('ts.builder_id')
                ->get();

                $leadcountlist = DB::table('tbl_leads as tl')->select('tb.builder_id', DB::raw('count(*) as total'))
                ->leftJoin('tbl_project as tp','tp.project_id','=','tl.project_id')
                ->leftJoin('tbl_builder as tb','tb.builder_id','=','tp.builder_id')
                ->where('tp.status', 1)
                ->groupBy('tp.builder_id')
                ->get();
            }

            $builder = $builder->getbuilder($data);

            if ($builder) {
                $builderdata = array();
                foreach ($builder as $item) {
                    if($item->builder_logo==NULL|| $item->builder_logo=="")
                    {
                        $item->logopath = fixedimagepathbuilderlogo;
                    }
                    else
                    {
                        if (does_url_exists(mediapath.$item->builder_logo)) {
                            $item->logopath = imagedisplaypath.$item->builder_logo;
                        } else {
                            $item->logopath = fixedimagepathbuilderlogo;
                        }
                           
                    }
                    if($item->builder_featured_img==NULL|| $item->builder_featured_img=="")
                    {
                        $item->featuredpath = fixedimagepathbuilderfeaturedimage;
                    }
                    else
                    {
                        if (does_url_exists(mediapath.$item->builder_featured_img)) {
                            $item->featuredpath = imagedisplaypath.$item->builder_featured_img;
                        } else {
                            $item->featuredpath = fixedimagepathbuilderfeaturedimage;
                        }
                   
                    }

                    $isfound = 0;
                    foreach($projectcountlist as $itepcl){
                        if($itepcl->builder_id === $item->builder_id){
                            $item->total_projects = $itepcl->total;
                            $isfound = 1;
                        }
                    }
                    if($isfound == 0){ $item->total_projects = 0; }

                    $isfound = 0;
                    foreach($salesexecutivecountlist as $itepcl){
                        if($itepcl->builder_id === $item->builder_id){
                            $item->total_salesexecutives = $itepcl->total;
                            $isfound = 1;
                        }
                    }
                    if($isfound == 0){ $item->total_salesexecutives = 0; }

                    $isfound = 0;
                    foreach($leadcountlist as $itepcl){
                        if($itepcl->builder_id === $item->builder_id){
                            $item->total_leads = $itepcl->total;
                            $isfound = 1;
                        }
                    }
                    if($isfound == 0){ $item->total_leads = 0; }

                    array_push($builderdata, $item);

                }
                return response()->json(['status' => 200, 'count' => count($builder), 'data' => $builderdata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
    public function getbuilderbyid($builder_id) {
        $data=array();
        $query = DB::table('tbl_builder as b')->select('b.*')
        ->where('b.guid', '=' , $builder_id)
        ->where('b.status', '=' ,1)
        ->get();

        //dd($query);

        $builderdata = array();
        foreach ($query as $item) {
            if($item->builder_logo==NULL|| $item->builder_logo=="")
            {
                $item->logopath = fixedimagepathbuilderlogo;
            }
            else
            {
                if(does_url_exists(imagedisplaypath.$item->builder_logo)){
                    $item->logopath = imagedisplaypath.$item->builder_logo;
                }else{
                    $item->logopath = fixedimagepathbuilderlogo;
                }
                /*if (does_url_exists(imagedisplaypath.$item->builder_logo)) {
                    $item->logopath = imagedisplaypath.$item->builder_logo;
                } else {
                    $item->logopath = fixedimagepathbuilderlogo;
                }*/
            }
            if($item->builder_featured_img==NULL|| $item->builder_featured_img=="")
            {
                $item->featuredpath = fixedimagepathbuilderfeaturedimage;
            }
            else
            {
                if (does_url_exists(mediapath.$item->builder_featured_img)) {
                    $item->featuredpath = imagedisplaypath.$item->builder_featured_img;
                } else {
                    $item->featuredpath = fixedimagepathbuilderfeaturedimage;
                }
           
            }
            array_push($builderdata, $item);
          
        }

         $data['builderdata'] = $builderdata;         
        return view('editbuilder', $data);
    }
    public function updatebuilder(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "builder_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
            $imgpath = null;
            $imgpath1 = null;
            $builder_id=$request->input('builder_id');

           $builder_featured_img_base64= $request->input('builder_featured_img');
            /*$builder_logo_base64= $request->input('builder_logo');

            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
                $imgpath=fileupload($builder_logo_base64);
                
                $query = DB::table('tbl_builder')->select('tbl_builder.builder_logo')->where('builder_id',$builder_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->builder_logo;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                          
                            //unlink($path);
                        } 
                    }
                   
                }
            }*/

            $builder_logo_base64= $request->input('builder_logo');

            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
                $responsedata = fileUploadSlim($request, "builder_logo");
                $imgpath = $responsedata['folderpath'];
                
                $query = DB::table('tbl_builder')->select('tbl_builder.builder_logo')->where('builder_id',$builder_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->builder_logo;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                          
                            //unlink($path);
                        } 
                    }
                   
                }
            }

           
                
            $builder_featured_img_base64= $request->input('builder_featured_img');
            if($builder_featured_img_base64!="" && $builder_featured_img_base64!=null && $builder_featured_img_base64!="null")
            {
                $imgpath1=fileupload($builder_featured_img_base64);
                    $query = DB::table('tbl_builder')->select('tbl_builder.builder_featured_img')->where('builder_id',$builder_id)->get();
                    if(count($query)>0)
                    {
                        $img=$query[0]->builder_featured_img;
                        if($img!=NULL|| $img!="")
                        {
                            if (does_url_exists(mediapath.$img)) {
                                $path = mediapath.$img;
                               // unlink($path);
                            } 
                        }
                       
                    }
            }
    
            $newrequest = $request->except(['builder_id','builder_logo','builder_featured_img','builder_password']);
            if($request->input('builder_password')!="" && $request->input('builder_password')!=null)
            {
                $newrequest['builder_password']=md5($request->input('builder_password'));
            }
            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
				$newrequest['builder_logo'] = $imgpath;
            }
            if($builder_featured_img_base64!="" && $builder_featured_img_base64!=null && $builder_featured_img_base64!="null")
            {
				$newrequest['builder_featured_img'] = $imgpath1;
            }
            $result = $builder->where('builder_id',$builder_id)->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Builder updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletebuilder(Request $request) {
        $valid = Validator::make($request->all(), [
                    "builder_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
            $builder_id= $request->input('builder_id');
            $query = DB::table('tbl_builder')->select('tbl_builder.builder_featured_img','tbl_builder.builder_logo')->where('builder_id',$builder_id)->get();
            if(count($query)>0)
            {
            $img=$query[0]->builder_featured_img;
            if($img!=NULL|| $img!="")
            {
                if (does_url_exists(mediapath.$img)) {
                    $path = mediapath.$img;
                  //  unlink($path);
                } 
            }
            }

            if(count($query)>0)
            {
                $img=$query[0]->builder_logo;
                if($img!=NULL|| $img!="")
                {
                    if (does_url_exists(mediapath.$img)) {
                        $path = mediapath.$img;
                     //   unlink($path);
                    } 
                }
            }

            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['builder_id']);
            $result = $builder->where('builder_id', $request->input('builder_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updatebuilderlogo(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "builder_id" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
            $imgpath = null;
            
            $builder_id=$request->input('builder_id');

            $builder_logo_base64= $request->input('builder_logo');

            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
                $responsedata = fileUploadSlim($request, "builder_logo");
                $imgpath = $responsedata['folderpath'];
                
                $query = DB::table('tbl_builder')->select('tbl_builder.builder_logo')->where('builder_id',$builder_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->builder_logo;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                          
                            //unlink($path);
                        } 
                    }
                   
                }
            }
    
            $newrequest = $request->except(['builder_id','builder_logo']);

            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
				$newrequest['builder_logo'] = $imgpath;
            }
            $result = $builder->where('builder_id',$builder_id)->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Builder logo updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function updatebuilderfeaturedimage(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "builder_id" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
            $imgpath = null;
            
            $builder_id=$request->input('builder_id');

            $builder_logo_base64= $request->input('builder_featured_img');

            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
                $responsedata = fileUploadSlim($request, "builder_featured_img");
                $imgpath = $responsedata['folderpath'];
                
                $query = DB::table('tbl_builder')->select('tbl_builder.builder_featured_img')->where('builder_id',$builder_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->builder_featured_img;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                          
                            //unlink($path);
                        } 
                    }
                   
                }
            }
    
            $newrequest = $request->except(['builder_id','builder_featured_img']);

            if($builder_logo_base64!="" && $builder_logo_base64!=null && $builder_logo_base64!="null")
            {
				$newrequest['builder_featured_img'] = $imgpath;
            }
            $result = $builder->where('builder_id',$builder_id)->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Builder featured image updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
  
     
}
