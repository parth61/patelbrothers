<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomProduct;
use App\EcomCart;
use App\EcomProductCategory;
use App\EcomProductGallery;
use App\EcomWishlist;
use DB;

class EcomCartController extends Controller
{

    public function ReminderCartMail() {
        $cart = new Ecomcart();
        $cartuser =  DB::table('tbl_ecom_cart as ct')->select('ct.user_id','ur.user_firstname','ur.user_lastname','ur.user_mobile','ur.user_email')
        ->leftJoin('tbl_user as ur','ur.user_id','=','ct.user_id')
        ->where('ct.status', '!=' ,0)
        /*->whereBetween("ct.updated_at",[Carbon::yesterday(-1),Carbon::yesterday()])*/
        ->groupBy('user_id')
        ->get();
        
        if (count($cartuser)) {
            foreach ($cartuser as $item) {
                $cart = new Ecomcart();
                $data['offset'] = 0;
                $data['limit'] = 1000;
                $data['user_id'] = $item->user_id;
                $cart = $cart->getremindercart($data);
                $data['cart'] =$cart; 
                $data['user'] =$item;
                /*print_r($data);*/
                /*return view('emails.remindercart',$data);
                exit();*/
                $emailstatus  = onReminerCartMail($data);
                unset($data);
                unset($cart);
                unset($item);
                unset($userid);
            }
            return response()->json(['status' => 200, 'data' => $cartuser]);
            /*return view('emails.remindercart',$user);
            exit();*/
        }else{
            return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
        }
    }

     public function addcart(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "product_id" => "required",
            "cart_quantity" => "required|numeric|min:1"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            /* check product already exist in cart or not */
            $carts = new Ecomcart();
            $productmodel = new EcomProduct();

            if($request->has('user_id') && $request->filled('user_id')){
                $cartdata = $carts->where('product_id', $request->input('product_id'))->where('user_id', $request->input('user_id'))
                ->where('status', 1)->where('site_id', $request->input('site_id'))->get();
            }else if($request->has('visitor_tag') && $request->filled('visitor_tag')){
                $cartdata = $carts->where('product_id', $request->input('product_id'))->where('visitor_tag', $request->input('visitor_tag'))
                ->where('status', 1)->where('site_id', $request->input('site_id'))->get();
            }else{
                return response()->json(['status' => 400, 'error' => "Visitor tag or userid is missing."], 400);
            }
            

            if(count($cartdata) > 0){
                $updatedata = array();
                $updatedata['cart_quantity'] = (int)$cartdata[0]->cart_quantity +(int)$request->input('cart_quantity');
                $carts = new Ecomcart();
                $result = $carts->where('cart_id', $cartdata[0]->cart_id)->update($updatedata);
                
                if($result) {
                    return response()->json(['status' => 200, 'data' => "Product added to cart"]);
                }else{
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
                
            }else{

                $productdata = $productmodel->where('product_id', $request->input('product_id'))->first();

                if($productdata->has_variation == "1"){
                    return response()->json(['status' => 400, 'error' => "Please select variant."], 400);
                }else{
                    $carts->product_id = $request->input('product_id');
                    if($request->has('user_id') && $request->filled('user_id')){
                        $carts->user_id = $request->input('user_id');
                    }
                    if($request->has('visitor_tag') && $request->filled('visitor_tag')){
                        $carts->visitor_tag = $request->input('visitor_tag');
                    }
                    $carts->attribute_id = $request->has('attribute_id')?$request->input('attribute_id'):null;
                    $carts->attribute_term_id = $request->has('attribute_term_id')?$request->input('attribute_term_id'):null;
                    $carts->attribute_term_name = $request->has('attribute_term_name')?$request->input('attribute_term_name'):null;
                    $carts->cart_quantity = $request->has('cart_quantity')?(int)$request->input('cart_quantity'):1;
                    $carts->attribute_id = $request->has('attributeidlist')?$request->input('attributeidlist'):null;
                    $carts->attribute_term_id = $request->has('attributetermidlist')?$request->input('attributetermidlist'):null;
                    $carts->attribute_term_name = $request->has('attributetermnamelist')?$request->input('attributetermnamelist'):null;
                    $carts->isbuynow = $request->has('isbuynow')?$request->input('isbuynow'):null;
                    $carts->site_id = $request->input('site_id');
                    $carts->created_by = $request->has('user_id')?$request->input('user_id'):null;
                    $carts->updated_by = $request->has('user_id')?$request->input('user_id'):null;
                    $carts->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $carts->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $carts->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $carts->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $carts->save();
                    if ($result) {

                        /* Remove product from wishlist it add to cart from wishlist */
                        if($request->has('from')){
                            if($request->input('from')=="wishlist"){
                                $updatedata = array();
                                $updatedata['status'] = '0';
                                $wishlistmodel = new EcomWishlist();
                                $updateresult = $wishlistmodel->where('product_id', $request->input('product_id'))->where('user_id', $request->input('user_id'))->update($updatedata);
                            }
                        }

                        return response()->json(['status' => 200, 'data' => "Prodct added to cart successfully."]);
                    } else {
                        return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                    }
                }

            }
        }
    }

    public function getallcart(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $cart = new Ecomcart();
            $productmodel = new EcomProduct();
            $productcategory = new EcomProductCategory();
            $productgallery = new EcomProductGallery();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('cart_id') && $request->input('cart_id') != "") {
                $data['cart_id'] = $request->input('cart_id');
            }

            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            
            if ($request->has('visitor_tag') && $request->input('visitor_tag') != "") {
                $data['visitor_tag'] = $request->input('visitor_tag');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $cart = $cart->getcart($data);

            $totalitems = 0;

            foreach ($cart as $item) {

                $temp = array();
                $temp['product_id'] = $item->product_id;
                $item->categorylist = $productcategory->getproductcategorylist($temp);
                $temp['offset'] = 0;
                $temp['limit'] = 5;
                $productgalleryobj = $productgallery->getproductgallery($temp);
                $productgallerydata=array();
                foreach ($productgalleryobj as $item1) {
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }  
                    array_push($productgallerydata, $item1->product_image_path);
                }
                $item->product_gallery=$productgallerydata;
                $totalitems = $totalitems + (int)$item->cart_quantity;
                if($item->product_type == "variant"){
                    $item->product_attrdata=$productmodel->getattributeandattrtermlistforproductid($item->product_id);
                }
            }
            return response()->json(['status' => 200, 'count' => $totalitems, 'data' => $cart]);
        }

    }

    public function updatecart(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "cart_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $cart = new Ecomcart();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['cart_id','accesstoken','user_id']);

            if ($request->has('cart_quantity')) {
                if($request->input('cart_quantity') > 0){
                    $newrequest['cart_quantity'] = $request->input('cart_quantity');
                }else{
                    return response()->json(['status' => 400, 'error' => "Invalid cart quantity."], 400);
                }
            }

            $result = $cart->where('cart_id', $request->input('cart_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product updated"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletecart(Request $request) {

        $valid = Validator::make($request->all(), [
                    "cart_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $cart = new Ecomcart();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['cart_id','accesstoken','user_id']);
            $result = $cart->where('cart_id', $request->input('cart_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product removed"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}

