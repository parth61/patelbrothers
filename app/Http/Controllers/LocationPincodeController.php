<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\LocationPincode;
class LocationPincodeController extends Controller
{
    public function getalllocationpincode(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $locationpincode = new LocationPincode();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('location_pincode_id') && $request->input('location_pincode_id') != "") {
                $data['location_pincode_id'] = $request->input('location_pincode_id');
            }
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
            if ($request->has('pincode_id') && $request->input('pincode_id') != "") {
                $data['pincode_id'] = $request->input('pincode_id');
            }
            $locationpincode = $locationpincode->getlocationpincode($data);
            return response()->json(['status' => 200, 'count' => count($locationpincode), 'data' => $locationpincode]);
        }
    }
    public function getallcustomlocationpincode(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $locationpincode = new LocationPincode();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('location_pincode_id') && $request->input('location_pincode_id') != "") {
                $data['location_pincode_id'] = $request->input('location_pincode_id');
            }
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
            if ($request->has('pincode_id') && $request->input('pincode_id') != "") {
                $data['pincode_id'] = $request->input('pincode_id');
            }
            $locationpincode = $locationpincode->getcustomlocationpincode($data);
            return response()->json(['status' => 200, 'count' => count($locationpincode), 'data' => $locationpincode]);
        }
    }
    
    
}
