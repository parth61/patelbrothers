<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomShippingClass;
use App\EcomShippingMethod;
use App\EcomShipping;
use App\Slug;

class EcomShippingController extends Controller
{
    public function addshipping(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "shipping_method_id" => "required",
            "shipping_class_id" => "required",
            "shipping_rate" => "required",
            "shipping_city" => "required",
            "shipping_state" => "required",
            "shipping_country" => "required",
            "shipping_pincode" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shipping = new EcomShipping();
            $shipping->site_id = $request->input('site_id');
            $shipping->shipping_method_id = $request->input('shipping_method_id');
            $shipping->shipping_class_id = $request->input('shipping_class_id');
            $shipping->shipping_rate = $request->input('shipping_rate');
            $shipping->shipping_city = $request->input('shipping_city');
            $shipping->shipping_state = $request->input('shipping_state');
            $shipping->shipping_country = $request->input('shipping_country');
            $shipping->shipping_pincode = $request->input('shipping_pincode');
            $shipping->created_by = $request->input('user_id');
            $shipping->updated_by = $request->input('user_id');
            $shipping->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $shipping->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $shipping->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $shipping->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $shipping->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Shipping Added Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallshipping(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shipping = new EcomShipping();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('shipping_id') && $request->input('shipping_id') != "") {
                $data['shipping_id'] = $request->input('shipping_id');
            }
            if ($request->has('shipping_method_id') && $request->input('shipping_method_id') != "") {
                $data['shipping_method_id'] = $request->input('shipping_method_id');
            }
            if ($request->has('shipping_class_id') && $request->input('shipping_class_id') != "") {
                $data['shipping_class_id'] = $request->input('shipping_class_id');
            }
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $shippingdata = $shipping->getshippings($data);

            return response()->json(['status' => 200, 'count' => count($shippingdata), 'data' => $shippingdata]);
        }

    }

    public function updateshipping(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "shipping_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shipping = new EcomShipping();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['site_id','shipping_id','accesstoken','user_id']);
            $result = $shipping->where('shipping_id', $request->input('shipping_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Shipping Updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteshipping(Request $request) {

        $valid = Validator::make($request->all(), [
            "shipping_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shipping = new EcomShipping();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['shipping_id','accesstoken','site_id','user_id']);
            $result = $shipping->where('shipping_id', $request->input('shipping_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "shippingclass Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
