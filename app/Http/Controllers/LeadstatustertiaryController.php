<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Leadstatustertiary;

class LeadstatustertiaryController extends Controller
{
    
     public function addleadstatustertiary(Request $request) {
        //validations
        $leadstatustertiary = new Leadstatustertiary();
        $valid = Validator::make($request->all(), [
                    "lead_status_tertiary_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $leadstatustertiary->lead_status_tertiary_name = $request->input('lead_status_tertiary_name');
            $leadstatustertiary->lead_status_secondary_id = $request->input('lead_status_secondary_id');
            $leadstatustertiary->lead_status_primary_id = $request->input('lead_status_primary_id');
            $leadstatustertiary->status = $request->input('status');
             $leadstatustertiary->created_by = $request->input('created_by');
            $leadstatustertiary->updated_by = $request->input('updated_by');
            $leadstatustertiary->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $leadstatustertiary->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $leadstatustertiary->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $leadstatustertiary->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $leadstatustertiary->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadstatustertiary added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallleadstatustertiary(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadstatustertiary = new Leadstatustertiary();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('lead_status_secondary_id') && $request->input('lead_status_secondary_id') != "") {
                $data['lead_status_secondary_id'] = $request->input('lead_status_secondary_id');
            }
            if ($request->has('lead_status_primary_id') && $request->input('lead_status_primary_id') != "") {
                $data['lead_status_primary_id'] = $request->input('lead_status_primary_id');
            }
            $leadstatustertiarydata = $leadstatustertiary->getleadtertiary($data);
            
            return response()->json(['status' => 200, 'count' => count($leadstatustertiarydata), 'data' => $leadstatustertiarydata]);
        }
    }
    public function updateleadstatustertiary(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_status_tertiary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $leadstatustertiary = new Leadstatustertiary();
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['lead_status_tertiary_id']);
            $result = $leadstatustertiary->where('lead_status_tertiary_id', $request->input('lead_status_tertiary_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadstatustertiary Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteleadstatustertiary(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_status_tertiary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadstatustertiary = new Leadstatustertiary();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['lead_status_tertiary_id']);
            $result = $leadstatustertiary->where('lead_status_tertiary_id', $request->input('lead_status_tertiary_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
