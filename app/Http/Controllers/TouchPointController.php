<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TouchPoint;
use DB;
use Validator;
class TouchPointController extends Controller
{
    public function getalltouchpoint(Request $request) {



    $valid = Validator::make($request->all(), [
                "limit" => "required|numeric",
                "offset" => "required|numeric",
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $touchpoint = new TouchPoint();
        $data = array();
        $data['offset'] = $request->input('offset');
        $data['limit'] = $request->input('limit');

        if ($request->has('touchpoint_id') && $request->input('touchpoint_id') != "") {
            $data['touchpoint_id'] = $request->input('touchpoint_id');
        }
        $touchpoint = $touchpoint->gettouchpoint($data);

        return response()->json(['status' => 200, 'count' => count($touchpoint), 'data' => $touchpoint]);
    }
}
}
