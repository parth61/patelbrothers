<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Highlight;

class HighlightController extends Controller
{
    public function addhighlight(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "highlight_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $highlight = new Highlight();
            $highlight->highlight_name = $request->input('highlight_name');
            $highlight->created_by = $request->input('created_by');
            $highlight->updated_by = $request->input('updated_by');
            $highlight->status = $request->input('status');
            $highlight->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $highlight->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $highlight->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $highlight->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $highlight->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Highlight Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallhighlight(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $highlight = new Highlight();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('highlight_id') && $request->input('highlight_id') != "") {
                $data['highlight_id'] = $request->input('highlight_id');
            }

            $highlight = $highlight->gethighlight($data);

            return response()->json(['status' => 200, 'count' => count($highlight), 'data' => $highlight]);
        }
    }

    public function updatehighlight(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "highlight_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $highlight = new Highlight();
            $newrequest = $request->except(['highlight_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $highlight->where('highlight_id', $request->input('highlight_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Highlight Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletehighlight(Request $request) {
        $valid = Validator::make($request->all(), [
                    "highlight_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $highlight = new Highlight();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['highlight_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $highlight->where('highlight_id', $request->input('highlight_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function managehighlight(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "highlight_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->has('highlight_id') && $request->filled('highlight_id')){
                $highlight = new Highlight();
                $newrequest = $request->except(['highlight_id']);
                $request->request->add(['updated_by' =>  $request->input('updated_by')]);
                $result = $highlight->where('highlight_id', $request->input('highlight_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Highlight Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $highlight = new Highlight();
                $highlight->highlight_name = $request->input('highlight_name');
                $highlight->created_by = $request->input('created_by');
                $highlight->updated_by = $request->input('updated_by');
                $highlight->status = $request->input('status');
                $highlight->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $highlight->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $highlight->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $highlight->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $highlight->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Highlight Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
        }
    }

}
