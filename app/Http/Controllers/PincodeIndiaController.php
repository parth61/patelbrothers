<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\PincodeIndia;
class PincodeIndiaController extends Controller
{
     public function getallcountry(Request $request) {

        $valid = Validator::make($request->all(), [
                  
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincodeindia = new PincodeIndia();
            $data = array();
          
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

           
            $countrydata = $pincodeindia->getcountry($data);
            if($countrydata)
            {
                return response()->json(['status' => 200, 'count' => count($countrydata), 'data' => $countrydata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function getallstate(Request $request) {

        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincodeindia = new PincodeIndia();
            $data = array();
          
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('country') && $request->input('country') != "") {
                $data['country'] = $request->input('country');
            }

           
            $statedata = $pincodeindia->getstate($data);
            if($statedata)
            {
                return response()->json(['status' => 200, 'count' => count($statedata), 'data' => $statedata]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function getalldistrict(Request $request) {

        $valid = Validator::make($request->all(), [
           
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincodeindia = new PincodeIndia();
            $data = array();
          
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('state') && $request->input('state') != "") {
                $data['state'] = $request->input('state');
            }

           
            $districtdata = $pincodeindia->getdistrict($data);
            if($districtdata)
            {
                return response()->json(['status' => 200, 'count' => count($districtdata), 'data' => $districtdata]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function getallpincode(Request $request) {

        $valid = Validator::make($request->all(), [
           
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincodeindia = new PincodeIndia();
            $data = array();
          
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('district') && $request->input('district') != "") {
                $data['district'] = $request->input('district');
            }

           
            $pincodedata = $pincodeindia->getpincode($data);
            if($pincodedata)
            {
                return response()->json(['status' => 200, 'count' => count($pincodedata), 'data' => $pincodedata]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }
    public function getallpincodeindia(Request $request) {

        $valid = Validator::make($request->all(), [
           
                    "site_id" => "required",
                    
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincodeindia = new PincodeIndia();
            $data = array();
          
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

           
            $pincodeindiadata = $pincodeindia->getpincodeindia($data);
            if($pincodeindiadata)
            {
                return response()->json(['status' => 200, 'count' => count($pincodeindiadata), 'data' => $pincodeindiadata]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

}
