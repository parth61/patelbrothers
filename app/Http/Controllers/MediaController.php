<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Media;
use DB;
use Illuminate\Support\Facades\File;

class MediaController extends Controller
{
    public function addmedia(Request $request) {

        //validations
        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
                    "accesstoken" => "required",       
                    "media_type" => "required",
                    "media_file" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

                  $media = new Media();
                $filepath=null;

                if($request->input('media_type')=="image")
                {
                        //BASE 64 IMAGE UPLOAD
                        $media_file_base64= $request->input('media_file');
                        if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                        {
                            //$filepath=fileupload($media_file_base64);
                            $responsedata = fileUploadSlim($media_file_base64, "media_file");
                            $filepath = $responsedata['folderpath'];
                        }
                }
                else
                {
                    if ($request->hasFile('media_file')) 
                    {
                    
                        $files = $request->file('media_file');
                       
                        $filename =$files->getClientOriginalName();
                        $filepath= fileuploads($filename, $files);
                    } 
                }
                
                $media->media_file = $filepath;
                $media->media_type = $request->input('media_type');
                $media->media_title = $request->input('media_title');
                $media->media_alt = $request->input('media_alt');
                $media->media_caption = $request->input('media_caption');
                $media->media_description = $request->input('media_description');
                $media->created_by = $request->input('user_id');
                $media->site_id = $request->input('site_id');
                $media->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $media->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $media->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $media->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $media->save();

                if($result) {
                    $media_id=$media->id;
                    $data['status'] = 1;
                    $data['offset'] = 0;
                    $data['limit'] = 1;
                    $data['media_id'] = $media_id;
                    $media = $media->getmedia($data);
                    $mediadata = array();
                    foreach ($media as $item) {
                       /*  $file = imagedisplaypath.$item->media_file;
                        $file_headers = @get_headers($file);
                        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                           $item->media_file_path = fixedpagefeaturedimage;
                          
                        }
                        else {
                             $item->media_file_path = imagedisplaypath.$item->media_file;
                        } */
                        if(fileuploadtype=="local")
                        {
                            if($item->media_file==""||$item->media_file==null||$item->media_file=="null")
                            {
                                $item->media_file_path =fixedpagefeaturedimage;
                            }
                            else
                            {
                                if (File::exists(baseimagedisplaypath .$item->media_file)) {
                                    $item->media_file_path = imagedisplaypath.$item->media_file;
                                } else {
                                    $item->media_file_path =fixedpagefeaturedimage;
                                }
                            }
                        }
                        else
                        {
 
                            if (does_url_exists(imagedisplaypath.$item->media_file)) {
                                $item->media_file_path = imagedisplaypath.$item->media_file;
                            } else {
                                $item->media_file_path =fixedpagefeaturedimage;
                            } 
                        }
                      

                        array_push($mediadata, $item);
                    }
                    return response()->json(['status' => 200, 'count' => count($media), 'data' => $mediadata]);
                    //return response()->json(['status' => 200, 'data' => "Media Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            
            
        }
    }
   
    public function getallmedia(Request $request) {
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $media = new Media();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('media_id') && $request->input('media_id') != "") {
                $data['media_id'] = $request->input('media_id');
            }
            if ($request->has('media_type') && $request->input('media_type') != "") {
                $data['media_type'] = $request->input('media_type');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('month') && $request->input('month') != "") {
                $data['month'] = $request->input('month');
            }
            if ($request->has('year') && $request->input('year') != "") {
                $data['year'] = $request->input('year');
            }
            /* Added Dynamic Query Condition */
            if ($request->has('fromdate') && $request->filled('fromdate') && $request->has('todate') && $request->filled('todate')) {
                $data['fromdate'] = $request->input('fromdate');
                $data['todate'] = $request->input('todate');
            }

            $media = $media->getmedia($data);
            
            if ($media) {
                $mediadata = array();
                foreach ($media as $item) {
                    if(fileuploadtype=="local")
                    {
                        if($item->media_file==""||$item->media_file==null||$item->media_file=="null")
                        {
                            $item->media_file_path =fixedpagefeaturedimage;
                        }
                        else
                        {
                        if (File::exists(baseimagedisplaypath .$item->media_file)) {
                            $item->media_file_path = imagedisplaypath.$item->media_file;
                        } else {
                            $item->media_file_path =fixedpagefeaturedimage;
                        }
                        }
                    }
                    else
                    {

                        if (does_url_exists(imagedisplaypath.$item->media_file)) {
                            $item->media_file_path = imagedisplaypath.$item->media_file;
                        } else {
                            $item->media_file_path =fixedpagefeaturedimage;
                        } 
                    }
                        /* $file = imagedisplaypath.$item->media_file;
                        $file_headers = @get_headers($file);
                        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                           $item->media_file_path = fixedpagefeaturedimage;
                          
                        }
                        else {
                             $item->media_file_path = imagedisplaypath.$item->media_file;
                        } */
                    array_push($mediadata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($media), 'data' => $mediadata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updatemedia(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "media_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $media = new Media();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['media_id','user_id','accesstoken','media_file']);
            $result = $media->where('media_id', $request->input('media_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Media Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletemedia(Request $request) {
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "media_id" => "required",
            "media_file"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $media = new Media();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['media_id','user_id','accesstoken']);
            $result = $media->where('media_id', $request->input('media_id'))->update($newrequest);
            if ($result) {
                if ($request->has('media_file') && $request->input('media_file') != "") {
                    /*if (does_url_exists(mediapath.$request->has('media_file')) ){
                        $path = mediapath.$request->has('media_file');
                        Storage::disk('s3')->delete($path); 
                    }*/
                }
               return response()->json(['status' => 200, 'data' => "Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
