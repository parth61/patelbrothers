<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\City;

class CityController extends Controller
{
    
     public function addcity(Request $request) {
        //validations
        $city = new City();
        $valid = Validator::make($request->all(), [
                    "city_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $city->city_name = $request->input('city_name');
            $city->state_id = $request->input('state_id');
            $city->country_id = $request->input('country_id');
            $city->status = $request->input('status');
             $city->created_by = $request->input('created_by');
            $city->updated_by = $request->input('updated_by');
            $city->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $city->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $city->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $city->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $city->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "City added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallcity(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $city = new City();
            $data = array();
            if ($request->has('country_id') && $request->input('country_id') != "") {
                $data['country_id'] = $request->input('country_id');
            }
            if ($request->has('state_id') && $request->input('state_id') != "") {
                $data['state_id'] = $request->input('state_id');
            }
            if ($request->has('city_id') && $request->input('city_id') != "") {
                $data['city_id'] = $request->input('city_id');
            }
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $citydata = $city->getcity($data);
            
            return response()->json(['status' => 200, 'count' => count($citydata), 'data' => $citydata]);
        }
    }
    public function updatecity(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "city_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $city = new City();
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['city_id']);
            $result = $city->where('city_id', $request->input('city_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "City Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletecity(Request $request) {
        $valid = Validator::make($request->all(), [
                    "city_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $city = new City();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['city_id']);
            $result = $city->where('city_id', $request->input('city_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
