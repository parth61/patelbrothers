<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomAttributeTerm;
use App\Slug;
class EcomAttributeTermController extends Controller
{
    public function addattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "attribute_term" => "required",
            "attribute_id"=> "required|numeric"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attributeterm = new EcomAttributeTerm();
            $attributeterm->attribute_term = $request->input('attribute_term');
             //code for checking if slug is not passed than take name
            if ($request->has('attribute_term_slug') && $request->input('attribute_term_slug') != "")
            {
                $slug_name =$request->input('attribute_term_slug');
            }
            else
            {
                $slug_name =$request->input('attribute_term');
            }

            $slug_name=checkslugavailability($slug_name, "productattributeterm",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="productattributeterm";
            $slug->site_id =$request->input('site_id');
            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }

           //take slug id and save attribute term
            $attributeterm->slug_id = $slug_id;*/
            $attributeterm->slug_name = $slug_name;
            $attributeterm->attribute_term_colorcode = $request->input('attribute_term_colorcode');
            $attributeterm->attribute_id = $request->input('attribute_id');
            $attributeterm->created_by = $request->input('user_id');
            $attributeterm->updated_by = $request->input('user_id');
            $attributeterm->site_id = $request->input('site_id');
            $attributeterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $attributeterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $attributeterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $attributeterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $attributeterm->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Attributeterm added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallattributeterm(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attributeterm = new EcomAttributeTerm();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('attribute_term_id') && $request->input('attribute_term_id') != "") {
                $data['attribute_term_id'] = $request->input('attribute_term_id');
            }
            
            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('attribute_id') && $request->input('attribute_id') != "") {
                $data['attribute_id'] = $request->input('attribute_id');
            }
            $attributeterm = $attributeterm->getattributeterm($data);

            return response()->json(['status' => 200, 'count' => count($attributeterm), 'data' => $attributeterm]);
        }

    }

    public function updateattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "attribute_term_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            /* Update Attribute */
            $attributeterm = new EcomAttributeTerm();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['slug_id','attribute_term_id','accesstoken','user_id','attribute_term_slug','slug_name']);

            if($request->has('slug_name')){
                $attributeterm = $attributeterm->where('attribute_term_id', $request->input('attribute_term_id'))->first();
                if($attributeterm->slug_name != $request->input('slug_name')){
                   $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "productattributeterm", $request->input('site_id'));
                }
            }

            $result = $attributeterm->where('attribute_term_id', $request->input('attribute_term_id'))->update($newrequest);

            if ($result) {

                $attributeterm = new EcomAttributeTerm();
                $attributetermdata = $attributeterm->where('attribute_term_id', $request->input('attribute_term_id'))->first();
                $slug_id = $attributetermdata->slug_id;
                /* update slug */
                $slug = new Slug();
                $updatedata['slug_name'] = $request->input('attribute_term_slug');
                $slug->where('slug_id', $request->input('slug_id'))->update($updatedata);
                return response()->json(['status' => 200, 'data' => "Attribute term updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteattributeterm(Request $request) {

        $valid = Validator::make($request->all(), [
                    "attribute_term_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attributeterm = new EcomAttributeTerm();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['attribute_term_id','accesstoken','user_id']);
            $result = $attributeterm->where('attribute_term_id', $request->input('attribute_term_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Attribute term deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
