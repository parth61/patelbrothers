<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomCategory;
use App\EcomProductCategory;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Facades\File;
use DB;

class DataSyncController extends Controller
{
	 public function syncSlugtoTable(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "slug_type" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

        	$slug_type = $request->input('slug_type');

        	$table_name = "";
		    if($slug_type == "productattribute"){ $table_name = "tbl_ecom_attribute"; }
		    if($slug_type == "productattributeterm"){ $table_name = "tbl_ecom_attribute_term"; }
		    if($slug_type == "productcategory"){ $table_name = "tbl_ecom_category"; }
		    if($slug_type == "product"){ $table_name = "tbl_ecom_product"; }
		    if($slug_type == "productshippingclass"){ $table_name = "tbl_ecom_shipping_class"; }
		    if($slug_type == "producttag"){ $table_name = "tbl_ecom_tag"; }
		    if($slug_type == "page"){ $table_name = "tbl_page"; }
		    if($slug_type == "postcategory"){ $table_name = "tbl_postcategory"; }
		    if($slug_type == "post"){ $table_name = "tbl_post"; }
		    if($slug_type == "posttag"){ $table_name = "tbl_posttag"; }

		    $slugdata = DB::table('tbl_slug')->select('*')->where('slug_type', '=' ,$slug_type)->where('status', 1)->get();

		    /* Update each row by data */
		    $updatestats = array();
		    foreach($slugdata as $item){
		    	$result = DB::table($table_name)->where('slug_id', $item->slug_id)->update(array("slug_name"=>$item->slug_name));
		    	array_push($updatestats, $result);
		    }

		    return response()->json(['status' => 200, 'totalupdates'=>count($updatestats), 'table_name'=>$table_name, 'slug_data'=>$slugdata, 'data' => $updatestats]);

        }

    }
}