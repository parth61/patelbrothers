<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\ProjectMainCertificate;
use Illuminate\Support\Facades\File;
use DB;
class ProjectMainCertificateController extends Controller
{
    public function addprojectmaincertificate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "certificate_no" => "required",
                    "certificate_type" => "required",
                    "project_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    "certificate_file" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectmaincertificate = new ProjectMainCertificate();
            if ($request->hasFile('certificate_file')) {
                    
                $files = $request->file('certificate_file');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileuploads($filename, $files);
                } 
            $projectmaincertificate->certificate_no = $request->input('certificate_no');
            $projectmaincertificate->certificate_file = $imgpath;
            $projectmaincertificate->created_by = $request->input('created_by');
            $projectmaincertificate->updated_by = $request->input('updated_by');
            $projectmaincertificate->project_id = $request->input('project_id');
            $projectmaincertificate->certificate_type = $request->input('certificate_type');
            $projectmaincertificate->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $projectmaincertificate->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $projectmaincertificate->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $projectmaincertificate->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $projectmaincertificate->save();

             if($result) {
                return response()->json(['status' => 200, 'data' => "Project Certificate Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
   
    public function getallprojectmaincertificate(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectmaincertificate = new ProjectMainCertificate();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('project_main_certificate_id') && $request->input('project_main_certificate_id') != "") {
                $data['project_main_certificate_id'] = $request->input('project_main_certificate_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $projectmaincertificate = $projectmaincertificate->getprojectmaincertificate($data);
            
            if ($projectmaincertificate) {
                $projectmaincertificatedata = array();
                foreach ($projectmaincertificate as $item) {
                    if (does_url_exists(mediapath.$item->certificate_file)) {
                        $item->project_main_certificate_path = imagedisplaypath.$item->certificate_file;
                    } else {
                        $item->project_main_certificate_path = fixedimagepathdocument;
                    }
                    array_push($projectmaincertificatedata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($projectmaincertificate), 'data' => $projectmaincertificatedata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updateprojectmaincertificate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_main_certificate_id" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectmaincertificate = new ProjectMainCertificate();
            $imgpath = null;
            $project_main_certificate_id = $request->input('project_main_certificate_id');

            if ($request->hasFile('certificate_file')) {
                    
                $files = $request->file('certificate_file');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileuploads($filename, $files);
                $query = DB::table('tbl_project_main_certificate')->select('tbl_project_main_certificate.certificate_file')->where('project_main_certificate_id',$project_main_certificate_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->certificate_file;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                            unlink($path);
                        } 
                    }
                   
                }
                }
            $newrequest = $request->except(['project_main_certificate_id','certificate_file']);

            if ($request->hasFile('certificate_file')) {
				$newrequest['certificate_file'] = $imgpath;
		    }

            $result = $projectmaincertificate->where('project_main_certificate_id', $project_main_certificate_id)->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Sales Executive Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteprojectmaincertificate(Request $request) {
        $valid = Validator::make($request->all(), [
                    "project_main_certificate_id" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectmaincertificate = new ProjectMainCertificate();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['project_main_certificate_id']);
            $result = $projectmaincertificate->where('project_main_certificate_id', $request->input('project_main_certificate_id'))->update($newrequest);
            if ($result) {
                $projectprojectmaincertificate = new projectmaincertificate();
                $project_main_certificate_id= $request->input('project_main_certificate_id');
                $query = DB::table('tbl_project_main_certificate')->select('tbl_project_main_certificate.certificate_file')->where('project_main_certificate_id',$project_main_certificate_id)->get();
                if(count($query)>0)
                {
                $img=$query[0]->certificate_file;
                if($img!=NULL|| $img!="")
                {
                    if (does_url_exists(mediapath.$img)) {
                        $path = mediapath.$img;
                        unlink($path);
                    } 
                }
                }
				$projectprojectmaincertificate->where('project_main_certificate_id', $request->input('project_main_certificate_id'))->delete();
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
