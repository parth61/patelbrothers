<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomCoupon;
use App\EcomCouponCategory;
use App\EcomCouponProduct;
use App\EcomOrder;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
class EcomCouponController extends Controller
{
    public function addcoupon(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "coupon_code" => "required"

        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $coupon = new Ecomcoupon();
           
            $coupon->coupon_code = $request->input('coupon_code');
            $coupon->coupon_description = $request->input('coupon_description');
            $coupon->coupon_amount = $request->input('coupon_amount');
            $coupon->discount_type = $request->input('discount_type');
            $coupon->coupon_startdate = $request->input('coupon_startdate');
            $coupon->coupon_enddate = $request->input('coupon_enddate');
            $coupon->coupon_min_amount = $request->input('coupon_min_amount');
            $coupon->coupon_max_amount = $request->input('coupon_max_amount');
            $coupon->usage_limit_coupon = $request->input('usage_limit_coupon');
            $coupon->usage_limit_user = $request->input('usage_limit_user');
            $coupon->is_freeshipping = $request->input('is_freeshipping');
            $coupon->is_visible = $request->input('is_visible');
            $coupon->is_individual_use = $request->input('is_individual_use');
            $coupon->excluded_sale_item = $request->input('excluded_sale_item');
            $coupon->created_by = $request->input('user_id');
            $coupon->site_id = $request->input('site_id');
            $coupon->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $coupon->coupon_title = $request->has('coupon_title')?$request->input('coupon_title'):null;
            $coupon->coupon_subtitle = $request->has('coupon_subtitle')?$request->input('coupon_subtitle'):null;
            $coupon->coupon_image = $request->has('coupon_image')?$request->input('coupon_image'):null;
            $coupon->is_visible_offer = $request->has('is_visible_offer')?$request->input('is_visible_offer'):null;
            $coupon->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $coupon->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $coupon->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $coupon->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Coupon added sucessfully",'coupon_id'=>$coupon->id]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallcoupon(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $coupon = new Ecomcoupon();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('coupon_id') && $request->input('coupon_id') != "") {
                $data['coupon_id'] = $request->input('coupon_id');
            }
            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('coupon_id') && $request->input('coupon_id') != "") {
                $data['coupon_id'] = $request->input('coupon_id');
            }
            $coupon = $coupon->getcoupon($data);
            $coupondata = array();
            foreach($coupon as $item)
            {
                $coupon_id = $item->coupon_id;
                
                $categorynamelist = DB::table('tbl_ecom_coupon_category as cc')
                ->leftJoin('tbl_ecom_category as category', 'cc.category_id', '=', 'category.category_id')
                ->where('cc.status', '=', 1)
                ->where('cc.coupon_id', '=', $coupon_id)
                ->orderBy('category.category_name', 'ASC')
                ->pluck('category.category_name');

                $categoryidlist = DB::table('tbl_ecom_coupon_category as cc')
                ->where('cc.status', '=', 1)
                ->where('cc.coupon_id', '=', $coupon_id)
                ->pluck('cc.category_id');

                $productnamelist = DB::table('tbl_ecom_coupon_product as cp')
                ->leftJoin('tbl_ecom_product as attr', 'attr.product_id', '=', 'cp.product_id')
                ->where('cp.status', '=', 1)
                ->where('cp.coupon_id', '=', $coupon_id)
                ->orderBy('attr.product_name', 'ASC')
                ->pluck('attr.product_name');

                $productidlist= DB::table('tbl_ecom_coupon_product as cp')
                ->where('cp.status', '=', 1)
                ->where('cp.coupon_id', '=', $coupon_id)
                ->pluck('cp.product_id');

                $item->productnamelist= $productnamelist;
                $item->productidlist = $productidlist;
                $item->categorynamelist= $categorynamelist;
                $item->categoryidlist= $categoryidlist;
                if(fileuploadtype=="local"){
                    if($item->coupon_media_path==""||$item->coupon_media_path==null||$item->coupon_media_path=="null")
                    {
                        $item->coupon_media_path =fixedpagefeaturedimage;
                    }else{

                      
                        // $item->coupon_media_path = imagedisplaypath.$item->coupon_media_path;
                        // echo baseimagedisplaypath.$item->coupon_media_path;
                        if (does_url_exists(imagedisplaypath.$item->coupon_media_path)) {
                            $item->coupon_media_path = imagedisplaypath.$item->coupon_media_path;
                        } else {
                            $item->coupon_media_path = fixedpagefeaturedimage;
                        }
                        // if (File::exists(imagedisplaypath.$item->coupon_media_path)) {
                        //     $item->coupon_media_path = imagedisplaypath.$item->coupon_media_path;
                        // } else {
                        //     $item->coupon_media_path = fixedpagefeaturedimage;
                        // }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->coupon_media_path)) {
                        $item->coupon_media_path = imagedisplaypath.$item->coupon_media_path;
                    } else {
                        $item->coupon_media_path =fixedpagefeaturedimage;
                    } 
                }
                array_push($coupondata,$item);
            }
            return response()->json(['status' => 200, 'count' => count($coupon), 'data' => $coupondata]);
        }

    }

    public function updatecoupon(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "coupon_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $coupon = new Ecomcoupon();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['productlist','categorylist','coupon_id','user_id','accesstoken']);
            $result = $coupon->where('coupon_id', $request->input('coupon_id'))->update($newrequest);

            if ($result) {
               $coupon_id=$request->input('coupon_id');
             
               //add product in coupon_product table which holds the category for this coupon on which we can apply the coupon

                if ($request->has('productlist')) {


                    //for updating product coupon relation entry
                    $productlist = $request->input('productlist');
                   

                    if (count($productlist) > 0) {
                       $couponproduct = new EcomCouponProduct();
                      
                      //status 0 code                
                       $newrequest = array();
                       $newrequest['status'] = 0;
                       $newrequest['updated_by'] = $request->input('user_id');
                       $result = $couponproduct->where('coupon_id', $coupon_id)->whereNotIn('product_id', $productlist)->update($newrequest);

                      // $couponproduct->where('coupon_id', $coupon_id)->whereNotIn('product_id', $productlist)->delete();
                       for ($i = 0; $i < count($productlist); $i++) {
                           $checkcount = count($couponproduct->where('product_id', $productlist[$i])->where('coupon_id', $coupon_id)->where('status', '1')->get());
                           if ($checkcount === 0) {
                               $couponproduct = new EcomCouponProduct();
                               $couponproduct->product_id = $productlist[$i];
                               $couponproduct->coupon_id = $coupon_id;
                               
                               $couponproduct->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $couponproduct->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $couponproduct->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $couponproduct->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                               $couponproduct->save();
                           }
                       }
                  
                } else {
                   $couponproduct = new EcomCouponProduct();
                   $couponproduct->where('coupon_id', $request->input('coupon_id'))
                              ->update(array('status' => 0)); 
                       //  $couponproduct->where('coupon_id', $coupon_id)->delete();
                   }
               }
                //add dynamic category in coupon_category table which holds the category for this coupon on which we can apply the coupon

               if ($request->has('categorylist')) {
                   //for updating coupon category relation entry
                   $categorylist = $request->input('categorylist');
                   if (count($categorylist) > 0) {
                      $couponcategory = new EcomCouponCategory();
                     
                     //status 0 code
                      $newrequest = array();
                      
                      $newrequest['status'] = 0;
                       $newrequest['updated_by'] = $request->input('user_id');
                      //$newrequest = $request->except(['coupon_id','categorylist','categorylist','user_id','accesstoken','productlist']);
                      $result = $couponcategory->where('coupon_id', $coupon_id)->whereNotIn('category_id', $categorylist)->update($newrequest);

                     // $couponcategory->where('coupon_id', $coupon_id)->whereNotIn('category_id', $categorylist)->delete();
                      for ($i = 0; $i < count($categorylist); $i++) {
                          $checkcount = count($couponcategory->where('category_id', $categorylist[$i])->where('coupon_id', $coupon_id)->where('status', '1')->get());
                          if ($checkcount === 0) {
                              $couponcategory = new EcomCouponCategory();
                              $couponcategory->category_id = $categorylist[$i];
                              $couponcategory->coupon_id = $coupon_id;
                              $couponcategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                              $couponcategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                              $couponcategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                              $couponcategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                              $couponcategory->save();
                          }
                      }
                 
               } else {
                  $couponcategory = new EcomCouponCategory();
                  $couponcategory->where('coupon_id', $coupon_id)
                             ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                 //  $couponcategory->where('coupon_id', $coupon_id)->delete();
                  }
              }

                return response()->json(['status' => 200, 'data' => "Coupon updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletecoupon(Request $request) {

        $valid = Validator::make($request->all(), [
                    "coupon_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $coupon = new Ecomcoupon();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['coupon_id','user_id','accesstoken']);
            $result = $coupon->where('coupon_id', $request->input('coupon_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  public function applycoupon(Request $request) {

        $valid = Validator::make($request->all(), [
            "coupon_code" => "required",
            "accesstoken" => "required",
            "site_id" => "required",
            "order_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $coupon = new Ecomcoupon();
            $couponcode = $coupon->where('site_id', $request->input('site_id'))->where('is_visible', 1)->where('status', 1)->where('coupon_code', $request->input('coupon_code'))->whereDate('coupon_startdate','<=',date("Y-m-d"))->whereDate('coupon_enddate','>=',date("Y-m-d"))->get();
             
            if(count($couponcode)>0){

                $ordermodel = new EcomOrder();
                $orderdata = $ordermodel->where('order_id', $request->input('order_id'))->get();

                if($couponcode[0]->discount_type == "2"){
                
                    /* update order info */

                    $coupondiscount = (floatval($orderdata[0]->order_amount) + floatval($orderdata[0]->order_tax))*(floatval($couponcode[0]->coupon_amount)/100);

                    $discount = $coupondiscount;
                    
                    if($couponcode[0]->coupon_max_amount != null){
                      $discount = ($coupondiscount > $couponcode[0]->coupon_max_amount)?$couponcode[0]->coupon_max_amount:$coupondiscount;
                    }
                    if($couponcode[0]->coupon_min_amount != null){
                      $discount = ($coupondiscount < $couponcode[0]->coupon_min_amount)?$couponcode[0]->coupon_min_amount:$coupondiscount;
                    }

                    $grandtotal = floatval($orderdata[0]->order_amount) - floatval($discount) + floatval($orderdata[0]->order_tax) + floatval($orderdata[0]->order_shipping_charge);

                    if($coupondiscount > $orderdata[0]->order_amount){
                      return response()->json(['status' => 400, 'error' => 'Discount is more than cart amount. Invalid discout coupon.']);
                    }else{

                      $updatedata = array();
                      $updatedata['coupon_id'] = $couponcode[0]->coupon_id;
                      $updatedata['order_discount'] = $discount;
                      $updatedata['order_grandtotal'] = $grandtotal;
                      $ordermodel->where('order_id', $request->input('order_id'))->update($updatedata);

                      $orderdata = $ordermodel->where('order_id', $request->input('order_id'))->get();

                      return response()->json(['status' => 200, 'data' => $orderdata]);

                    }

                    
                
                }else{
                    return response()->json(['status' => 400, 'error' => 'Coupon is expired or invalid or not featured.']);
                }

            }else{
                return response()->json(['status' => 400, 'error' => 'Coupon is expired or invalid.']);
            }

            
        }

    }
  
}
