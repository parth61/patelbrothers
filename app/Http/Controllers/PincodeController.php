<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Pincode;
use App\PincodeIndia;

class PincodeController extends Controller {

    public function addpincode(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "pincode" => "required|numeric",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincode = new Pincode();
            $pincode->pincode = $request->input('pincode');
            $pincode->created_by = $request->input('created_by');
            $pincode->updated_by = $request->input('updated_by');
            $pincode->area_name = $request->input('area_name');
            $pincode->status = $request->input('status');
            $pincode->country_id = $request->input('country_id');
            $pincode->state_id = $request->input('state_id');
              $pincode->city_id = $request->input('city_id');
              $pincode->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $pincode->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $pincode->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $pincode->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $pincode->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Pincode Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallpincode(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincode = new Pincode();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            
            if ($request->has('pincode_id') && $request->input('pincode_id') != "") {
                $data['pincode_id'] = $request->input('pincode_id');
            }
            
            $pincode = $pincode->getpincode($data);
            return response()->json(['status' => 200, 'count' => count($pincode), 'data' => $pincode]);
        }
    }
   
    public function updatepincode(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "pincode_id" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincode = new Pincode();
            $newrequest = $request->except(['pincode_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $pincode->where('pincode_id', $request->input('pincode_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Pincode Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletepincode(Request $request) {
        $valid = Validator::make($request->all(), [
                    "pincode_id" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $pincode = new Pincode();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['pincode_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $pincode->where('pincode_id', $request->input('pincode_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function managepincode(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "pincode" => "required|numeric",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->has('pincode_id') && $request->filled('pincode_id')){
                $pincode = new Pincode();
                $newrequest = $request->except(['pincode_id']);
                
                $result = $pincode->where('pincode_id', $request->input('pincode_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Pincode Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $pincode = new Pincode();
                $pincode->pincode = $request->input('pincode');
                $pincode->created_by = $request->input('created_by');
                $pincode->updated_by = $request->input('updated_by');
                $pincode->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $pincode->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $pincode->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $pincode->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $pincode->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Pincode Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
        }
    }


    // get country
    public function getallcountry(Request $request) {
        $valid = Validator::make($request->all(), [
                       "limit" => "required|numeric",
                       "offset" => "required|numeric",
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
              $pincodes = new PincodeIndia();
              $data = array();
              $data['offset'] = $request->input('offset');
              $data['limit'] = $request->input('limit');

              $pincode = $pincodes->getcountry($data);
   
              return response()->json(['status' => 200, 'count' => count($pincode), 'data' => $pincode]);
           }
       }

      // get state
       public function getallstate(Request $request) {
        $valid = Validator::make($request->all(), [
               "limit" => "required|numeric",
               "offset" => "required|numeric",
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
               $pincodes = new PincodeIndia();
               $data = array();
               $data['offset'] = $request->input('offset');
               $data['limit'] = $request->input('limit');
               if($request->has('country')){
                  $data['country'] = $request->input('country');
               }
               $state = $pincodes->getstate($data);
   
               return response()->json(['status' => 200, 'count' => count($state), 'data' => $state]);
           }
       }

       //get district
       public function getalldistrict(Request $request) {
        $valid = Validator::make($request->all(), [
                       "limit" => "required|numeric",
                       "offset" => "required|numeric",
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
               $pincodes = new PincodeIndia();
               $data = array();
               $data['offset'] = $request->input('offset');
               $data['limit'] = $request->input('limit');
               if($request->has('state')){
                  $data['state'] = $request->input('state');
               }
               $district = $pincodes->getdistrict($data);
   
               return response()->json(['status' => 200, 'count' => count($district), 'data' => $district]);
           }
       } 
       //get pincode
       
       public function getallpincodedata(Request $request) {
        $valid = Validator::make($request->all(), [
                 "district" => "required",
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
                $pincodes = new PincodeIndia();
               $data = array();
               if($request->has('district')){
                  $data['district'] = $request->input('district');
               }
               $pincodedata = $pincodes->getpincode($data);
   
               return response()->json(['status' => 200, 'count' => count($pincodedata), 'data' => $pincodedata]);
           }
       } 
}
