<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Occupation;

class OccupationController extends Controller
{
    

     public function addmasteroccupation(Request $request) {
        //validations
        $occupation = new Occupation();
        $valid = Validator::make($request->all(), [
                    "occupation_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $occupation->occupation_name = $request->input('occupation_name');
            $occupation->created_by = $request->input('created_by');
            $occupation->updated_by = $request->input('updated_by');
            $occupation->status = $request->input('status');
            $occupation->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $occupation->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $occupation->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $occupation->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $occupation->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Occupation added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallmasteroccupation(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $occupation = new Occupation();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $occupationdata = $occupation->getoccupation($data);
            
            return response()->json(['status' => 200, 'count' => count($occupationdata), 'data' => $occupationdata]);
        }
    }
    public function updatemasteroccupation(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "occupation_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $occupation = new Occupation();
            $newrequest = $request->except(['occupation_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $occupation->where('occupation_id', $request->input('occupation_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Occupation Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletemasteroccupation(Request $request) {
        $valid = Validator::make($request->all(), [
                    "occupation_id" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $occupation = new Occupation();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['occupation_id']);
            $result = $occupation->where('occupation_id', $request->input('occupation_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
