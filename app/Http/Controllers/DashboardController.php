<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Project;
use App\SalesExecutive;
use App\Lead;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
	  
	public function getDashboardStats(Request $request) {

        $finaldata = array(); 

        if($request->has('builder_id') && $request->filled('builder_id')){

            $projectlist = [];

            $projectmodel = new Project();
            $projectlist = $projectmodel->where('status', 1)->where('builder_id', $request->input('builder_id'))->pluck('project_id');

            $salesexecutivemodel = new SalesExecutive();
            $finaldata['totalsalesexecutive'] = $salesexecutivemodel->where('status', 1)->where('builder_id', $request->input('builder_id'))->count();
            
            $projectmodel = new Project();
            $finaldata['totalprojects'] = $projectmodel->where('status', 1)->whereIn('project_id', $projectlist)->count();

            $leadmodel = new Lead();
            $finaldata['totalleads'] = $leadmodel->where('status', 1)->whereIn('project_id', $projectlist)->count();

        }else{
            $salesexecutivemodel = new SalesExecutive();
            $finaldata['totalsalesexecutive'] = $salesexecutivemodel->where('status', 1)->count();

            $projectmodel = new Project();
            $finaldata['totalprojects'] = $projectmodel->where('status', 1)->count();

            $leadmodel = new Lead();
            $finaldata['totalleads'] = $leadmodel->where('status', 1)->count();
        }
        
        $finaldata['totalregistartions'] = DB::table('tbl_user as tu')->where('status', 1)->count();

        return response()->json(['status' => 200, 'data' => $finaldata]);

    }
      
	public function getProjectwiseData(Request $request) {

        $leaddata = array();

        if($request->has('builder_id') && $request->filled('builder_id')){
            $leaddata = $query = DB::table('tbl_leads as lead')->select('project.project_name', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->where('lead.status', 1)
            ->where('project.status', 1)
            ->where('project.builder_id', $request->input('builder_id'))
            ->groupBy('project.project_name')
            ->get();

        }else{
            $leaddata = $query = DB::table('tbl_leads as lead')->select('project.project_name', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->where('lead.status', 1)
            ->where('project.status', 1)
            ->groupBy('project.project_name')
            ->get();
        }

        $finaldata = array(); $project_name = array(); $lead_count = array();

        foreach($leaddata as $item){
            array_push($project_name, $item->project_name);
            array_push($lead_count, $item->total);
        }

        $finaldata['projects'] = $project_name;
        $finaldata['leads'] = $lead_count;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }

    public function getLeadstatuswiseData(Request $request) {

        $leaddata = array();

        if($request->has('builder_id') && $request->filled('builder_id')){
            $leaddata = $query = DB::table('tbl_leads as lead')->select('lead.lead_status_primary', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->where('lead.status', 1)
            ->where('project.status', 1)
            ->where('project.builder_id', $request->input('builder_id'))
            ->groupBy('lead.lead_status_primary')
            ->get();

        }else{
            $leaddata = $query = DB::table('tbl_leads as lead')->select('lead.lead_status_primary', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->where('lead.status', 1)
            ->where('project.status', 1)
            ->groupBy('lead.lead_status_primary')
            ->get();
        }

        $finaldata = array(); $lead_status_primary = array(); $lead_count = array();

        foreach($leaddata as $item){
            array_push($lead_status_primary, $item->lead_status_primary);
            array_push($lead_count, $item->total);
        }

        $finaldata['lead_status'] = $lead_status_primary;
        $finaldata['leads'] = $lead_count;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }
    
    public function getSalesExecutiveWiseData(Request $request) {

        $leaddata = array(); $sales_executive_list = array();

        if($request->has('builder_id') && $request->filled('builder_id')){
            $leaddata = DB::table('tbl_leads as lead')->select('se.sales_executive_id', DB::raw('concat(se.sales_executive_firstname," ",se.sales_executive_lastname) as sales_executive_name'), DB::raw('count(*) as total'))
            ->leftJoin('tbl_project_sales_executive as pse','pse.project_id','=','lead.project_id')
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->leftJoin('tbl_sales_executive as se','se.sales_executive_id','=','pse.sales_executive_id')
            ->where('lead.status', 1)
            ->where('project.status', 1)
            ->where('project.builder_id', $request->input('builder_id'))
            ->groupBy('pse.sales_executive_id')
            ->get();

            $sales_executive_list = DB::table('tbl_sales_executive as tse')->where('status', 1)->where('builder_id', $request->input('builder_id'))->get();

        }else{
            $leaddata = DB::table('tbl_leads as lead')->select('se.sales_executive_id', DB::raw('concat(se.sales_executive_firstname," ",se.sales_executive_lastname) as sales_executive_name'), DB::raw('count(*) as total'))
            ->leftJoin('tbl_project_sales_executive as pse','pse.project_id','=','lead.project_id')
            ->leftJoin('tbl_sales_executive as se','se.sales_executive_id','=','pse.sales_executive_id')
            ->where('lead.status', 1)
            ->where('se.status', 1)
            ->groupBy('pse.sales_executive_id')
            ->get();

            $sales_executive_list = DB::table('tbl_sales_executive as tse')->where('status', 1)->get();
        }

        $finaldata = array(); $sales_executive_name = array(); $lead_count = array();

        foreach($sales_executive_list as $tseitem){
            $isleadassigned = 0;
            foreach($leaddata as $item){
                if($item->sales_executive_id == $tseitem->sales_executive_id){
                    array_push($sales_executive_name, $item->sales_executive_name);
                    array_push($lead_count, $item->total);
                    $tseitem->totalleads = $item->total;
                    $isleadassigned = 1;
                }
            }
            if($isleadassigned==0){$tseitem->totalleads = 0;}
        }
        
        $finaldata['sales_executive_list'] = $sales_executive_list;
        $finaldata['sales_executive'] = $sales_executive_name;
        $finaldata['leads'] = $lead_count;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }

    public function getTouchpointWiseLeadData(Request $request) {

        $leaddata = array();

        if($request->has('builder_id') && $request->filled('builder_id')){

            $leaddata = $query = DB::table('tbl_lead_touchpoint as tlt')->select('tp.touchpoint_name',DB::raw('count(*) as total'))
            ->leftJoin('tbl_touchpoint as tp','tp.touchpoint_id','=','tlt.touchpoint_id')
            ->leftJoin('tbl_leads as tl','tl.lead_id','=','tlt.lead_id')
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('tp.status', 1)
            ->where('project.status', 1)
            ->where('project.builder_id', $request->input('builder_id'))
            ->groupBy('tp.touchpoint_id')
            ->get();

        }else{
            $leaddata = $query = DB::table('tbl_lead_touchpoint as tlt')->select('tp.touchpoint_name',DB::raw('count(*) as total'))
            ->leftJoin('tbl_touchpoint as tp','tp.touchpoint_id','=','tlt.touchpoint_id')
            ->leftJoin('tbl_leads as tl','tl.lead_id','=','tlt.lead_id')
            ->where('tl.status', 1)
            ->where('tp.status', 1)
            ->groupBy('tp.touchpoint_id')
            ->get();
        }

        $finaldata = array(); $touchpoint_name = array(); $lead_count = array();

        foreach($leaddata as $item){
            array_push($touchpoint_name, $item->touchpoint_name);
            array_push($lead_count, $item->total);
        }

        $finaldata['touchpoint_name'] = $touchpoint_name;
        $finaldata['leads'] = $lead_count;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }

    public function getDayWiseLeadData(Request $request) {

        $leaddata = array(); $projectwisedata = array();
        $fromdate = $request->input('fromdate');
        $todate = $request->input('todate');

        if($request->has('builder_id') && $request->filled('builder_id')){

            $projectlist = DB::table('tbl_project as tp')->where('builder_id', $request->input('builder_id'))->where('status', 1)->pluck('project_id');

            $leaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'),DB::raw('count(*) as total'))
            ->where('tl.status', 1)
            ->whereIn('tl.project_id', $projectlist)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->get();

            $projectwisedata = $query = DB::table('tbl_leads as tl')->select("project.project_name",DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->whereIn('tl.project_id', $projectlist)
            ->where('project.status', 1)
            ->groupBy('project.project_id')
            ->orderBy('total', "DESC")
            ->get();

        }else{
            $leaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'),DB::raw('count(*) as total'))
            ->where('tl.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->get();

            $projectwisedata = $query = DB::table('tbl_leads as tl')->select("project.project_name",DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('project.status', 1)
            ->groupBy('project.project_id')
            ->orderBy('total', "DESC")
            ->get();
        }

        $finaldata = array(); $touchpoint_name = array(); $lead_count = array();
        $dateslist = array();

        while (strtotime($fromdate) <= strtotime($todate)) {
            array_push($dateslist, $fromdate);
            $fromdate = date ("Y-m-d", strtotime("+1 day", strtotime($fromdate)));
            $isfound = 0;            
            foreach($leaddata as $item){
                if($item->dates == $fromdate){
                    array_push($lead_count, $item->total);
                    $isfound = 1;
                }
            }
            if($isfound===0){ array_push($lead_count, 0); }
        }

        $finaldata['dates'] = $dateslist;
        $finaldata['leads'] = $lead_count;
        $finaldata['projectwise_leads'] = $projectwisedata;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }

    public function getDayWiseLeadStatusData(Request $request) {

        $leaddata = array();
        $fromdate = $request->input('fromdate');
        $todate = $request->input('todate');

        if($request->has('builder_id') && $request->filled('builder_id')){

            $openleaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'), DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('project.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->where('project.builder_id', $request->input('builder_id'))
            ->where('tl.lead_status_primary', 'Open')
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->get();

            $qualifiedleaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'), DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('project.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->where('project.builder_id', $request->input('builder_id'))
            ->where('tl.lead_status_primary', 'Qualified')
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->get();
            
            $notqualifiedleaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'), DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('project.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->where('project.builder_id', $request->input('builder_id'))
            ->where('tl.lead_status_primary', 'Not Qualified')
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->get();

        }else{

            $openleaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'), DB::raw('count(*) as total'))
            ->where('tl.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->where('tl.lead_status_primary', 'Open')
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->groupBy('tl.lead_status_primary')
            ->get();

            $qualifiedleaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'), DB::raw('count(*) as total'))
            ->where('tl.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->where('tl.lead_status_primary', 'Qualified')
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->groupBy('tl.lead_status_primary')
            ->get();

            $notqualifiedleaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'), DB::raw('count(*) as total'))
            ->where('tl.status', 1)
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])
            ->where('tl.lead_status_primary', 'Not Qualified')
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->groupBy('tl.lead_status_primary')
            ->get();
        }

        $finaldata = array(); $openleads = array(); $qualifiedleads = array(); $notqualifiedleads = array();  $dateslist = array();
        $openleadscount = 0; $qualifiedleadscount = 0; $notqualifiedleadscount = 0;

        while (strtotime($fromdate) <= strtotime($todate)) {
            array_push($dateslist, $fromdate);
            $fromdate = date ("Y-m-d", strtotime("+1 day", strtotime($fromdate)));
            $isfound1 = 0; $isfound2 = 0; $isfound3 = 0;           
            foreach($qualifiedleaddata as $item){
                if($item->dates == $fromdate){
                    array_push($qualifiedleads, $item->total);
                    $isfound1 = 1; 
                    $qualifiedleadscount = $qualifiedleadscount + $item->total;
                }
            }
            if($isfound1===0){ array_push($qualifiedleads, 0); }
            foreach($notqualifiedleaddata as $item){
                if($item->dates == $fromdate){
                    array_push($qualifiedleads, $item->total);
                    $isfound2 = 1; 
                    $notqualifiedleadscount = $notqualifiedleadscount + $item->total;
                }
            }
            if($isfound2===0){ array_push($notqualifiedleads, 0); } 
            foreach($openleaddata as $item){
                if($item->dates == $fromdate){
                    array_push($openleads, $item->total);
                    $isfound3 = 1; 
                    $openleadscount = $openleadscount + $item->total;
                }
            }
            if($isfound3===0){ array_push($openleads, 0); }
        }

        $finaldata['dates'] = $dateslist;
        $finaldata['openleads'] = $openleads;
        $finaldata['qualifiedleads'] = $qualifiedleads;
        $finaldata['notqualifiedleads'] = $notqualifiedleads;
        $finaldata['openleadscount'] = $openleadscount;
        $finaldata['qualifiedleadscount'] = $qualifiedleadscount;
        $finaldata['notqualifiedleadscount'] = $notqualifiedleadscount;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }

    public function getBuilderwiseLeads(Request $request) {

        $builderlist = $query = DB::table('tbl_builder as tb')->select('tb.*')->where('tb.status', 1)->get();

        $builderleads = $query = DB::table('tbl_leads as tl')->select('tb.builder_id', 'tb.builder_name', 'tl.lead_status_primary', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->leftJoin('tbl_builder as tb','tb.builder_id','=','project.builder_id')
            ->where('tl.status', 1)
            ->where('project.status', 1)
            ->where('tb.status', 1)
            ->groupBy('tb.builder_id')
            ->groupBy('tl.lead_status_primary')
            ->get();

        $finaldata = array(); $buildersdata = array(); $leaddata = array();

        foreach($builderlist as $item){
            $isfound = 0;
            foreach($builderleads as $subitem){
                if($item->builder_id == $subitem->builder_id){
                    array_push($buildersdata, $subitem->builder_name);
                    array_push($leaddata, $subitem->total);
                    $isfound =1;
                }
            }
            if($isfound == 0){
                array_push($buildersdata, $item->builder_name);
                array_push($leaddata, 0);
            }
        }

        $finaldata['builderdata'] = $buildersdata;
        $finaldata['leaddata'] = $leaddata;
        
        return response()->json(['status' => 200, 'data' => $finaldata]);

    }

    public function getSourcewiseLeads(Request $request) {

        $sourcewiseleads = $query = DB::table('tbl_leads as tl')->select('tu.usource', 'tl.lead_status_primary', DB::raw('count(*) as total'))
            ->leftJoin('tbl_user as tu','tu.user_id','=','tl.user_id')
            ->where('tl.status', 1)
            ->where('tu.status', 1)
            ->groupBy('tu.usource')
            ->groupBy('tl.lead_status_primary')
            ->get();

        return response()->json(['status' => 200, 'data' => $sourcewiseleads]);

    }

    /* Get Sales Executive Dashboard Data */

    public function getSalesExecutiveDashboardStats(Request $request) {

        $finaldata = array(); 

        if($request->has('sales_executive_id') && $request->filled('sales_executive_id')){

            $finaldata['totalassignedleads'] = DB::table('tbl_leads as tl')->where('sales_executive_id', $request->input('sales_executive_id'))->where('status', 1)->count();
            $finaldata['openleads'] = DB::table('tbl_leads as tl')->where('lead_status_primary', "Open")->where('sales_executive_id', $request->input('sales_executive_id'))->where('status', 1)->count();
            $finaldata['qualifiedleads'] = DB::table('tbl_leads as tl')->where('lead_status_primary', "Qualified")->where('sales_executive_id', $request->input('sales_executive_id'))->where('status', 1)->count();
            $finaldata['notqualifiedleads'] = DB::table('tbl_leads as tl')->where('lead_status_primary', "Not Qualified")->where('sales_executive_id', $request->input('sales_executive_id'))->where('status', 1)->count();
            $finaldata['projectassigned'] = DB::table('tbl_project_sales_executive as tpse')->distinct('project_id')->where('sales_executive_id', $request->input('sales_executive_id'))->where('status', 1)->count();

            return response()->json(['status' => 200, 'data' => $finaldata]);

        }else{
            return response()->json(['status' => 400, 'error' => 'Enter sales executive id'], 400);
        }
    }

    public function getDayWiseLeadDataforSalesExecutive(Request $request) {

        $leaddata = array(); $projectwisedata = array();
        $fromdate = $request->input('fromdate');
        $todate = $request->input('todate');

        if($request->has('sales_executive_id') && $request->filled('sales_executive_id')){

            $leaddata = $query = DB::table('tbl_leads as tl')->select(DB::raw('Date(tl.created_at) as dates'),DB::raw('count(*) as total'))
            ->leftJoin('tbl_project_sales_executive as tpse','tpse.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('tpse.status', 1)
            ->where('tpse.sales_executive_id', $request->input('sales_executive_id'))
            ->whereRaw("(tl.created_at >= ? AND tl.created_at <= ?)", [$fromdate." 00:00:00", $todate." 23:59:59"])            
            ->groupBy(DB::raw('Date(tl.created_at)'))
            ->get();

            $projectwisedata = $query = DB::table('tbl_leads as tl')->select("project.project_name",DB::raw('count(*) as total'))
            ->leftJoin('tbl_project_sales_executive as tpse','tpse.project_id','=','tl.project_id')
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('tpse.status', 1)
            ->where('project.status', 1)
            ->where('tpse.sales_executive_id', $request->input('sales_executive_id'))
            ->groupBy('tpse.project_id')
            ->orderBy('total', "DESC")
            ->get();

            $finaldata = array(); $touchpoint_name = array(); $lead_count = array();
            $dateslist = array();

            while (strtotime($fromdate) <= strtotime($todate)) {
                array_push($dateslist, $fromdate);
                $fromdate = date ("Y-m-d", strtotime("+1 day", strtotime($fromdate)));
                $isfound = 0;            
                foreach($leaddata as $item){
                    if($item->dates == $fromdate){
                        array_push($lead_count, $item->total);
                        $isfound = 1;
                    }
                }
                if($isfound===0){ array_push($lead_count, 0); }
            }

            $finaldata['dates'] = $dateslist;
            $finaldata['leads'] = $lead_count;
            $finaldata['projectwise_leads'] = $projectwisedata;
            
            return response()->json(['status' => 200, 'data' => $finaldata]);

        }else{
            return response()->json(['status' => 400, 'error' => 'Enter sales executive id'], 400);
        }

    }

    public function getProjectwiseDataforSalesExecutive(Request $request) {

        $leaddata = array();

        if($request->has('salesexecutive_id') && $request->filled('salesexecutive_id')){
            $leaddata = $query = DB::table('tbl_leads as lead')->select('project.project_name', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project_sales_executive as tpse','tpse.project_id','=','lead.project_id')
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->where('lead.status', 1)
            ->where('tpse.status', 1)
            ->where('project.status', 1)
            ->where('tpse.sales_executive_id', $request->input('salesexecutive_id'))
            ->groupBy('project.project_name')
            ->get();

            $finaldata = array(); $project_name = array(); $lead_count = array();

            foreach($leaddata as $item){
                array_push($project_name, $item->project_name);
                array_push($lead_count, $item->total);
            }

            $finaldata['projects'] = $project_name;
            $finaldata['leads'] = $lead_count;
            
            return response()->json(['status' => 200, 'data' => $finaldata]);

        }else{
            return response()->json(['status' => 400, 'error' => 'Enter sales executive id'], 400);
        }

        

    }

    public function getTouchpointWiseLeadDataforSalesExecutive(Request $request) {

        $leaddata = array();

        if($request->has('salesexecutive_id') && $request->filled('salesexecutive_id')){

            $leaddata = $query = DB::table('tbl_lead_touchpoint as tlt')->select('tp.touchpoint_name',DB::raw('count(*) as total'))
            ->leftJoin('tbl_touchpoint as tp','tp.touchpoint_id','=','tlt.touchpoint_id')
            ->leftJoin('tbl_leads as tl','tl.lead_id','=','tlt.lead_id')
            ->leftJoin('tbl_project as project','project.project_id','=','tl.project_id')
            ->leftJoin('tbl_project_sales_executive as tpse','tpse.project_id','=','tl.project_id')
            ->where('tl.status', 1)
            ->where('tp.status', 1)
            ->where('tpse.status', 1)
            ->where('project.status', 1)
            ->where('tpse.sales_executive_id', $request->input('salesexecutive_id'))
            ->groupBy('tp.touchpoint_id')
            ->get();

            $finaldata = array(); $touchpoint_name = array(); $lead_count = array();

            foreach($leaddata as $item){
                array_push($touchpoint_name, $item->touchpoint_name);
                array_push($lead_count, $item->total);
            }

            $finaldata['touchpoint_name'] = $touchpoint_name;
            $finaldata['leads'] = $lead_count;
            
            return response()->json(['status' => 200, 'data' => $finaldata]);

        }else{
            
            return response()->json(['status' => 400, 'error' => 'Enter sales executive id'], 400);

        }

        

    }

    public function getLeadstatuswiseDataForSalesExecutive(Request $request) {

        $leaddata = array();

        if($request->has('salesexecutive_id') && $request->filled('salesexecutive_id')){

            $leaddata = $query = DB::table('tbl_leads as lead')->select('lead.lead_status_primary', DB::raw('count(*) as total'))
            ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
            ->leftJoin('tbl_project_sales_executive as tpse','tpse.project_id','=','lead.project_id')
            ->where('lead.status', 1)
            ->where('project.status', 1)
            ->where('tpse.status', 1)
            ->where('tpse.sales_executive_id', $request->input('salesexecutive_id'))
            ->groupBy('lead.lead_status_primary')
            ->get();
    
            $finaldata = array(); $lead_status_primary = array(); $lead_count = array();

            foreach($leaddata as $item){
                array_push($lead_status_primary, $item->lead_status_primary);
                array_push($lead_count, $item->total);
            }

            $finaldata['lead_status'] = $lead_status_primary;
            $finaldata['leads'] = $lead_count;
            
            return response()->json(['status' => 200, 'data' => $finaldata]);
            
        }else{
            return response()->json(['status' => 400, 'error' => 'Enter sales executive id'], 400);
        }

    }

    public function getCustomerOfferClaimStats(Request $request){
        $customerofferclaims = $query = DB::table('tbl_customerofferclaims as tc')->select('tc.verified_status', DB::raw('count(*) as total'))            
            ->where('tc.status', 1)
            ->groupBy('tc.verified_status')
            ->get();

        return response()->json(['status' => 200, 'data' => $customerofferclaims]);
    }


        //hootcms dashboard statistics
    public function gethootdashboarddata(Request $request){
 
		if($request->has('site_id')){
			$postcount = DB::table('tbl_post as pos')->select('pos.*')            
			->where('pos.status', '!=' , 0)  
			->where('pos.site_id', '=' , $request->input('site_id'))		
			->get();

			$postpublishcount = DB::table('tbl_post as pos')->select('pos.*')     
			->where('pos.status', 1)
			->where('pos.site_id', '=' , $request->input('site_id'))		
			->get();

			$postdraftcount = DB::table('tbl_post as pos')->select('pos.*')            
			->where('pos.status', 2)
			->where('pos.site_id', '=' , $request->input('site_id'))
			->get();


			$pagecount = DB::table('tbl_page as page')->select('page.*')            
			->where('page.status', '!=' , 0)
			->where('page.site_id', '=' , $request->input('site_id'))			
			->get();

			$pagepublishcount = DB::table('tbl_page as page')->select('page.*')     
			->where('page.status', 1) 
			->where('page.site_id', '=' , $request->input('site_id'))
			->get();

			$pagedraftcount = DB::table('tbl_page as page')->select('page.*')            
			->where('page.status', 2)
			->where('page.site_id', '=' , $request->input('site_id'))
			->get();

			$usercount = DB::table('tbl_admin as admin')->select('admin.*')            
			->where('admin.status', 1)
			->where('admin.site_id', '=' , $request->input('site_id'))
			->get();
		}else{
			$postcount = DB::table('tbl_post as pos')->select('pos.*')            
			->where('pos.status', '!=' , 0)      
			->get();

			$postpublishcount = DB::table('tbl_post as pos')->select('pos.*')     
			->where('pos.status', 1)       
			->get();

			$postdraftcount = DB::table('tbl_post as pos')->select('pos.*')            
			->where('pos.status', 2)
			->get();


			$pagecount = DB::table('tbl_page as page')->select('page.*')            
			->where('page.status', '!=' , 0)        
			->get();

			$pagepublishcount = DB::table('tbl_page as page')->select('page.*')     
			->where('page.status', 1)       
			->get();

			$pagedraftcount = DB::table('tbl_page as page')->select('page.*')            
			->where('page.status', 2)
			->get();

			$usercount = DB::table('tbl_admin as admin')->select('admin.*')            
			->where('admin.status', 1)
			->get();
		}
        
        $data=array();

        $data['postcount']=count($postcount);
        $data['postpublishcount']=count($postpublishcount);
        $data['postdraftcount']=count($postdraftcount);
        $data['pagecount']=count($pagecount);
        $data['pagepublishcount']=count($pagepublishcount);
        $data['pagedraftcount']=count($pagedraftcount);
        $data['usercount']=count($usercount);

        return response()->json(['status' => 200,'data'=>$data]);
    }
}