<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\RelationPostTag;
class RelationPostTagController extends Controller
{
    public function addrelationposttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "post_id"=>"required",
            "posttag_id"=>"required"
         
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $relationposttag = new RelationPostTag();
            $relationposttag->post_id = $request->input('post_id');
            $relationposttag->posttag_id = $request->input('posttag_id');
            $relationposttag->status = $request->input('status');
            $relationposttag->created_by = $request->input('user_id');
            $relationposttag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $relationposttag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $relationposttag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $relationposttag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $relationposttag->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "relationposttag Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallrelationposttag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relationposttag = new RelationPostTag();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('relation_post_tag_id') && $request->input('relation_post_tag_id') != "") {
                $data['relation_post_tag_id'] = $request->input('relation_post_tag_id');
            }

            if ($request->has('post_id') && $request->input('post_id') != "") {
                $data['post_id'] = $request->input('post_id');
            }

            if ($request->has('posttag_id') && $request->input('posttag_id') != "") {
                $data['posttag_id'] = $request->input('posttag_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $relationposttag = $relationposttag->getrelationposttag($data);
            if($relationposttag)
            {
                return response()->json(['status' => 200, 'count' => count($relationposttag), 'data' => $relationposttag]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updaterelationposttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "relation_post_tag_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relationposttag = new RelationPostTag();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['relation_post_tag_id','user_id','accesstoken']);
            $result = $relationposttag->where('relation_post_tag_id', $request->input('relation_post_tag_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "relationposttag Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleterelationposttag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "relation_post_tag_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relationposttag = new RelationPostTag();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['relation_post_tag_id','user_id','accesstoken']);
            $result = $relationposttag->where('relation_post_tag_id', $request->input('relation_post_tag_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
