<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Slim;
use App\Project;
use App\EcomOrder;
use App\EcomOrderDetails;
use App\EcomOrderItems;
use App\EcomOrderItemsTax;
use App\EcomBusinessSetting;
use App\User;
use App\EcomAddress;
use PDF;
use File;
use Illuminate\Support\Facades\Storage;
use Validator;

class TestingController extends Controller
{
    public function test(Request $request) {
        return view('cropper');
	}
	  
	public function uploadImage(Request $request) {

		$response = fileUploadSlim($request, 'testdata');

		// Return results as JSON String
		
		return response()->json(['status' => 200, 'data' => $response]);
	}

	public function sendorderemail(Request $request){

		$valid = Validator::make($request->all(), [
            "user_id" => "required",
            "site_id" => "required",
            "order_id" => "required"
        ]);

        if ($valid->fails()) {

            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);

        } else {

			$user = new User();
			$businessinfo = new EcomBusinessSetting();
	        $order = new EcomOrder();
	        $orderdetail = new EcomOrderDetails();
	        $orderitems = new EcomOrderItems();
	        $ecomaddress = new EcomAddress();

	        $content["userinfo"] = $user->where('user_id', $request->input('user_id'))->first();
	        $content["billingaddress"] = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->first();
	        $content["shippingaddress"] = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->first();
	        $content["businessinfo"] = $businessinfo->where('site_id', $request->input('site_id'))->first();
	        $content['order'] = $order->where('order_id', $request->input('order_id'))->first();
	        $content['orderdetail'] = $orderdetail->where('order_id', $request->input('order_id'))->first();
	        /* Generate Order Items */
	        $orderdata = $orderitems->where('order_id', $request->input('order_id'))->get();
	        foreach($orderdata as $oitem){
	            $taxdata = array(
	                "order_item_id" => $oitem['order_item_id']
	            );
	            $ecomorderitemtax = new EcomOrderItemsTax();
	            $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
	        }
	        $content['orderitems'] = $orderdata;

            /* Send order Email */
            $emailstatus = orderConfirmEmail($content, $content["userinfo"]->user_email);
            $logdata = "Order Success : #".$request->input('order_id').", Customer Email sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";
            $emailstatus = orderConfirmEmail($content, "sagar@themidnight.in");
            $logdata = "Order Success : #".$request->input('order_id').", Admin Email sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";

            $logdata = $logdata . json_encode($content);
            Storage::append('order-sucess-'.$request->input('site_id').'.txt', $logdata."\n");

	        return response()->json(['status' => 200, 'data' => $logdata]);

        }

	}

	public function getNearByLocationList(Request $request){
		$locationlist= DB::table('tbl_location')
					->select('location_id', DB::raw("( 6371 * acos( cos( radians(".$request->input('latitude').") ) * cos( radians( location_latitude ) )
					* cos( radians( location_longitude ) - radians(".$request->input('longitude').") ) + sin( radians(".$request->input('latitude').") ) 
					* sin( radians( location_latitude ) ) ) ) AS distance"))
					->havingRaw("distance<=".$request->input('nearbykm'))
					->where("location_id","!=",$request->input('location_id'))
					->orderBy('distance')
					->pluck('location_id');

		return response()->json(['status' => 200, 'data' => $locationlist]);
	}
	  
	public function getPDFlink(Request $request){
		
		$data=array();
		$finaldata = array();

		$data["subject"]="Download Brochure | CREDAI365.COM";

		$Project = new Project();

		$data1 = array();
		$data1['offset'] = 0;
		$data1['limit'] = 1;
		$data1['project_id'] = $request->input("project_id");

		$Projects = $Project->getproject($data1);

		if (count($Projects) > 0) {

			foreach ($Projects as $item) {

				$projectdata = array();
				$project_id = $item->project_id;
				$item->project_id;

				//code for getting sales-executive info

				$SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
						->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
						->where('pse.status', '=', 1)
						->where('se.sales_executive_status_id', '=', 1)
						->where('pse.project_id', '=', $project_id)
						->limit(1)
						->get();
						foreach ($SalesExecutive as $item1) {
							if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
								$item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
							} else {
								if (does_url_exists(mediapath . '/' . $item1->sales_executive_photo)) {
									$item1->sales_executive_photo_path = imagedisplaypath . '/' . $item1->sales_executive_photo;
								} else {
									$item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
								}
							}
						}
	
						$item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
						
				$Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
								->where('pp.status', '=', 1)
								->where('pp.project_id', '=', $project_id)
								->get();

								$photosarray=array();
								foreach ($Projectphotos as $item2) {
									if ($item2->project_photo == NULL || $item2->project_photo == "") {
										$item2->project_photo = fixedimagepathprojectfeaturedimage;
									} else {
										if (does_url_exists(mediapath . '/' . $item2->project_photo)) {
											$item2->project_photo = imagedisplaypath . '/' . $item2->project_photo;
										} else {
											$item2->project_photo = fixedimagepathprojectfeaturedimage;
										}
									}
									array_push($photosarray,$item2);
								}
								$item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

				$Amenities = DB::table('tbl_project_amenities as pa')
						->select('a.amenities_name')
						->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
						->where('pa.status', '=', 1)
						->where('pa.project_id', '=', $project_id)
						->get();
						$amenitiesarray=array();
						foreach ($Amenities as $ame) {
							array_push($amenitiesarray,$ame->amenities_name);
						}
						$item->amenities = (count($Amenities)>0) ? $amenitiesarray : "";
						
				$Highlight = DB::table('tbl_project_highlights as ph')
						->select('h.highlight_name')
						->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
						->where('ph.status', '=', 1)
						->where('ph.project_id', '=', $project_id)
						->get();
						$highlightarray=array();
						foreach ($Highlight as $high) {
							array_push($highlightarray,$high->highlight_name);
						}
						$item->highlight = (count($Highlight)>0) ? $highlightarray : "";
						
				$Budget = DB::table('tbl_project_budget as pb')->select(DB::raw("MIN(budget.budget_min_amount) AS minbudget, MAX(budget.budget_max_amount) AS maxbudget"))
						->leftJoin('tbl_budget as budget', 'budget.budget_id', '=', 'pb.budget_id')
						->where('pb.status', '=', 1)
						->where('pb.project_id', '=', $project_id)
						->get();

				$Configurations = DB::table('tbl_project_configuration as pc')
								->leftJoin('tbl_configuration as conf', 'pc.configuration_id', '=', 'conf.configuration_id')
								->where('pc.status', '=', 1)
								->where('pc.project_id', '=', $project_id)
								->pluck('conf.configuration_name')->implode(',');

				$SizeRange = DB::table('tbl_project_size_range as psr')
								->leftJoin('tbl_size_range as sr', 'sr.size_range_id', '=', 'psr.size_range_id')
								->where('psr.status', '=', 1)
								->where('psr.project_id', '=', $project_id)
								->pluck('sr.size_range_name')->implode(',');

				if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
					$item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
				} else {
					if (does_url_exists(mediapath . '/' . $item->project_featured_img)) {
						$item->project_featured_img_path = imagedisplaypath . '/' . $item->project_featured_img;
					} else {
						$item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
					}
				}

				if ($item->project_logo == NULL || $item->project_logo == "") {
					$item->project_logo_path = fixedimagepathbuilderlogo;
				} else {
					if (does_url_exists(mediapath . '/' . $item->project_logo)) {
						$item->project_logo_path = imagedisplaypath . '/' . $item->project_logo;
					} else {
						$item->project_logo_path = fixedimagepathbuilderlogo;
					}
				}
				$item->configuration_info = ($Configurations != "") ? $Configurations : "";
				$item->size_range_info = ($SizeRange != "") ? $SizeRange : "";
				$item->budget_info = (count($Budget) > 0) ? $Budget[0] : $Budget;
				
				array_push($finaldata, $item);
			}
			
			$data['projectdata']=$finaldata;
			
			//code for generating pdf and send as attachment
			$data['brochurelink']=  brochurelink.$finaldata[0]->project_id;
			$pdf = PDF::loadView('emails.brochure', $data);
			$filename = projectbrochureprefix.$request->input("project_id").".pdf";
			Storage::delete($filename);
			Storage::put($filename, $pdf->output());
			$filename = url('/')."/storage/app/".projectbrochureprefix.$request->input("project_id").".pdf";
			return response()->json(['status' => 200, 'data' => $filename]);

		}
	}

	public function generateinvoice(Request $request){
		$order_id = 8;
		$data = array();
		$business = new EcomBusinessSetting();
		$order = new EcomOrder();
		$orderdetail = new EcomOrderDetails();
		$orderitems = new EcomOrderItems();
		$data['businessinfo'] = $business->where('site_id',1)->first();
		$data['order'] = $order->where('order_id', $order_id)->first();
		$data['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();
		$data['orderitems'] = $orderitems->where('order_id', $order_id)->get();
		$filename = generateinvoice($order_id, $data);
		$data['invoice'] = $filename;
		return response()->json(['status' => 200, 'data' => $data]);
	}

	public function testimagepath(Request $request){
		$contentdata = DB::table('tbl_page as tp')->select('tp.*')->where('tp.page_id', '=' , '12')->where('tp.status', '=' ,1)->get();
		$content = $contentdata[0]->page_body;
		$masterdata = Storage::disk('local')->get('template1.html');
		$data['master'] = str_replace('@content', $content, $masterdata);
		return view('testbinding', $data);
	}

	public function getcontent($slug){
		$contentdata = DB::table('tbl_page as tp')->select('tp.*')->where('tp.page_slug', '=' , $slug)->where('tp.status', '=' ,1)->get();
		$templatedata = DB::table('tbl_template as tt')->select('tt.*')->where('tt.template_id', '=' , $contentdata[0]->template_id)->where('tt.status', '=' ,1)->get();
		$content = $contentdata[0]->page_body;
		$masterdata = Storage::disk('local')->get($templatedata[0]->template_file);
		$data = str_replace('@content', $content, $masterdata);
		//return view('testbinding', $data);*/
		return $data;
	}
}