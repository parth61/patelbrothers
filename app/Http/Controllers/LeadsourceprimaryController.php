<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Leadsourceprimary;

class LeadsourceprimaryController extends Controller
{
    

     public function addleadsourceprimary(Request $request) {
        //validations
        $leadsourceprimary = new Leadsourceprimary();
        $valid = Validator::make($request->all(), [
                    "lead_source_primary_name" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $leadsourceprimary->lead_source_primary_name = $request->input('lead_source_primary_name');
            $leadsourceprimary->created_by = $request->input('created_by');
            $leadsourceprimary->updated_by = $request->input('updated_by');
            $leadsourceprimary->status = $request->input('status');
            $leadsourceprimary->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $leadsourceprimary->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $leadsourceprimary->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $leadsourceprimary->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $leadsourceprimary->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadsourceprimary added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallleadsourceprimary(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadsourceprimary = new Leadsourceprimary();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $leadsourceprimarydata = $leadsourceprimary->getleadsourceprimary($data);
            
            return response()->json(['status' => 200, 'count' => count($leadsourceprimarydata), 'data' => $leadsourceprimarydata]);
        }
    }
    public function updateleadsourceprimary(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_source_primary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $leadsourceprimary = new Leadsourceprimary();
            $newrequest = $request->except(['lead_source_primary_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $leadsourceprimary->where('lead_source_primary_id', $request->input('lead_source_primary_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadsourceprimary Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteleadsourceprimary(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_source_primary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadsourceprimary = new Leadsourceprimary();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['lead_source_primary_id']);
            $result = $leadsourceprimary->where('lead_source_primary_id', $request->input('lead_source_primary_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
