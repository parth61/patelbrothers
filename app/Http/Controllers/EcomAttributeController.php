<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Str;
use App\EcomBusinessSetting;
use App\EcomAttribute;
use DB;

class EcomAttributeController extends Controller
{
    public function addattribute(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "attribute_name" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attributes = new EcomAttribute();
            $attributes->attribute_name = $request->input('attribute_name');

            //code for checking if slug is not passed than take name
            if ($request->has('attribute_slug') && $request->input('attribute_slug') != "")
            {
                $slug_name =$request->input('attribute_slug');
            }
            else
            {
                $slug_name =$request->input('attribute_name');
            }

            //checking slug avaialbility (this function checks for unique slug which is defined in helper)
            $slug_name=checkslugavailability($slug_name, "productattribute",$request->input('site_id'));
          

            //code for adding slug
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="productattribute";
            $slug->site_id =$request->input('site_id');
            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $attributes->slug_id = $slug_id;*/
            $attributes->slug_name = $slug_name;
            $attributes->display_layout = $request->input('display_layout');
            $attributes->is_searchable = $request->input('is_searchable');
            $attributes->is_comparable = $request->input('is_comparable');
            $attributes->is_use_in_product_listing = $request->input('is_use_in_product_listing');
            $attributes->created_by = $request->input('user_id');
            $attributes->site_id = $request->input('site_id');
            $attributes->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $attributes->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $attributes->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $attributes->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $attributes->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Attribute added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallattribute(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attribute = new EcomAttribute();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('attribute_id') && $request->input('attribute_id') != "") {
                $data['attribute_id'] = $request->input('attribute_id');
            }
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $attribute = $attribute->getattribute($data);
            $attributedata = array();
            foreach($attribute as $item)
            {
                $attribute_id = $item->attribute_id;
                
                //get attribute term  name related to attribute
                $attributetermnamelist = DB::table('tbl_ecom_attribute_term as at')
                ->where('at.status', '=', 1)
                ->where('at.attribute_id', '=', $attribute_id)
                ->orderBy('at.attribute_term_id', 'ASC')
                ->pluck('at.attribute_term');
                
                //get attribute term  name related to attribute
                $attributetermidlist = DB::table('tbl_ecom_attribute_term as at')
                ->where('at.status', '=', 1)
                ->where('at.attribute_id', '=', $attribute_id)
                ->orderBy('at.attribute_term_id', 'ASC')
                ->pluck('at.attribute_term_id');

                $item->attributetermidlist= $attributetermidlist;
                $item->attributetermnamelist = $attributetermnamelist;
               
                array_push($attributedata, $item);
            }
            return response()->json(['status' => 200, 'count' => count($attribute), 'data' => $attributedata]);
        }

    }

    public function updateattribute(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "attribute_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attribute = new EcomAttribute();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['slug_id','attribute_id','accesstoken','user_id','slug_name']);

            if($request->has('slug_name')){
                $attribute = $attribute->where('attribute_id', $request->input('attribute_id'))->first();
                if($attribute->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "productattribute", $request->input('site_id'));
                }
            }

            $result = $attribute->where('attribute_id', $request->input('attribute_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Attribute updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteattribute(Request $request) {

        $valid = Validator::make($request->all(), [
                    "attribute_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attribute = new EcomAttribute();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['attribute_id','accesstoken','user_id','site_id']);
            $result = $attribute->where('attribute_id', $request->input('attribute_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

    public function getallattributewithcount(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $attribute = new EcomAttribute();
            $businesssettings = new EcomBusinessSetting();

            $data = array();
            $data['offset'] = 0;
            $data['limit'] = 1;
            $data['site_id'] = $request->input('site_id');
            $businessdata = $businesssettings->getbusinesssetting($data);

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if (count($businessdata)>0) {
                $data['is_variation'] = $businessdata[0]->is_show_variation;
            }

            if ($request->has('attribute_id') && $request->input('attribute_id') != "") {
                $data['attribute_id'] = $request->input('attribute_id');
            }
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
             if ($request->has('has_variation') ) {
                $data['has_variation'] = $request->input('has_variation');
            }
            
            $attributedata = $attribute->getattributewithcount($data);
            
            return response()->json(['status' => 200, 'count' => count($attributedata), 'data' => $attributedata]);
        }

    }

}
