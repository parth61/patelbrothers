<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Slider;
use DB;

class SliderController extends Controller
{
    public function addslider(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "slider_name" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slidermodel = new Slider();
            $slidermodel->slider_name = $request->input('slider_name');
            $slidermodel->site_id = $request->input('site_id');
            $slidermodel->created_by = $request->input('user_id');
            $slidermodel->updated_by = $request->input('user_id');
            $slidermodel->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $slidermodel->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $slidermodel->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $slidermodel->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $slidermodel->save();

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Slider Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }

    public function getallslider(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('slider_id') && $request->input('slider_id') != "") {
                $data['slider_id'] = $request->input('slider_id');
            }
            if ($request->has('site_id') && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            $slidermodel = new Slider();
            $sliderdata = $slidermodel->getsliders($data);
            
            return response()->json(['status' => 200, 'data' => $sliderdata]);

        }
    }
    
    public function updateslider(Request $request) {

        $valid = Validator::make($request->all(), [
            "slider_id" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slidermodel = new Slider();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['slider_id','site_id','accesstoken','user_id']);
            $result = $slidermodel->where('slider_id', $request->input('slider_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Slider Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }

    public function deleteslider(Request $request) {

        $valid = Validator::make($request->all(), [
            "slider_id" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slidermodel = new Slider();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['slider_id','site_id','accesstoken','user_id']);
            $newrequest['status'] = 0;
            $result = $slidermodel->where('slider_id', $request->input('slider_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Slider Deleted succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }
}
