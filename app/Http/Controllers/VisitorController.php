<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Visitors;

class VisitorController extends Controller
{
    
    public function addVisitor(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "visitor_tag" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $visitors = new Visitors();
            $visitordata = $visitors->where('visitor_tag', $request->input('visitor_tag'))->count();

            if($visitordata == 0){
                $visitors = new Visitors();
                $visitors->visitor_tag = $request->input('visitor_tag');
                $visitors->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $visitors->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $visitors->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $visitors->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $visitors->save();
            }
            
            return response()->json(['status' => 200, 'data' => 'Visitor tag registered.']);
        }
    }

    //Get all admin code
    public function getAllVisitors(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $visitors = new Visitors();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if($request->has('visitor_tag') && $request->filled('visitor_tag')){
                $data['visitor_tag'] = $request->input('visitor_tag');
            }

            $visitorsdata = $visitors->getvisitors($data);
            
            return response()->json(['status' => 200, 'count' => count($visitorsdata), 'data' => $visitorsdata]);
        }
    }
}
