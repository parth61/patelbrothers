<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Location;
use App\LocationPincode;
use DB;
use Illuminate\Support\Facades\File;
class LocationController extends Controller {

    public function addlocation(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "location_name" => "required",
                    "location_slug" => "required",
                    "created_by" => "required",
                    "updated_by" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $location = new Location();
           $imgpath="";
           /*
            if ($request->hasFile('location_featured_image')) {
                    
                $files = $request->file('location_featured_image');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileupload($filename, $files);
               // return response()->json(['status' => 200, 'data' => $imgpath]);
                }  */
                
            $location_base64= $request->input('location_featured_image');
            if($location_base64!="" && $location_base64!=null && $location_base64!="null")
            {
                $imgpath=fileupload($location_base64);
            }
           
            $location->location_name = $request->input('location_name');
            $location->location_slug = $request->input('location_slug');
            $location->location_description = $request->input('location_description');
            $location->location_latitude = $request->input('location_latitude');
            $location->location_longitude = $request->input('location_longitude');
            $location->location_featured_image = $imgpath;
            $location->created_by = $request->input('created_by');
            $location->updated_by = $request->input('updated_by');
            $location->status = $request->input('status');
            $location->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $location->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $location->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $location->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $location->save();
            if ($result) {
                   if ($request->has('pincodelist')) {
                        $pincodelist = $request->input('pincodelist');
                        if (count($pincodelist) > 0) {
                            for ($i = 0; $i < count($pincodelist); $i++) {
                                $LocationPincode = new LocationPincode();
                                $checkcount = count($LocationPincode->where('pincode_id', $pincodelist[$i])->where('location_id', $location->id)->where('status', '1')->get());
                                if ($checkcount === 0) {
                                    $LocationPincode->pincode_id = $pincodelist[$i];
                                    $LocationPincode->location_id = $location->id;
                                    $LocationPincode->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $LocationPincode->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $LocationPincode->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $LocationPincode->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                    $LocationPincode->save();
                                }
                            }
                        }
                    }  
               
                return response()->json(['status' => 200, 'data' => $location->id]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalllocation(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $location = new Location();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
           
            $locationdata = $location->getlocation($data);
			return response()->json(['status' => 200, 'count' => count($locationdata), 'data' => $locationdata]);
            /*if (count($location)>0) {
                $locationdata = array();
                foreach ($location as $item) {
                    if($item->location_featured_image==NULL|| $item->location_featured_image=="")
                    {
                        $item->featuredimagepath = fixedimagepathlocation;
                    }
                    else
                    {
                    if (does_url_exists(mediapath.$item->location_featured_image)) {
                        $item->featuredimagepath = imagedisplaypath.$item->location_featured_image;
                    } else {
                        $item->featuredimagepath = fixedimagepathlocation;
                    }
                    }
                    array_push($locationdata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($location), 'data' => $location]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }*/
           
        }
    }
	
	public function getlocationwhichhasproject(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $location = new Location();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
           
            $location = $location->getlocationwhichhasproject($data);
            if ($location) {
                return response()->json(['status' => 200, 'count' => count($location), 'data' => $location]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
        }
    }
	
    public function getalllocationwithhighestproject(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $location = new Location();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
           
            $location = $location->getlocationwithhighestproject($data);
            if ($location) {
                $locationdata = array();
                foreach ($location as $item) {
                    if($item->location_featured_image==NULL|| $item->location_featured_image=="")
                    {
                        $item->featuredimagepath = fixedimagepathlocation;
                    }
                    else
                    {
                    if (does_url_exists(mediapath.$item->location_featured_image)) {
                        $item->featuredimagepath = imagedisplaypath.$item->location_featured_image;
                    } else {
                        $item->featuredimagepath = fixedimagepathlocation;
                    }
                    }
                    array_push($locationdata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($location), 'data' => $locationdata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
        }
    }
    public function getlocationbyid($location_id) {
        $data=array();
        $query = DB::table('tbl_location')->select('*')->where('location_id',$location_id)->get();
        $locationdata = array();
        foreach ($query as $item) {
            if($item->location_featured_image==NULL|| $item->location_featured_image=="")
            {
                $item->featuredpath = fixedimagepathlocation;
            }
            else
            {
            if (does_url_exists(mediapath.$item->location_featured_image)) {
                $item->featuredpath = imagedisplaypath.$item->location_featured_image;
            } else {
                $item->featuredpath = fixedimagepathlocation;
            }
            }
            array_push($locationdata, $item);
        }
         $data['locationdata'] = $locationdata;     
         return view('editlocation', $data);
    }

    public function updatelocation(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "location_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $location = new Location();
            $imgpath = null;
            $location_id= $request->input('location_id');
            $location_base64= $request->input('location_featured_image');
            $location_description= $request->input('location_description');
            if($location_base64!="" && $location_base64!=null && $location_base64!="null")
            {
                $imgpath=fileupload($location_base64);
                $query = DB::table('tbl_location')->select('tbl_location.location_featured_image')->where('location_id',$location_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->location_featured_image;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                            unlink($path);
                        } 
                    }
                   
                }
            }
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['location_id','location_featured_image','pincodelist']);
           
            if($location_base64!="" && $location_base64!=null)
            {
				$newrequest['location_featured_image'] = $imgpath;
		    }
            $result = $location->where('location_id',$location_id )->update($newrequest);

            if ($result) {
                
                if ($request->has('pincodelist') ) {
                    $pincodelist = json_decode($request->input('pincodelist'));
                    if (count($pincodelist) > 0) {
                        $LocationPincode = new LocationPincode();
                        $LocationPincode->where('location_id', $location_id)->whereNotIn('pincode_id', $pincodelist)->delete();
                        for ($i = 0; $i < count($pincodelist); $i++) {
                            $checkcount = count($LocationPincode->where('pincode_id', $pincodelist[$i])->where('location_id', $location_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $LocationPincode = new LocationPincode();
                                $LocationPincode->pincode_id = $pincodelist[$i];
                                $LocationPincode->location_id = $location_id;
                                $LocationPincode->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $LocationPincode->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $LocationPincode->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $LocationPincode->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $LocationPincode->save();
                            }
                        }
                    }
					else
					{
                        $LocationPincode = new LocationPincode();
						$LocationPincode->where('location_id', $location_id)->delete();	   
                    }  
                }
                return response()->json(['status' => 200, 'data' => "Location Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => $result], 400);
            }
        }
    }


    public function deletelocation(Request $request) {
        $valid = Validator::make($request->all(), [
                    "location_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $location = new Location();
            $location_id= $request->input('location_id');
            $query = DB::table('tbl_location')->select('tbl_location.location_featured_image')->where('location_id',$location_id)->get();
            if(count($query)>0)
            {
            $img=$query[0]->location_featured_image;
            if($img!=NULL|| $img!="")
            {
                if (does_url_exists(mediapath.$img)) {
                    $path = mediapath.$img;
                    unlink($path);
                } 
            }
            }
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['location_id']);
            $result = $location->where('location_id',$location_id)->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}

