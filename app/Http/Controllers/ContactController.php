<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Contact;
use App\EcomBusinessSetting;
class ContactController extends Controller
{
    public function addcontact(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "first_name" => "required",
            "last_name" => "required",
            "email" => "required",
            "mobile" => "required|numeric",
            "site_id"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $contact = new Contact();
            $contact->first_name = $request->has('first_name')?$request->input('first_name'):null;
            $contact->last_name = $request->has('last_name')?$request->input('last_name'):null;
            $contact->email = $request->has('email')?$request->input('email'):null;
            $contact->mobile = $request->has('mobile')?$request->input('mobile'):null;
            $contact->countrycode = $request->has('countrycode')?$request->input('countrycode'):null;
            $contact->countryiso = $request->has('countryiso')?$request->input('countryiso'):null;
            $contact->subject = $request->has('subject')?$request->input('subject'):null;
            $contact->message = $request->has('message')?$request->input('message'):null;
            $contact->site_id = $request->has('site_id')?$request->input('site_id'):null;
            $contact->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $contact->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $contact->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $contact->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $contact->type = $request->has('type')?$request->input('type'):null;
            $result = $contact->save();
            if ($result) {
                $data = array();
                $data['first_name'] = $request->has('first_name')?$request->input('first_name'):null;
                $data['last_name'] = $request->has('last_name')?$request->input('last_name'):null;
                $data['email'] = $request->has('email')?$request->input('email'):null;
                $data['mobile'] = $request->has('mobile')?$request->input('mobile'):null;
                $data['countrycode'] = $request->has('countrycode')?$request->input('countrycode'):null;
                $data['subject'] = $request->has('subject')?$request->input('subject'):null;
                $data['messagedata'] = $request->has('message')?$request->input('message'):"NA";
                $businessinfo = new EcomBusinessSetting();
                $data["businessinfo"] = $businessinfo->where('site_id', $request->input('site_id'))->first();
                $emailstatus = sendContactInquiry($data, $request->input('email'), $data["businessinfo"]->business_name);
                if($emailstatus){
                     return response()->json(['status' => 200, 'data' => "Contact added sucessfully",'emailstatus'=>$emailstatus]);
                }else{
                     return response()->json(['status' => 200, 'data' => "Contact added sucessfully",'emailstatus'=>$emailstatus]); 
                }
               
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallcontact(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $contact = new Contact();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('contact_id') && $request->input('contact_id') != "") {
                $data['contact_id'] = $request->input('contact_id');
            }
            if ($request->has('type') && $request->input('type') != "") {
                $data['type'] = $request->input('type');
            }

            $contact = $contact->getcontact($data);

            return response()->json(['status' => 200, 'count' => count($contact), 'data' => $contact]);
        }
    }

    public function updatecontact(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "contact_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $contact = new Contact();
            $newrequest = $request->except(['contact_id']);
            $result = $contact->where('contact_id', $request->input('contact_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Contact updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletecontact(Request $request) {
        $valid = Validator::make($request->all(), [
                    "contact_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $contact = new Contact();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['contact_id']);
            $result = $contact->where('contact_id', $request->input('contact_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
