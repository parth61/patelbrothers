<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\ProjectHighlight;
use DB;
class ProjectHighlightController extends Controller
{
    public function addprojecthighlight(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
                    "highlight_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectHighlight = new ProjectHighlight();
            $ProjectHighlight->project_id = $request->input('project_id');
            $ProjectHighlight->highlight_id = $request->input('highlight_id');
            $ProjectHighlight->project_highlight_alias = $request->input('project_highlight_alias');
            $ProjectHighlight->created_by = $request->input('created_by');
            $ProjectHighlight->updated_by = $request->input('updated_by');
            $ProjectHighlight->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ProjectHighlight->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ProjectHighlight->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ProjectHighlight->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ProjectHighlight->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Highlight Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function getallprojecthighlight(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectHighlight = new ProjectHighlight();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('project_highlights_id') && $request->input('project_highlights_id') != "") {
                $data['project_highlights_id'] = $request->input('project_highlights_id');
            }
            if ($request->has('highlight_id') && $request->input('highlight_id') != "") {
                $data['highlight_id'] = $request->input('highlight_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $ProjectHighlight = $ProjectHighlight->getprojecthighlight($data);
            return response()->json(['status' => 200, 'count' => count($ProjectHighlight), 'data' => $ProjectHighlight]);
        }
    }
    
    public function updateprojecthighlight(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_highlights_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectHighlight = new ProjectHighlight();
            $newrequest = $request->except(['project_highlights_id']);
            $result = $ProjectHighlight->where('project_highlights_id', $request->input('project_highlights_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Highlight Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function deleteprojecthighlight(Request $request) {
        $valid = Validator::make($request->all(), [
            "project_highlights_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectHighlight = new ProjectHighlight();
            $result = $ProjectHighlight->where('project_highlights_id', $request->input('project_highlights_id'))->delete();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
