<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\TrackingCode;
use DB;

class TrackingCodeController extends Controller
{

    public function getAllTrackingCode(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $trackingcodemodel = new TrackingCode();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('tracking_code_id') && $request->input('tracking_code_id') != "") {
                $data['tracking_code_id'] = $request->input('tracking_code_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $trackingcodedata = $trackingcodemodel->gettrackingcode($data);
            if($trackingcodedata)
            {
                return response()->json(['status' => 200, 'count' => count($trackingcodedata), 'data' => $trackingcodedata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function manageTrackingCode(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $trackingcodemodel = new TrackingCode();
            $trackingcodecount = $trackingcodemodel->where('site_id', '=', $request->input('site_id'))->where('status', '=', 1)->count();

            if($trackingcodecount > 0)
            {
                $request->request->add(['updated_by' =>  $request->input('user_id')]);
                $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                $newrequest = $request->except(['tracking_code_id','user_id','accesstoken','site_id']);
                $result = $trackingcodemodel->where('tracking_code_id', $request->input('tracking_code_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Tracking codes updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
            else
            {
                $trackingcodemodel->tracking_googleanalytics = $request->has('tracking_googleanalytics')?$request->input('tracking_googleanalytics'):null;
                $trackingcodemodel->tracking_facebook_pixelcode = $request->has('tracking_facebook_pixelcode')?$request->input('tracking_facebook_pixelcode'):null;
                $trackingcodemodel->tracking_googleadwords = $request->has('tracking_googleadwords')?$request->input('tracking_googleadwords'):null;
                $trackingcodemodel->tracking_googleadwords_label_phone = $request->has('tracking_googleadwords_label_phone')?$request->input('tracking_googleadwords_label_phone'):null;
                $trackingcodemodel->tracking_googleadwords_label_conversation = $request->has('tracking_googleadwords_label_conversation')?$request->input('tracking_googleadwords_label_conversation'):null;
                $trackingcodemodel->tracking_facebook_pixelid = $request->has('tracking_facebook_pixelid')?$request->input('tracking_facebook_pixelid'):null;
                $trackingcodemodel->tracking_taboolaid = $request->has('tracking_taboolaid')?$request->input('tracking_taboolaid'):null;
                $trackingcodemodel->tracking_colombia_pixel = $request->has('tracking_colombia_pixel')?$request->input('tracking_colombia_pixel'):null;
                $trackingcodemodel->tracking_colombia_pixel_conversation = $request->input('tracking_colombia_pixel_conversation')?$request->input('tracking_colombia_pixel_conversation'):null;
                $trackingcodemodel->tracking_colombia_pixel_phoneconversation = $request->input('tracking_colombia_pixel_phoneconversation')?$request->input('tracking_colombia_pixel_phoneconversation'):null;
                $trackingcodemodel->site_id = $request->input('site_id'); 
                $trackingcodemodel->updated_by = $request->input('user_id');
                $trackingcodemodel->created_by = $request->input('user_id');  
                $trackingcodemodel->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $trackingcodemodel->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $trackingcodemodel->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $trackingcodemodel->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;         
                $result = $trackingcodemodel->save();

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Tracking codes Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
       
    }
    }
}
