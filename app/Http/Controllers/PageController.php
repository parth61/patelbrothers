<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Page;
use App\Slug;
use Illuminate\Support\Facades\File;
class PageController extends Controller
{
    public function addpage(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "page_title"=>"required",
            
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $page = new Page();
            $page->page_title = $request->input('page_title');
            if ($request->has('page_slug') && $request->input('page_slug') != "")
            {
                $slug_name =$request->input('page_slug');
            }
            else
            {
                $slug_name =$request->input('page_title');
            }

            $slug_name=checkslugavailability($slug_name, "page",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="page";

            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $page->slug_id = $slug_id;*/
            $page->slug_name = $slug_name;
            $page->page_body = $request->input('page_body');
            $page->page_featured_img = $request->input('page_featured_img');
            $page->template_id = $request->input('template_id');
            $page->status = $request->input('status');
            $page->isvisible = $request->input('isvisible');
            $page->created_by = $request->input('created_by');
            $page->updated_by = $request->input('updated_by');
            $page->site_id = $request->input('site_id');
            
            $page->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $page->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $page->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $page->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $page->save();
            $pagecategorydata = $page->latest()->first();
            if ($result) {
                return response()->json(['status' => 200, 'message' => "page Added Sucessfully",'data'=>$pagecategorydata]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallpage(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $page = new Page();
            $data = array();
            
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            if ($request->has('page_id') && $request->input('page_id') != "") {
                $data['page_id'] = $request->input('page_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $page = $page->getpage($data);
            if($page)
            {
                $pagedata = array();
                foreach ($page as $item) {
                    /* $file = imagedisplaypath.$item->page_media_file;
                    $file_headers = @get_headers($file);
                    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $item->page_featured_img_path = fixedpagefeaturedimage;
                    }
                    else {
                        $item->page_featured_img_path = imagedisplaypath.$item->page_media_file;
                    }

                    $file1 = imagedisplaypath.$item->seo_media_file;
                    $file_headers1 = @get_headers($file1);
                    if(!$file_headers1 || $file_headers1[0] == 'HTTP/1.1 404 Not Found') {
                       
                        $item->seo_img_path = fixedseofeaturedimage;
                    } else {
                        $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                    }

                    $file2 = imagedisplaypath.$item->template_file;
                    $file_headers2 = @get_headers($file);
                    if(!$file_headers2 || $file_headers2[0] == 'HTTP/1.1 404 Not Found') {
                        $item->template_path = fixedtemplatefile;
                    }
                    else {
                        $item->template_path = imagedisplaypath.$item->template_file;
                    } */

                    if(fileuploadtype=="local")
                    {
                        //for local

                        //page
                        if($item->page_media_file==""||$item->page_media_file==null||$item->page_media_file=="null")
                        {
                            $item->page_featured_img_path = fixedpagefeaturedimage;
                        }
                        else
                        {
                            if (File::exists(baseimagedisplaypath .$item->page_media_file)) {
                                $item->page_featured_img_path = imagedisplaypath.$item->page_media_file;
                            } else {
                                $item->page_featured_img_path = fixedpagefeaturedimage;
                            }
                        }
                       
                        if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null")
                        {
                            $item->seo_img_path = fixedseofeaturedimage;
                        }
                        else
                        {
                             //code for seo image
                            if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            }
                        }

                        if($item->template_file==""||$item->template_file==null||$item->template_file=="null")
                        {
                            $item->template_path = fixedtemplatefile;
                        }
                        else
                        {
                                 //code for seo image
                                if (File::exists(baseimagedisplaypath .$item->template_file)) {
                                    $item->template_path = imagedisplaypath.$item->template_file;
                                } else {
                                    $item->template_path = fixedtemplatefile;
                                }
                         }

                    }
                    else
                    {
                            //page featured 
                            if (does_url_exists(imagedisplaypath.$item->page_media_file)) {
                                $item->page_featured_img_path = imagedisplaypath.$item->page_media_file;
                            } else {
                                $item->page_featured_img_path = fixedpagefeaturedimage;
                            }
                        
                            //seo image
                            if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            } 

                            //template
                            if (does_url_exists(imagedisplaypath.$item->template_file)) {
                                $item->template_path = imagedisplaypath.$item->template_file;
                            } else {
                                $item->template_path = fixedtemplatefile;
                            } 
                    }


                    array_push($pagedata, $item);
                }

                
                return response()->json(['status' => 200, 'count' => count($page), 'data' => $pagedata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updatepage(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "page_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $page = new Page();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['page_id','user_id','accesstoken','slug_id','slug_name']);

            if($request->has('slug_name')){
                $page = $page->where('page_id', $request->input('page_id'))->first();
                if($page->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "page", $request->input('site_id'));
                }
            }

            $result = $page->where('page_id', $request->input('page_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "page Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletepage(Request $request) {

        $valid = Validator::make($request->all(), [
                    "page_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $page = new Page();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['page_id','user_id','accesstoken']);
            $result = $page->where('page_id', $request->input('page_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
