<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Video;
use App\Slug;

use Illuminate\Support\Facades\File;

class VideoController extends Controller
{
    public function addvideo(Request $request) {
        //validations
        
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "video_title" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $video = new Video();
            $video->video_title = $request->input('video_title');
         /*    if ($request->has('video_slug') && $request->input('video_slug') != "")
            {
                $slug_name =$request->input('video_slug');
            }
            else
            {
                $slug_name =$request->input('video_title');
            }

            $slug_name = checkslugavailability($slug_name, "video",$request->input('site_id'));
          
            $slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="video";

            $slug->created_by = $request->input('user_id');

            $slug->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $slug->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $slug->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $slug->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            } */
           
            // $video->slug_name = $slug_name;
            $video->video_body = $request->input('video_body');
            $video->video_featured_img = $request->input('video_featured_img');
            $video->status = $request->input('status');
            $video->isvisible = $request->input('isvisible');
            $video->site_id = $request->input('site_id');
            $video->created_by = $request->input('created_by');
            $video->updated_by = $request->input('created_by');
            $video->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $video->video_link = $request->has('video_link')?$request->input('video_link'):null;
            $video->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $video->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $video->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $video->video_guid = generateAccessToken(20); 
            $result = $video->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "video Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallvideo(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $video = new Video();
            $data = array();
           
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
            if ($request->has('order_by') && $request->filled('order_by')) {
                $data["order_by"] = $request->input("order_by");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('video_id') && $request->input('video_id') != "") {
                $data['video_id'] = $request->input('video_id');
            }
            if ($request->has('video_guid') && $request->input('video_guid') != "") {
                $data['video_guid'] = $request->input('video_guid');
            }
            

            /* if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
           */
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            // if ($request->has('sortby') && $request->input('sortby') != "") {
            //     $data['sortby'] = $request->input('sortby');
            // }
            // if ($request->has('sorttype') && $request->input('sorttype') != "") {
            //     $data['sorttype'] = $request->input('sorttype');
            // }
            
            $video = $video->getvideo($data);
            if($video)
            {
                $videodata = array();
                foreach ($video as $item) {
                    if(fileuploadtype=="local")
                    {
                        if($item->video_media_file==""||$item->video_media_file==null||$item->video_media_file=="null")
                        {
                            $item->video_featured_img_path = fixedpagefeaturedimage;
                        }
                        else
                        {
                            //code for video featured image
                            if (File::exists(baseimagedisplaypath .$item->video_media_file)) {
                                $item->video_featured_img_path = imagedisplaypath.$item->video_media_file;
                            } else {
                                $item->video_featured_img_path = fixedpagefeaturedimage;
                            }
                        }
                        array_push($videodata, $item);  
                    }
                    else
                    {

                         //for s3

                             //code for video featured image
                            if (does_url_exists(imagedisplaypath.$item->video_media_file)) {
                                $item->video_featured_img_path = imagedisplaypath.$item->video_media_file;
                            } else {
                                $item->video_featured_img_path = fixedpagefeaturedimage;
                            }

                            
                            array_push($videodata, $item);  
                      }
                    //get comma separated videocategory id list
                 
                }
                return response()->json(['status' => 200, 'count' => count($video), 'data' => $videodata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
            
        }

    }
  

    public function updatevideo(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "video_id" => "required",
                 "site_id" => "required",
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $video = new Video();
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['video_id','slug_id','user_id','accesstoken','videotaglist','videocategorylist','video_slug']);
       
            $result = $video->where('video_id', $request->input('video_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "video Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletevideo(Request $request) {

        $valid = Validator::make($request->all(), [
                    "video_id" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $video = new Video();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['video_id','user_id','accesstoken']);
            $result = $video->where('video_id', $request->input('video_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
