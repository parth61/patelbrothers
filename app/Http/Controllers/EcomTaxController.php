<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomTax;
class EcomTaxController extends Controller
{
    public function addtax(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",
            "tax_class_id" => "required",
            "tax_name"=>"required",
            "tax_percentage"=>"required",
            "tax_priority"=>"required",
            "tax_country"=>"required",
            "tax_state"=>"required",
            "tax_city"=>"required",
            "tax_zipcode"=>"required",
            "is_applied_to_shipping"=>"required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tax = new EcomTax();
            $tax->tax_class_id  = $request->input('tax_class_id');
            $tax->tax_name  = $request->input('tax_name');
            $tax->tax_percentage  = $request->input('tax_percentage');
            $tax->tax_priority  = $request->input('tax_priority');
            $tax->tax_country  = $request->input('tax_country');
            $tax->tax_state  = $request->input('tax_state');
            $tax->tax_city  = $request->input('tax_city');
            $tax->tax_zipcode  = $request->input('tax_zipcode');
            $tax->is_applied_to_shipping  = $request->input('is_applied_to_shipping');
            $tax->created_by = $request->input('user_id');
            $tax->updated_by = $request->input('user_id');
            $tax->site_id = $request->input('site_id');
            $tax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $tax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $tax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $tax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $tax->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Tax Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalltax(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tax = new EcomTax();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('tax_id') && $request->input('tax_id') != "") {
                $data['tax_id'] = $request->input('tax_id');
            }
            if ($request->has('tax_class_id') && $request->input('tax_class_id') != "") {
                $data['tax_class_id '] = $request->input('tax_class_id ');
            }
            if ($request->has('tax_country') && $request->input('tax_country') != "") {
                $data['tax_country '] = $request->input('tax_country ');
            }
            if ($request->has('tax_state') && $request->input('tax_state') != "") {
                $data['tax_state '] = $request->input('tax_state');
            }
            if ($request->has('tax_city') && $request->input('tax_city') != "") {
                $data['tax_city '] = $request->input('tax_city ');
            }
            if ($request->has('tax_zipcode') && $request->input('tax_zipcode') != "") {
                $data['tax_zipcode '] = $request->input('tax_zipcode');
            }
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            $tax = $tax->gettax($data);

            return response()->json(['status' => 200, 'count' => count($tax), 'data' => $tax]);
        }

    }

    public function updatetax(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "tax_id" => "required|numeric",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tax = new EcomTax();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tax_id','user_id','accesstoken']);
            $result = $tax->where('tax_id', $request->input('tax_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Tax Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletetax(Request $request) {

        $valid = Validator::make($request->all(), [
                    "tax_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tax = new EcomTax();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tax_id','user_id','accesstoken']);
            $result = $tax->where('tax_id', $request->input('tax_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
