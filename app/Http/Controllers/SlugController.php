<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Str;
use DB;
class SlugController extends Controller
{
    public function addslug(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "slug_type"=>"required",
            "slug_name"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $slug = new slug();
            $slug->slug_type = $request->input('slug_type');
            $slug->slug_name = checkslugavailability($request->input('slug_name'), $request->input('slug_type'),$request->input('site_id'));
            $slug->created_by = $request->input('user_id');
            $slug->site_id = $request->input('site_id');
            $slug->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $slug->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $slug->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $slug->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $slug->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "slug Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallslug(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $slug = new slug();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('slug_id') && $request->input('slug_id') != "") {
                $data['slug_id'] = $request->input('slug_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $slug = $slug->getslug($data);
            if($slug)
            {
                
                return response()->json(['status' => 200, 'count' => count($slug), 'data' => $slug]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updateslugname(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "slug_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slug = new slug();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);

            $newrequest = $request->except(['id','user_id','accesstoken','slug_name']);

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $slug_name=$request->input('slug_name');
                $slug_id=$request->input('slug_id');
                $slug_name= Str::slug($slug_name);
                $slugcount = DB::table('tbl_slug as slug')
                ->where('slug.status', '=', 1)
                ->where('slug.slug_name', '=',Str::slug($slug_name))
                ->where('slug.slug_id', '!=' ,$slug_id)
                ->get();
          
                if(count($slugcount)==0)
                {
                  $slug_name=$slug_name;
                }
                else
                {
                  up:
                  $slug_name1=$slug_name."-".getthreestring();
                  $slugcounter = DB::table('tbl_slug as slug')
                  ->where('slug.status', '=', 1)
                  ->where('slug.slug_name', '=',Str::slug($slug_name1))
                  ->where('slug.slug_id', '!=' ,$slug_id)
                  ->get();
                  if(count($slugcounter)==0)
                  {
                    $slug_name=$slug_name1;
                  }
                  else
                  {
                      goto up;
                  }
                }
                $newrequest['slug_name']= $slug_name;
            }

            $result = $slug->where('slug_id', $request->input('slug_id'))->update($newrequest);

            if ($result) {
                $data = array();
                $data['offset'] = 0;
                $data['limit'] = 1;
                $data['slug_id']=$request->input('slug_id');
                $slug = $slug->getslug($data);
                return response()->json(['status' => 200, 'data' => $slug[0]->slug_name]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }


    public function updateslug(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "id" => "required",
            "slug_type" => "required",
            "slug_name" => "required",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slug_type = $request->input('slug_type');

            $table_name = ""; $id_key = "";
            if($slug_type == "productattribute"){ $table_name = "tbl_ecom_attribute"; $id_key = "attribute_id"; }
            if($slug_type == "productattributeterm"){ $table_name = "tbl_ecom_attribute_term"; $id_key = "attribute_term_id"; }
            if($slug_type == "productcategory"){ $table_name = "tbl_ecom_category"; $id_key = "category_id"; }
            if($slug_type == "product"){ $table_name = "tbl_ecom_product"; $id_key = "product_id"; }
            if($slug_type == "productshippingclass"){ $table_name = "tbl_ecom_shipping_class"; $id_key = "shipping_class_id"; }
            if($slug_type == "producttag"){ $table_name = "tbl_ecom_tag"; $id_key = "tag_id"; }
            if($slug_type == "page"){ $table_name = "tbl_page"; $id_key = "page_id"; }
            if($slug_type == "postcategory"){ $table_name = "tbl_postcategory"; $id_key = "postcategory_id"; }
            if($slug_type == "post"){ $table_name = "tbl_post"; $id_key = "post_id"; }
            if($slug_type == "posttag"){ $table_name = "tbl_posttag"; $id_key = "posttag_id"; }

            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);

            $slug_name = $request->input('slug_name');
            $site_id = $request->input('site_id');

            $slug_name = checkslugavailability($slug_name, $slug_type, $site_id);
            
            $result = DB::table($table_name)->where($id_key, '=',$request->input('id'))->where('site_id', $site_id)->update(array("slug_name"=>$slug_name));

            if ($result) {
                return response()->json(['status' => 200, 'data' => $slug_name]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }

    public function deleteslug(Request $request) {

        $valid = Validator::make($request->all(), [
                    "slug_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $slug = new slug();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['slug_id','user_id','accesstoken']);
            $result = $slug->where('slug_id', $request->input('slug_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "SlUG Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
    public function checkslug(Request $request) {

        $valid = Validator::make($request->all(), [
            "slug_name" => "required",
            "slug_type" => "required",
            "accesstoken" => "required",
            "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slug_type = $request->input('slug_type');
            $slug_name = $request->input('slug_name');
            $site_id = $request->input('site_id');

            $slug_name = checkslugavailability($slug_name, $slug_type, $site_id);
            
            return response()->json(['status' => 200, 'data' => $slug_name]);
        }

    }

    public function getDataFromSlug(Request $request){

        $valid = Validator::make($request->all(), [
            "slug_name" => "required",
            "accesstoken" => "required",
            "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $slug = new slug();
            $slug_name=$slug->getDataFromSlug($request->input('slug_name'), $request->input('site_id'));
            return response()->json(['status' => 200, 'data' => $slug_name]);
        }

    }
    
    
}
