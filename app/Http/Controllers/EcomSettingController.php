<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomSetting;
use Carbon\Carbon;
class EcomSettingController extends Controller
{

    public function managecomsetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "settinglist"=>"required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            if ($request->has('settinglist')) {
                $settinglist = json_decode($request->input('settinglist'), TRUE);
                
                if (count($settinglist) > 0) {
                    $newrequest = array();
                    foreach ($settinglist as $item) {
                        foreach($item as $key => $value){
                            return response()->json(['status' => 200, $key => $value ]);
                            $newrequest[$key] = $value;    
                        }
                        
                        $ecomsetting = new EcomSetting();
                        $checkcount = count($ecomsetting->where('key', $key)->where('status', '1')->get());
                        if ($checkcount === 0) {
                            //add
                            $ecomsetting = new EcomSetting();
                            $ecomsetting->setting_key = $key;
                            $ecomsetting->setting_value =$value;
                            $ecomsetting->site_id =$request->input('site_id');
                            $ecomsetting->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $ecomsetting->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $ecomsetting->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $ecomsetting->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $result=$ecomsetting->save();
                            if ($result) {
                                return response()->json(['status' => 200, 'data' => "ecomsetting added succesfully"]);
                            } else {
                                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                            }
                        }
                        else
                        {
                            //update
                            $ecomsetting = new EcomSetting();
                            $result = $ecomsetting->where('setting_key', $key)->update([
                                'setting_value' => $value,
                                'updated_by'=>$request->input('user_id')
                             ]);;
                
                            if ($result) {
                                return response()->json(['status' => 200, 'data' => "ecomsetting Updated succesfully"]);
                            } else {
                                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                            }
                        }
                    }
                }
            }
           
        }
    }

    public function addecomsetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "setting_key" => "required",
            "setting_value" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomsetting = new EcomSetting();
           
            $ecomsetting->setting_key = $request->input('setting_key');
            $ecomsetting->setting_value = $request->input('setting_value');
            $ecomsetting->created_by = $request->input('user_id');
            $ecomsetting->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ecomsetting->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ecomsetting->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ecomsetting->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ecomsetting->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "ecomsetting Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallecomsetting(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomsetting = new EcomSetting();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('setting_id') && $request->input('setting_id') != "") {
                $data['setting_id'] = $request->input('setting_id');
            }
            if ($request->has('setting_value') && $request->input('setting_value') != "") {
                $data['setting_value'] = $request->input('setting_value');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('setting_key') && $request->input('setting_key') != "") {
                $data['setting_key'] = $request->input('setting_key');
            }
            $ecomsetting = $ecomsetting->getecomsetting($data);

            return response()->json(['status' => 200, 'count' => count($ecomsetting), 'data' => $ecomsetting]);
        }

    }

    public function updateecomsetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "setting_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomsetting = new EcomSetting();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['setting_id','user_id','accesstoken']);
            $result = $ecomsetting->where('setting_id', $request->input('setting_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "ecomsetting Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteecomsetting(Request $request) {

        $valid = Validator::make($request->all(), [
                    "setting_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomsetting = new EcomSetting();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['setting_id','user_id','accesstoken']);
            $result = $ecomsetting->where('setting_id', $request->input('setting_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  
}
