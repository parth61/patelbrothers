<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Module;

class ModuleController extends Controller
{
    
    public function manageModule(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "module_name" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $module = new Module();
            $moduledata = $module->where('module_id', $request->input('module_id'))->count();

            if($moduledata == 0){
                $module = new Module();
                $module->module_name = $request->input('module_name');
                $module->package_id = $request->input('package_id');
                $module->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $module->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $module->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $module->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $module->save();
            }else{
            $module = new Module();
            // $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['site_id','module_id','accesstoken','user_id']);
            $result = $module->where('module_id', $request->input('module_id'))->update($newrequest);
            }
            
            return response()->json(['status' => 200, 'data' => 'module registered.']);
        }
    }

    //Get all admin code
    public function getAllmodule(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $module = new Module();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if($request->has('module_id') && $request->filled('module_id')){
                $data['module_id'] = $request->input('module_id');
            }
            if($request->has('package_id') && $request->filled('package_id')){
                $data['package_id'] = $request->input('package_id');
            }
            $moduledata = $module->getModule($data);
            return response()->json(['status' => 200, 'count' => count($moduledata), 'data' => $moduledata]);
        }
    }
}
