<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;
use App\ProjectPhoto;
use DB;
class ProjectPhotoController extends Controller
{
    public function addprojectphoto(Request $request) {

        //validations
        $valid = Validator::make($request->all(), [
                   
                    "project_id" => "required",
                    "type" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    "project_photo" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $projectphoto = new ProjectPhoto();
            $projectphotocount = $projectphoto->where('type', $request->input('type'))->where('project_id', $request->input('project_id'))->where('status', 1)->count();

            if($projectphotocount>=5){
                return response()->json(['status' => 400, 'error' => "Already 05 photos uploaded.", 'count' => $projectphotocount], 400);
            }else{
                $projectphoto = new ProjectPhoto();
                $imgpath=null;

                //BASE 64 IMAGE UPLOAD
                $project_photo_base64= $request->input('project_photo');
                if($project_photo_base64!="" && $project_photo_base64!=null && $project_photo_base64!="null")
                {
                    //$imgpath=fileupload($project_photo_base64);
                    $responsedata = fileUploadSlim($request, "project_photo");
                    $imgpath = $responsedata['folderpath'];
                }
            
            
                $projectphoto->project_photo = $imgpath;
                $projectphoto->type = $request->input('type');
                $projectphoto->created_by = $request->input('created_by');
                $projectphoto->updated_by = $request->input('updated_by');
                $projectphoto->project_id = $request->input('project_id');
                $projectphoto->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $projectphoto->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $projectphoto->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $projectphoto->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $projectphoto->save();

                if($result) {
                    return response()->json(['status' => 200, 'data' => "Project Photo Added Sucessfully", 'count' => $projectphotocount]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong.", 'count' => $projectphotocount], 400);
                }
            }
            
        }
    }
   
    public function getallprojectphoto(Request $request) {
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectphoto = new ProjectPhoto();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('project_photo_id') && $request->input('project_photo_id') != "") {
                $data['project_photo_id'] = $request->input('project_photo_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            if ($request->has('type') && $request->input('type') != "") {
                $data['type'] = $request->input('type');
            }
            $projectphoto = $projectphoto->getprojectphoto($data);
            
            if ($projectphoto) {
                $projectphotodata = array();
                foreach ($projectphoto as $item) {
                    if (does_url_exists(mediapath.$item->project_photo)) {
                        $item->project_photo_path = imagedisplaypath.$item->project_photo;
                    } else {
                        $item->project_photo_path = fixedimagepathprojectgalleryimage;
                    }
                    array_push($projectphotodata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($projectphoto), 'data' => $projectphotodata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updateprojectphoto(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_photo_id" => "required",
                    "type" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectphoto = new ProjectPhoto();
            $imgpath = null;
            $project_photo_id= $request->input('project_photo_id');
            $type= $request->input('type');

            if($project_photo_base64!="" && $project_photo_base64!=null && $project_photo_base64!="null")
            {
                $imgpath=fileupload($project_photo_base64);
                
                $query = DB::table('tbl_project_photos')->select('tbl_project_photos.project_photo')->where('type',$type)->where('project_photo_id',$project_photo_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->project_photo;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                          
                            //unlink($path);
                        } 
                    }
                   
                }
            }
            $newrequest = $request->except(['project_photo_id','project_photo','type']);

            if($project_photo_base64!="" && $project_photo_base64!=null && $project_photo_base64!="null")
            {
				$newrequest['project_photo'] = $imgpath;
            }
            $result = $projectphoto->where('project_photo_id', $project_photo_id)->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Photos Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteprojectphoto(Request $request) {
        $valid = Validator::make($request->all(), [
                    "project_photo_id" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectphoto = new ProjectPhoto();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['project_photo_id']);
            $result = $projectphoto->where('project_photo_id', $request->input('project_photo_id'))->update($newrequest);
            if ($result) {
                $projectprojectphoto = new ProjectPhoto();
                $project_photo_id= $request->input('project_photo_id');
                $query = DB::table('tbl_project_photos')->select('tbl_project_photos.project_photo')->where('project_photo_id',$project_photo_id)->get();
                if(count($query)>0)
                {
                $img=$query[0]->project_photo;
                if($img!=NULL|| $img!="")
                {
                    if (does_url_exists(mediapath.$img)) {
                        $path = mediapath.$img;
                      //  unlink($path);
                    } 
                }
                }
				$projectprojectphoto->where('project_photo_id', $request->input('project_photo_id'))->delete();
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
