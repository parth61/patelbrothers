<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\ExotelCallLog;
use App\Lead;
use Storage;
use Carbon\Carbon;
class WhatsappController extends Controller
{
    public function userOptIn(Request $request) {

		$valid = Validator::make($request->all(), [
			"usermobileno" => "required|numeric"
		]);

		if ($valid->fails()) {
			return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
		} else {
			
			$curl = curl_init();

			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/app/opt/in/CREDAIChennai",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "user=".$request->input('usermobileno'),
		  CURLOPT_HTTPHEADER => array(
				"apikey: 5c63e92c5313479ac77c5c5d54f24a9c",
				"cache-control: no-cache",
				"content-type: application/x-www-form-urlencoded",
				"postman-token: 5b77e9f6-f4ed-b422-c4d5-82a0c731f298"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);

			if ($err) {
				return response()->json(['status' => 400, 'error' => $err]);
			} else {
				return response()->json(['status' => 200, 'data' => $httpcode]);
			}
			

			
			
			//echo $response;
			/*
			if ($server_output == "OK") { 
				return response()->json(['status' => 200, 'data' => 'User opted in successfully.']);
			} else { 
				return response()->json(['status' => 400, 'error' => $server_output]);
			}
			*/
			
		}
	}
	
	public function geUserOptInList() {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/users/CREDAIChennai",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"apikey: 5c63e92c5313479ac77c5c5d54f24a9c",
			"cache-control: no-cache",
			"postman-token: 35ba5186-bbf8-2bc7-b14d-febf2c25f6b7"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return response()->json(['status' => 200, 'data' =>$response]);
		}

		
		
		//echo $response;
		/*
		if ($server_output == "OK") { 
			return response()->json(['status' => 200, 'data' => 'User opted in successfully.']);
		} else { 
			return response()->json(['status' => 400, 'error' => $server_output]);
		}
		*/
	}
	
	public function sendResponse(Request $request) {
		
		$defaultmessage = "Hello, \nKindly help us to know what information are interested in.*Reply with number of your choice*\n	\n*Type 1* for About the Project\n*Type 2* for Amenities/Highlights\n*Type 3* for Project Location\n*Type 4* for Download Brochure\n*Type 5* for Schedule a Site Visit\n*Type 6* for Call Now\n*Type 7* for Request a callback\n*Type 8* for Request a WhatsApp Video Call\n*Type 9* for Visit Website\nEnter # to go back to see the main menu";
			
		
		if($request->has('type')){
			$payload = $request->input('payload');
			$sender = $request->input('sender');
			if($request->input('type')==="user-event"){
				if($payload['type']==="opted-in"){
					$message = "Thank you for Subscribing in Worldhome Super. You may notedown the project code  WH101.";
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/msg",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "channel=whatsapp&source=917834811114&destination=917400416661&src.name=CREDAIChennai&message=%7B%0A%20%20%20%20%20%20%20%20%22isHSM%22%3A%22true%22%2C%0A%20%20%20%20%20%20%20%20%22type%22%3A%20%22text%22%2C%0A%20%20%20%20%20%20%20%20%22text%22%3A%20%22Hi%20John%2C%20how%20are%20you%3F%22%0A%7D",
					  CURLOPT_HTTPHEADER => array(
						"apikey: 5c63e92c5313479ac77c5c5d54f24a9c",
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded",
						"postman-token: 1fdab451-ef44-ffbc-86a0-0e12a6be0e83"
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);
				}
			}else if($request->input('type')==="message"){
				$payloadtext = $payload['payload'];

				//get all sender info
				$senderinfo = $payload['sender'];
               	//Get phone no of customer
				$phone=$senderinfo['dial_code'];


				
				if($payloadtext['text']==="1"){


					//code for getting the last project id which the person has done enquiry
					$leaddata = DB::table('tbl_leads as l')
						->select('l.project_id')
						->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
						->where('u.user_mobile','=',$phone)
						->orderBy('l.lead_id', 'DESC')
						->limit(1)
						->get();

					if(count($leaddata)>0)
					{
						//code for getting project id
						$projectid= $leaddata[0]->project_id;

						//code for getting all project info
						$projectdata = DB::table('tbl_project as pro')->select('pro.*','tbl_builder.builder_name','tbl_project_status.project_status_name','tbl_category.category_name')
							->leftJoin('tbl_builder','tbl_builder.builder_id','=','pro.builder_id')
							->leftJoin('tbl_category','tbl_category.category_id','=','pro.category_id')
							->leftJoin('tbl_project_status','tbl_project_status.project_status_id','=','pro.project_status_id')
							->where('pro.project_id', '=' ,$projectid)->where('pro.status', '=' ,1)->get();
						  
							if(count($projectdata)>0)
							{
								//code for getting single info
								$project_name= $projectdata[0]->project_name;
								$project_website= $projectdata[0]->project_website;
								$project_description= $projectdata[0]->project_description;
								$project_shortdescription= $projectdata[0]->project_shortdescription;
								$project_rera_no= $projectdata[0]->project_rera_no;
								$project_category= $projectdata[0]->category_name;
								$project_status= $projectdata[0]->project_status_name;
								$project_builder= $projectdata[0]->builder_name;
								//code for getting the final msg
								$msg="*Name:*"." $project_name"."\n\n"."*Website url:*"." $project_website"."\n\n"."*Category:*"." $project_category"."\n\n"."*Project Status:*"." $project_status"."\n\n".
								"*Overview:*"." $project_shortdescription"."\n\n"."*Summary:*"." $project_description";
							}
							else
							{
								$msg="No Project Data found";
							}

					}
					else
					{
					  $msg="No latest inquiry"."\n\n".$defaultmessage;
					}

					//message to be sent on wtsp
					$message = $msg;
					
				}
				
				else if($payloadtext['text']==="2"){
					
					$leaddata = DB::table('tbl_leads as l')
					->select('l.project_id')
					->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
					->where('u.user_mobile','=',$phone)
					->orderBy('l.lead_id', 'DESC')
					->limit(1)
					->get();

					if(count($leaddata)>0)
					{
						 //code for getting project id
						 $projectid= $leaddata[0]->project_id;

						 //code for getting all amenities info
						 $projectamenities = DB::table('tbl_project_amenities as pa')
									->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
									->where('pa.status', '=', 1)
									->where('pa.project_id', '=', $projectid)
									->pluck('a.amenities_name')->implode(',');

						 //code for getting all highlight info
						 $projecthighlight = DB::table('tbl_project_highlights as ph')
						 ->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
						 ->where('ph.status', '=', 1)
						 ->where('ph.project_id', '=', $projectid)
						 ->pluck('h.highlight_name')->implode(',');
		
							//msg for amenities
							if($projectamenities==""||$projectamenities==null)
							{
								$amenitiesmsg="No amenities found";
							}
							else
							{
								$amenitiesmsg=$projectamenities;
							}
							//msg for highlight
							if($projecthighlight==""||$projecthighlight==null)
							{
								 $highlightmsg="No amenities found";
							}
							else
							{
								$highlightmsg=$projecthighlight;
							}

							//code for getting the final msg
							$msg="*Amenities:*"." $amenitiesmsg"."\n\n"."*Highlights:*"." $highlightmsg"; 
					}
					else
					{
						$msg="No latest inquiry";
					}
					$message = $msg;
				}
				
				else if($payloadtext['text']==="3"){
					 //code for getting the last project id which the person has done enquiry
					 $leaddata = DB::table('tbl_leads as l')
					 ->select('l.project_id')
					 ->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
					 ->where('u.user_mobile','=',$phone)
					 ->orderBy('l.lead_id', 'DESC')
					 ->limit(1)
					 ->get();

					if(count($leaddata)>0)
					{
					  //code for getting project id
					  $projectid= $leaddata[0]->project_id;

					  //code for getting all project info
					  $projectdata = DB::table('tbl_project as pro')->select('pro.*','l.location_name')
					  ->leftJoin('tbl_location as l','l.location_id','=','pro.location_id')
							  ->where('pro.project_id', '=' ,$projectid)->where('pro.status', '=' ,1)->get();
							  if(count($projectdata)>0)
							  {
								  //code for getting single info
								  $project_latitude= $projectdata[0]->project_latitude;
								  $project_longitude= $projectdata[0]->project_longitude;
								  $project_address= urlencode($projectdata[0]->project_address);
								  $location_name=urlencode($projectdata[0]->location_name);
								  $userphone = "91".$phone;
									$curl = curl_init();
									
									curl_setopt_array($curl, array(
									CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/msg",
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING => "",
									CURLOPT_MAXREDIRS => 10,
									CURLOPT_TIMEOUT => 0,
									CURLOPT_FOLLOWLOCATION => true,
									CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST => "POST",
									CURLOPT_POSTFIELDS => "channel=whatsapp&source=917776888868&destination=91$phone&src.name=TotalityRE&message=%7B%0A%20%20%20%20%20%20%20%20%22type%22%3A%20%22location%22%2C%0A%20%20%20%20%20%20%20%20%22longitude%22%3A%20$project_longitude%2C%0A%20%20%20%20%20%20%20%20%22latitude%22%3A%20$project_latitude%2C%0A%20%20%20%20%20%20%20%20%22name%22%3A%20%22$location_name%22%2C%0A%20%20%20%20%20%20%20%20%22address%22%3A%20%22$project_address%22%0A%7D",
									CURLOPT_HTTPHEADER => array(
										"Content-Type: application/x-www-form-urlencoded",
										"apikey: 6c11590e91314940c934c1c96a5b3f88"
									),
									));

									$response = curl_exec($curl);
									curl_close($curl);
									
									$msg = "";
								}
							  else
							  {
								$msg="No Project Data found";
							  }

					}
					else
					{
					  $msg="No latest inquiry";
					}
					$message =$msg;
				}
				
				else if($payloadtext['text']==="4"){
					$leaddata = DB::table('tbl_leads as l')
					->select('l.project_id')
					->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
					->where('u.user_mobile','=',$phone)
					->orderBy('l.lead_id', 'DESC')
					->limit(1)
					->get();
					
					$filename= "";

					if(count($leaddata)>0)
					{
						//code for getting project id
						$projectid= $leaddata[0]->project_id;
						$msg="*Brochure URL:* ".url('/')."/storage/app/".projectbrochureprefix.$projectid.".pdf";
						//$filename = url('/')."/storage/app/".projectbrochureprefix.$projectid.".pdf";
						$filename = "https://credai365.hoothoot.in/storage/app/CREDAI365-Project-Brochure-$projectid.pdf";
						/* "channel=whatsapp&source=917834811114&destination=91$phone&src.name=CREDAIChennai&message=%7B%0A%20%20%20%20%20%20%20%20%22type%22%3A%20%22file%22%2C%0A%20%20%20%20%20%20%20%20%22url%22%3A%20%22https%3A//www.buildquickbots.com/whatsapp/media/sample/pdf/sample01.pdf%22%2C%0A%20%20%20%20%20%20%20%20%22filename%22%3A%20%22Sample%20file%22%0A%7D", */
						$curl = curl_init();
							
						curl_setopt_array($curl, array(
							CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/msg",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "POST",
							CURLOPT_POSTFIELDS => "channel=whatsapp&source=917776888868&destination=91$phone&src.name=TotalityRE&message=%7B%0A%20%20%20%20%20%20%20%20%22type%22%3A%20%22file%22%2C%0A%20%20%20%20%20%20%20%20%22url%22%3A%20%22$filename%22%2C%0A%20%20%20%20%20%20%20%20%22filename%22%3A%20%22Download-Brochure-Credai365.pdf%22%0A%7D",
							CURLOPT_HTTPHEADER => array(
								"Content-Type: application/x-www-form-urlencoded",
								"apikey: 6c11590e91314940c934c1c96a5b3f88"
							),
						));

						$response = curl_exec($curl);
						curl_close($curl); 
						$msg="";
					}
					else
					{
						$msg="No latest inquiry";
					}
				
					
					
				}
				
				else if($payloadtext['text']==="5"){
						
						 $leaddata = DB::table('tbl_leads as l')
						 ->select('l.*')
						 ->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
						 ->where('u.user_mobile','=',$phone)
						 ->orderBy('l.lead_id', 'DESC')
						 ->limit(1)
						 ->get();

					 if(count($leaddata)>0)
					 {
							  
								$projectid= $leaddata[0]->project_id;
							   $lead = new Lead();
							   $lead->user_id = $leaddata[0]->user_id;
							   $lead->project_id = $projectid;
							   $lead->sales_executive_id = $leaddata[0]->sales_executive_id;
							   $lead->lead_status_primary = "Open";
							   $lead->touchpoint_id = 2;
							   $lead->source_id = 1; 
							   $lead->created_by = $leaddata[0]->user_id;
							   $lead->updated_by = $leaddata[0]->user_id;

								$lead->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
								$lead->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
								$lead->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
								$lead->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
							   $result = $lead->save(); 
							  $msg="Thank you! In short period of time our sales executive will call back";
					}
					else
					{
					$msg="No latest inquiry";
					}

		
					$message = $msg;
					$message = "Press 05 : Enter Date in format of DD/MM/YYYY";
				}
				
				else if($payloadtext['text']==="6"){

					 $leaddata = DB::table('tbl_leads as l')
					 ->select('l.project_id','l.lead_id')
					 ->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
					 ->where('u.user_mobile','=',$phone)
					 ->orderBy('l.lead_id', 'DESC')
					 ->limit(1)
					 ->get();

					if(count($leaddata)>0)
					{
						$lead_id = $leaddata[0]->lead_id;

					  $projectid= $leaddata[0]->project_id;
						$SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('se.sales_executive_phone')
								->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
								->where('pse.status', '=', 1)
								->where('se.sales_executive_status_id', '=', 1)
								->where('pse.project_id', '=', $projectid)
								->limit(1)
								->get();
								if(count($SalesExecutive)>0)
								{
									$salesexecutive_phone=$SalesExecutive[0]->sales_executive_phone;
									$sales_executive_phone=$salesexecutive_phone;
									$user_mobile=$phone;

								   if(isset($sales_executive_phone) && isset($user_mobile))
								   {
									   $post_data = array(
										   'From'     => $user_mobile,
										   'To'       => $sales_executive_phone,
										   'CallerId' => '09513886363'
									   ); 
									   $api_key     = exotel_api_key; 
									   $api_token   = exotel_api_token; 
									   $exotel_sid  = exotel_sid;
									   $url = "https://" . $api_key .  ":"  . $api_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/Calls/connect.json"; 
									   $ch  = curl_init();
									   curl_setopt($ch, CURLOPT_VERBOSE, 1);
									   curl_setopt($ch, CURLOPT_URL, $url);
									   curl_setopt($ch, CURLOPT_POST, 1);
									   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									   curl_setopt($ch, CURLOPT_FAILONERROR, 0);
									   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
									   curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
									   $http_result = curl_exec($ch);
									   curl_close($ch);
									   if(isset($http_result))
									   {
										   $tmp=json_decode($http_result);
										   $exotelresponse=$tmp->Call;
										   
										   $exotelcalllog=new ExotelCallLog;
										   $exotelcalllog->lead_id = $lead_id;
										   $exotelcalllog->sid = $exotelresponse->Sid;
										   $exotelcalllog->parentcallsid = $exotelresponse->ParentCallSid;
										   $exotelcalllog->datecreated = $exotelresponse->DateCreated;
										   $exotelcalllog->dateupdated = $exotelresponse->DateUpdated;
										   $exotelcalllog->accountsid = $exotelresponse->AccountSid;
										   $exotelcalllog->exotel_to =$exotelresponse->To;
										   $exotelcalllog->exotel_from = $exotelresponse->From;
										   $exotelcalllog->phonenumbersid = $exotelresponse->PhoneNumberSid;
										   $exotelcalllog->status =$exotelresponse->Status; 
										   $exotelcalllog->starttime = $exotelresponse->StartTime;
										   $exotelcalllog->endtime =$exotelresponse->EndTime;
										   $exotelcalllog->duration =$exotelresponse->Duration;
										   $exotelcalllog->price = $exotelresponse->Price;
										   $exotelcalllog->direction = $exotelresponse->Direction;
										   $exotelcalllog->answerdby =$exotelresponse->AnsweredBy;
										   $exotelcalllog->forwardedfrom = $exotelresponse->ForwardedFrom;
										   $exotelcalllog->callername = $exotelresponse->CallerName;
										   $exotelcalllog->uri = $exotelresponse->Uri;
										   $exotelcalllog->recordingurl = $exotelresponse->RecordingUrl;										   
											$exotelcalllog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
											$exotelcalllog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
											$exotelcalllog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
											$exotelcalllog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
										   $exotelcalllog->save();
										   $msg="Thank you for contacting our Sales Executive.We will call you in short time.";
									   }
									
								   }  
					   
								}
								else
								{
									$msg="Sorry for inconvenience,Currently no Sales Executive is present to attend call";
								}
					}
					else
					{
					  $msg="No latest inquiry";
					}

					$message = $msg;
				}
				
				elseif($payloadtext['text']==="7"){

				 $leaddata = DB::table('tbl_leads as l')
								 ->select('l.*')
								 ->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
								 ->where('u.user_mobile','=',$phone)
								 ->orderBy('l.lead_id', 'DESC')
								 ->limit(1)
								 ->get();

			 	if(count($leaddata)>0)
			 	{

						    $projectid= $leaddata[0]->project_id;

						   $lead = new Lead();
						   $lead->user_id = $leaddata[0]->user_id;
						   $lead->project_id = $projectid;
						   $lead->sales_executive_id = $leaddata[0]->sales_executive_id;
						   $lead->lead_status_primary = "Open";
						   $lead->touchpoint_id = 2;
						   $lead->source_id = 1; 
						   $lead->created_by = $leaddata[0]->user_id;
						   $lead->updated_by = $leaddata[0]->user_id;
							$lead->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
							$lead->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
							$lead->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
							$lead->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
						   $result = $lead->save(); 
						  $msg="Thank you! In short period of time our sales executive will call back";
				}
				else
				{
				$msg="No latest inquiry";
				}
					$message = $msg;
				}
				
				else if($payloadtext['text']==="8"){

					 $leaddata = DB::table('tbl_leads as l')
					 ->select('l.*')
					 ->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
					 ->where('u.user_mobile','=',$phone)
					 ->orderBy('l.lead_id', 'DESC')
					 ->limit(1)
					 ->get();

				 if(count($leaddata)>0)
				 {

					$projectid= $leaddata[0]->project_id;

					$lead = new Lead();
					$lead->user_id = $leaddata[0]->user_id;
					$lead->project_id = $projectid;
					$lead->sales_executive_id = $leaddata[0]->sales_executive_id;
					$lead->lead_status_primary = "Open";
					$lead->touchpoint_id = 4;
					$lead->source_id = 1; 
					$lead->created_by = $leaddata[0]->user_id;
					$lead->updated_by = $leaddata[0]->user_id;
					$lead->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
					$lead->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
					$lead->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
					$lead->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
					$result = $lead->save();
					$msg="Thank you! In short period of time our sales executive will join you for video call";
				}
				else
				{
					$msg="No latest inquiry";
				}
		
				$message = $msg;
					
					}else if($payloadtext['text']==="9"){

				 $leaddata = DB::table('tbl_leads as l')
				 ->select('l.project_id')
				 ->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
				 ->where('u.user_mobile','=',$phone)
				 ->orderBy('l.lead_id', 'DESC')
				 ->limit(1)
				 ->get();
	
					if(count($leaddata)>0)
					{
					  $projectid= $leaddata[0]->project_id;
					  $projectdata = DB::table('tbl_project as pro')->select('pro.project_website')
							  ->where('pro.project_id', '=' ,$projectid)->where('pro.status', '=' ,1)->get();
							  if(count($projectdata)>0)
							  {
								  $project_website= $projectdata[0]->project_website;
								  $msg="*Thank you!*"."\n\n"."*Website URL:*"." $project_website";
								}
							  else
							  {
								$msg="No Project Data found".count($projectdata)."id:".$projectid;
							  }
	
					}
					else
					{
					  $msg="No latest inquiry";
					}
						$message =$msg;
				}
				
				else{
					$message = $defaultmessage;
				}
				
				$leaddata = DB::table('tbl_leads as l')
				->select('l.*')
				->leftJoin('tbl_user as u','u.user_id','=','l.user_id')
				->where('u.user_mobile','=',$phone)
				->orderBy('l.lead_id', 'DESC')
				->limit(1)
				->get();
				if(count($leaddata)>0)
				{
					$user_id = $leaddata[0]->user_id;
					$current_date_time = Carbon::now()->toDateTimeString();
					$filename="chat_".$user_id.".txt";
					$customermsg=$payloadtext['text'];
					$dataput="<p class='datetime'>$current_date_time</p><p class='customer'>$customermsg</p><p class='bot'>$message</p>";
					Storage::append($filename,$dataput);
				}
				else
				{
					$message="No latest inquiry";
				}
				
				
			}else{
				$message = $defaultmessage;
			}
			
		}else{
			$message = "Invalid Input. Enter # to back to main menu";
		}
		
		return response($message, 200)->header('Content-Type', 'text/plain');
		
	}
	
}
