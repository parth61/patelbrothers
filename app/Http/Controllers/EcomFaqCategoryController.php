<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomFaqCategory;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Facades\File;

class EcomFaqCategoryController extends Controller
{
    public function addecomfaqcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "faq_category" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $ecomfaqcategory = new EcomFaqCategory();
            $ecomfaqcategory->faq_category = $request->input('faq_category');
            $ecomfaqcategory->site_id = $request->input('site_id');
            $ecomfaqcategory->created_by = $request->input('user_id');
            $ecomfaqcategory->updated_by = $request->input('user_id');
            $ecomfaqcategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ecomfaqcategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ecomfaqcategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ecomfaqcategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ecomfaqcategory->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "FAQ category added sucessfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        
        }
    }

    public function getallecomfaqcategory(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaqcategory = new EcomFaqCategory();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('faq_category_id') && $request->input('faq_category_id') != "") {
                $data['faq_category_id'] = $request->input('faq_category_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $ecomfaqcategorydata = $ecomfaqcategory->getecomfaqcategory($data);
            
            return response()->json(['status' => 200, 'count' =>count($ecomfaqcategorydata), 'data' => $ecomfaqcategorydata]);
           
        }

    }

    public function getecomfaqcategorywithfaq(Request $request) {

        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaqcategory = new EcomFaqCategory();
            $data = array();
            
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            $ecomfaqcategorydata = $ecomfaqcategory->getecomfaqcategorywithfaq($data);
            return response()->json(['status' => 200, 'count' =>count($ecomfaqcategorydata), 'data' => $ecomfaqcategorydata]);
           
        }

    }

    public function updatecomfaqcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "faq_category_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaqcategory = new EcomFaqCategory();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
       
            $newrequest = $request->except(['faq_category_id','accesstoken','user_id','site_id']);
          
            $result = $ecomfaqcategory->where('faq_category_id', $request->input('faq_category_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "FAQ category updated succesfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteecomfaqcategory(Request $request) {

        $valid = Validator::make($request->all(), [
            "faq_category_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaqcategory = new EcomFaqCategory();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['faq_category_id','accesstoken','user_id','site_id']);
            $result = $ecomfaqcategory->where('faq_category_id', $request->input('faq_category_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "FAQ category deleted successfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
