<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductTag;
use Carbon\Carbon;
class EcomProductTagController extends Controller
{
    public function addproducttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required",
            "tag_id" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $producttag = new EcomProductTag();
           
            $producttag->product_id = $request->input('product_id');
            $producttag->tag_id = $request->input('tag_id');
            $producttag->created_by = $request->input('user_id');
            $producttag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $producttag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $producttag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $producttag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $producttag->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "producttag Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallproducttag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $producttag = new EcomProductTag();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_tag_id') && $request->input('product_tag_id') != "") {
                $data['product_tag_id'] = $request->input('product_tag_id');
            }
            if ($request->has('tag_id') && $request->input('tag_id') != "") {
                $data['tag_id'] = $request->input('tag_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $producttag = $producttag->getproducttag($data);

            return response()->json(['status' => 200, 'count' => count($producttag), 'data' => $producttag]);
        }

    }

    public function updateproducttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_tag_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $producttag = new EcomProductTag();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_tag_id','user_id','accesstoken']);
            $result = $producttag->where('product_tag_id', $request->input('product_tag_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "producttag Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproducttag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_tag_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $producttag = new EcomProductTag();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_tag_id','user_id','accesstoken']);
            $result = $producttag->where('product_tag_id', $request->input('product_tag_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  
}
