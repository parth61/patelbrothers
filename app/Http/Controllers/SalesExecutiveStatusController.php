<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesExecutiveStatus;
use Validator;
use DB;
class SalesExecutiveStatusController extends Controller
{
    public function addsalesexecutivestatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "sales_executive_status_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesexecutivestatus = new SalesExecutiveStatus();
            $salesexecutivestatus->sales_executive_status_name = $request->input('sales_executive_status_name');
            $salesexecutivestatus->created_by = $request->input('created_by');
            $salesexecutivestatus->updated_by = $request->input('updated_by');
            $salesexecutivestatus->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $salesexecutivestatus->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $salesexecutivestatus->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $salesexecutivestatus->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $salesexecutivestatus->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "salesexecutivestatus Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
      public function getallsalesexecutivestatus(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesexecutivestatus = new SalesExecutiveStatus();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            
            if ($request->has('sales_executive_status_id') && $request->input('sales_executive_status_id') != "") {
                $data['sales_executive_status_id'] = $request->input('sales_executive_status_id');
            }
            $salesexecutivestatus = $salesexecutivestatus->getsalesexecutivestatus($data);
            return response()->json(['status' => 200, 'count' => count($salesexecutivestatus), 'data' => $salesexecutivestatus]);
        }
    }
    
     public function updatesalesexecutivestatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "sales_executive_status_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesexecutivestatus = new SalesExecutiveStatus();
            $newrequest = $request->except(['sales_executive_status_id']);
            $result = $salesexecutivestatus->where('sales_executive_status_id', $request->input('sales_executive_status_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "salesexecutivestatus Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
     public function deletesalesexecutivestatus(Request $request) {
        $valid = Validator::make($request->all(), [
                    "sales_executive_status_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesexecutivestatus = new SalesExecutiveStatus();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['sales_executive_status_id']);
            $result = $salesexecutivestatus->where('sales_executive_status_id', $request->input('sales_executive_status_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
