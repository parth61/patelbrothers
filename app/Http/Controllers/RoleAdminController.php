<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\RoleAdmin;
use Carbon\Carbon;
class RoleAdminController extends Controller
{

    public function addroleadmin(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "role_admin_name" => "required",
                   
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $roleadmin = new RoleAdmin();
            $roleadmin->role_admin_name = $request->input('role_admin_name');
            $roleadmin->created_by = $request->input('user_id');
            $roleadmin->status = $request->input('status');
            $roleadmin->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $roleadmin->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $roleadmin->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $roleadmin->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $roleadmin->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Admin Role Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function getallroleadmin(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $roleadmin = new RoleAdmin();
            $data = array();
           
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            
            if ($request->has('role_admin_id') && $request->input('role_admin_id') != "") {
                $data['role_admin_id'] = $request->input('role_admin_id');
            }
            $roleadmin = $roleadmin->getroleadmin($data);
            return response()->json(['status' => 200, 'count' => count($roleadmin), 'data' => $roleadmin]);
        }
    }
    public function updateroleadmin(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "role_admin_id" => "required",
                    
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $roleadmin = new RoleAdmin();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['role_admin_id']);
            
            $result = $roleadmin->where('role_admin_id', $request->input('role_admin_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Role Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
     public function deleteroleadmin(Request $request) {
        $valid = Validator::make($request->all(), [
                    "role_admin_id" => "required",
                    
                    
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $roleadmin = new RoleAdmin();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['role_admin_id']);
            $result = $roleadmin->where('role_admin_id', $request->input('role_admin_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
