<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomOrderDetails;
use App\EcomProductGallery;
use DB;
use Illuminate\Support\Facades\File;
use App\EcomOrderItems; 
use App\EcomProduct;
class EcomOrderDetailController extends Controller
{
    public function addorderdetail(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "order_id" => "required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $orderdetails = new EcomOrderItems();
            $orderdetails->order_id = $request->input('order_id');
            $orderdetails->product_id = $request->input('product_id');
            $orderdetails->tax_id = $request->input('tax_id');
            $orderdetails->order_product_quantity = $request->input('order_product_quantity');
            $orderdetails->order_product_amount = $request->input('order_product_amount');
            $orderdetails->order_product_tax = $request->input('order_product_tax');
            $orderdetails->order_product_total = $request->input('order_product_total');
            $orderdetails->created_by = $request->input('user_id');    
            $orderdetails->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $orderdetails->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $orderdetails->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $orderdetails->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $orderdetails->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Order detail added successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallorderdetail(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $orderdetail = new EcomOrderItems();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('order_details_id') && $request->input('order_details_id') != "") {
                $data['order_details_id'] = $request->input('order_details_id');
            }
            
            if ($request->has('order_id') && $request->input('order_id') != "") {
                $data['order_id'] = $request->input('order_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            
            $orderdetail = $orderdetail->getorderitems($data);
            $orderdetaildata = array();
            foreach($orderdetail as $item)
            {
                $product_id = $item->product_id;    

                //call for getting gallery image with product
                $productgallery = new EcomProductGallery();
                $data['product_id']=$product_id;
                $data['isfeatured']=1;
                $productgallery = $productgallery->getproductgallery($data);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                 
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                    $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                } else {
                                    $item1->product_image_path =fixedproductgalleryimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath.'/'. $item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath.'/'. $item1->product_image_media_file;
                            } else {
                                $item1->product_image_path = fixedproductgalleryimage;
                            }
                        }
                    }  

                   
                    array_push($productgallerydata, $item1);
                }
                $item->product_gallery=$productgallerydata;
                array_push($orderdetaildata, $item);
            }
            return response()->json(['status' => 200, 'count' => count($orderdetail), 'data' => $orderdetaildata]);
        }

    }
    
    public function updateorderdetail(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "order_details_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $orderdetail = new EcomOrderItems();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['order_details_id','accesstoken','user_id']);
            $result = $orderdetail->where('order_details_id', $request->input('order_details_id'))->update($newrequest);

            if ($result) {

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $orders->id;
                $ecomorderactivity->order_activity_type = "Order Details Updated";
                $ecomorderactivity->order_activity_details = "Order details has been updated.";
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                return response()->json(['status' => 200, 'data' => "Order details updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteorderdetail(Request $request) {

        $valid = Validator::make($request->all(), [
                    "order_details_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $orderdetail = new EcomOrderItems();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['order_details_id','accesstoken','user_id']);
            $result = $orderdetail->where('order_details_id', $request->input('order_details_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Order details deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
