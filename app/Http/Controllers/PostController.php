<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Post;
use App\Slug;
use App\RelationPostTag;
use Illuminate\Support\Facades\File;
use App\RelationPostCategory;
class PostController extends Controller
{
    public function addpost(Request $request) {
        //validations
        
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "post_title" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $post = new Post();
            $post->post_title = $request->input('post_title');
            if ($request->has('post_slug') && $request->input('post_slug') != "")
            {
                $slug_name =$request->input('post_slug');
            }
            else
            {
                $slug_name =$request->input('post_title');
            }

            $slug_name = checkslugavailability($slug_name, "post",$request->input('site_id'));
          
            $slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="post";

            $slug->created_by = $request->input('user_id');

            $slug->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $slug->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $slug->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
           
            $slug->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $post->slug_name = $slug_name;
            $post->post_body = $request->input('post_body');
            
            $post->post_featured_img = $request->input('post_featured_img');
            $post->status = $request->input('status');
            $post->isvisible = $request->input('isvisible');
            $post->site_id = $request->input('site_id');
            $post->created_by = $request->input('created_by');
            $post->updated_by = $request->input('created_by');
            $post->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $post->post_short_description = $request->has('post_short_description')?$request->input('post_short_description'):null;
            $post->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $post->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $post->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $post->save();
            if ($result) {
                 //dynamic entries in tbl_relation_post_tag
            if ($request->has('posttaglist')) {
                $posttaglist = $request->input('posttaglist');
                if (count($posttaglist) > 0) {
                    foreach ($posttaglist as $tmmp) {
                        $RelationPostTag = new RelationPostTag();
                        $checkcount = count($RelationPostTag->where('posttag_id', $tmmp)->where('post_id', $post->id)->where('status', '1')->get());
                        if ($checkcount === 0) {
                            $RelationPostTag->posttag_id = $tmmp;
                            $RelationPostTag->post_id = $post->id;
                            $RelationPostTag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $RelationPostTag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $RelationPostTag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $RelationPostTag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $RelationPostTag->save();
                        }
                    }
                }
            }

                 //dynamic entries in tbl_relation_post_tag
                 if ($request->has('postcategorylist')) {
                    $postcategorylist = $request->input('postcategorylist');
                    if (count($postcategorylist) > 0) {
                        foreach ($postcategorylist as $tmmp) {
                            $RelationPostCategory = new RelationPostCategory();
                            $checkcount = count($RelationPostCategory->where('postcategory_id', $tmmp)->where('post_id', $post->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $RelationPostCategory->postcategory_id = $tmmp;
                                $RelationPostCategory->post_id = $post->id;
                                $RelationPostCategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $RelationPostCategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $RelationPostCategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $RelationPostCategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $RelationPostCategory->save();
                            }
                        }
                    }
                }
                return response()->json(['status' => 200, 'data' => "post Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallpost(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $post = new Post();
            $data = array();
           
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
            if ($request->has('order_by') && $request->filled('order_by')) {
                $data["order_by"] = $request->input("order_by");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('post_id') && $request->input('post_id') != "") {
                $data['post_id'] = $request->input('post_id');
            }
            if ($request->has('author') && $request->input('author') != "") {
                $data['author'] = $request->input('author');
            }
            

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('admin_slug') && $request->input('admin_slug') != "") {
                $data['admin_slug'] = $request->input('admin_slug');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            if ($request->has('postcategory_id') && $request->input('postcategory_id') != "") {
                $data['postcategory_id'] = $request->input('postcategory_id');
            }
            $post = $post->getpost($data);
            if($post)
            {
                $postdata = array();
                foreach ($post as $item) {


                /*     $file = imagedisplaypath.$item->post_media_file;
                    $file_headers = @get_headers($file);
                    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $item->post_featured_img_path = fixedpostfeaturedimage;
                    }
                    else {
                        $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                    }

                    $file1 = imagedisplaypath.$item->seo_media_file;
                    $file_headers1 = @get_headers($file1);
                    if(!$file_headers1 || $file_headers1[0] == 'HTTP/1.1 404 Not Found') {
                       
                        $item->seo_img_path = fixedseofeaturedimage;
                    } else {
                        $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                    } */
                    if(fileuploadtype=="local")
                    {
                        //for local

                        if($item->post_media_file==""||$item->post_media_file==null||$item->post_media_file=="null")
                        {
                            $item->post_featured_img_path = fixedpostfeaturedimage;
                        }
                        else
                        {
                        //code for post featured image
                        if (File::exists(baseimagedisplaypath .$item->post_media_file)) {
                            $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                        } else {
                            $item->post_featured_img_path = fixedpostfeaturedimage;
                        }
                    }

                    if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null")
                    {
                        $item->seo_img_path = fixedseofeaturedimage;
                    }
                    else
                    {
                        //code for seo image
                        if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        }
                    }
                    }
                    else
                    {

                        //for s3

                             //code for post featured image
                            if (does_url_exists(imagedisplaypath.$item->post_media_file)) {
                                $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                            } else {
                                $item->post_featured_img_path = fixedpostfeaturedimage;
                            }

                            //code for seo image
                            if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            } 
                      }
                    //get comma separated postcategory id list
                    $postcategoryid = DB::table('tbl_relation_post_category as rpc')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->pluck('rpc.postcategory_id');
                    $item->postcategoryidlist=$postcategoryid;
    
                     //code for getting postcategory name list
                    $postcategorynamelist = DB::table('tbl_relation_post_category as rpc')
                    ->leftJoin('tbl_postcategory as pc', 'pc.postcategory_id', '=', 'rpc.postcategory_id')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->pluck('pc.postcategory_name');
                    $item->postcategorynamelist=$postcategorynamelist;
    
                      //get comma separated posttag id list
                      $posttagid = DB::table('tbl_relation_post_tag as rpt')
                      ->where('rpt.status', '=' ,1)
                      ->where('rpt.post_id', '=' ,$item->post_id)
                      ->pluck('rpt.posttag_id');
                      $item->posttagidlist=$posttagid;
      
                       //code for getting posttag name list
                      $posttagnamelist = DB::table('tbl_relation_post_tag as rpt')
                      ->leftJoin('tbl_posttag as pt', 'pt.posttag_id', '=', 'rpt.posttag_id')
                      ->where('rpt.status', '=' ,1)
                      ->where('rpt.post_id', '=' ,$item->post_id)
                      ->pluck('pt.posttag_name');
                      $item->posttagnamelist=$posttagnamelist;
                    array_push($postdata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($post), 'data' => $postdata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
            
        }

    }
    public function getallpostbycategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $post = new Post();
            $data = array();
           
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
            if ($request->has('order_by') && $request->filled('order_by')) {
                $data["order_by"] = $request->input("order_by");
            }
            if ($request->has('admin_slug') && $request->input('admin_slug') != "") {
                $data['admin_slug'] = $request->input('admin_slug');
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('post_id') && $request->input('post_id') != "") {
                $data['post_id'] = $request->input('post_id');
            }
            
            if ($request->has('author') && $request->input('author') != "") {
                $data['author'] = $request->input('author');
            }
            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
            if ($request->has('post_slug') && $request->input('post_slug') != "") {
                $data['post_slug'] = $request->input('post_slug');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            if ($request->has('postcategory_id') && $request->input('postcategory_id') != "") {
                $data['postcategory_id'] = $request->input('postcategory_id');
            }
            if ($request->has('categoryslug_name') && $request->input('categoryslug_name') != "") {
                $data['categoryslug_name'] = $request->input('categoryslug_name');
            }
            $post = $post->getpostbycategory($data);
            if($post)
            {
                $postdata = array();
                foreach ($post as $item) {


                /*     $file = imagedisplaypath.$item->post_media_file;
                    $file_headers = @get_headers($file);
                    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $item->post_featured_img_path = fixedpostfeaturedimage;
                    }
                    else {
                        $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                    }

                    $file1 = imagedisplaypath.$item->seo_media_file;
                    $file_headers1 = @get_headers($file1);
                    if(!$file_headers1 || $file_headers1[0] == 'HTTP/1.1 404 Not Found') {
                       
                        $item->seo_img_path = fixedseofeaturedimage;
                    } else {
                        $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                    } */
                    if(fileuploadtype=="local")
                    {
                        //for local

                        if($item->post_media_file==""||$item->post_media_file==null||$item->post_media_file=="null")
                        {
                            $item->post_featured_img_path = fixedpostfeaturedimage;
                        }
                        else
                        {
                        //code for post featured image
                        if (File::exists(baseimagedisplaypath .$item->post_media_file)) {
                            $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                        } else {
                            $item->post_featured_img_path = fixedpostfeaturedimage;
                        }
                    }

                    if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null")
                    {
                        $item->seo_img_path = fixedseofeaturedimage;
                    }
                    else
                    {
                        //code for seo image
                        if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        }
                    }
                    }
                    else
                    {

                        //for s3

                             //code for post featured image
                            if (does_url_exists(imagedisplaypath.$item->post_media_file)) {
                                $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                            } else {
                                $item->post_featured_img_path = fixedpostfeaturedimage;
                            }

                            //code for seo image
                            if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            } 
                      }
                    //get comma separated postcategory id list
                    $postcategoryid = DB::table('tbl_relation_post_category as rpc')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->pluck('rpc.postcategory_id');
                    $item->postcategoryidlist=$postcategoryid;
    
                     //code for getting postcategory name list
                    $postcategorynamelist = DB::table('tbl_relation_post_category as rpc')
                    ->leftJoin('tbl_postcategory as pc', 'pc.postcategory_id', '=', 'rpc.postcategory_id')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->pluck('pc.postcategory_name');
                    $item->postcategorynamelist=$postcategorynamelist;
    
                      //get comma separated posttag id list
                      $posttagid = DB::table('tbl_relation_post_tag as rpt')
                      ->where('rpt.status', '=' ,1)
                      ->where('rpt.post_id', '=' ,$item->post_id)
                      ->pluck('rpt.posttag_id');
                      $item->posttagidlist=$posttagid;
      
                       //code for getting posttag name list
                      $posttagnamelist = DB::table('tbl_relation_post_tag as rpt')
                      ->leftJoin('tbl_posttag as pt', 'pt.posttag_id', '=', 'rpt.posttag_id')
                      ->where('rpt.status', '=' ,1)
                      ->where('rpt.post_id', '=' ,$item->post_id)
                      ->pluck('pt.posttag_name');
                      $item->posttagnamelist=$posttagnamelist;
                    array_push($postdata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($post), 'data' => $postdata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
            
        }

    }



    public function getallpostbytags(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $post = new Post();
            $data = array();
           
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
            if ($request->has('order_by') && $request->filled('order_by')) {
                $data["order_by"] = $request->input("order_by");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('post_id') && $request->input('post_id') != "") {
                $data['post_id'] = $request->input('post_id');
            }
            if ($request->has('author') && $request->input('author') != "") {
                $data['author'] = $request->input('author');
            }
            if ($request->has('admin_slug') && $request->input('admin_slug') != "") {
                $data['admin_slug'] = $request->input('admin_slug');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
            if ($request->has('post_slug') && $request->input('post_slug') != "") {
                $data['post_slug'] = $request->input('post_slug');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            if ($request->has('postcategory_id') && $request->input('postcategory_id') != "") {
                $data['postcategory_id'] = $request->input('postcategory_id');
            }
            if ($request->has('posttag_name') && $request->filled('posttag_name')) {
                $data["posttag_name"] = $request->input("posttag_name");
            }
            $post = $post->getpostbytags($data);
            if($post)
            {
                $postdata = array();
                foreach ($post as $item) {


                /*     $file = imagedisplaypath.$item->post_media_file;
                    $file_headers = @get_headers($file);
                    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $item->post_featured_img_path = fixedpostfeaturedimage;
                    }
                    else {
                        $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                    }

                    $file1 = imagedisplaypath.$item->seo_media_file;
                    $file_headers1 = @get_headers($file1);
                    if(!$file_headers1 || $file_headers1[0] == 'HTTP/1.1 404 Not Found') {
                       
                        $item->seo_img_path = fixedseofeaturedimage;
                    } else {
                        $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                    } */
                    if(fileuploadtype=="local")
                    {
                        //for local

                        if($item->post_media_file==""||$item->post_media_file==null||$item->post_media_file=="null")
                        {
                            $item->post_featured_img_path = fixedpostfeaturedimage;
                        }
                        else
                        {
                        //code for post featured image
                        if (File::exists(baseimagedisplaypath .$item->post_media_file)) {
                            $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                        } else {
                            $item->post_featured_img_path = fixedpostfeaturedimage;
                        }
                    }

                    if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null")
                    {
                        $item->seo_img_path = fixedseofeaturedimage;
                    }
                    else
                    {
                        //code for seo image
                        if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        }
                    }
                    }
                    else
                    {

                        //for s3

                             //code for post featured image
                            if (does_url_exists(imagedisplaypath.$item->post_media_file)) {
                                $item->post_featured_img_path = imagedisplaypath.$item->post_media_file;
                            } else {
                                $item->post_featured_img_path = fixedpostfeaturedimage;
                            }

                            //code for seo image
                            if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            } 
                      }
                    //get comma separated postcategory id list
                    $postcategoryid = DB::table('tbl_relation_post_category as rpc')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->pluck('rpc.postcategory_id');
                    $item->postcategoryidlist=$postcategoryid;
    
                     //code for getting postcategory name list
                    $postcategorynamelist = DB::table('tbl_relation_post_category as rpc')
                    ->leftJoin('tbl_postcategory as pc', 'pc.postcategory_id', '=', 'rpc.postcategory_id')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->pluck('pc.postcategory_name');
                    $item->postcategorynamelist=$postcategorynamelist;

                    //code for getting postcategory name list
                    $categoryslug_name = DB::table('tbl_relation_post_category as rpc')->select('slug_name','postcategory_name')
                    ->leftJoin('tbl_postcategory as pc', 'pc.postcategory_id', '=', 'rpc.postcategory_id')
                    ->where('rpc.status', '=' ,1)
                    ->where('rpc.post_id', '=' ,$item->post_id)
                    ->get();
                    $item->categoryslug_name= $categoryslug_name;
    
                      //get comma separated posttag id list
                      $posttagid = DB::table('tbl_relation_post_tag as rpt')
                      ->where('rpt.status', '=' ,1)
                      ->where('rpt.post_id', '=' ,$item->post_id)
                      ->pluck('rpt.posttag_id');
                      $item->posttagidlist=$posttagid;
      
                       //code for getting posttag name list
                      $posttagnamelist = DB::table('tbl_relation_post_tag as rpt')
                      ->leftJoin('tbl_posttag as pt', 'pt.posttag_id', '=', 'rpt.posttag_id')
                      ->where('rpt.status', '=' ,1)
                      ->where('rpt.post_id', '=' ,$item->post_id)
                      ->pluck('pt.posttag_name');
                      $item->posttagnamelist=$posttagnamelist;
                    array_push($postdata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($post), 'data' => $postdata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
            
        }

    }

    public function updatepost(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "post_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $post = new Post();
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['post_id','slug_id','user_id','accesstoken','posttaglist','postcategorylist','post_slug']);
       
            $result = $post->where('post_id', $request->input('post_id'))->update($newrequest);

            if ($result) {
                if ($request->has('posttaglist')) {

                    $posttaglist = $request->input('posttaglist');
                    if (count($posttaglist) > 0) {
                        $RelationPostTag = new RelationPostTag();
                        $RelationPostTag->where('post_id', $request->input('post_id'))->whereNotIn('posttag_id', $posttaglist)->delete();
                        for ($i = 0; $i < count($posttaglist); $i++) {
                            $checkcount = count($RelationPostTag->where('posttag_id', $posttaglist[$i])->where('post_id', $request->input('post_id'))->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $RelationPostTag = new RelationPostTag();
                                $RelationPostTag->posttag_id = $posttaglist[$i];
                                $RelationPostTag->post_id = $request->input('post_id');
                                $RelationPostTag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $RelationPostTag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $RelationPostTag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $RelationPostTag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $RelationPostTag->save();
                            }
                        }
                    } else {
                        $RelationPostTag = new RelationPostTag();
                        $RelationPostTag->where('post_id', $request->input('post_id'))->delete();
                    }
                }

                if ($request->has('postcategorylist')) {

                    $postcategorylist = $request->input('postcategorylist');
                    if (count($postcategorylist) > 0) {
                        $RelationPostCategory = new RelationPostCategory();
                        $RelationPostCategory->where('post_id', $request->input('post_id'))->whereNotIn('postcategory_id', $postcategorylist)->delete();
                        for ($i = 0; $i < count($postcategorylist); $i++) {
                            $checkcount = count($RelationPostCategory->where('postcategory_id', $postcategorylist[$i])->where('post_id', $request->input('post_id'))->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $RelationPostCategory = new RelationPostCategory();
                                $RelationPostCategory->postcategory_id = $postcategorylist[$i];
                                $RelationPostCategory->post_id = $request->input('post_id');
                                $RelationPostCategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $RelationPostCategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $RelationPostCategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $RelationPostCategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $RelationPostCategory->save();
                            }
                        }
                    } else {
                        $RelationPostCategory = new RelationPostCategory();
                        $RelationPostCategory->where('post_id', $request->input('post_id'))->delete();
                    }
                }
                return response()->json(['status' => 200, 'data' => "post Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletepost(Request $request) {

        $valid = Validator::make($request->all(), [
                    "post_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $post = new Post();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['post_id','user_id','accesstoken']);
            $result = $post->where('post_id', $request->input('post_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
