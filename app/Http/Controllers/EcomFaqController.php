<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomFaqCategory;
use App\EcomFaq;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Facades\File;

class EcomFaqController extends Controller
{
    public function addecomfaq(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "faq_category_id" => "required",
            "faq_question" => "required",
            "faq_answer" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $ecomfaq = new EcomFaq();
            $ecomfaq->faq_category_id = $request->input('faq_category_id');
            $ecomfaq->faq_question = $request->input('faq_question');
            $ecomfaq->faq_answer = $request->input('faq_answer');
            $ecomfaq->site_id = $request->input('site_id');
            $ecomfaq->created_by = $request->input('user_id');
            $ecomfaq->updated_by = $request->input('user_id');
            $ecomfaq->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ecomfaq->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ecomfaq->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ecomfaq->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ecomfaq->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "FAQ added sucessfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        
        }
    }

    public function getallecomfaq(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required",
            "faq_category_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaq = new EcomFaq();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('faq_category_id') && $request->input('faq_category_id') != "") {
                $data['faq_category_id'] = $request->input('faq_category_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $ecomfaqdata = $ecomfaq->getecomfaq($data);
            
            return response()->json(['status' => 200, 'count' =>count($ecomfaqdata), 'data' => $ecomfaqdata]);
           
        }

    }
    
    public function updateecomfaq(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "faq_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaq = new EcomFaq();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
       
            $newrequest = $request->except(['faq_category_id','accesstoken','user_id','site_id','faq_id']);
          
            $result = $ecomfaq->where('faq_id', $request->input('faq_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "FAQ updated succesfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteecomfaq(Request $request) {

        $valid = Validator::make($request->all(), [
            "faq_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomfaqcategory = new EcomFaq();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['faq_id','accesstoken','user_id','site_id']);
            $result = $ecomfaqcategory->where('faq_id', $request->input('faq_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "FAQ deleted successfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
