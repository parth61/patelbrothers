<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Project;
use App\ProjectAmenities;
use App\ProjectCertificate;
use App\ProjectHighlight;
use App\ProjectSizeRange;
use App\ProjectPhoto;
use App\ProjectBudget;
use App\ProjectConfiguration;
use App\Banks;
use App\ProjectBank;
use DB;
use Illuminate\Support\Facades\File;



class ProjectController extends Controller {

    public function addproject(Request $request) {

        $valid = Validator::make($request->all(), [
                    "builder_id" => "required|numeric",
                    "project_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $Project = new Project();
            $imgpath = "";
            $imgpath1 = "";
            $brochurepath = "";
          /*   if ($request->hasFile('project_logo')) {

                $files = $request->file('project_logo');

                $filename = $files->getClientOriginalName();
                $imgpath1 = fileupload($filename, $files);
            }
            if ($request->hasFile('project_featured_img')) {

                $files = $request->file('project_featured_img');

                $filename = $files->getClientOriginalName();
                $imgpath = fileupload($filename, $files);
            } */

            $project_logo_base64= $request->input('project_logo');
            if($project_logo_base64!="" && $project_logo_base64!=null)
            {
                $imgpath1=fileupload($project_logo_base64);
            }

            $project_featured_img_base64= $request->input('project_featured_img');
            if($project_featured_img_base64!="" && $project_featured_img_base64!=null)
            {
                $imgpath=fileupload($project_featured_img_base64);
            }
            if ($request->hasFile('project_brochure')) {

                $files = $request->file('project_brochure');

                $filename = $files->getClientOriginalName();
                $brochurepath = fileupload($filename, $files);
            }


            $Project->builder_id = $request->input('builder_id');
            $Project->project_name = $request->input('project_name');
            $Project->location_id = $request->input('location_id');
            $Project->pincode_id = $request->input('pincode_id');
            $Project->project_logo = $imgpath1;
            $Project->project_featured_img = $imgpath;
            $Project->project_brochure = $brochurepath;
            $Project->category_id = $request->input('category_id');
            $Project->project_rera_no = $request->input('project_rera_no');
            $Project->project_address = $request->input('project_address');
            $Project->project_shortdescription = $request->input('project_shortdescription');
            $Project->project_description = $request->input('project_description');
            $Project->project_slug = $request->input('project_slug');
            $Project->project_latitude = $request->input('project_latitude');
            $Project->project_longitude = $request->input('project_longitude');
            $Project->project_slug = $request->input('project_slug');
            $Project->project_website = $request->input('project_website');
            $Project->project_status_id = $request->input('project_status_id');
            $Project->project_total_units = $request->input('project_total_units');
            $Project->created_by = $request->input('created_by');
            $Project->updated_by = $request->input('updated_by');
            $Project->guid = generateAccessToken(16);
            $Project->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $Project->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $Project->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $Project->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $Project->save();
            if ($result) {
                //dynamic entries in tbl_project_amenities
                if ($request->has('amenitieslist')) {
                    $amenitieslist = json_decode($request->input('amenitieslist'), TRUE);
                    if (count($amenitieslist) > 0) {
                        foreach ($amenitieslist as $tmmp) {
                            $ProjectAmenities = new ProjectAmenities();
                            $checkcount = count($ProjectAmenities->where('amenities_id', $tmmp['amenities_id'])->where('project_amenities_alias', $tmmp['amenitiesalias'])->where('project_id', $Project->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectAmenities->amenities_id = $tmmp['amenities_id'];
                                $ProjectAmenities->project_id = $Project->id;
                                $ProjectAmenities->project_amenities_alias = $tmmp['amenitiesalias'];
                                $ProjectAmenities->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectAmenities->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectAmenities->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectAmenities->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectAmenities->save();
                            }
                        }
                    }
                }
                //dynamic entries in tbl_project_highlight
                if ($request->has('highlightlist')) {
                    $highlightlist = json_decode($request->input('highlightlist'), TRUE);

                    if (count($highlightlist) > 0) {
                        foreach ($highlightlist as $tmp) {
                            $ProjectHighlight = new ProjectHighlight();
                            $checkcount = count($ProjectHighlight->where('highlight_id', $tmp['highlight_id'])->where('project_highlight_alias', $tmp['highlightalias'])->where('project_id', $Project->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectHighlight->highlight_id = $tmp['highlight_id'];
                                $ProjectHighlight->project_id = $Project->id;
                                $ProjectHighlight->project_highlight_alias = $tmp['highlightalias'];
                                $ProjectHighlight->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectHighlight->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectHighlight->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectHighlight->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectHighlight->save();
                            }
                        }
                    }
                }
                //dynamic entries in tbl_project_size_range
                if ($request->has('sizerangelist')) {
                    $sizerangelist = $request->input('sizerangelist');
                    if (count($sizerangelist) > 0) {
                        for ($i = 0; $i < count($sizerangelist); $i++) {
                            $ProjectSizeRange = new ProjectSizeRange();
                            $checkcount = count($ProjectSizeRange->where('size_range_id', $sizerangelist[$i])->where('project_id', $Project->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectSizeRange->size_range_id = $sizerangelist[$i];
                                $ProjectSizeRange->project_id = $Project->id;
                                $ProjectSizeRange->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectSizeRange->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectSizeRange->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectSizeRange->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectSizeRange->save();
                            }
                        }
                    }
                }
                //dynamic entries in tbl_project_configuration
                if ($request->has('configurationlist')) {
                    $configurationlist = $request->input('configurationlist');
                    if (count($configurationlist) > 0) {
                        for ($i = 0; $i < count($configurationlist); $i++) {
                            $ProjectConfiguration = new ProjectConfiguration();
                            $checkcount = count($ProjectConfiguration->where('configuration_id', $configurationlist[$i])->where('project_id', $Project->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectConfiguration->configuration_id = $configurationlist[$i];
                                $ProjectConfiguration->project_id = $Project->id;
                                $ProjectConfiguration->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectConfiguration->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectConfiguration->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectConfiguration->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectConfiguration->save();
                            }
                        }
                    }
                }
                //dynamic entries in tbl_project_configuration
                if ($request->has('budgetlist')) {
                    $budgetlist = $request->input('budgetlist');
                    if (count($budgetlist) > 0) {
                        for ($i = 0; $i < count($budgetlist); $i++) {
                            $ProjectBudget = new ProjectBudget();
                            $checkcount = count($ProjectBudget->where('budget_id', $budgetlist[$i])->where('project_id', $Project->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectBudget->budget_id = $budgetlist[$i];
                                $ProjectBudget->project_id = $Project->id;
                                $ProjectBudget->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectBudget->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectBudget->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectBudget->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectBudget->save();
                            }
                        }
                    }
                }
                //dynamic entries in tbl_project_highlight
                if ($request->has('banklist')) {
                    $banklist = json_decode($request->input('banklist'), TRUE);

                    if (count($banklist) > 0) {
                        foreach ($banklist as $tmp) {
                            $projectbank = new ProjectBank();
                            $checkcount = count($projectbank->where('bank_id', $tmp['bank_id'])->where('project_id', $Project->id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $projectbank->bank_id = $tmp['bank_id'];
                                $projectbank->project_id = $Project->id;
                                $projectbank->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $projectbank->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $projectbank->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $projectbank->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $projectbank->save();
                            }
                        }
                    }
                }

              /*   if ($request->has('projectconfigurationdetails')) {
                    $configurationdetails = json_decode($request->input('projectconfigurationdetails'), TRUE);

                    if (count($configurationdetails) > 0) {
                        foreach ($configurationdetails as $tmp) {
                            $ProjectConfigurationDetails = new ProjectConfigurationDetails();
                            $checkcount = count($ProjectConfigurationDetails->where('configuration_id', $tmp['configuration_id'])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectConfigurationDetails = new ProjectConfigurationDetails();
                                $ProjectConfigurationDetails->project_id = $Project->id;
                                $ProjectConfigurationDetails->configuration_id = $tmp['configuration_id'];
                                $ProjectConfigurationDetails->budget_min = $tmp['budget_min'];
                                $ProjectConfigurationDetails->budget_max = $tmp['budget_max'];
                                $ProjectConfigurationDetails->size_range_min = $tmp['size_range_min'];
                                $ProjectConfigurationDetails->size_range_max = $tmp['size_range_max'];
                                $ProjectConfigurationDetails->save();
                            }
                        }
                    }
                    else {
                        $ProjectConfigurationDetails = new ProjectConfigurationDetails();
                        $ProjectConfigurationDetails->where('project_id', $Project->id)->delete();
                    }
                } */

                //dynamic entries in tbl_project_photos
                if ($request->has('project_photo')) {

                    foreach ($request->file('project_photo') as $files) {
                        $filename = $files->getClientOriginalName();
                        $imgpath = fileuploads($filename, $files);
                        $ProjectPhoto = new ProjectPhoto();
                        $ProjectPhoto->project_photo = $imgpath;
                        $ProjectPhoto->project_id = $Project->id;
                        $ProjectPhoto->created_by = $request->input('created_by');
                        $ProjectPhoto->updated_by = $request->input('updated_by');
                        $ProjectPhoto->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $ProjectPhoto->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $ProjectPhoto->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $ProjectPhoto->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $result = $ProjectPhoto->save();
                    }
                }

                /* Generate PDF */
                //$pdfpath = generateBrochurePDF($Project->id);

                return response()->json(['status' => 200, 'data' => $Project->guid]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallproject(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $Project = new Project();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            if ($request->has('builder_id') && $request->input('builder_id') != "") {
                $data['builder_id'] = $request->input('builder_id');
            }
            if ($request->has('category_id') && $request->input('category_id') != "") {
                $data['category_id'] = $request->input('category_id');
            }
            if ($request->has('project_status_id') && $request->input('project_status_id') != "") {
                $data['project_status_id'] = $request->input('project_status_id');
            }
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
            if ($request->has('budget_id') && $request->input('budget_id') != "") {
                $data['budget_id'] = $request->input('budget_id');
            }
            $Project = $Project->getproject($data);
            if ($Project) {
                $Projectdata = array();
                foreach ($Project as $item) {
                    if ($item->project_logo == NULL || $item->project_logo == "") {
                        $item->logopath = fixedimagepathbuilderlogo;
                    } else {
                        if (does_url_exists(mediapath .$item->project_logo)) {
                            $item->logopath = imagedisplaypath .$item->project_logo;
                        } else {
                            $item->logopath = fixedimagepathbuilderlogo;
                        }
                    }
                    if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                        $item->featuredpath = fixedimagepathprojectfeaturedimage;
                    } else {
                        if (does_url_exists(mediapath .$item->project_featured_img)) {
                            $item->featuredpath = imagedisplaypath .$item->project_featured_img;
                        } else {
                            $item->featuredpath = fixedimagepathprojectfeaturedimage;
                        }
                    }
                    array_push($Projectdata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($Project), 'data' => $Projectdata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function getallprojectlist(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $Project = new Project();
            $data = array();
            $finaldata = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            if ($request->has('builder_id') && $request->input('builder_id') != "") {
                $data['builder_id'] = $request->input('builder_id');
            }
            if ($request->has('category_id') && $request->input('category_id') != "") {
                $data['category_id'] = $request->input('category_id');
            }
            if ($request->has('project_status_id') && $request->input('project_status_id') != "") {
                $data['project_status_id'] = $request->input('project_status_id');
            }
            if ($request->has('location_id') && $request->input('location_id') != "") {
                $data['location_id'] = $request->input('location_id');
            }
            
			if ($request->has('location_latitude') && $request->input('location_latitude') != "" && $request->has('location_longitude') && $request->input('location_longitude') != "") {
                $data['location_latitude'] = $request->input('location_latitude');
                $data['location_longitude'] = $request->input('location_longitude');
                $data['nearbyprojectkilometer']= nearbyprojectkilometer;
            }
			
			if ($request->has('minbudgetval') && $request->input('minbudgetval') != "" && $request->has('maxbudgetval') && $request->input('maxbudgetval') != "") {
                $data['budget_id'] = $request->input('budget_id');
            }

            $Projects = $Project->getproject($data);
          
            if (count($Projects) > 0) {

                foreach ($Projects as $item) {
                    $projectdata = array();
                    $project_id = $item->project_id;

                    $item->project_id;

                    //code for getting sales-executive info

                    $SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id','se.sales_executive_lastname','se.sales_executive_firstname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
                            ->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
                            ->where('pse.status', '=', 1)
                            ->where('se.sales_executive_status_id', '=', 1)
                            ->where('pse.project_id', '=', $project_id)
                            ->limit(1)
                            ->get();

                    $Budget = DB::table('tbl_project_budget as pb')->select(DB::raw("MIN(budget.budget_min_amount) AS minbudget"), DB::raw("MAX(budget.budget_max_amount) AS maxbudget"))
                            ->leftJoin('tbl_budget as budget', 'budget.budget_id', '=', 'pb.budget_id')
                            ->where('pb.status', '=', 1)
                            ->where('pb.project_id', '=', $project_id)
                            ->orderBy('budget.budget_min_amount', 'ASC')
                            ->get();

                    $Configurations = DB::table('tbl_project_configuration as pc')
                                    ->leftJoin('tbl_configuration as conf', 'pc.configuration_id', '=', 'conf.configuration_id')
                                    ->where('pc.status', '=', 1)
                                    ->where('pc.project_id', '=', $project_id)
                                    ->orderBy('conf.configuration_id', 'ASC')
                                    ->pluck('conf.configuration_name')->implode(',');

                    $SizeRange = DB::table('tbl_project_size_range as psr')
                                    ->leftJoin('tbl_size_range as sr', 'sr.size_range_id', '=', 'psr.size_range_id')
                                    ->where('psr.status', '=', 1)
                                    ->where('psr.project_id', '=', $project_id)
                                    ->orderBy('sr.size_range_id', 'ASC')
                                    ->pluck('sr.size_range_name')->implode(',');
                    
                    $item->configuration_details_info = DB::table('tbl_project_configuration_details as tpcd')
                                    ->select('tpcd.*','tc.configuration_name')
                                    ->leftJoin('tbl_configuration as tc', 'tc.configuration_id', '=', 'tpcd.configuration_id')
                                    ->where('tpcd.status', '=', 1)
                                    ->where('tpcd.project_id', '=', $project_id)
                                    ->orderBy('tpcd.pcdid', 'ASC')
                                    ->get();  
                                    
                    $item->project_rera_no = DB::table('tbl_project_main_certificate as tpmc')
                                    ->where('tpmc.status', '=', 1)
                                    ->where('tpmc.project_id', '=', $project_id)
                                    ->orderBy('tpmc.project_main_certificate_id', 'ASC')
                                    ->pluck('tpmc.certificate_no');  

                    if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                        $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                    } else {
                        if (does_url_exists(mediapath .$item->project_featured_img)) {
                            $item->project_featured_img_path = imagedisplaypath .$item->project_featured_img;
                        } else {
                            $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                        }
                    }
                    $item->configuration_info = ($Configurations != "") ? $Configurations : "";
                    $item->size_range_info = ($SizeRange != "") ? $SizeRange : "";
					
					if($Budget[0]->maxbudget<$Budget[0]->minbudget){
						$tempbudget = $Budget[0]->maxbudget;
						$Budget[0]->maxbudget=$Budget[0]->minbudget;
						$Budget[0]->minbudget=$tempbudget;
					}
					
                    $item->budget_info = (count($Budget) > 0) ? $Budget[0] : $Budget;
                    foreach ($SalesExecutive as $item1) {
                        if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
                            $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                        } else {
                            if (does_url_exists(mediapath .$item1->sales_executive_photo)) {
                                $item1->sales_executive_photo_path = imagedisplaypath .$item1->sales_executive_photo;
                            } else {
                                $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                            }
                        }
                    }

                    $item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";

                    $projectbankmodel = new ProjectBank();
                    $item->banklist = $projectbankmodel->where('project_id',$project_id)->where('status',1)->pluck('bank_id');

                    $bankmodel = new Banks();
                    $item->bankname = $bankmodel->where('status',1)->whereIn('bankid',$item->banklist)->get();

                    $featuredimagelist = array();

                    $featuredimagelist = DB::table('tbl_project_photos as tpp')->select('tpp.*')
                                    ->where('tpp.status', '=', 1)
                                    ->where('tpp.type', '=', 'featuredimage')
                                    ->where('tpp.project_id', '=', $project_id)
                                    ->orderBy('tpp.project_photo_id', 'ASC')
                                    ->get();

                    $temp = array(); $tempdata = array();
                    $temp['project_id'] = $project_id;
                    $temp['type'] = 'featuredimage';
                    $temp['project_photo'] = $item->project_featured_img_path;

                    array_push($tempdata, $temp);

                    foreach ($featuredimagelist as $item1) {
                        if ($item1->project_photo == NULL || $item1->project_photo == "") {
                            $item1->project_photo = fixedimagepathprojectfeaturedimage;
                        } else {
                            if (does_url_exists(mediapath .$item1->project_photo)) {
                                $item1->project_photo = imagedisplaypath .$item1->project_photo;
                            } else {
                                $item1->project_photo = fixedimagepathprojectfeaturedimage;
                            }
                        }
                        array_push($tempdata, $item1);
                    }

                    

                    $item->featuredimagelist = $tempdata;

                    array_push($finaldata, $item);
                }

                return response()->json(['status' => 200, 'count' => count($Projects), 'data' => $finaldata]);
            } else {
                return response()->json(['status' => 200, 'count' => count($Projects), 'data' => $finaldata]);
            }
        }
    }
   
    public function getprojectbyid($project_id) {
        $data = array();
        $query = DB::table('tbl_project as pro')->select('pro.*','pro.guid as projectguid', 'tbl_builder.*', 'tbl_location.location_name', 'pro.created_at as created_at', 'tbl_project_status.project_status_name', 'tbl_category.category_name', 'tbl_category.category_colorcode')
                ->leftJoin('tbl_builder', 'tbl_builder.builder_id', '=', 'pro.builder_id')
                ->leftJoin('tbl_category', 'tbl_category.category_id', '=', 'pro.category_id')
                ->leftJoin('tbl_location', 'tbl_location.location_id', '=', 'pro.location_id')
                ->leftJoin('tbl_project_status', 'tbl_project_status.project_status_id', '=', 'pro.project_status_id')
                ->where('pro.status', '=', 1)
                ->where('pro.guid', '=', $project_id)
                ->get();

        $projectdata = array();
        foreach ($query as $item) {
            if ($item->project_logo == NULL || $item->project_logo == "") {
                $item->logopath = fixedimagepathbuilderlogo;
            } else {
                if (does_url_exists(mediapath .$item->project_logo)) {
                    $item->logopath = imagedisplaypath .$item->project_logo;
                } else {
                    $item->logopath = fixedimagepathbuilderlogo;
                }
            }

            if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                $item->featuredpath = fixedimagepathprojectfeaturedimage;
            } else {
                if (does_url_exists(mediapath .$item->project_featured_img)) {
                    $item->featuredpath = imagedisplaypath .$item->project_featured_img;
                } else {
                    $item->featuredpath = fixedimagepathprojectfeaturedimage;
                }
            }

            $projectbankmodel = new ProjectBank();
            $item->banklist = $projectbankmodel->where('project_id',$item->project_id)->where('status',1)->pluck('bank_id');

            $bankmodel = new Banks();
            $item->bankname = $bankmodel->where('status',1)->whereIn('bankid',$item->banklist)->pluck('bank_name');

            $salesexecutivedata = DB::table('tbl_project_sales_executive as tpse')->select('tse.*')
                ->leftJoin('tbl_sales_executive as tse', 'tse.sales_executive_id', '=', 'tpse.sales_executive_id')
                ->where('tse.status', '=', 1)
                ->where('tse.sales_executive_status_id', '=', 1)
                ->where('tpse.status', '=', 1)
                ->where('tpse.project_id', '=', $item->project_id)
                ->first();

            if(isset($salesexecutivedata->sales_executive_photo)){
                if ($salesexecutivedata->sales_executive_photo == NULL || $salesexecutivedata->sales_executive_photo == "") {
                    $item->salesexecutivepath = fixedimagepathbuilderlogo;
                } else {
                    if (does_url_exists(mediapath.$salesexecutivedata->sales_executive_photo)) {
                        $item->salesexecutivepath = imagedisplaypath.$salesexecutivedata->sales_executive_photo;
                    } else {
                        $item->salesexecutivepath = fixedimagepathbuilderlogo;
                    }
                }
            }else{
                $item->salesexecutivepath = "";
            }

            array_push($projectdata, $item);

        }



        $data['projectdata'] = $projectdata;

        return view('editproject', $data);
    }

    public function updateproject(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $Project = new Project();

            $imgpath = null;
            $imgpath1 = null;

            $project_id = $request->input('project_id');
           /*  if ($request->hasFile('project_logo')) {

                $files = $request->file('project_logo');

                $filename = $files->getClientOriginalName();
                $imgpath = fileupload($filename, $files);
                $query = DB::table('tbl_project')->select('tbl_project.project_logo')->where('project_id', $project_id)->get();
                if (count($query) > 0) {
                    $img = $query[0]->project_logo;
                    if ($img != NULL || $img != "") {
                        if (does_url_exists(mediapath .$img)) {
                            $path = mediapath .$img;
                            unlink($path);
                        }
                    }
                }
            } */
				$project_logo_base64= $request->input('project_logo');
                if($project_logo_base64!="" && $project_logo_base64!=null && $project_logo_base64!="null")
                {
                    $project_logo_new_path =fileupload($project_logo_base64);
                    $query = DB::table('tbl_project')->select('tbl_project.project_logo')->where('project_id', $project_id)->get();
					if (count($query) > 0) {
						$img = $query[0]->project_logo;
						if ($img != NULL || $img != "") {
							if (does_url_exists(mediapath .$img)) {
								$path = mediapath .$img;
							  //  unlink($path);
							}
						}
					}
                }
           
          
                $project_featured_img_base64= $request->input('project_featured_img');
                if($project_featured_img_base64!="" && $project_featured_img_base64!=null && $project_featured_img_base64!="null")
                {
                    $project_feature_img_new_path=fileupload($project_featured_img_base64);
                    $query = DB::table('tbl_project')->select('tbl_project.project_featured_img')->where('project_id', $project_id)->get();
                    if (count($query) > 0) {
                        $img1 = $query[0]->project_featured_img;
                        if ($img1 != NULL || $img1 != "") {
                            if (does_url_exists(mediapath .$img1)) {
                                $path1 = mediapath .$img1;
                                //unlink($path1);
                            }
                        }
                    }
                }
                if ($request->hasFile('project_brochure')) {

                    $files = $request->file('project_brochure');
    
                    $filename = $files->getClientOriginalName();
                    $brochurepath = fileupload($filename, $files);
                    $query = DB::table('tbl_project')->select('tbl_project.project_brochure')->where('project_id', $project_id)->get();
                    if (count($query) > 0) {
                        $brochure = $query[0]->project_brochure;
                        if ($brochure != NULL || $brochure != "") {
                            if (does_url_exists(mediapath .$brochure)) {
                                $path = mediapath .$brochure;
                               // unlink($path);
                            }
                        }
                    }
                }

            $newrequest = $request->except(['banklist','builder_id','budgetlist', 'project_id', 'project_photo', 'configurationlist', 'amenitieslist', 'highlightlist', 'sizerangelist', 'project_logo', 'project_featured_img', 'project_brochure']);

            if ($request->hasFile('project_brochure')) {
                $newrequest['project_brochure'] = $brochurepath;
            }

            if($project_logo_base64!="" && $project_logo_base64!=null && $project_logo_base64!="null")
            {
                $newrequest['project_logo'] = $project_logo_new_path;
            }
            if($project_featured_img_base64!="" && $project_featured_img_base64!=null && $project_featured_img_base64!="null")
            {
                $newrequest['project_featured_img'] = $project_feature_img_new_path;
            }

            $result = $Project->where('project_id', $project_id)->update($newrequest);
            if ($result) {
                $project_id = $request->input('project_id');
                //update Project_amenities table
                if ($request->has('amenitieslist')) {
                    $amenitieslist = json_decode($request->input('amenitieslist'), TRUE);
                    if (count($amenitieslist) > 0) {
                        foreach ($amenitieslist as $tmmp) {
                            $ProjectAmenities = new ProjectAmenities();
                            $checkcount = count($ProjectAmenities->where('amenities_id', $tmmp['amenities_id'])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectAmenities = new ProjectAmenities();
                                $ProjectAmenities->amenities_id = $tmmp['amenities_id'];
                                $ProjectAmenities->project_id = $project_id;
                                $ProjectAmenities->project_amenities_alias = $tmmp['amenitiesalias'];
                                $ProjectAmenities->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectAmenities->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectAmenities->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectAmenities->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectAmenities->save();
                            }
                        }
                    }else {
                        $ProjectAmenities = new ProjectAmenities();
                        $ProjectAmenities->where('project_id', $request->input('project_id'))->delete();
                    }
                }

                if ($request->has('highlightlist')) {
                    $highlightlist = json_decode($request->input('highlightlist'), TRUE);

                    if (count($highlightlist) > 0) {
                        foreach ($highlightlist as $tmp) {
                            $ProjectHighlight = new ProjectHighlight();
                            $checkcount = count($ProjectHighlight->where('highlight_id', $tmp['highlight_id'])->where('project_highlight_alias', $tmp['highlightalias'])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectHighlight = new ProjectHighlight();
                                $ProjectHighlight->highlight_id = $tmp['highlight_id'];
                                $ProjectHighlight->project_id = $project_id;
                                $ProjectHighlight->project_highlight_alias = $tmp['highlightalias'];
                                $ProjectHighlight->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectHighlight->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectHighlight->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectHighlight->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectHighlight->save();
                            }
                        }
                    }
                    else {
                        $ProjectHighlight = new ProjectHighlight();
                        $ProjectHighlight->where('project_id', $request->input('project_id'))->delete();
                    }
                }

                if ($request->has('sizerangelist')) {

                    $sizerangelist = $request->input('sizerangelist');
                    if (count($sizerangelist) > 0) {
                        $ProjectSizeRange = new ProjectSizeRange();
                        $ProjectSizeRange->where('project_id', $request->input('project_id'))->whereNotIn('size_range_id', $sizerangelist)->delete();
                        for ($i = 0; $i < count($sizerangelist); $i++) {
                            $checkcount = count($ProjectSizeRange->where('size_range_id', $sizerangelist[$i])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectSizeRange = new ProjectSizeRange();
                                $ProjectSizeRange->size_range_id = $sizerangelist[$i];
                                $ProjectSizeRange->project_id = $project_id;
                                $ProjectSizeRange->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $ProjectSizeRange->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $ProjectSizeRange->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $ProjectSizeRange->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $ProjectSizeRange->save();
                            }
                        }
                    } else {
                        $ProjectSizeRange = new ProjectSizeRange();
                        $ProjectSizeRange->where('project_id', $request->input('project_id'))->delete();
                    }
                }

               /*  if ($request->has('configurationlist')) {
                    $configurationlist = $request->input('configurationlist');
                    if (count($configurationlist) > 0) {
                        $ProjectConfiguration = new ProjectConfiguration();
                        $ProjectConfiguration->where('project_id', $request->input('project_id'))->whereNotIn('configuration_id', $configurationlist)->delete();
                        for ($i = 0; $i < count($configurationlist); $i++) {

                            $checkcount = count($ProjectConfiguration->where('configuration_id', $configurationlist[$i])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectConfiguration = new ProjectConfiguration();
                                $ProjectConfiguration->configuration_id = $configurationlist[$i];
                                $ProjectConfiguration->project_id = $project_id;
                                $ProjectConfiguration->save();
                            }
                        }
                    }
                    else {
                        $ProjectConfiguration = new ProjectConfiguration();
                        $ProjectConfiguration->where('project_id', $request->input('project_id'))->delete();
                    }
                } */ 
                //project budget
                /* if ($request->has('budgetlist')) {
                    $budgetlist = $request->input('budgetlist');
                    if (count($budgetlist) > 0) {
                        for ($i = 0; $i < count($budgetlist); $i++) {
                            $ProjectBudget = new ProjectBudget();
                            $ProjectBudget->where('project_id', $request->input('project_id'))->whereNotIn('budget_id', $budgetlist)->delete();
                            $checkcount = count($ProjectBudget->where('budget_id', $budgetlist[$i])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectBudget = new ProjectBudget();
                                $ProjectBudget->budget_id = $budgetlist[$i];
                                $ProjectBudget->project_id = $project_id;
                                $ProjectBudget->save();
                            }
                        }
                    }
                    else {
                        $ProjectBudget = new ProjectBudget();
                        $ProjectBudget->where('project_id', $request->input('project_id'))->delete();
                    }
                } */
                // Project Bank info
                if ($request->has('banklist')) {    
                    $banklist = $request->input('banklist');
                    if (count($banklist) > 0) {
                        for ($i = 0; $i < count($banklist); $i++) {
                            $projectbank = new ProjectBank();
                            $projectbank->where('project_id', $request->input('project_id'))->whereNotIn('bank_id', $banklist)->delete();
                            $checkcount = count($projectbank->where('bank_id', $banklist[$i])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $projectbank = new ProjectBank();
                                $projectbank->bank_id = $banklist[$i];
                                $projectbank->project_id = $project_id;
                                
                                $projectbank->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $projectbank->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $projectbank->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $projectbank->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $projectbank->save();
                            }
                        }
                    }
                    else {
                        $projectbank = new ProjectBank();
                        $projectbank->where('project_id', $request->input('project_id'))->delete();
                    }
                }
                else {
                    $projectbank = new ProjectBank();
                    $newupdateddata['status'] = 0;
                    $projectbank->where('project_id', $request->input('project_id'))->update($newupdateddata);
                }

               /*  if ($request->has('projectconfigurationdetails')) {
                    $configurationdetails = json_decode($request->input('projectconfigurationdetails'), TRUE);

                    if (count($configurationdetails) > 0) {
                        foreach ($configurationdetails as $tmp) {
                            $ProjectConfigurationDetails = new ProjectConfigurationDetails();
                            $checkcount = count($ProjectConfigurationDetails->where('configuration_id', $tmp['configuration_id'])->where('project_id', $project_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $ProjectConfigurationDetails = new ProjectConfigurationDetails();
                                $ProjectConfigurationDetails->project_id = $project_id;
                                $ProjectConfigurationDetails->configuration_id = $tmp['configuration_id'];
                                $ProjectConfigurationDetails->budget_min = $tmp['budget_min'];
                                $ProjectConfigurationDetails->budget_max = $tmp['budget_max'];
                                $ProjectConfigurationDetails->size_range_min = $tmp['size_range_min'];
                                $ProjectConfigurationDetails->size_range_max = $tmp['size_range_max'];
                                $ProjectConfigurationDetails->save();
                            }
                        }
                    }
                    else {
                        $ProjectConfigurationDetails = new ProjectConfigurationDetails();
                        $ProjectConfigurationDetails->where('project_id', $request->input('project_id'))->delete();
                    }
                } */

                //dynamic entries in tbl_project_photos
               /*  if ($request->has('project_photo')) {
                    $ProjectPhoto = new ProjectPhoto();
                   
                        foreach ($request->file('project_photo') as $files) {
                            $query = DB::table('tbl_project_photos as pc')
                            ->select('pc.*')
                            ->where('pc.status', '=' ,1)
                            ->where('pc.project_id', '=' ,$request->input('project_id'))->get();
                            if(count($query)<5)
                            {
                            $filename = $files->getClientOriginalName();
                            $imgpath = fileuploads($filename, $files);
                            $ProjectPhoto = new ProjectPhoto();
                            $ProjectPhoto->project_photo = $imgpath;
                            $ProjectPhoto->project_id = $request->input('project_id');
                            $ProjectPhoto->created_by = $request->input('created_by');
                            $ProjectPhoto->updated_by = $request->input('updated_by');
                            $result = $ProjectPhoto->save();
                        }
                        else
                        {
                            return response()->json(['status' => 400, 'error' => "You can upload max 5 photos only"], 400);
                        }
                    }
                   
                   
                } */

                /* Generate PDF */
                $pdfpath = generateBrochurePDF($project_id);

                return response()->json(['status' => 200, 'data' => "Project Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteproject(Request $request) {
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $project = new Project();
            $ProjectAmenities = new ProjectAmenities();

            $project_id = $request->input('project_id');
            $query = DB::table('tbl_project')->select('tbl_project.project_logo', 'tbl_project.project_featured_img')->where('project_id', $project_id)->get();
            if (count($query) > 0) {
                $img = $query[0]->project_logo;
                if ($img != NULL || $img != "") {
                    if (does_url_exists(mediapath .$img)) {
                        $path = mediapath .$img;
                       // unlink($path);
                    }
                }

                $img1 = $query[0]->project_featured_img;
                if ($img1 != NULL || $img1 != "") {
                    if (does_url_exists(mediapath .$img1)) {
                        $path1 = mediapath .$img1;
                      //  unlink($path1);
                    }
                }
            }



            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['project_id']);
            $result = $project->where('project_id', $request->input('project_id'))->update($newrequest);
            if ($result) {

                $ProjectPhoto = new ProjectPhoto();
                $result1 = $ProjectPhoto->select('tbl_project_photos.project_photo')->where('project_id', $request->input('project_id'))->get();
                if (count($result1) > 0) {
                    foreach ($result1 as $img) {
                        $unlinkimg = $img->project_photo;
                        if ($unlinkimg != NULL || $unlinkimg != "") {
                            if (does_url_exists(mediapath .$unlinkimg)) {
                                $path = mediapath .$unlinkimg;
                               // unlink($path);
                            }
                        }
                    }
                }
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function getallbrochure($project_id) {

       
            $Project = new Project();
            $data1 = array();
            $data=array();
            $finaldata = array();
            $data1['offset'] = 0;
            $data1['limit'] = 1;

            $data1['project_id'] = 203;

            $Projects = $Project->getproject($data1);
            if (count($Projects) > 0) {

                foreach ($Projects as $item) {
                    $projectdata = array();
                    $project_id = $item->project_id;

                    $item->project_id;

                    //code for getting sales-executive info

                    $SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
                            ->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
                            ->where('pse.status', '=', 1)
                            ->where('se.sales_executive_status_id', '=', 1)
                            ->where('pse.project_id', '=', $project_id)
                            ->limit(1)
                            ->get();
                            foreach ($SalesExecutive as $item1) {
                                if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
                                    $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                                } else {
                                    if (does_url_exists(mediapath .$item1->sales_executive_photo)) {
                                        $item1->sales_executive_photo_path = imagedisplaypath .$item1->sales_executive_photo;
                                    } else {
                                        $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                                    }
                                }
                            }
        
                            $item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
                            
                    $Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
                                    ->where('pp.status', '=', 1)
                                    ->where('pp.project_id', '=', $project_id)
                                    ->get();

                                    $photosarray=array();
                                    foreach ($Projectphotos as $item2) {
                                        if ($item2->project_photo == NULL || $item2->project_photo == "") {
                                            $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                        } else {
                                            if (does_url_exists(mediapath .$item2->project_photo)) {
                                                $item2->project_photo = imagedisplaypath .$item2->project_photo;
                                            } else {
                                                $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                            }
                                        }
                                        array_push($photosarray,$item2);
                                    }
                                    $item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

                    $Amenities = DB::table('tbl_project_amenities as pa')
                            ->select('a.amenities_name')
                            ->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
                            ->where('pa.status', '=', 1)
                            ->where('pa.project_id', '=', $project_id)
                            ->get();
                           $amenitiesarray=array();
                            foreach ($Amenities as $ame) {
                               array_push($amenitiesarray,$ame->amenities_name);
                            }
                            $item->amenities = (count($Amenities)>0) ? $amenitiesarray : "";
                          
                  $Highlight = DB::table('tbl_project_highlights as ph')
                            ->select('h.highlight_name')
                            ->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
                            ->where('ph.status', '=', 1)
                            ->where('ph.project_id', '=', $project_id)
                            ->get();
                            $highlightarray=array();
                            foreach ($Highlight as $high) {
                               array_push($highlightarray,$high->highlight_name);
                            }
                            $item->highlight = (count($Highlight)>0) ? $highlightarray : "";
                          
                    $Budget = DB::table('tbl_project_budget as pb')->select(DB::raw("MIN(budget.budget_min_amount) AS minbudget, MAX(budget.budget_max_amount) AS maxbudget"))
                            ->leftJoin('tbl_budget as budget', 'budget.budget_id', '=', 'pb.budget_id')
                            ->where('pb.status', '=', 1)
                            ->where('pb.project_id', '=', $project_id)
                            ->get();

                    $Configurations = DB::table('tbl_project_configuration as pc')
                                    ->leftJoin('tbl_configuration as conf', 'pc.configuration_id', '=', 'conf.configuration_id')
                                    ->where('pc.status', '=', 1)
                                    ->where('pc.project_id', '=', $project_id)
                                    ->pluck('conf.configuration_name')->implode(',');

                    $SizeRange = DB::table('tbl_project_size_range as psr')
                                    ->leftJoin('tbl_size_range as sr', 'sr.size_range_id', '=', 'psr.size_range_id')
                                    ->where('psr.status', '=', 1)
                                    ->where('psr.project_id', '=', $project_id)
                                    ->pluck('sr.size_range_name')->implode(',');

                    if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                        $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                    } else {
                        if (does_url_exists(mediapath .$item->project_featured_img)) {
                            $item->project_featured_img_path = imagedisplaypath .$item->project_featured_img;
                        } else {
                            $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                        }
                    }

                    if ($item->project_logo == NULL || $item->project_logo == "") {
                        $item->project_logo_path = fixedimagepathbuilderlogo;
                    } else {
                        if (does_url_exists(mediapath .$item->project_logo)) {
                            $item->project_logo_path = imagedisplaypath .$item->project_logo;
                        } else {
                            $item->project_logo_path = fixedimagepathbuilderlogo;
                        }
                    }
                    $item->configuration_info = ($Configurations != "") ? $Configurations : "";
                    $item->size_range_info = ($SizeRange != "") ? $SizeRange : "";
                    $item->budget_info = (count($Budget) > 0) ? $Budget : "";
					
					$item->photos = DB::table('tbl_project_photos as tpp')
                                    ->where('tpp.status', '=', 1)
                                    ->where('tpp.project_id', '=', $project_id)
                                    ->get();
									
					$item->configdetails = DB::table('tbl_project_configuration_details as tpc')->select('tc.configuration_name','tpc.*')
						->leftJoin('tbl_configuration as tc', 'tc.configuration_id', '=', 'tpc.configuration_id')
                                    ->where('tpc.status', '=', 1)
                                    ->where('tpc.project_id', '=', $project_id)
                                    ->get();
									
					$item->banklist = DB::table('tbl_project_banks as tpb')
						->leftJoin('tbl_banks as tb', 'tb.bankid', '=', 'tpb.bank_id')
                                    ->where('tpb.status', '=', 1)
                                    ->where('tpb.project_id', '=', $project_id)
                                    ->pluck('tb.bank_name')->implode(',');
					
					$item->maincertificates = DB::table('tbl_project_main_certificate as tpmc')->select('tpmc.certificate_no','tpmc.certificate_file')
                                    ->where('tpmc.status', '=', 1)
                                    ->where('tpmc.project_id', '=', $project_id)
                                    ->get();
                   
                    array_push($finaldata, $item);
                }
                 //           return response()->json(['status' => 200, 'count' => count($Projects), 'data' => $finaldata]);
                $data['projectdata']=$finaldata;
                  //dd($data);
                return view('brochure', $data);
            } else {
                return response()->json(['status' => 200, 'count' => count($Projects), 'data' => $finaldata]);
            }
    }

    
    public function getallbrochurefrontside() {

    
        $Project = new Project();
        $data1 = array();
        $data=array();
        $finaldata = array();
        $data1['offset'] = 0;
        $data1['limit'] = 1;

        $data1['project_id'] = 513;

        $Projects = $Project->getproject($data1);
        if (count($Projects) > 0) {

            foreach ($Projects as $item) {
                $projectdata = array();
                $project_id = $item->project_id;

                $item->project_id;

                //code for getting sales-executive info

                $SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
                        ->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
                        ->where('pse.status', '=', 1)
                        ->where('se.sales_executive_status_id', '=', 1)
                        ->where('pse.project_id', '=', $project_id)
                        ->limit(1)
                        ->get();
                        foreach ($SalesExecutive as $item1) {
                            if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
                                $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                            } else {
                                if (does_url_exists(mediapath .$item1->sales_executive_photo)) {
                                    $item1->sales_executive_photo_path = imagedisplaypath .$item1->sales_executive_photo;
                                } else {
                                    $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                                }
                            }
                        }
    
                        $item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
                        
                $Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
                                ->where('pp.status', '=', 1)
                                ->where('pp.project_id', '=', $project_id)
                                ->get();

                                $photosarray=array();
                                foreach ($Projectphotos as $item2) {
                                    if ($item2->project_photo == NULL || $item2->project_photo == "") {
                                        $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                    } else {
                                        if (does_url_exists(mediapath .$item2->project_photo)) {
                                            $item2->project_photo = imagedisplaypath .$item2->project_photo;
                                        } else {
                                            $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                        }
                                    }
                                    array_push($photosarray,$item2);
                                }
                                $item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

                $Amenities = DB::table('tbl_project_amenities as pa')
                        ->select('a.amenities_name')
                        ->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
                        ->where('pa.status', '=', 1)
                        ->where('pa.project_id', '=', $project_id)
                        ->get();
                        $amenitiesarray=array();
                        foreach ($Amenities as $ame) {
                            array_push($amenitiesarray,$ame->amenities_name);
                        }
                        $item->amenities = (count($Amenities)>0) ? $amenitiesarray : "";
                        
                $Highlight = DB::table('tbl_project_highlights as ph')
                        ->select('h.highlight_name')
                        ->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
                        ->where('ph.status', '=', 1)
                        ->where('ph.project_id', '=', $project_id)
                        ->get();
                        $highlightarray=array();
                        foreach ($Highlight as $high) {
                            array_push($highlightarray,$high->highlight_name);
                        }
                        $item->highlight = (count($Highlight)>0) ? $highlightarray : "";
                        
                $Budget = DB::table('tbl_project_budget as pb')->select(DB::raw("MIN(budget.budget_min_amount) AS minbudget, MAX(budget.budget_max_amount) AS maxbudget"))
                        ->leftJoin('tbl_budget as budget', 'budget.budget_id', '=', 'pb.budget_id')
                        ->where('pb.status', '=', 1)
                        ->where('pb.project_id', '=', $project_id)
                        ->get();

                $Configurations = DB::table('tbl_project_configuration as pc')
                                ->leftJoin('tbl_configuration as conf', 'pc.configuration_id', '=', 'conf.configuration_id')
                                ->where('pc.status', '=', 1)
                                ->where('pc.project_id', '=', $project_id)
                                ->pluck('conf.configuration_name')->implode(',');

                $SizeRange = DB::table('tbl_project_size_range as psr')
                                ->leftJoin('tbl_size_range as sr', 'sr.size_range_id', '=', 'psr.size_range_id')
                                ->where('psr.status', '=', 1)
                                ->where('psr.project_id', '=', $project_id)
                                ->pluck('sr.size_range_name')->implode(',');

                if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                    $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                } else {
                    if (does_url_exists(mediapath .$item->project_featured_img)) {
                        $item->project_featured_img_path = imagedisplaypath .$item->project_featured_img;
                    } else {
                        $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                    }
                }

                if ($item->project_logo == NULL || $item->project_logo == "") {
                    $item->project_logo_path = fixedimagepathbuilderlogo;
                } else {
                    if (does_url_exists(mediapath .$item->project_logo)) {
                        $item->project_logo_path = imagedisplaypath .$item->project_logo;
                    } else {
                        $item->project_logo_path = fixedimagepathbuilderlogo;
                    }
                }
                $item->configuration_info = ($Configurations != "") ? $Configurations : "";
                $item->size_range_info = ($SizeRange != "") ? $SizeRange : "";
                $item->budget_info = (count($Budget) > 0) ? $Budget[0] : $Budget;
                
                
                
                
                array_push($finaldata, $item);
            }

            $data['projectdata']=$finaldata;

            return view('brochure', $data);
        } else {
            return response()->json(['status' => 200, 'count' => count($Projects), 'data' => $finaldata]);
        }
    }

    public function updateprojectlogo(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $Project = new Project();

            $project_logo_new_path = null;

            $project_id = $request->input('project_id');

            $project_logo_base64= $request->input('project_logo');
            if($project_logo_base64!="" && $project_logo_base64!=null && $project_logo_base64!="null")
            {
                $responsedata = fileUploadSlim($request, "project_logo");
                $project_logo_new_path = $responsedata['folderpath'];

                $query = DB::table('tbl_project')->select('tbl_project.project_logo')->where('project_id', $project_id)->get();
                if (count($query) > 0) {
                    $img = $query[0]->project_logo;
                    if ($img != NULL || $img != "") {
                        if (does_url_exists(mediapath .$img)) {
                            $path = mediapath .$img;
                            //  unlink($path);
                        }
                    }
                }
            }

            $newrequest = $request->except(['project_id', 'project_logo']);

            if($project_logo_base64!="" && $project_logo_base64!=null && $project_logo_base64!="null")
            {
                $newrequest['project_logo'] = $project_logo_new_path;
            }
            
            $result = $Project->where('project_id', $project_id)->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Logo Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function updateprojectfeaturedimage(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $Project = new Project();
            $project_id = $request->input('project_id');
            $project_feature_img_new_path = null;

            $project_featured_img_base64= $request->input('project_featured_img');
            if($project_featured_img_base64!="" && $project_featured_img_base64!=null && $project_featured_img_base64!="null")
            {
                $responsedata = fileUploadSlim($request, "project_featured_img");
                $project_feature_img_new_path = $responsedata['folderpath'];
                
                $query = DB::table('tbl_project')->select('tbl_project.project_featured_img')->where('project_id', $project_id)->get();
                if (count($query) > 0) {
                    $img1 = $query[0]->project_featured_img;
                    if ($img1 != NULL || $img1 != "") {
                        if (does_url_exists(mediapath .$img1)) {
                            $path1 = mediapath .$img1;
                            //unlink($path1);
                        }
                    }
                }
            }

            $newrequest = $request->except(['project_id', 'project_featured_img']);

            if($project_featured_img_base64!="" && $project_featured_img_base64!=null && $project_featured_img_base64!="null")
            {
                $newrequest['project_featured_img'] = $project_feature_img_new_path;
            }
            
            $result = $Project->where('project_id', $project_id)->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Featured Image Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
	
	public function generatebrochureforallprojects(Request $request){
		
		$project = new Project();
		$projectlist = $project->where('status',1)->where('isvisible', 1)->where('project_id', '>', $request->input('project_id'))->get();
		
		$index=0; $pathlist = array();
		foreach($projectlist as $item){
			$index++;
			$pdfpath = generateBrochurePDF($item->project_id);
			array_push($pathlist, $pdfpath);
		}
		
		return response()->json(['status' => 200, 'total' => $index, 'paths' => $pathlist, 'data' => "Project Updated succesfully"]);
		
	}

}
