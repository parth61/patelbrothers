<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomOrderDetails;
use App\EcomProductGallery;
use App\EcomOrderNotes;
use App\EcomOrderActivity;
use DB;
use Illuminate\Support\Facades\File;
use App\EcomProduct;

class EcomOrderNotesController extends Controller
{
    public function addordernotes(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "order_id" => "required",
            "order_notes" => "required",
            "order_notes_for" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ordernotes = new EcomOrderNotes();
            $ordernotes->order_id = $request->input('order_id');
            $ordernotes->order_notes = $request->input('order_notes');
            $ordernotes->order_notes_for = $request->input('order_notes_for');
            $ordernotes->site_id = $request->input('site_id');
            $ordernotes->created_by = $request->input('user_id');
            $ordernotes->updated_by = $request->input('user_id');  
            $ordernotes->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ordernotes->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ordernotes->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ordernotes->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ordernotes->save();
            if ($result) {

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $request->input('order_id');
                $ecomorderactivity->order_activity_type = "Order Notes Added";
                $ecomorderactivity->order_activity_details = "Order notes has been added ".$request->input('order_notes_for');
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                return response()->json(['status' => 200, 'data' => "Order notes added successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallordernotes(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $ordernotes = new EcomOrderNotes();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('order_notes_id') && $request->input('order_notes_id') != "") {
                $data['order_notes_id'] = $request->input('order_notes_id');
            }
            
            if ($request->has('order_id') && $request->input('order_id') != "") {
                $data['order_id'] = $request->input('order_id');
            }

            if ($request->has('order_notes_for') && $request->input('order_notes_for') != "") {
                $data['order_notes_for'] = $request->input('order_notes_for');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }


            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            
            $ordernotesdata = $ordernotes->getordernotes($data);

            return response()->json(['status' => 200, 'count' => count($ordernotesdata), 'data' => $ordernotesdata]);
        }

    }
    
    public function updateordernote(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "order_notes_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ordernotes = new EcomOrderNotes();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['order_notes_id','accesstoken','user_id','site_id']);
            $result = $ordernotes->where('order_notes_id', $request->input('order_notes_id'))->update($newrequest);

            if ($result) {

                $ordernotesdata = $ordernotes->where('order_notes_id', $request->input('order_notes_id'))->first();

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $ordernotesdata->order_id;
                $ecomorderactivity->order_activity_type = "Order Notes Updated";
                $ecomorderactivity->order_activity_details = "Order notes has been updated.";
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                return response()->json(['status' => 200, 'data' => "Order notes updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteordernote(Request $request) {

        $valid = Validator::make($request->all(), [
            "order_notes_id" => "required",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ordernotes = new EcomOrderNotes();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['order_notes_id','accesstoken','user_id','site_id']);
            $result = $ordernotes->where('order_notes_id', $request->input('order_notes_id'))->update($newrequest);
            if ($result) {

                $ordernotesdata = $ordernotes->where('order_notes_id', $request->input('order_notes_id'))->first();

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $ordernotesdata->order_id;
                $ecomorderactivity->order_activity_type = "Order Notes Deleted";
                $ecomorderactivity->order_activity_details = "Order notes has been deleted";
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                return response()->json(['status' => 200, 'data' => "Order notes deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
