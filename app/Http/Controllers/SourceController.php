<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Source;
use Validator;
class SourceController extends Controller
{
    public function getallsource(Request $request) {
     $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $source = new Source();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('source_id') && $request->input('source_id') != "") {
                $data['source_id'] = $request->input('source_id');
            }
            $source = $source->getsource($data);

            return response()->json(['status' => 200, 'count' => count($source), 'data' => $source]);
        }
    }

    public function getallprimarysource(Request $request) {
        $valid = Validator::make($request->all(), [
                       "limit" => "required|numeric",
                       "offset" => "required|numeric",
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
                
                $sourcemodel = new Source();
                $source = $sourcemodel->select('primary_source')->distinct()->where('status',1)->get();
   
               return response()->json(['status' => 200, 'count' => count($source), 'data' => $source]);
        }
    }
    
    public function getallsecondarysource(Request $request) {
        $valid = Validator::make($request->all(), [
                "limit" => "required|numeric",
                "offset" => "required|numeric",
                "primarysource" => "required|string"
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
                
                $sourcemodel = new Source();
                $source = $sourcemodel->select('secondary_source')->where("primary_source", $request->input('primarysource'))->where('status',1)->get();
   
               return response()->json(['status' => 200, 'count' => count($source), 'data' => $source]);
        }
    }
}
