<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\ProjectStatus;
class ProjectStatusController extends Controller
{
    public function addprojectstatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_status_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectstatus = new projectstatus();
            $projectstatus->project_status_name = $request->input('project_status_name');
            $projectstatus->created_by = $request->input('created_by');
            $projectstatus->updated_by = $request->input('updated_by');
            $projectstatus->status = $request->input('status');
            $projectstatus->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $projectstatus->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $projectstatus->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $projectstatus->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $projectstatus->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Status Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallprojectstatus(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectstatus = new projectstatus();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('project_status_id') && $request->input('project_status_id') != "") {
                $data['project_status_id'] = $request->input('project_status_id');
            }

            $projectstatus = $projectstatus->getprojectstatus($data);

            return response()->json(['status' => 200, 'count' => count($projectstatus), 'data' => $projectstatus]);
        }
    }

    public function updateprojectstatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_status_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectstatus = new projectstatus();
            $newrequest = $request->except(['project_status_id']);
              $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $projectstatus->where('project_status_id', $request->input('project_status_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Status Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteprojectstatus(Request $request) {
        $valid = Validator::make($request->all(), [
                    "project_status_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectstatus = new projectstatus();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['project_status_id']);
              $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $projectstatus->where('project_status_id', $request->input('project_status_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    
    public function manageprojectstatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_status_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->has('project_status_id') && $request->filled('project_status_id')){
                $projectstatus = new projectstatus();
                $newrequest = $request->except(['project_status_id']);
                  $request->request->add(['updated_by' =>  $request->input('updated_by')]);
                $result = $projectstatus->where('project_status_id', $request->input('project_status_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Project Status Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $projectstatus = new projectstatus();
                $projectstatus->project_status_name = $request->input('project_status_name');
                $projectstatus->created_by = $request->input('created_by');
                $projectstatus->updated_by = $request->input('updated_by');
                $projectstatus->status = $request->input('status');
                $projectstatus->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $projectstatus->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $projectstatus->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $projectstatus->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $projectstatus->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Project Status Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
        }
    }
}
