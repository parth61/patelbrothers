<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomAddress;
class EcomAddressController extends Controller
{
    // add address for ecom customer
    public function addaddress(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "address_type" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $address = new EcomAddress();
            $addresslist = $address->where('address_type', $request->input('address_type'))->where('status', 1)->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->get();

            if(count($addresslist)>0){
                $address = new EcomAddress();
                $newrequest = $request->except(['address_id','user_id','accesstoken','site_id','address_type']);
                $result = $address->where('address_id', $request->input('address_id'))->where('site_id', $request->input('site_id'))->update($newrequest);
                
                if ($result) {
                    $address = $address->where('address_id', $request->input('address_id'))->where('site_id', $request->input('site_id'))->first();
                    return response()->json(['status' => 200, 'data' => $address]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Missing address ID."], 400);
                }
                
            }else{
                $address = new EcomAddress();
                $address->address_type = $request->input('address_type');
                $address->user_id = $request->input('user_id');
                $address->city = $request->input('city');
                $address->state = $request->input('state');
                $address->country = $request->input('country');
                $address->pincode = $request->input('pincode');
                $address->address = $request->input('address');
                $address->landmark = $request->input('landmark');
                $address->isprimary = $request->input('isprimary');
                $address->created_by = $request->input('user_id');
                
                $address->site_id = $request->has('site_id')?$request->input('site_id'):null;
                $address->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $address->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $address->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $address->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $address->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => $address]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                } 
                
            }
        }
    }

    //get all addresses for ecom customer
    public function getalladdress(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "user_id"=>"required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $address = new EcomAddress();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('address_id') && $request->input('address_id') != "") {
                $data['address_id'] = $request->input('address_id');
            }

            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }

            if ($request->has('address_type') && $request->input('address_type') != "") {
                $data['address_type'] = $request->input('address_type');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
         
            $address = $address->getaddress($data);

            return response()->json(['status' => 200, 'count' => count($address), 'data' => $address]);
        }

    }

    //update address 
    public function updateaddress(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "address_id" => "required",
                 "site_id" => "required",
                 "user_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $address = new EcomAddress();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['address_id','user_id','accesstoken']);
            $result = $address->where('address_id', $request->input('address_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Adderss updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    //delete address 
    public function deleteaddress(Request $request) {

        $valid = Validator::make($request->all(), [
                    "address_id" => "required",
                    "accesstoken" => "required",
                    "user_id" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $address = new EcomAddress();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['address_id','user_id','accesstoken']);
            $result = $address->where('address_id', $request->input('address_id'))->where('site_id', $request->input('site_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  

}
