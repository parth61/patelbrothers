<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Regions;

class RegionsController extends Controller
{

    public function addregion(Request $request) {
        //validations
        $region = new Regions();
        $valid = Validator::make($request->all(), [
                    "region" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $region->region = $request->input('region');
            $region->created_by = $request->input('created_by');
            $region->updated_by = $request->input('updated_by');
            $region->status = $request->input('status');
            $region->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $region->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $region->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $region->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $region->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Region added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function getallregions(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $region = new Regions();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('region_id') && $request->input('region_id') != "") {
                $data['region_id'] = $request->input('region_id');
            }
            $regionlist = $region->getregion($data);

            return response()->json(['status' => 200, 'count' => count($regionlist), 'data' => $regionlist]);
        }
    }
    public function updateregion(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "region_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $region = new Regions();
            $newrequest = $request->except(['region_id']);
              $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $region->where('region_id', $request->input('region_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Region Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteregion(Request $request) {
        $valid = Validator::make($request->all(), [
                    "region_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $region = new Regions();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['region_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $region->where('region_id', $request->input('region_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
