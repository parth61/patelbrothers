<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomPayments;
use App\User;
use App\EcomBusinessSetting;

/* Main Order */
use App\EcomOrder;
use App\EcomOrderTax;
use App\EcomOrderDetails;
use App\EcomOrderItems;
use App\EcomOrderItemsTax;
use App\EcomOrderNotes;
use App\EcomOrderActivity;

/* Temp Order */
use App\EcomTempOrder;
use App\EcomTempOrderTax;
use App\EcomTempOrderDetails;
use App\EcomTempOrderItems;
use App\EcomTempOrderItemsTax;
use App\EcomTempOrderNotes;
use App\EcomTempOrderActivity;

/* Processed Order */ 
use App\EcomAddress;
use App\EcomCart;
use App\EcomProduct;
use DB;
use Mail;
use Softon\Indipay\Facades\Indipay;
use Illuminate\Support\Facades\Storage;
use Razorpay\Api\Api;
class EcomPaymentController extends Controller
{
    // add address for ecom customer
    public function addpayment(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",
            "order_id" => "required",
            "payment_type" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $txn_id = generateAccessToken(25);
            $user = new User();
            $userdata = $user->where('user_id', $request->input('user_id'))->first();

            $order = new EcomOrder();
            $orderdata = $order->where('order_id', $request->input('order_id'))->first();

            /* Update Billing Address */
            $ecomaddress = new EcomAddress();
            $billingaddresscount = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->count();
            if($billingaddresscount>0){
                $updateorderdetails = array();
                $updateorderdetails['address'] = ($request->has('billing_address')?$request->input('billing_address'):null);
                $updateorderdetails['landmark'] = ($request->has('billing_landmark')?$request->input('billing_landmark'):null);
                $updateorderdetails['city'] = ($request->has('billing_city')?$request->input('billing_city'):null);
                $updateorderdetails['state'] = ($request->has('billing_state')?$request->input('billing_state'):null);
                $updateorderdetails['country'] = ($request->has('billing_country')?$request->input('billing_country'):null);
                $updateorderdetails['pincode'] = ($request->has('billing_pincode')?$request->input('billing_pincode'):null);
                $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->update($updateorderdetails);
            }else{
                $ecomaddress->site_id = $request->input('site_id');
                $ecomaddress->user_id = $request->input('user_id');
                $ecomaddress->address_type = 'billing';
                $ecomaddress->address = ($request->has('billing_address')?$request->input('billing_address'):null);
                $ecomaddress->landmark = ($request->has('billing_landmark')?$request->input('billing_landmark'):null);
                $ecomaddress->city = ($request->has('billing_city')?$request->input('billing_city'):null);
                $ecomaddress->state = ($request->has('billing_state')?$request->input('billing_state'):null);
                $ecomaddress->country = ($request->has('billing_country')?$request->input('billing_country'):null);
                $ecomaddress->pincode = ($request->has('billing_pincode')?$request->input('billing_pincode'):null);
                    $ecomaddress->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $ecomaddress->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $ecomaddress->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $ecomaddress->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $ecomaddress->save();
            }

            /* Add Order notes */
            if($request->has('order_notes')){

                $ecomodernotes = new EcomOrderNotes();
                $ecomodernotes->order_id = $request->input('order_id');
                $ecomodernotes->site_id = $request->input('site_id');
                $ecomodernotes->order_notes = $request->input('order_notes');
                $ecomodernotes->order_notes_for = 'forcustomer';
                $ecomodernotes->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $ecomodernotes->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $ecomodernotes->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $ecomodernotes->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $ecomodernotes->save();

            }
            


            /* Update shipping Address */
            $ecomaddress = new EcomAddress();
            $billingaddresscount = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->count();
            if($billingaddresscount>0){
                $updateorderdetails = array();
                $updateorderdetails['address'] = ($request->has('shipping_address') && $request->input('shipping_address') != "")?$request->input('shipping_address'):$request->input('billing_address');
                $updateorderdetails['landmark'] = ($request->has('shipping_landmark') && $request->input('shipping_landmark') != "")?$request->input('shipping_landmark'):$request->input('billing_landmark');
                $updateorderdetails['city'] = ($request->has('shipping_city') && $request->input('shipping_city') != "")?$request->input('shipping_city'):$request->input('billing_city');
                $updateorderdetails['state'] = ($request->has('shipping_state') && $request->input('shipping_state') != "")?$request->input('shipping_state'):$request->input('billing_state');
                $updateorderdetails['country'] = ($request->has('shipping_country') && $request->input('shipping_country') != "")?$request->input('shipping_country'):$request->input('billing_country');
                $updateorderdetails['pincode'] = ($request->has('shipping_pincode') && $request->input('shipping_pincode') != "")?$request->input('shipping_pincode'):$request->input('billing_pincode');
                $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->update($updateorderdetails);
            }else{
                $ecomaddress->site_id = $request->input('site_id');
                $ecomaddress->user_id = $request->input('user_id');
                $ecomaddress->address_type = 'shipping';
                $ecomaddress->address = ($request->has('shipping_address') && $request->input('shipping_address') != "")?$request->input('shipping_address'):$request->input('billing_address');
                $ecomaddress->landmark = ($request->has('shipping_landmark') && $request->input('shipping_landmark') != "")?$request->input('shipping_landmark'):$request->input('billing_landmark');
                $ecomaddress->city = ($request->has('shipping_city') && $request->input('shipping_city') != "")?$request->input('shipping_city'):$request->input('billing_city');
                $ecomaddress->state = ($request->has('shipping_state') && $request->input('shipping_state') != "")?$request->input('shipping_state'):$request->input('billing_state');
                $ecomaddress->country = ($request->has('shipping_country') && $request->input('shipping_country') != "")?$request->input('shipping_country'):$request->input('billing_country');
                $ecomaddress->pincode = ($request->has('shipping_pincode') && $request->input('shipping_pincode') != "")?$request->input('shipping_pincode'):$request->input('billing_pincode');
                $ecomaddress->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ecomaddress->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ecomaddress->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ecomaddress->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $ecomaddress->save();
            }

            /* Take latest address */
            $ecomaddress = new EcomAddress();
            $ecomaddressbillingsdata = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->first();
            $ecomaddressshippingsdata = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->first();

            $ecomorderdetails = new EcomOrderDetails();
            $updateorderdetails = array();
            if($ecomaddressbillingsdata != null){
                $updateorderdetails['billing_address'] = $ecomaddressbillingsdata->address;
                $updateorderdetails['billing_landmark'] = $ecomaddressbillingsdata->landmark;
                $updateorderdetails['billing_city'] = $ecomaddressbillingsdata->city;
                $updateorderdetails['billing_state'] = $ecomaddressbillingsdata->state;
                $updateorderdetails['billing_country'] = $ecomaddressbillingsdata->country;
                $updateorderdetails['billing_pincode'] = $ecomaddressbillingsdata->pincode;
            }
            if($ecomaddressshippingsdata != null){
                $updateorderdetails['shipping_address'] = $ecomaddressshippingsdata->address;
                $updateorderdetails['shipping_landmark'] = $ecomaddressshippingsdata->landmark;
                $updateorderdetails['shipping_city'] = $ecomaddressshippingsdata->city;
                $updateorderdetails['shipping_state'] = $ecomaddressshippingsdata->state;
                $updateorderdetails['shipping_country'] = $ecomaddressshippingsdata->country;
                $updateorderdetails['shipping_pincode'] = $ecomaddressshippingsdata->pincode;
            }
            $updateorderdetails['user_phone'] = $request->has('checkout_user_mobile')?$request->input('checkout_user_mobile'):$userdata->user_mobile_code.$userdata->user_mobile;
            $updateorderdetails['user_first_name'] = $userdata->user_firstname;
            $updateorderdetails['user_last_name'] = $userdata->user_lastname;

            $ecomorderdetails->where('order_id', $request->input('order_id'))->update($updateorderdetails);

            /* cash on delivery */
            if($request->input('payment_type')=="cashondelivery"){
                
                /* Add Payment */
                $payment = new EcomPayments();
                $payment->order_id = $request->input('order_id');
                $payment->user_id = $request->input('user_id');
                $payment->site_id = $request->input('site_id');
                $payment->txn_id = $txn_id;
                $payment->amount = $orderdata->order_grandtotal;
                $payment->user_name = $userdata->user_firstname;
                $payment->user_email = $userdata->user_email;
                $payment->user_phone = $userdata->user_mobile;
                $payment->payment_type = $request->input('payment_type');
                $payment->product_info = "productinfo";
                $payment->payment_status = 'success';
                $payment->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $payment->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $payment->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $payment->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $payment->save();

                if($result){

                    $ecomcart = new EcomCart();
                    $ecomcart->where('user_id', $request->input('user_id'))->where('status', 1)->update(
                        array(
                            'status' => 2
                        )
                    );
                }

                /* Manage Order Status */
                $data = array();
                $data['order_status'] = 'success'; 
                $order = new EcomOrder();
                $order->where('order_id', $request->input('order_id'))->update($data);

                return response()->json(['status' => 200, 'data' => $payment]);
			}
            /* online payment */
            else if($request->input('payment_type')=="onlinepayment"){
                $payment = new EcomPayments();
                $payment->order_id = $request->input('order_id');
                $payment->user_id = $request->input('user_id');
                $payment->site_id = $request->input('site_id');
                $payment->txn_id = $txn_id;
                $payment->amount = $orderdata->order_grandtotal;
                $payment->user_name = $userdata->user_firstname;
                $payment->user_email = $userdata->user_email;
                $payment->user_phone = $userdata->user_mobile;
                $payment->product_info = "productinfo";
                $payment->payment_type = $request->input('payment_type');
                $payment->payment_status = 'initiated';
                $payment->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $payment->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $payment->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $payment->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $payment->save();
                if ($result) {
					
                    $parameters = [
                        'txnid' => $txn_id,
                        'order_id' => $request->input('order_id'),
                        'amount' => $orderdata->order_grandtotal,
                        'firstname' => $userdata->user_firstname,
                        'lastname' => $userdata->user_lastname,
                        'email' => $userdata->user_email,
                        'phone' => $userdata->user_mobile,
                        'productinfo' => '123prod',
                        'service_provider' => '',
                        'zipcode' => $ecomaddressshippingsdata->pincode,
                        'city' => $ecomaddressshippingsdata->city,
                        'state' => $ecomaddressshippingsdata->state,
                        'country' => $ecomaddressshippingsdata->country,
                        'address1' => $ecomaddressshippingsdata->address,
                        'address2' => $ecomaddressshippingsdata->landmark,
                        'curl' => url('updatepaymentbypayu'),
                      ];

                    /* Record Order Activity */
                    $ecomorderactivity = new EcomOrderActivity();
                    $ecomorderactivity->order_id = $request->input('order_id');
                    $ecomorderactivity->order_activity_type = "Payment Initiated";
                    $ecomorderactivity->order_activity_details = "Payment process has been initiated.";
                    $ecomorderactivity->site_id = $request->input('site_id');
                    $ecomorderactivity->created_by = $request->input('user_id');
                    $ecomorderactivity->updated_by = $request->input('user_id');
                    $result = $ecomorderactivity->save();
                    
                      
                    return response()->json(['status' => 200, 'data' => $parameters]);

                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong2."], 400);
                }
            }else{
                return response()->json(['status' => 400, 'error' => "Invalid payment method."], 400);
            }

        }
    }

    public function payumoney($params) {
        $MERCHANT_KEY = "SVJDZrAu";
        $SALT = "mfAMHeUjkE";

        //$PAYU_BASE_URL = "https://sandboxsecure.payu.in";       // For Sandbox Mode
        $PAYU_BASE_URL = "https://secure.payu.in";            // For Production Mode

          $action ='';
        $posted = array();
        if(!empty($params)) {
            foreach($params as $key => $value) {    
                $posted[$key] = $value;  
            }
        }
        $posted['key'] = $MERCHANT_KEY;
        $formError = 0;

        if(empty($posted['txnid'])) {
        // Generate random transaction id
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
        $txnid = $posted['txnid'];
        }
        $hash = '';
        // Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($posted['hash']) && sizeof($posted) > 0) {
        if(
                empty($posted['key'])
                || empty($posted['txnid'])
                || empty($posted['amount'])
                || empty($posted['firstname'])
                || empty($posted['email'])
                || empty($posted['phone'])
                || empty($posted['productinfo'])
                || empty($posted['surl'])
                || empty($posted['furl'])
                || empty($posted['service_provider'])
        ) {
            $formError = 1;
        } else {
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';  
            foreach($hashVarsSeq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
            }

            $hash_string .= $SALT;


            $hash = strtolower(hash('sha512', $hash_string));
            $action = $PAYU_BASE_URL . '/_payment';
        }
        } elseif(!empty($posted['hash'])) {
        $hash = $posted['hash'];
        $action = $PAYU_BASE_URL . '/_payment';
        }
        return view('payments.payumoney')->with('MERCHANT_KEY', $MERCHANT_KEY)->with('hash', $hash)->with('action', $action)->with('posted', $posted)->with('formError', $formError);

    }

    //get all addresses for ecom customer
    public function payumoney_response(Request $request) {
        dd($_POST);
        $status=$_POST["status"];
        $firstname=$_POST["firstname"];
        $amount=$_POST["amount"];
        $txnid=$_POST["txnid"];
        $posted_hash=$_POST["hash"];
        $key=$_POST["key"];
        $productinfo=$_POST["productinfo"];
        $email=$_POST["email"];
        $salt="mfAMHeUjkE";

        // Salt should be same Post Request 
        if($_POST["status"] == 'success' && $_POST['unmappedstatus'] == 'captured') {
            echo "<h3>Thank You. Your order status is ". $status .".</h3>";
            echo "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";
            echo "<h4>We have received a payment of Rs. " . $amount . ". Your order will soon be shipped.</h4>";
        } else {
            echo "<h3>Thank You. Your order status is ". $status .".</h3>";
            echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
        }
    }
    
    public function getallpaymentprocessinfo(Request $request) {

        $valid = Validator::make($request->all(), [
            "txnid" => "required|numeric",
            "site_id" => "required",
            "user_id"=>"required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $data = DB::table('tbl_ecom_payments as tep')->select('tep.*','us.*','order.*')
                ->leftJoin('tbl_user as us', 'us.user_id', '=', 'us.user_id')
                ->leftJoin('tbl_ecom_order as order', 'order.order_id', '=', 'tep.user_id')
                ->where('tep.txn_id', $request->input('txn_id'))
                ->where('tep.order_id', $request->input('order_id'))
                ->where('tep.user_id', $request->input('user_id'))->get();

            return response()->json(['status' => 200, 'count' => count($data), 'data' => $data]);
        }
    }

    //get all addresses for ecom customer
    public function getallpayment(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $payment = new EcomPayments();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('payment_id') && $request->input('payment_id') != "") {
                $data['payment_id'] = $request->input('payment_id');
            }

            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }

            if ($request->has('payment_status') && $request->input('payment_status') != "") {
                $data['payment_status'] = $request->input('payment_status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

			if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
         
            $address = $payment->getpayments($data);

            return response()->json(['status' => 200, 'count' => count($address), 'data' => $address]);
        }
    }

    //update address 
    public function updatepaymentbytxnid(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "txn_id" => "required",
            "site_id" => "required",
            "status" => "required",
            "bank_ref_num" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $payment = new EcomPayments();
            $newrequest = array();
            $newrequest['txn_id'] = $request->input('txn_id');
            $newrequest['payment_status'] = $request->input('status');
            $newrequest['payment_obj'] = $request->input('bank_ref_num');
            $result = $payment->where('txn_id', $request->input('txn_id'))->update($newrequest);

            if ($result) {

                $status = false;
                $payment = new EcomPayments();
                $paymentobj = $payment->where('txn_id', $request->input('txn_id'))->get();

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $paymentobj[0]->order_id;
                $ecomorderactivity->order_activity_type = "Payment Updated";
                $ecomorderactivity->order_activity_details = "Payment has been ".$request->input('status')." & Reference number : ".$request->input('bank_ref_num')." & Transaction No: ".$request->input('txn_id');
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                $ecomorder = new EcomOrder();
                
                if($request->input('status') === "success"){
                    $order_id = (count($paymentobj)>0)?$paymentobj[0]->order_id:"";

                    $ecomcart = new EcomCart();
                    $ecomcart->where('user_id', $paymentobj[0]->user_id)->where('status', 1)->update(
                        array(
                            'status' => 2
                        )
                    );

                    $user = new User();
                    $businessinfo = new EcomBusinessSetting();
                    $order = new EcomOrder();
                    $orderdetail = new EcomOrderDetails();
                    $orderitems = new EcomOrderItems();
                    $ecomaddress = new EcomAddress();
                    $ordernotes = new EcomOrderNotes();
                    $ordertax = new EcomOrderTax();

                    $content["userinfo"] = $user->where('user_id', $paymentobj[0]->user_id)->first();
                    $content["billingaddress"] = $ecomaddress->where('address_type', 'billing')->where('user_id', $paymentobj[0]->user_id)->first();
                    $content["shippingaddress"] = $ecomaddress->where('address_type', 'shipping')->where('user_id', $paymentobj[0]->user_id)->first();
                    $content["businessinfo"] = $businessinfo->where('site_id', $request->input('site_id'))->first();
                    $content['order'] = $order->where('order_id', $order_id)->first();
                    $content['ordertax'] = $ordertax->where('order_id', $order_id)->where('status', 1)->get();
                    $content['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();
                    $content['ordernotes'] = $ordernotes->where('order_id', $order_id)->where('order_notes_for', 'forcustomer')->whereNotNull('order_notes')->where('status', 1)->pluck('order_notes');
                    /* Generate Order Items */
                    $orderdata = $orderitems->where('order_id', $order_id)->get();
                    foreach($orderdata as $oitem){
                        $taxdata = array(
                            "order_item_id" => $oitem->order_item_id
                        );
                        $ecomorderitemtax = new EcomOrderItemsTax();
                        $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);

                        /* Update Stock */
                        $productmodel = new EcomProduct();
                        $productdata = $productmodel->where('product_id', $oitem->product_id)->first();
                        if($productdata->is_stockmanagement == "1"){
                            $currentstock = (int)$productdata->product_stock - 1;
                            if($currentstock < 1){
                                $productmodel->where('product_id', $oitem->product_id)->update(
                                    array(
                                        "product_stock" => $currentstock,
                                        "stock_status" => 0
                                    )
                                );
                            }else{
                                $productmodel->where('product_id', $oitem->product_id)->update(
                                    array(
                                        "product_stock" => $currentstock
                                    )
                                );
                            }
                        }

                    }
                    $content['orderitems'] = $orderdata;
                    /* Generate Payment Info */
                    $tempdata['offset'] = 0;
                    $tempdata['limit'] = 100;
                    $tempdata['order_id'] =  $order_id;
                    $temp = $payment->getpayments($tempdata);
                    $content['paymentinfo'] = count($temp)>0?$temp[0]:null;

                    /* Generate Invoice No */
                    $updatedata = array();
                    $updatedata['order_status'] = 'processing';
                    $ecomorder->where('order_id', $order_id)->update($updatedata);

                    generateinvoice($order_id, $content['order']->order_invoice_no, $content);

                    /* Send order Email */
                    $emailstatus = orderConfirmEmail($content, $content["userinfo"]->user_email);
                    if($emailstatus){
                        $logdata = "Order Success : #".$order_id.", Customer Email sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";  
                    }else{
                         $logdata = "Order Success : #".$order_id.", Customer Email is Not sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n"; 
                    }
                    
                    $emailstatus = orderConfirmEmail($content, $content["businessinfo"]->business_admin_email);
                    if($emailstatus){
                        $logdata = "Order Success : #".$order_id.", Admin Email sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";
                    }else{
                        $logdata = "Order Success : #".$order_id.", Admin Email Not sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";
                    }
                    

                    $logdata = $logdata . json_encode($content);
                    Storage::append('order-sucess-'.$request->input('site_id').'.txt', $logdata."\n");

                }else{
                    $order_id = (count($paymentobj)>0)?$paymentobj[0]->order_id:"";
                    $updatedata = array();
                    $updatedata['order_status'] = 'failed';
                    $ecomorder->where('order_id', $order_id)->update($updatedata);

                    /* Record Order Activity */
                    $ecomorderactivity = new EcomOrderActivity();
                    $ecomorderactivity->order_id = $order_id;
                    $ecomorderactivity->order_activity_type = "Payment Updated";
                    $ecomorderactivity->order_activity_details = "Payment has been failed.";
                    $ecomorderactivity->site_id = $request->input('site_id');
                    $ecomorderactivity->created_by = $request->input('user_id');
                    $ecomorderactivity->updated_by = $request->input('user_id');
                    $result = $ecomorderactivity->save();

                    $ecomorderprocessed_model = new EcomOrderProcessed();
                    $ecomorderprocessed_model->order_id = $paymentobj[0]->order_id;
                    $ecomorderprocessed_model->order_invoice_id = null;
                    $ecomorderprocessed_model->order_status = "failed";
                    $ecomorderprocessed_model->save();

                }
                
                return response()->json(['status' => 200, 'data' => "Payment updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }



    ///update paymentbyrazoropayment
    public function updatepaymentbyrazorpaytxnid(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "txn_id" => "required",
            "site_id" => "required",
            "status" => "required",
            "bank_ref_num" => "required",
            "razorpay_signature" => "required",
            "razorpay_payment_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $payment = new EcomPayments();
            $newrequest = array();
            $newrequest['txn_id'] = $request->input('txn_id');
            $newrequest['payment_status'] = $request->input('status');
            $newrequest['payment_obj'] = $request->input('bank_ref_num');
            $result = $payment->where('txn_id', $request->input('txn_id'))->update($newrequest);

            if ($result) {

                $status = false;
                $payment = new EcomPayments();
                $paymentobj = $payment->where('txn_id', $request->input('txn_id'))->get();

                /* Record Order Activity */
                $api = new Api(razorpaykeyid, razorpaykeysecret);
                $attributes  = array('razorpay_signature'  =>  $request->input('razorpay_signature'),  'razorpay_payment_id'  =>  $request->input('razorpay_payment_id') ,  'razorpay_order_id' => $request->input('razorpay_order_id'));
                $order  = $api->utility->verifyPaymentSignature($attributes);
                
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $paymentobj[0]->order_id;
                $ecomorderactivity->order_activity_type = "Payment Updated";
                $ecomorderactivity->order_activity_details = "Payment has been ".$request->input('status')." & Reference number : ".$request->input('bank_ref_num')." & Transaction No: ".$request->input('txn_id');
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                $ecomorder = new EcomOrder();
                
                if($request->input('status') === "success"){
                    $order_id = (count($paymentobj)>0)?$paymentobj[0]->order_id:"";

                    $ecomcart = new EcomCart();
                    $ecomcart->where('user_id', $paymentobj[0]->user_id)->where('status', 1)->update(
                        array(
                            'status' => 2
                        )
                    );

                    $user = new User();
                    $businessinfo = new EcomBusinessSetting();
                    $order = new EcomOrder();
                    $orderdetail = new EcomOrderDetails();
                    $orderitems = new EcomOrderItems();
                    $ecomaddress = new EcomAddress();
                    $ordernotes = new EcomOrderNotes();
                    $ordertax = new EcomOrderTax();

                    $content["userinfo"] = $user->where('user_id', $paymentobj[0]->user_id)->first();
                    $content["billingaddress"] = $ecomaddress->where('address_type', 'billing')->where('user_id', $paymentobj[0]->user_id)->first();
                    $content["shippingaddress"] = $ecomaddress->where('address_type', 'shipping')->where('user_id', $paymentobj[0]->user_id)->first();
                    $content["businessinfo"] = $businessinfo->where('site_id', $request->input('site_id'))->first();
                    $content['order'] = $order->where('order_id', $order_id)->first();
                    $content['ordertax'] = $ordertax->where('order_id', $order_id)->where('status', 1)->get();
                    $content['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();
                    $content['ordernotes'] = $ordernotes->where('order_id', $order_id)->where('order_notes_for', 'forcustomer')->whereNotNull('order_notes')->where('status', 1)->pluck('order_notes');
                    /* Generate Order Items */
                    $orderdata = $orderitems->where('order_id', $order_id)->get();
                    foreach($orderdata as $oitem){
                        $taxdata = array(
                            "order_item_id" => $oitem->order_item_id
                        );
                        $ecomorderitemtax = new EcomOrderItemsTax();
                        $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);

                        /* Update Stock */
                        $productmodel = new EcomProduct();
                        $productdata = $productmodel->where('product_id', $oitem->product_id)->first();
                        if($productdata->is_stockmanagement == "1"){
                            $currentstock = (int)$productdata->product_stock - 1;
                            if($currentstock < 1){
                                $productmodel->where('product_id', $oitem->product_id)->update(
                                    array(
                                        "product_stock" => $currentstock,
                                        "stock_status" => 0
                                    )
                                );
                            }else{
                                $productmodel->where('product_id', $oitem->product_id)->update(
                                    array(
                                        "product_stock" => $currentstock
                                    )
                                );
                            }
                        }

                    }
                    $content['orderitems'] = $orderdata;
                    /* Generate Payment Info */
                    $tempdata['offset'] = 0;
                    $tempdata['limit'] = 100;
                    $tempdata['order_id'] =  $order_id;
                    $temp = $payment->getpayments($tempdata);
                    $content['paymentinfo'] = count($temp)>0?$temp[0]:null;

                    /* Generate Invoice No */
                    $updatedata = array();
                    $updatedata['order_status'] = 'processing';
                    $ecomorder->where('order_id', $order_id)->update($updatedata);

                    generateinvoice($order_id, $content['order']->order_invoice_no, $content);

                    /* Send order Email */
                    $emailstatus = orderConfirmEmail($content, $content["userinfo"]->user_email);
                    $logdata = "Order Success : #".$order_id.", Customer Email sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";
                    $emailstatus = orderConfirmEmail($content, $content["businessinfo"]->business_admin_email);
                    $logdata = "Order Success : #".$order_id.", Admin Email sent : " . $emailstatus. " " .date("Y-m-d H:i:s")."\n";

                    $logdata = $logdata . json_encode($content);
                    Storage::append('order-sucess-'.$request->input('site_id').'.txt', $logdata."\n");

                }else{
                    $order_id = (count($paymentobj)>0)?$paymentobj[0]->order_id:"";
                    $updatedata = array();
                    $updatedata['order_status'] = 'failed';
                    $ecomorder->where('order_id', $order_id)->update($updatedata);

                    /* Record Order Activity */
                    $ecomorderactivity = new EcomOrderActivity();
                    $ecomorderactivity->order_id = $order_id;
                    $ecomorderactivity->order_activity_type = "Payment Updated";
                    $ecomorderactivity->order_activity_details = "Payment has been failed.";
                    $ecomorderactivity->site_id = $request->input('site_id');
                    $ecomorderactivity->created_by = $request->input('user_id');
                    $ecomorderactivity->updated_by = $request->input('user_id');
                    $result = $ecomorderactivity->save();

                    $ecomorderprocessed_model = new EcomOrderProcessed();
                    $ecomorderprocessed_model->order_id = $paymentobj[0]->order_id;
                    $ecomorderprocessed_model->order_invoice_id = null;
                    $ecomorderprocessed_model->order_status = "failed";
                    $ecomorderprocessed_model->save();

                }
                
                return response()->json(['status' => 200, 'data' => "Payment updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }
    //update address 
    public function updatepaymentbypayu(Request $request) {

        $response = Indipay::response($request);
        if($response['status'] =="success" && $response['unmappedstatus'] =="captured"){

            $payment = new EcomPayments();
            $newrequest = array();
            $newrequest['txn_id'] = $response['txn_id'];
            $newrequest['payment_status'] = $response['status'];
            $newrequest['payment_obj'] = json_encode($response);
            $result = $payment->where('txn_id', $response['txn_id'])->update($newrequest);

            $payment = new EcomPayments();
            $paymentobj = $payment->where('txn_id', $response['txn_id'])->get();

            $ecomorder = new EcomOrder();
            
            if($request->input('status') === "success"){
                $order_id = (count($paymentobj)>0)?$paymentobj[0]->order_id:"";
                $updatedata = array();
                $updatedata['order_status'] = 'processing';
                $updatedata['order_invoice_no'] = md5($order_id);
                $ecomorder->where('order_id', $order_id)->update($updatedata);
            }else{
                $order_id = (count($paymentobj)>0)?$paymentobj[0]->order_id:"";
                $updatedata = array();
                $updatedata['order_status'] = 'failed';
                $ecomorder->where('order_id', $order_id)->update($updatedata);
            }
              
            return redirect(site_url.'/order-status?status=success');
        }else{
            return redirect(site_url.'/order-status?status=failure');
        }

    }

    //update address 
    public function updatepayment(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "payment_id" => "required",
                 "site_id" => "required",
                 "user_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            /* Update and validate payment */
            $payment = new EcomPayments();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['payment_id','user_id','accesstoken']);
            $result = $address->where('payment_id', $request->input('payment_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Payment updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }
	
	function preparePayuPaymentForApp(Request $request){
		
		$valid = Validator::make($request->all(), [
                 "txnid" => "required",
                 "amount" => "required",
                 "productinfo" => "required",
                 "firstname" => "required",
				 "email" => "required",
				 "phone" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
			
			$MERCHANT_KEY = "SVJDZrAu";
			$SALT = "mfAMHeUjkE";
			$params = array(
			   "key" => $MERCHANT_KEY,
			   "txnid" => $request->input("txnid"),
			   "amount" => $request->input("amount"),
			   "SALT_KEY" => $SALT,
			   "productinfo" => $request->input("productinfo"),
			   "firstname" => $request->input("firstname"),
			   "email" => $request->input("email"),
			   "phone"=> $request->input("phone"),
			   "udf1" => "",
			   "udf2" => "",
			   "udf3" => "",
			   "udf4" => "",
			   "udf5" => "",
			   "udf6" => "",
			   "udf7" => "",
			   "udf8" => "",
			   "udf9" => "",
			   "udf10" => ""
			);
			
			/* function to generate payment */ 
			
			$SS_URL = (isset($params["SS_URL"]))? $params["SS_URL"] : "";
			$FF_URL = (isset($params["FF_URL"]))? $params["FF_URL"] : "";
			$params["key"] = $MERCHANT_KEY;    
			
			$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
			
			$formError = 0;
			$txnid = "";
			if(empty($params['txnid']))
			{
			  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			}
			else
			{
			  $txnid = $params['txnid'];
			}    
			
			$params['txnid'] = $txnid;
			$hash = '';
			
			if(empty($params['hash']) && sizeof($params) > 0)
			{
				if(empty($params['txnid']) || empty($params['amount']) || empty($params['firstname']) || empty($params['email']) || empty($params['phone']) || empty($params['productinfo']))
				{
					$formError = 1;
				}
				else
				{
					$hashVarsSeq = explode('|', $hashSequence);
					$hash_string = '';    
					foreach($hashVarsSeq as $hash_var)
					{
						  $hash_string .= isset($params[$hash_var]) ? $params[$hash_var] : '';
						  $hash_string .= '|';
					  }
					$hash_string .= $params["SALT_KEY"];
					$hash = strtolower(hash('sha512', $hash_string));
				}
			}
			else if(!empty($params['hash']))
			{
				$hash = $params['hash'];
			}
			
			$params["hash"] = $hash;
			
			return response()->json(['status' => 200, 'form_status'=>$formError,'data' => $params]);
			
			/*return array("status"=>$formError, "params"=>$params);
			$returnData = preparePayuPayment($allParmas);
			echo json_encode($returnData);*/
		}
	}
	
}