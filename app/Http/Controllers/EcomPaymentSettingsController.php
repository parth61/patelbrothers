<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomPaymentSettings;
class EcomPaymentSettingsController extends Controller
{
    
    public function addpaymentsettings(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
                $ecomPaymentSettings = new EcomPaymentSettings();
                $ecomPaymentSettings->site_id = $request->has('site_id')?$request->input('site_id'):null;
                $ecomPaymentSettings->gateway_name = $request->has('gateway_name')?$request->input('gateway_name'):null;
                $ecomPaymentSettings->sendbox_url = $request->has('sendbox_url')?$request->input('sendbox_url'):null;
                $ecomPaymentSettings->sendbox_key = $request->has('sendbox_key')?$request->input('sendbox_key'):null;
                $ecomPaymentSettings->sendbox_secret = $request->has('sendbox_secret')?$request->input('sendbox_secret'):null;
                $ecomPaymentSettings->production_key = $request->has('production_key')?$request->input('production_key'):null;
                $ecomPaymentSettings->created_by = $request->has('created_by')?$request->input('created_by'):null;
                $ecomPaymentSettings->updated_by = $request->has('updated_by')?$request->input('updated_by'):null;
                $ecomPaymentSettings->production_secret = $request->has('production_secret')?$request->input('production_secret'):null;
                $ecomPaymentSettings->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $ecomPaymentSettings->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $ecomPaymentSettings->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $ecomPaymentSettings->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $ecomPaymentSettings->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => $ecomPaymentSettings]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                } 
                
            }
        }
   

    //get all addresses for ecom customer
    public function getallpaymentsettings(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $ecomPaymentSettings = new EcomPaymentSettings();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('ecom_payment_settings_id') && $request->input('ecom_payment_settings_id') != "") {
                $data['ecom_payment_settings_id'] = $request->input('ecom_payment_settings_id');
            }

         

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
         
            $ecomPaymentSettings = $ecomPaymentSettings->getpaymentsettings($data);

            return response()->json(['status' => 200, 'count' => count($ecomPaymentSettings), 'data' => $ecomPaymentSettings]);
        }

    }

    //update address 
    public function updatepaymentsettings(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "ecom_payment_settings_id" => "required",
                 "site_id" => "required",
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $ecomPaymentSettings = new EcomPaymentSettings();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['ecom_payment_settings_id','user_id','accesstoken']);
            $result = $ecomPaymentSettings->where('ecom_payment_settings_id', $request->input('ecom_payment_settings_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Payment updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    //delete address 
    public function deletepaymentsettings(Request $request) {

        $valid = Validator::make($request->all(), [
                    "ecom_payment_settings_id" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $ecomPaymentSettings = new EcomPaymentSettings();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['ecom_payment_settings_id','user_id','accesstoken']);
            $result = $ecomPaymentSettings->where('ecom_payment_settings_id', $request->input('ecom_payment_settings_id'))->where('site_id', $request->input('site_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  

}
