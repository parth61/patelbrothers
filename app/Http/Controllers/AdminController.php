<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use App\Admin;
class AdminController extends Controller
{
     public function checklogin(Request $request) {
        $res = DB::table('tbl_builder')
                ->where('builder_email', $request->input('builder_email'))
                ->where('builder_password', $request->input('builder_password'))
                ->where('status', 1)
                //->where('urid', 2)
                //->orWhere('urid', 3)
                ->get();
        if (count($res) > 0) {
            $newresult = $res[0];
            $user->where('user_id', $newresult->user_id)->update(array('accesstoken' => generateAccessToken()));
            $newresult = $user->where('user_id', $newresult->user_id)->get(); 
            session()->put('builderdata', $newresult[0]);
            return response()->json(['status' => 200, 'data' => Session::all()]);
        } else {
            return response()->json(['status' => 400, 'error' => 'No User Found.']);
        }
    }

    public function addadmin(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
           // "site_id" => "required",
           // "accesstoken" => "required",
            "admin_email"=>"required",
            "role_admin_id"=>"required"
          //  "admin_password"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $admin = new Admin();
            $admin->admin_email = $request->input('admin_email');
            $admin->admin_lastname = $request->input('admin_lastname');
            $admin->admin_firstname = $request->input('admin_firstname');
            $admin->role_admin_id = $request->input('role_admin_id');
            $admin->admin_password = md5($request->input('admin_password'));
            $admin->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $admin->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $admin->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $admin->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            //$admin->created_by = $request->input('user_id');
            
            $result = $admin->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Admin Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

   //Get all admin code
   public function getalladmin(Request $request) {
        
    $valid = Validator::make($request->all(), [
                "limit" => "required|numeric",
                "offset" => "required|numeric",
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $admin = new Admin();
        $data = array();
        $data['offset'] = $request->input('offset');
        $data['limit'] = $request->input('limit');
        if ($request->has('admin_id') && $request->input('admin_id') != "") {
            $data['admin_id'] = $request->input('admin_id');
        }
        
        if ($request->has('sortby') && $request->input('sortby') != "") {
            $data['sortby'] = $request->input('sortby');
        }
        if ($request->has('sorttype') && $request->input('sorttype') != "") {
            $data['sorttype'] = $request->input('sorttype');
        }
        if ($request->has('admin_slug') && $request->input('admin_slug') != "") {
            $data['admin_slug'] = $request->input('admin_slug');
        }
        if ($request->has('admin_role_id') && $request->input('admin_role_id') != "") {
            $data['admin_role_id'] = $request->input('admin_role_id');
        }
        if ($request->has('status') && $request->input('status') != "") {
            $data['status'] = $request->input('status');
        }
        $admin = $admin->getadmin($data);
        return response()->json(['status' => 200, 'count' => count($admin), 'data' => $admin]);
    }
}


public function updateadmin(Request $request) {
    //validations
    $valid = Validator::make($request->all(), [
             "admin_id" => "required",
             
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $admin = new Admin();
        $request->request->add(['updated_by' =>  $request->input('admin_id')]);
        $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
        $newrequest = $request->except(['admin_id']);
        $newrequest['admin_password'] =md5($request->input('admin_password'));
        $result = $admin->where('admin_id', $request->input('admin_id'))->update($newrequest);
        //md5($request->input('newpassword'));
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Admin information updated succesfully."]);
        } else {
            return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
        }
    }

}

public function deleteadmin(Request $request) {

    $valid = Validator::make($request->all(), [
                "admin_id" => "required",       
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $admin = new Admin();
        $request->request->add(['status' => 0]);
        $request->request->add(['updated_by' =>  $request->input('admin_id')]);
        $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
        $newrequest = $request->except(['admin_id']);
        $result = $admin->where('admin_id', $request->input('admin_id'))->update($newrequest);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Admin deleted successfully"]);
        } else {
            return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
        }
    }

}
}
