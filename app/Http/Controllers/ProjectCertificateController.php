<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\ProjectCertificate;
use Illuminate\Support\Facades\File;
use DB;
class ProjectCertificateController extends Controller
{
    public function addprojectcertificate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_certificate_name" => "required",
                    "project_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    "project_certificate_file" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectCertificate = new ProjectCertificate();
            if ($request->hasFile('project_certificate_file')) {
                    
                $files = $request->file('project_certificate_file');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileuploads($filename, $files);
                } 
            $ProjectCertificate->project_certificate_name = $request->input('project_certificate_name');
            $ProjectCertificate->project_certificate_file = $imgpath;
            $ProjectCertificate->created_by = $request->input('created_by');
            $ProjectCertificate->updated_by = $request->input('updated_by');
            $ProjectCertificate->project_id = $request->input('project_id');
            $ProjectCertificate->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ProjectCertificate->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ProjectCertificate->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ProjectCertificate->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ProjectCertificate->save();

             if($result) {
                return response()->json(['status' => 200, 'data' => "Project Certificate Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
   
    public function getallprojectcertificate(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectCertificate = new ProjectCertificate();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('project_certificate_id') && $request->input('project_certificate_id') != "") {
                $data['project_certificate_id'] = $request->input('project_certificate_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $ProjectCertificate = $ProjectCertificate->getprojectcertificate($data);
            
            if ($ProjectCertificate) {
                $ProjectCertificatedata = array();
                foreach ($ProjectCertificate as $item) {
                    if (does_url_exists(mediapath.$item->project_certificate_file)) {
                        $item->project_certificate_path = imagedisplaypath.$item->project_certificate_file;
                    } else {
                        $item->project_certificate_path = fixedimagepathdocument;
                    }
                    array_push($ProjectCertificatedata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($ProjectCertificate), 'data' => $ProjectCertificatedata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updateprojectcertificate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_certificate_id" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectCertificate = new ProjectCertificate();
            $imgpath = null;
            $project_certificate_id = $request->input('project_certificate_id');

            if ($request->hasFile('project_certificate_file')) {
                    
                $files = $request->file('project_certificate_file');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileuploads($filename, $files);
                $query = DB::table('tbl_project_certificate')->select('tbl_project_certificate.project_certificate_file')->where('project_certificate_id',$project_certificate_id)->get();
                if(count($query)>0)
                {
                    $img=$query[0]->project_certificate_file;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                            unlink($path);
                        } 
                    }
                   
                }
                }
            $newrequest = $request->except(['project_certificate_id','project_certificate_file']);

            if ($request->hasFile('project_certificate_file')) {
				$newrequest['project_certificate_file'] = $imgpath;
		    }

            $result = $ProjectCertificate->where('project_certificate_id', $project_certificate_id)->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Sales Executive Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteprojectcertificate(Request $request) {
        $valid = Validator::make($request->all(), [
                    "project_certificate_id" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectCertificate = new ProjectCertificate();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['project_certificate_id']);
            $result = $ProjectCertificate->where('project_certificate_id', $request->input('project_certificate_id'))->update($newrequest);
            if ($result) {
                $projectProjectCertificate = new ProjectCertificate();
                $project_certificate_id= $request->input('project_certificate_id');
                $query = DB::table('tbl_project_certificate')->select('tbl_project_certificate.project_certificate_file')->where('project_certificate_id',$project_certificate_id)->get();
                if(count($query)>0)
                {
                $img=$query[0]->project_certificate_file;
                if($img!=NULL|| $img!="")
                {
                    if (does_url_exists(mediapath.$img)) {
                        $path = mediapath.$img;
                        unlink($path);
                    } 
                }
                }
				$projectProjectCertificate->where('project_certificate_id', $request->input('project_certificate_id'))->delete();
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
