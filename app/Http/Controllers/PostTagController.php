<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\PostTag;
use App\Slug;
class PostTagController extends Controller
{
    public function addposttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "posttag_name"=>"required",
          
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $posttag = new Posttag();
            $posttag->posttag_name = $request->input('posttag_name');
            if ($request->has('posttag_slug') && $request->input('posttag_slug') != "")
            {
                $slug_name =$request->input('posttag_slug');
            }
            else
            {
                $slug_name =$request->input('posttag_name');
            }

            $slug_name=checkslugavailability($slug_name, "posttag",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="posttag";

            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $posttag->slug_id = $slug_id;*/
            $posttag->slug_name = $slug_name;
            $posttag->posttag_description = $request->input('posttag_description');
            $posttag->created_by = $request->input('user_id');
            $posttag->site_id = $request->input('site_id');
            $posttag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $posttag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $posttag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $posttag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $posttag->save();
            $posttagdata = $posttag->latest()->first();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $posttagdata,"message"=>"Added Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallposttag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $posttag = new Posttag();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('posttag_id') && $request->input('posttag_id') != "") {
                $data['posttag_id'] = $request->input('posttag_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $posttag = $posttag->getposttag($data);
            if($posttag)
            {
                
                return response()->json(['status' => 200, 'count' => count($posttag), 'data' => $posttag]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updateposttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "posttag_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $posttag = new Posttag();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['posttag_id','user_id','accesstoken','slug_id','slug_name']);

            if($request->has('slug_name')){
                $posttag = $posttag->where('posttag_id', $request->input('posttag_id'))->first();
                if($posttag->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "posttag",$request->input('site_id'));
				}
            }
       
            $result = $posttag->where('posttag_id', $request->input('posttag_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "posttag Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteposttag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "posttag_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $posttag = new Posttag();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['posttag_id','user_id','accesstoken']);
            $result = $posttag->where('posttag_id', $request->input('posttag_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

    public function manageposttag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "posttag_name" => "required",
            "created_by" => "required",
            "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            if($request->has('posttag_id') && $request->filled('posttag_id')){
                $posttag = new posttag();
                $newrequest = $request->except(['posttag_id']);
                $result = $posttag->where('posttag_id', $request->input('posttag_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "posttag Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $posttag = new posttag();
                $posttag->posttag_name = $request->input('posttag_name');
                $posttag->created_by = $request->input('created_by');
                $posttag->updated_by = $request->input('updated_by');
                $posttag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $posttag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $posttag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $posttag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $posttag->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "posttag Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
            
        }
    }
}
