<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomAddress;
use App\EcomCart;
use App\EcomBusinessSetting;
use App\EcomOrder;
use App\EcomOrderTax;
use App\EcomOrderDetails;
use App\EcomOrderItems;
use App\EcomOrderItemsTax;
use App\EcomOrderNotes;
use App\EcomOrderActivity;
use App\EcomProduct;
use App\EcomProductGallery;
use App\EcomPayments;
use App\EcomShipping;
use App\EcomCoupon;
use App\EcomCouponCategory;
use App\EcomCouponProduct;
use App\EcomPaymentSettings;
use App\User;
use App\EcomTax;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
use \stdClass;
use Razorpay\Api\Api;

class EcomOrderController extends Controller
{

    public function addorder(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $orders = new EcomOrder();
            $orders->user_id = $request->input('user_id');
            $orders->coupon_id = $request->input('coupon_id');
            $orders->order_discount = $request->input('order_discount');
            $orders->order_tax = $request->input('order_tax');
            $orders->order_amount = $request->input('order_amount');
            $orders->order_total = $request->input('order_total');
            $orders->order_shipping_charge = $request->input('order_shipping_charge');
            $orders->order_grandtotal = $request->input('order_grandtotal');
            $orders->order_status = "initiated";
            $orders->order_guid = generateAccessToken(25);
            $orders->created_by = $request->input('user_id');
            $orders->site_id = $request->input('site_id');
            $orders->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $orders->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $orders->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $orders->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $orders->save();

            if ($result) {

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $orders->id;
                $ecomorderactivity->order_activity_type = "Order Created";
                $ecomorderactivity->order_activity_details = "Order has been created by customer.";
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                return response()->json(['status' => 200, 'data' => "Order added successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getcheckoutsumary(Request $request) {

        $cart = new EcomCart();
        $product = new EcomProduct();
        $user = new User(); 
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $ecomaddress = new EcomAddress();

            /* update billing address */
            if($request->has('billing_state')){
                if($request->input('billing_state') != null && $request->input('billing_state') != "null" ){
                    $updateorderdetails = array();
                    $updateorderdetails['address'] =$request->input('billing_address');
                    $updateorderdetails['landmark'] =$request->input('billing_landmark');
                    $updateorderdetails['city'] =$request->input('billing_city');
                    $updateorderdetails['state'] =$request->input('billing_state');
                    $updateorderdetails['country'] =$request->input('billing_country');
                    $updateorderdetails['pincode'] =$request->input('billing_pincode');
                    $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->update($updateorderdetails);  
                }
            }

            /* update shipping address */
            if($request->has('shipping_state')){
                if($request->input('shipping_state') != null && $request->input('shipping_state') != "null" ){
                    $updateorderdetails = array();
                    $updateorderdetails['address'] = ($request->has('shipping_address')?$request->input('shipping_address'):null);
                    $updateorderdetails['landmark'] = ($request->has('shipping_landmark')?$request->input('shipping_landmark'):null);
                    $updateorderdetails['city'] = ($request->has('shipping_city')?$request->input('shipping_city'):null);
                    $updateorderdetails['state'] = ($request->has('shipping_state')?$request->input('shipping_state'):null);
                    $updateorderdetails['country'] = ($request->has('shipping_country')?$request->input('shipping_country'):null);
                    $updateorderdetails['pincode'] = ($request->has('shipping_pincode')?$request->input('shipping_pincode'):null);
                    $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->update($updateorderdetails);
                }
            }

            $cartidlist = $cart->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->where('cart_quantity', '<=', 0)->where('status', 1)->pluck('cart_id');

            if (count($cartidlist) > 0){
                return response()->json(['status' => 400, 'error' => 'Items with 0 or less than 0 quantity not allowed in the cart.'], 400);
            }else{
                $cartidlist = $cart->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->where('status', 1)->pluck('cart_id');

                if (count($cartidlist) > 0) 
                {

                    /* Business Data */
                    $business = new EcomBusinessSetting();
                    $businessdata = $business->where('site_id',$request->input('site_id'))->first();

                    /* Create an Order Details from order */
                    $user = new User();
                    $userdata = $user->where('user_id', $request->input('user_id'))->get();

                    /* update user address */
                    $ecomaddress = new EcomAddress();
                    $ecomshippingaddressdata = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->get();
                    $ecombillingaddressdata = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->get();
                    
                    /* Prepare Summary */

                    $total = 0;
                    $tax = 0;
                    $order_amount = 0;
                    $discount = 0;
                    $shipping_charge = 0;


                    /* Create an Order from cart */
                    $order = array();

                    /* Clone order details */
                    $orderdetails = array();
                   if(count($ecombillingaddressdata)>0){
                    $orderdetails['user_first_name'] = $userdata[0]->user_first_name;
                    $orderdetails['user_last_name'] = $userdata[0]->user_last_name;
                   
                    $orderdetails['billing_address'] = $ecombillingaddressdata[0]->address;
                    $orderdetails['billing_landmark'] = $ecombillingaddressdata[0]->landmark;
                    $orderdetails['billing_city'] = $ecombillingaddressdata[0]->city;
                    $orderdetails['billing_state'] = $ecombillingaddressdata[0]->state;
                    $orderdetails['billing_country'] = $ecombillingaddressdata[0]->country;
                    $orderdetails['billing_pincode'] = $ecombillingaddressdata[0]->pincode;

                   }else{
                    $orderdetails['user_first_name'] = $userdata[0]->user_first_name;
                    $orderdetails['user_last_name'] = $userdata[0]->user_last_name;
                   
                    $orderdetails['billing_address'] = $request->input('billing_address');
                    $orderdetails['billing_landmark'] = $request->input('billing_landmark');
                    $orderdetails['billing_city'] = $request->input('billing_city');
                    $orderdetails['billing_state'] = $request->input('billing_state');
                    $orderdetails['billing_country'] = $request->input('billing_country');
                    $orderdetails['billing_pincode'] = $request->input('billing_pincode');
                    
                    $ecomaddress = new EcomAddress();
                    $ecomaddress->site_id = $request->input('site_id');
                    $ecomaddress->user_id = $request->input('user_id');
                    $ecomaddress->address_type = 'billing';
                    $ecomaddress->address = ($request->has('billing_address') && $request->input('billing_address') != "")?$request->input('billing_address'):$request->input('billing_address');
                    $ecomaddress->landmark = ($request->has('billing_landmark') && $request->input('billing_landmark') != "")?$request->input('billing_landmark'):$request->input('billing_landmark');
                    $ecomaddress->city = ($request->has('billing_city') && $request->input('billing_city') != "")?$request->input('billing_city'):$request->input('billing_city');
                    $ecomaddress->state = ($request->has('billing_state') && $request->input('billing_state') != "")?$request->input('billing_state'):$request->input('billing_state');
                    $ecomaddress->country = ($request->has('billing_country') && $request->input('billing_country') != "")?$request->input('billing_country'):$request->input('billing_country');
                    $ecomaddress->pincode = ($request->has('billing_pincode') && $request->input('billing_pincode') != "")?$request->input('billing_pincode'):$request->input('billing_pincode');
                    $ecomaddress->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $ecomaddress->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $ecomaddress->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $ecomaddress->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $ecomaddress->save();
                        

                   }
                   if(count($ecomshippingaddressdata)>0){
                    $orderdetails['shipping_address'] = $ecombillingaddressdata[0]->address;
                    $orderdetails['shipping_landmark'] = $ecomshippingaddressdata[0]->landmark;
                    $orderdetails['shipping_city'] = $ecomshippingaddressdata[0]->city;
                    $orderdetails['shipping_state'] = $ecomshippingaddressdata[0]->state;
                    $orderdetails['shipping_country'] = $ecomshippingaddressdata[0]->country;
                    $orderdetails['shipping_pincode'] = $ecomshippingaddressdata[0]->pincode;
                   }else{

                    $orderdetails['user_first_name'] = $userdata[0]->user_first_name;
                    $orderdetails['user_last_name'] = $userdata[0]->user_last_name;
                    $orderdetails['shipping_address'] = $request->input('shipping_address');
                    $orderdetails['shipping_landmark'] = $request->input('shipping_landmark');
                    $orderdetails['shipping_city'] = $request->input('shipping_city');
                    $orderdetails['shipping_state'] = $request->input('shipping_state');
                    $orderdetails['shipping_country'] = $request->input('shipping_country');
                    $orderdetails['shipping_pincode'] = $request->input('shipping_pincode');

                    $ecomaddress = new EcomAddress();
                    $ecomaddress->site_id = $request->input('site_id');
                    $ecomaddress->user_id = $request->input('user_id');
                    $ecomaddress->address_type = 'shipping';
                    $ecomaddress->address = ($request->has('shipping_address') && $request->input('shipping_address') != "")?$request->input('shipping_address'):$request->input('billing_address');
                    $ecomaddress->landmark = ($request->has('shipping_landmark') && $request->input('shipping_landmark') != "")?$request->input('shipping_landmark'):$request->input('billing_landmark');
                    $ecomaddress->city = ($request->has('shipping_city') && $request->input('shipping_city') != "")?$request->input('shipping_city'):$request->input('billing_city');
                    $ecomaddress->state = ($request->has('shipping_state') && $request->input('shipping_state') != "")?$request->input('shipping_state'):$request->input('billing_state');
                    $ecomaddress->country = ($request->has('shipping_country') && $request->input('shipping_country') != "")?$request->input('shipping_country'):$request->input('billing_country');
                    $ecomaddress->pincode = ($request->has('shipping_pincode') && $request->input('shipping_pincode') != "")?$request->input('shipping_pincode'):$request->input('billing_pincode');
                    $ecomaddress->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $ecomaddress->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $ecomaddress->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $ecomaddress->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $ecomaddress->save();

                   }
                  
                    

                    /* Add Order Items to the Order */

                    $cartdata = array();
                    $cartdata['user_id'] = $request->input('user_id');
                    $cartdata['offset'] = 0;
                    $cartdata['limit'] = 100000;
                    $cartdata['status'] = 1;
                    $cartdata['site_id'] = $request->input('site_id');
                    $cartitems = $cart->getcart($cartdata);

                    /* Create an Order Items from order */

                    $totaltax = 0; $totalshipping = 0; $totalamount = 0; $totalitems = 0;
                    $orderitemsarr = array();

                    $tempordertax = array();

                    foreach ($cartitems as $item) {

                        $totalnooftax = 1;
                        $tax = 0;
                        $amount = round(floatval($item->cart_amount)*intval($item->cart_quantity) ,2);
                        $baseprice = $amount;

                        $ecomtax = new EcomTax();
                        $user_state = (count($ecombillingaddressdata)>0 && $ecombillingaddressdata[0]->state != null)?$ecombillingaddressdata[0]->state:"";
                        $business_state = (isset($businessdata->business_state))?$businessdata->business_state:"";

                        if(count($ecombillingaddressdata) > 0){

                            $taxobj = $ecomtax->gettaxbyproductid($item->product_id, $request->input('site_id'), $request->input('user_id'), $ecombillingaddressdata[0], $businessdata);
                            $totalnooftax = count($taxobj);

                            

                            if(count($taxobj)>0){

                                $totalitemtax = 0;

                                foreach ($taxobj as $taxitem) {
                                    $totalitemtax = $totalitemtax + floatval($taxitem['tax_percentage']);
                                }

                                $baseprice = (floatval($amount)*100)/(floatval($totalitemtax)+100);

                                foreach ($taxobj as $taxitem) {
                                    //$currenttax = round($baseprice * (floatval($totalitemtax/count($taxobj))/100), 2);
                                    $currenttax = round((($baseprice * floatval($taxitem['tax_percentage']))/100), 2);
                                    //$currenttax = round($currenttax, 2);
                                    $tax = $tax + $currenttax;  
                                }
                            }

                        }
                        
                        /* Generate Order Item */
                        $orderitems = array();
                        $orderitems['order_id'] = "";
                        $orderitems['product_id'] = $item->product_id;
                        $orderitems['attribute_id'] = $item->attribute_id;
                        $orderitems['attribute_term_id'] = $item->attribute_term_id;
                        $orderitems['attribute_term_name'] = $item->attribute_term_name;
                        $orderitems['order_product_price_with_tax'] = $item->cart_amount;
                        $orderitems['order_product_quantity'] = $item->cart_quantity;
                        $orderitems['order_product_amount'] = round($baseprice, 2);
                        $orderitems['order_product_tax'] = round($tax, 2);
                        $orderitems['order_product_total'] = $amount;
                        $orderitems['product_name'] = $item->product_name;
                        $orderitems['browser_name'] = $request->has('browser_name')?$request->input('browser_name'):null;
                        $orderitems['browser_version'] = $request->has('browser_version')?$request->input('browser_version'):null;
                        $orderitems['browser_platform'] = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $orderitems['ip_address'] = $request->has('ip_address')?$request->input('ip_address'):null;

                        $tempdata['product_id'] = $item->product_id;
                        $tempdata['offset'] = 0;
                        $tempdata['limit'] = 100;
                        $tempdata['site_id'] = $request->input('site_id');
                        $product = new EcomProduct();
                        $products = $product->getproduct($tempdata);
                        //$item->products = $products;
                        if(count($products)>0){
                            
                            $productgallery = new EcomProductGallery();
                            $gallerydata = array();
                            $gallerydata['product_id']=$products[0]->product_id;
                            $gallerydata['offset'] = 0;
                            $gallerydata['limit'] = 1000;
                            $productgallery = $productgallery->getproductgallery($gallerydata);
                            $productgallerydata=array();
                            foreach ($productgallery as $item1) {
                            
                                if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                                {
                                    $item1->product_image_path =fixedproductgalleryimage;
                                
                                }
                                else
                                {
                                    if(fileuploadtype=="local")
                                    {
                                        if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                                $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                            } else {
                                                $item1->product_image_path =fixedproductgalleryimage;
                                            }
                                        
                                    }
                                    else
                                    {
                                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                                        } else {
                                            $item1->product_image_path = fixedproductgalleryimage;
                                        }
                                    }
                                }  

                            
                                array_push($productgallerydata, $item1);
                            }
                            $products[0]->product_gallery=$productgallerydata;

                            //product category
                            $categorynamelist = DB::table('tbl_ecom_product_category as pc')
                            ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                            ->where('pc.status', '=', 1)
                            /* ->where('pc.site_id', '=', $request->input('site_id')) */
                            ->where('pc.product_id', '=', $products[0]->product_id)
                            ->orderBy('category.category_name', 'DESC')
                            ->pluck('category.category_name');

                            $products[0]->categorynamelist=$categorynamelist;

                            $orderitems['productdata'] = count($products)>0?$products[0]:null;

                            /* Attribute Data */
                            $orderitems['productattrdata'] = $product->getattributeandattrtermlistforproductid($products[0]->product_id);

                        }else{
                            $orderitems['productdata'] = array();
                        }
                        
                        $orderitemtaxarr = array();

                        if(count($ecombillingaddressdata) > 0){
                            /* Generate Order Item Tax */
                            if(count($taxobj)>0){
                                foreach ($taxobj as $taxitem) {
                                    $orderitemstax = array();
                                    $orderitemstax['order_item_id'] = "";
                                    $orderitemstax['tax_id'] = $taxitem['tax_id'];
                                    $orderitemstax['tax_name'] = $taxitem['tax_name'];
                                    $orderitemstax['tax_percentage'] = $taxitem['tax_percentage'];
                                    $orderitemstax['tax_amount'] = round(($baseprice * floatval($taxitem['tax_percentage']))/100, 2);
                                    $orderitemstax['status'] = 1;
                                    $orderitemstax['browser_name'] = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $orderitemstax['browser_version'] = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $orderitemstax['browser_platform'] = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $orderitemstax['ip_address'] = $request->has('ip_address')?$request->input('ip_address'):null;
                                    array_push($orderitemtaxarr, $orderitemstax);
                                }
                                
                            }
                        }

                        $totaltax = $totaltax + $tax;
                        $totalamount = $totalamount + $baseprice;
                        $totalitems = $totalitems + intval($item->cart_quantity);

                        $orderitems['orderitemtax'] = $orderitemtaxarr;
                        $orderitems['itemtax'] = $tax;
                        $orderitems['itembaseprice'] = $baseprice;
                        $orderitems['itembaseprice'] = $baseprice;

                        array_push($orderitemsarr, $orderitems);
                    }

                    $order_shipping_charge = 0; $shipping_id = null; $shipping_class_id = 1; $shipping_method_id=1;

                    if(count($ecomshippingaddressdata)>0){
                        $shippingmodel = new EcomShipping();
                        $shippingobj = $shippingmodel->getshippingforsiteid($shipping_class_id, $shipping_method_id=1, $request->input('site_id'), $ecomshippingaddressdata[0]);

                        if(count($shippingobj)>0){
                            foreach ($shippingobj as $item) {
                                $order_shipping_charge = isset($item->shipping_rate)?$item->shipping_rate:0;
                                $shipping_id = $item->shipping_id;
                            }
                        }
                    }

                    /* round up values */
                    /*$totalamount = round($totalamount, 2);
                    $totaltax = round($totaltax, 2);
                    $discount = round($discount, 2);*/
                    $order_shipping_charge = round($order_shipping_charge, 2);
                    $order_total = round(floatval($totalamount) + floatval($totaltax), 2);

                    $order['coupon_code'] = "";
                    $order['coupon_id'] = "";

                    /* Check if coupon code passed */
                    if($request->has("coupon_code") && $request->filled("coupon_code")){
                        $coupon = new Ecomcoupon();
                        $couponcode = $coupon->where('site_id', $request->input('site_id'))->where('is_visible', 1)->where('status', 1)->where('coupon_code', $request->input('coupon_code'))->whereDate('coupon_startdate','<=',date("Y-m-d"))->whereDate('coupon_enddate','>=',date("Y-m-d"))->get();
                        if(count($couponcode)>0){
                            if($couponcode[0]->discount_type == "2"){
                
                                $coupondiscount = (floatval($order_total)*floatval($couponcode[0]->coupon_amount)/100);
                                
                                if($couponcode[0]->coupon_max_amount != null){
                                  $discount = ($coupondiscount > $couponcode[0]->coupon_max_amount)?$couponcode[0]->coupon_max_amount:$coupondiscount;
                                }
                                if($couponcode[0]->coupon_min_amount != null){
                                  $discount = ($coupondiscount < $couponcode[0]->coupon_min_amount)?$couponcode[0]->coupon_min_amount:$coupondiscount;
                                }

                                if($coupondiscount > $order_total){
                                  return response()->json(['status' => 400, 'error' => 'Discount is more than cart amount. Invalid discout coupon.']);
                                }else{
                                  $discount = $coupondiscount;
                                  $order['coupon_code'] = $couponcode[0]->coupon_code;
                                  $order['coupon_id'] = $couponcode[0]->coupon_id;
                                }

                            }else{
                                return response()->json(['status' => 400, 'error' => 'Coupon is expired or invalid or not featured.']);
                            }
                        }
                    }

                    $order_grandtotal = round(floatval($totalamount) + floatval($totaltax) - floatval($discount) + floatval($order_shipping_charge), 2);                 

                    $order['user_id'] = $request->input('user_id');
                    $order['shipping_id'] = ($request->has('shipping_id') && $request->filled('shipping_id')) ? $request->input('shipping_id') : null;
                    $order['order_amount'] = $totalamount;
                    $order['order_tax'] = $totaltax;
                    $order['order_total'] =$order_total;
                    $order['order_discount'] = $discount;
                    $order['order_shipping_charge'] = $order_shipping_charge;
                    $order['order_grandtotal'] = $order_grandtotal;
                    $order['order_status'] = 'pending';
                    $order['order_guid'] = generateAccessToken(25);
                    $order['site_id'] = $request->input('site_id');
                    $order['order_details'] = $orderdetails;
                    $order['order_items'] = $orderitemsarr;
                    
                    return response()->json(['status' => 200, 'base_price'=>$baseprice, 'data' => $order]);
                    
                } else {
                    return response()->json(['status' => 400, 'error' => 'There is no item in the cart.'], 400);
                }
            }
        }
    }

    public function checkoutandpay(Request $request) {

        $cart = new EcomCart();
        $product = new EcomProduct();
        $user = new User(); 
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $cartidlist = $cart->where('user_id', $request->input('user_id'))->where('cart_quantity', '<=', 0)->where('status', 1)->pluck('cart_id');

            if (count($cartidlist) > 0){
                return response()->json(['status' => 400, 'error' => 'Items with 0 or less than 0 quantity not allowed in the cart.'], 400);
            }else{

                $cartidlist = $cart->where('user_id', $request->input('user_id'))->where('status', 1)->pluck('cart_id');

                if (count($cartidlist) > 0) 
                {

                    /* Update Billing Address */
                    $ecomaddress = new EcomAddress();
                    $billingaddresscount = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->count();
                    if($billingaddresscount>0  ){
                        if($request->input('billing_state') != null && $request->input('billing_state') != "null" ){
                            $updateorderdetails = array();
                            $updateorderdetails['address'] = ($request->has('billing_address')?$request->input('billing_address'):null);
                            $updateorderdetails['landmark'] = ($request->has('billing_landmark')?$request->input('billing_landmark'):null);
                            $updateorderdetails['city'] = ($request->has('billing_city')?$request->input('billing_city'):null);
                            $updateorderdetails['state'] = ($request->has('billing_state')?$request->input('billing_state'):null);
                            $updateorderdetails['country'] = ($request->has('billing_country')?$request->input('billing_country'):null);
                            $updateorderdetails['pincode'] = ($request->has('billing_pincode')?$request->input('billing_pincode'):null);
                            $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->update($updateorderdetails);
                        }
                       
                    }else{
                        if($request->input('billing_state') != null ){
                            $ecomaddress->site_id = $request->input('site_id');
                            $ecomaddress->user_id = $request->input('user_id');
                            $ecomaddress->address_type = 'billing';
                            $ecomaddress->address = ($request->has('billing_address')?$request->input('billing_address'):null);
                            $ecomaddress->landmark = ($request->has('billing_landmark')?$request->input('billing_landmark'):null);
                            $ecomaddress->city = ($request->has('billing_city')?$request->input('billing_city'):null);
                            $ecomaddress->state = ($request->has('billing_state')?$request->input('billing_state'):null);
                            $ecomaddress->country = ($request->has('billing_country')?$request->input('billing_country'):null);
                            $ecomaddress->pincode = ($request->has('billing_pincode')?$request->input('billing_pincode'):null);
                            $ecomaddress->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $ecomaddress->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $ecomaddress->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $ecomaddress->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $ecomaddress->save();
                        }
                        
                    }

                    /* Update shipping Address */
                    $ecomaddress = new EcomAddress();
                    $billingaddresscount = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->count();
                    if($billingaddresscount>0){
                        if($request->input('shipping_state') != null && $request->input('shipping_state') != "" ){
                            $updateorderdetails = array();
                            $updateorderdetails['address'] = ($request->has('shipping_address') && $request->input('shipping_address') != "")?$request->input('shipping_address'):$request->input('billing_address');
                            $updateorderdetails['landmark'] = ($request->has('shipping_landmark') && $request->input('shipping_landmark') != "")?$request->input('shipping_landmark'):$request->input('billing_landmark');
                            $updateorderdetails['city'] = ($request->has('shipping_city') && $request->input('shipping_city') != "")?$request->input('shipping_city'):$request->input('billing_city');
                            $updateorderdetails['state'] = ($request->has('shipping_state') && $request->input('shipping_state') != "")?$request->input('shipping_state'):$request->input('billing_state');
                            $updateorderdetails['country'] = ($request->has('shipping_country') && $request->input('shipping_country') != "")?$request->input('shipping_country'):$request->input('billing_country');
                            $updateorderdetails['pincode'] = ($request->has('shipping_pincode') && $request->input('shipping_pincode') != "")?$request->input('shipping_pincode'):$request->input('billing_pincode');
                            $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->update($updateorderdetails);
                        }
                       
                    }else{
                        if($request->input('shipping_state') != null ){
                            $ecomaddress->site_id = $request->input('site_id');
                            $ecomaddress->user_id = $request->input('user_id');
                            $ecomaddress->address_type = 'shipping';
                            $ecomaddress->address = ($request->has('shipping_address') && $request->input('shipping_address') != "")?$request->input('shipping_address'):$request->input('billing_address');
                            $ecomaddress->landmark = ($request->has('shipping_landmark') && $request->input('shipping_landmark') != "")?$request->input('shipping_landmark'):$request->input('billing_landmark');
                            $ecomaddress->city = ($request->has('shipping_city') && $request->input('shipping_city') != "")?$request->input('shipping_city'):$request->input('billing_city');
                            $ecomaddress->state = ($request->has('shipping_state') && $request->input('shipping_state') != "")?$request->input('shipping_state'):$request->input('billing_state');
                            $ecomaddress->country = ($request->has('shipping_country') && $request->input('shipping_country') != "")?$request->input('shipping_country'):$request->input('billing_country');
                            $ecomaddress->pincode = ($request->has('shipping_pincode') && $request->input('shipping_pincode') != "")?$request->input('shipping_pincode'):$request->input('billing_pincode');
                            $ecomaddress->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $ecomaddress->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $ecomaddress->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $ecomaddress->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $ecomaddress->save();
                        }
                        
                    }

                    /* Business Data */
                    $business = new EcomBusinessSetting();
                    $businessdata = $business->where('site_id',$request->input('site_id'))->first();
                    
                    /* Create an Order Details from order */
                    $user = new User();
                    $userdata = $user->where('user_id', $request->input('user_id'))->get();

                    /* update user address */
                    $ecomaddress = new EcomAddress();
                    $ecomshippingaddressdata = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->get();
                    $ecombillingaddressdata = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->get();
                    
                    /*if (count($ecomshippingaddressdata) == 0 || count($ecomshippingaddressdata) == 0) 
                    {
                        return response()->json(['status' => 400, 'error' => 'Items with 0 or less than 0 quantity not allowed in the cart.'], 400);
                    }*/

                    /* Prepare Summary */

                    $total = 0;
                    $tax = 0;
                    $order_amount = 0;
                    $discount = 0;
                    $shipping_charge = 0;


                    /* Create an Order from cart */
                    $order = new EcomOrder();
                    $order->user_id = $request->input('user_id');
                    $order->shipping_id = ($request->has('shipping_id') && $request->filled('shipping_id')) ? $request->input('shipping_id') : null;
                    $order->coupon_id = ($request->has('coupon_id') && $request->filled('coupon_id')) ? $request->input('coupon_id') : null;
                    $order->order_amount = $order_amount;
                    $order->order_tax = $tax;
                    $order->order_total = $order_amount + $tax;
                    $order->order_discount = $discount;
                    $order->order_shipping_charge = $shipping_charge;
                    $order->order_grandtotal = $order_amount + $tax - $discount + $shipping_charge;
                    $order->order_status = 'pending';
                    $order->order_guid = generateAccessToken(25);
                    $order->site_id = $request->input('site_id');
                    $order->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $order->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $order->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $order->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $order->save();
                    $order_id = $order->id;

                    /* Order Invoice No */
                    $order_invoice_no = preg_replace('/\s*/', '', strtolower($businessdata->business_name)).date('Y').date('m').$order_id;

                    /* Add Order Notes */

                    if($request->has('site_id')){
                        $ordernotes = new EcomOrderNotes();
                        $ordernotes->order_id = $order_id;
                        $ordernotes->site_id = $request->input('site_id');
                        $ordernotes->order_notes = $request->input('order_notes');
                        $ordernotes->order_notes_for = 'forcustomer';
                        $ordernotes->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $ordernotes->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $ordernotes->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $ordernotes->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $ordernotes->save();
                    }



                    /* Clone order details */
                    // print_r($ecomshippingaddressdata);exit;
                    // return response()->json(['status' => 200, 'data' =>count($ecomshippingaddressdata)]);
                    if(count($ecombillingaddressdata)>0){
                      
                        $orderdetails = new EcomOrderDetails();
                        $orderdetails->order_id = $order->id;
                        $orderdetails->user_first_name = $userdata[0]->user_first_name;
                        $orderdetails->user_last_name = $userdata[0]->user_last_name;
                        $orderdetails->shipping_address =$ecomshippingaddressdata[0]->address;
                        $orderdetails->shipping_landmark =  $ecomshippingaddressdata[0]->landmark;
                        $orderdetails->shipping_city =  $ecomshippingaddressdata[0]->city;
                        $orderdetails->shipping_state =  $ecomshippingaddressdata[0]->state;
                        $orderdetails->shipping_country =  $ecomshippingaddressdata[0]->country;
                        $orderdetails->shipping_pincode =  $ecomshippingaddressdata[0]->pincode;
                        $orderdetails->billing_address =  $ecombillingaddressdata[0]->address;
                        $orderdetails->billing_landmark =  $ecombillingaddressdata[0]->landmark;
                        $orderdetails->billing_city =  $ecombillingaddressdata[0]->city;
                        $orderdetails->billing_state =  $ecombillingaddressdata[0]->state;
                        $orderdetails->billing_country =  $ecombillingaddressdata[0]->country;
                        $orderdetails->billing_pincode =  $ecombillingaddressdata[0]->pincode;
                        $orderdetails->created_by = $request->input('user_id');
                        $orderdetails->updated_by = $request->input('user_id');
                        $orderdetails->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $orderdetails->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $orderdetails->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $orderdetails->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $orderdetails->save();
                    }else{

                        $orderdetails = new EcomOrderDetails();
                        $orderdetails->order_id = $order->id;
                        $orderdetails->user_first_name = $userdata[0]->user_first_name;
                        $orderdetails->user_last_name = $userdata[0]->user_last_name;
                        $orderdetails->shipping_address =$request->input('shipping_address');
                        $orderdetails->shipping_landmark =$request->input('shipping_landmark');
                        $orderdetails->shipping_city =$request->input('shipping_city');
                        $orderdetails->shipping_state =$request->input('shipping_state');
                        $orderdetails->shipping_country =$request->input('shipping_country');
                        $orderdetails->shipping_pincode =$request->input('shipping_pincode');
                        $orderdetails->billing_address =$request->input('billing_address');
                        $orderdetails->billing_landmark =$request->input('billing_landmark');
                        $orderdetails->billing_city =$request->input('billing_city');
                        $orderdetails->billing_country =$request->input('billing_country');
                        $orderdetails->billing_pincode =$request->input('billing_pincode');
                        $orderdetails->created_by = $request->input('user_id');
                        $orderdetails->updated_by = $request->input('user_id');
                        $orderdetails->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $orderdetails->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $orderdetails->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $orderdetails->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $orderdetails->save();

                    }
                   

                    /* Add Order Items to the Order */

                    $cartdata = array();
                    $cartdata['user_id'] = $request->input('user_id');
                    $cartdata['offset'] = 0;
                    $cartdata['limit'] = 100000;
                    $cartdata['status'] = 1;
                    $cartitems = $cart->getcart($cartdata);

                    /* Create an Order Items from order */

                    $totaltax = 0; $totalshipping = 0; $totalamount = 0; $totalitems = 0;

                    foreach ($cartitems as $item) {

                        $totalnooftax = 1;
                        $tax = 0;
                        $amount = round(floatval($item->cart_amount)*intval($item->cart_quantity) ,2);
                        $baseprice = $amount;

                        $ecomtax = new EcomTax();
                        $user_state = (count($ecombillingaddressdata)>0 && $ecombillingaddressdata[0]->state != null)?$ecombillingaddressdata[0]->state:"";
                        $business_state = (isset($businessdata->business_state))?$businessdata->business_state:"";

                        if(count($ecombillingaddressdata) > 0){

                            $taxobj = $ecomtax->gettaxbyproductid($item->product_id, $request->input('site_id'), $request->input('user_id'), $ecombillingaddressdata[0], $businessdata);
                            $totalnooftax = count($taxobj);

                            if(count($taxobj)>0){

                                $totalitemtax = 0;

                                foreach ($taxobj as $taxitem) {
                                    $totalitemtax = $totalitemtax + floatval($taxitem['tax_percentage']);
                                }

                                $baseprice = (floatval($amount)*100)/(floatval($totalitemtax)+100);

                                foreach ($taxobj as $taxitem) {
                                    //$currenttax = round($baseprice * (floatval($totalitemtax/count($taxobj))/100), 2);
                                    $currenttax = round((($baseprice * floatval($taxitem['tax_percentage']))/100), 2);
                                    //$currenttax = round($currenttax, 2);
                                    $tax = $tax + $currenttax;  
                                }
                            }

                        }
                        
                        /* Generate Order Item */
                        $orderitems = new EcomOrderItems();
                        $orderitems->order_id = $order_id;
                        $orderitems->product_id = $item->product_id;
                        $orderitems->attribute_id = $item->attribute_id;
                        $orderitems->attribute_term_id = $item->attribute_term_id;
                        $orderitems->attribute_term_name = $item->attribute_term_name;
                        $orderitems->order_product_price_with_tax = $item->cart_amount;
                        $orderitems->order_product_quantity = $item->cart_quantity;
                        $orderitems->order_product_amount = round($baseprice, 2);
                        $orderitems->order_product_tax = round($tax, 2);
                        $orderitems->order_product_total = $amount;
                        $orderitems->product_name = $item->product_name;
                        $orderitems->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $orderitems->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $orderitems->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $orderitems->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $orderitems->save();

                        $order_item_id = $orderitems->id;

                        if(count($ecombillingaddressdata) > 0){
                            /* Generate Order Item Tax */
                            if(count($taxobj)>0){
                                foreach ($taxobj as $taxitem) {
                                    $orderitemstax = new EcomOrderItemsTax();
                                    $orderitemstax->order_item_id = $order_item_id;
                                    $orderitemstax->tax_id = $taxitem['tax_id'];
                                    $orderitemstax->tax_name = $taxitem['tax_name'];
                                    $orderitemstax->tax_percentage = $taxitem['tax_percentage'];
                                    $orderitemstax->tax_amount = round(($baseprice * floatval($taxitem['tax_percentage']))/100, 2);
                                    $orderitemstax->status = 1;
                                    $orderitemstax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $orderitemstax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $orderitemstax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $orderitemstax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                    $orderitemstax->save();

                                    $orderitems = new EcomOrderItems();
                                    $orderitems->where("order_item_id", $order_item_id)->update(array(
                                        "tax_id"=>$taxitem['tax_id']
                                    ));
                                }
                            }
                        }

                        $totaltax = $totaltax + $tax;
                        $totalamount = $totalamount + $baseprice;
                        $totalitems = $totalitems + intval($item->cart_quantity);

                    }

                    $orderitems = new EcomOrderItems();
                    $ecomorderitemlist = $orderitems->where('order_id', $order_id)->where('status', 1)->pluck('order_item_id');

                    $orderitemstax = new EcomOrderItemsTax();
                    $orderitemstaxdata = $orderitemstax->select('tax_name', DB::raw("SUM(tax_amount) as tax_amount"))->whereIn('order_item_id', $ecomorderitemlist)->groupby('tax_name')->get();

                    if(count($orderitemstaxdata) > 0){
                        foreach ($orderitemstaxdata as $item) {
                            $ecomodertax = new EcomOrderTax();
                            $ecomodertax->order_id = $order_id;
                            $ecomodertax->tax_name = $item->tax_name;
                            $ecomodertax->tax_percentage = 0;
                            $ecomodertax->tax_amount = $item->tax_amount;
                            $ecomodertax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $ecomodertax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $ecomodertax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $ecomodertax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $ecomodertax->save();
                        }
                    }

                    $order_shipping_charge = 0; $shipping_id = null; $shipping_class_id = 1; $shipping_method_id=1;

                    if(count($ecomshippingaddressdata)>0){
                        $shippingmodel = new EcomShipping();
                        $shippingobj = $shippingmodel->getshippingforsiteid($shipping_class_id, $shipping_method_id=1, $request->input('site_id'), $ecomshippingaddressdata[0]);

                        if(count($shippingobj)>0){
                            foreach ($shippingobj as $item) {
                                $order_shipping_charge = isset($item->shipping_rate)?$item->shipping_rate:0;
                                $shipping_id = $item->shipping_id;
                            }
                        }
                    }

                    /* round up values */
                    $order_shipping_charge = round($order_shipping_charge, 2);
                    $order_total = round(floatval($totalamount) + floatval($totaltax), 2);

                    /* Check if coupon code passed */
                    if($request->has("coupon_id") && $request->filled("coupon_id")){
                        $coupon = new Ecomcoupon();
                        $couponcode = $coupon->where('site_id', $request->input('site_id'))->where('is_visible', 1)->where('status', 1)->where('coupon_id', $request->input('coupon_id'))->whereDate('coupon_startdate','<=',date("Y-m-d"))->whereDate('coupon_enddate','>=',date("Y-m-d"))->get();
                        if(count($couponcode)>0){
                            if($couponcode[0]->discount_type == "2"){
                
                                $coupondiscount = (floatval($order_total)*floatval($couponcode[0]->coupon_amount)/100);
                                
                                if($couponcode[0]->coupon_max_amount != null){
                                  $discount = ($coupondiscount > $couponcode[0]->coupon_max_amount)?$couponcode[0]->coupon_max_amount:$coupondiscount;
                                }
                                if($couponcode[0]->coupon_min_amount != null){
                                  $discount = ($coupondiscount < $couponcode[0]->coupon_min_amount)?$couponcode[0]->coupon_min_amount:$coupondiscount;
                                }

                                if($coupondiscount > $order_total){
                                  return response()->json(['status' => 400, 'error' => 'Discount is more than cart amount. Invalid discout coupon.']);
                                }else{
                                  $discount = $coupondiscount;
                                }

                            }else{
                                return response()->json(['status' => 400, 'error' => 'Coupon is expired or invalid or not featured.']);
                            }
                        }
                    }

                    $order_grandtotal = round(floatval($totalamount) + floatval($totaltax) - floatval($discount) + floatval($order_shipping_charge), 2);

                    $updatedata = array();
                    $updatedata['shipping_id'] = $shipping_id;
                    $updatedata['order_shipping_charge'] = $order_shipping_charge;
                    $updatedata['order_discount'] = $discount;
                    $updatedata['order_amount'] = $totalamount;
                    $updatedata['order_tax'] = $totaltax;
                    $updatedata['order_total'] = $order_total;
                    $updatedata['order_grandtotal'] = $order_grandtotal;
                    $updatedata['order_invoice_no'] = $order_invoice_no;
                    $order = new EcomOrder();
                    $result = $order->where('order_id', $order_id)->update($updatedata);
                        
                    $business = new EcomBusinessSetting();
                    $order = new EcomOrder();
                    $orderdetail = new EcomOrderDetails();
                    $orderitems = new EcomOrderItems();
                    $ecomordertax = new EcomOrderTax();
                    $data['businessinfo'] = $businessdata;
                    $data['order'] = $order->where('order_id', $order_id)->first();
                    $data['ordertax'] = $ecomordertax->where('order_id', $order_id)->get();
                    $data['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();

                    /* Generate Order Items */
                    $orderdata = $orderitems->where('order_id', $order_id)->get();
                    foreach($orderdata as $oitem){
                        $taxdata = array(
                            "order_item_id" => $oitem['order_item_id']
                        );
                        $ecomorderitemtax = new EcomOrderItemsTax();
                        $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
                    }
                    $data['orderitems'] = $orderdata;
                    
                    $data['downloadinvoice'] = generateinvoice($order_id, $order_invoice_no, $data);
                    
                    /* Record Order Activity */
                    $ecomorderactivity = new EcomOrderActivity();
                    $ecomorderactivity->order_id = $order_id;
                    $ecomorderactivity->order_activity_type = "Order Created";
                    $ecomorderactivity->order_activity_details = "Order has been created by customer. Order Amount:".$totalamount." Order Tax:".$totaltax." Order Total:".$order_grandtotal;
                    $ecomorderactivity->site_id = $request->input('site_id');
                    $ecomorderactivity->created_by = $request->input('user_id');
                    $ecomorderactivity->updated_by = $request->input('user_id');
                    $result = $ecomorderactivity->save();

                    $txn_id = generateAccessToken(25);

                    /* cash on delivery */
                    if($request->input('payment_type')=="cashondelivery"){
                        
                        /* Add Payment */
                        $payment = new EcomPayments();
                        $payment->order_id = $request->input('order_id');
                        $payment->user_id = $request->input('user_id');
                        $payment->site_id = $request->input('site_id');
                        $payment->txn_id = $txn_id;
                        $payment->amount = $order_grandtotal;
                        $payment->user_name = $userdata[0]->user_firstname;
                        $payment->user_email = $userdata[0]->user_email;
                        $payment->user_phone = $userdata[0]->user_mobile;
                        $payment->payment_type = $request->input('payment_type');
                        $payment->product_info = "productinfo";
                        $payment->payment_status = 'success';
                        $payment->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $payment->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $payment->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $payment->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $result = $payment->save();

                        /* Manage Order Status */
                        $data = array();
                        $data['order_status'] = 'success'; 
                        $order = new EcomOrder();
                        $order->where('order_id', $request->input('order_id'))->update($data);

                        return response()->json(['status' => 200, 'data' => $payment]);
                    }
                    /* online payment */
                    else if($request->input('payment_type')=="onlinepayment"){
                        $payment = new EcomPayments();
                        $payment->order_id = $order_id;
                        $payment->user_id = $request->input('user_id');
                        $payment->site_id = $request->input('site_id');
                        $payment->txn_id = $txn_id;
                        $payment->amount = $order_grandtotal;
                        $payment->user_name = $userdata[0]->user_firstname;
                        $payment->user_email = $userdata[0]->user_email;
                        $payment->user_phone = $userdata[0]->user_mobile;
                        $payment->product_info = "productinfo";
                        $payment->payment_type = $request->input('payment_type');
                        $payment->payment_status = 'initiated';
                        $payment->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $payment->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $payment->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $payment->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $result = $payment->save();
                        $paymentid = $payment->id;
                        // return response()->json(['status' => 200, 'data' => $paymentid]);
                        
                        if ($result) {
                           
                            $EcomPaymentSettings = new EcomPaymentSettings();
                            $EcomPaymentSettingsdata = $EcomPaymentSettings->where('site_id', $request->input('site_id'))->where('status','1')->get();
                            // print_r($EcomPaymentSettingsdata);
                            if(count($EcomPaymentSettingsdata)>0){
                                $ecomorderactivity = new EcomOrderActivity();
                                $ecomorderactivity->order_id = $request->input('order_id');
                                $ecomorderactivity->order_activity_type = "Payment Initiated";
                                $ecomorderactivity->order_activity_details = "Payment process has been initiated.";
                                $ecomorderactivity->site_id = $request->input('site_id');
                                $ecomorderactivity->created_by = $request->input('user_id');
                                $ecomorderactivity->updated_by = $request->input('user_id');
                                $result = $ecomorderactivity->save();
                                
                                $gatewayname = preg_replace('/\s*/', '', $EcomPaymentSettingsdata[0]["gateway_name"]);
                                // echo  $gatewayname;
                                // convert the string to all lowercase
                                $gatewayname = strtolower($gatewayname);
                                
                                if( $gatewayname =="razorpay" ){

                                    // $receiptid = generateAccessToken(25);

                                    $orderData = [
                                        'receipt' => 'receipt_'.$txn_id,
                                        'amount'  =>$order_grandtotal*100,
                                        'currency' => 'INR',
                                        'payment_capture' => 1 // auto capture
                                    ];
                                    
                                    $api = new Api(razorpaykeyid, razorpaykeysecret);
                                    $order  = $api->order->create($orderData);
                                    $orderid = $order["id"];
                                    $payment->where('payment_id', $paymentid)->update(["txn_id"=>$orderid]);
                                    $ecomPaymentsdata  = $payment->where('payment_id',$paymentid)->latest()->get();

                                    return response()->json(['status' => 200, 'data' => $ecomPaymentsdata]);

                                  



                                }else{
                                    $parameters = [
                                        'txnid' => $txn_id,
                                        'order_id' => $order_id,
                                        'amount' => $order_grandtotal,
                                        'firstname' => $userdata[0]->user_firstname,
                                        'lastname' => $userdata[0]->user_lastname,
                                        'email' => $userdata[0]->user_email,
                                        'phone' => $userdata[0]->user_mobile,
                                        'productinfo' => '123prod',
                                        'service_provider' => '',
                                        'zipcode' => $ecombillingaddressdata[0]->pincode,
                                        'city' => $ecombillingaddressdata[0]->city,
                                        'state' => $ecombillingaddressdata[0]->state,
                                        'country' => $ecombillingaddressdata[0]->country,
                                        'address1' => $ecombillingaddressdata[0]->address,
                                        'address2' => $ecombillingaddressdata[0]->landmark,
                                        'curl' => url('updatepaymentbypayu'),
                                      ];
                                       /* Record Order Activity */
                                    $ecomorderactivity = new EcomOrderActivity();
                                    $ecomorderactivity->order_id = $order_id;
                                    $ecomorderactivity->order_activity_type = "Payment Initiated";
                                    $ecomorderactivity->order_activity_details = "Payment process has been initiated.";
                                    $ecomorderactivity->site_id = $request->input('site_id');
                                    $ecomorderactivity->created_by = $request->input('user_id');
                                    $ecomorderactivity->updated_by = $request->input('user_id');
                                    $result = $ecomorderactivity->save();
                                    return response()->json(['status' => 200, 'data' => $parameters]);
                                }
                                // return response()->json(['status' => 200, 'data' =>'sd' ]); 
                            }else{
                                $parameters = [
                                    'txnid' => $txn_id,
                                    'order_id' => $order_id,
                                    'amount' => $order_grandtotal,
                                    'firstname' => $userdata[0]->user_firstname,
                                    'lastname' => $userdata[0]->user_lastname,
                                    'email' => $userdata[0]->user_email,
                                    'phone' => $userdata[0]->user_mobile,
                                    'productinfo' => '123prod',
                                    'service_provider' => '',
                                    'zipcode' => $ecombillingaddressdata[0]->pincode,
                                    'city' => $ecombillingaddressdata[0]->city,
                                    'state' => $ecombillingaddressdata[0]->state,
                                    'country' => $ecombillingaddressdata[0]->country,
                                    'address1' => $ecombillingaddressdata[0]->address,
                                    'address2' => $ecombillingaddressdata[0]->landmark,
                                    'curl' => url('updatepaymentbypayu'),
                                  ];
                                   /* Record Order Activity */
                                $ecomorderactivity = new EcomOrderActivity();
                                $ecomorderactivity->order_id = $order_id;
                                $ecomorderactivity->order_activity_type = "Payment Initiated";
                                $ecomorderactivity->order_activity_details = "Payment process has been initiated.";
                                $ecomorderactivity->site_id = $request->input('site_id');
                                $ecomorderactivity->created_by = $request->input('user_id');
                                $ecomorderactivity->updated_by = $request->input('user_id');
                                $result = $ecomorderactivity->save();
                                return response()->json(['status' => 200, 'data' => $parameters]);
                            }
                            

                           

                        } else {
                            return response()->json(['status' => 400, 'error' => "Something went wrong2."], 400);
                        }
                    }else{
                        return response()->json(['status' => 400, 'error' => "Invalid payment method."], 400);
                    }
                    
                    /*return response()->json(['status' => 200, 'base_price'=>$baseprice, 'data' => $data, 'result' => $result, 'orderitemstaxdata'=>$orderitemstaxdata, 'ecomorderitemlist'=>$ecomorderitemlist]);*/
                    
                } else {
                    return response()->json(['status' => 400, 'error' => 'There is no item in the cart.'], 400);
                }
            }
        }
    }
    
    public function checkout(Request $request) {

        $cart = new EcomCart();
        $product = new EcomProduct();
        $user = new User(); 
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $cartidlist = $cart->where('user_id', $request->input('user_id'))->where('cart_quantity', '<=', 0)->where('status', 1)->pluck('cart_id');

            if (count($cartidlist) > 0){
                return response()->json(['status' => 400, 'error' => 'Items with 0 or less than 0 quantity not allowed in the cart.'], 400);
            }else{

                $cartidlist = $cart->where('user_id', $request->input('user_id'))->where('status', 1)->pluck('cart_id');

                if (count($cartidlist) > 0) 
                {

                    /* Business Data */
                    $business = new EcomBusinessSetting();
                    $businessdata = $business->where('site_id',$request->input('site_id'))->first();

                    /* Create an Order Details from order */
                    $user = new User();
                    $userdata = $user->where('user_id', $request->input('user_id'))->get();

                    /* update user address */
                    $ecomaddress = new EcomAddress();
                    $ecomshippingaddressdata = $ecomaddress->where('address_type', 'shipping')->where('user_id', $request->input('user_id'))->get();
                    $ecombillingaddressdata = $ecomaddress->where('address_type', 'billing')->where('user_id', $request->input('user_id'))->get();

                    /*if (count($ecomshippingaddressdata) == 0 || count($ecomshippingaddressdata) == 0) 
                    {
                        return response()->json(['status' => 400, 'error' => 'Items with 0 or less than 0 quantity not allowed in the cart.'], 400);
                    }*/

                    /* Prepare Summary */

                    $total = 0;
                    $tax = 0;
                    $order_amount = 0;
                    $discount = 0;
                    $shipping_charge = 0;


                    /* Create an Order from cart */
                    $order = new EcomOrder();
                    $order->user_id = $request->input('user_id');
                    $order->shipping_id = ($request->has('shipping_id') && $request->filled('shipping_id')) ? $request->input('shipping_id') : null;
                    $order->coupon_id = ($request->has('coupon_id') && $request->filled('coupon_id')) ? $request->input('coupon_id') : null;
                    $order->order_amount = $order_amount;
                    $order->order_tax = $tax;
                    $order->order_total = $order_amount + $tax;
                    $order->order_discount = $discount;
                    $order->order_shipping_charge = $shipping_charge;
                    $order->order_grandtotal = $order_amount + $tax - $discount + $shipping_charge;
                    $order->order_status = 'pending';
                    $order->order_guid = generateAccessToken(25);
                    $order->site_id = $request->input('site_id');
                    $order->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $order->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $order->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $order->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $order->save();
                    $order_id = $order->id;

                    /* Order Invoice No */
                    $order_invoice_no = preg_replace('/\s*/', '', strtolower($businessdata->business_name)).date('Y').date('m').$order_id;

                    /* Add Order Notes */

                    if($request->has('site_id')){
                        $ordernotes = new EcomOrderNotes();
                        $ordernotes->order_id = $order_id;
                        $ordernotes->site_id = $request->input('site_id');
                        $ordernotes->order_notes = $request->input('order_notes');
                        $ordernotes->order_notes_for = 'forcustomer';
                        $ordernotes->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $ordernotes->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $ordernotes->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $ordernotes->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $ordernotes->save();
                    }

                    /* Clone order details */

                    $orderdetails = new EcomOrderDetails();
                    $orderdetails->order_id = $order->id;
                    $orderdetails->user_first_name = $userdata[0]->user_first_name;
                    $orderdetails->user_last_name = $userdata[0]->user_last_name;
                    $orderdetails->shipping_address = count($ecomshippingaddressdata)>0?$ecomshippingaddressdata[0]->address:null;
                    $orderdetails->shipping_landmark = count($ecomshippingaddressdata)>0?$ecomshippingaddressdata[0]->landmark:null;
                    $orderdetails->shipping_city = count($ecomshippingaddressdata)>0?$ecomshippingaddressdata[0]->city:null;
                    $orderdetails->shipping_state = count($ecomshippingaddressdata)>0?$ecomshippingaddressdata[0]->state:null;
                    $orderdetails->shipping_country = count($ecomshippingaddressdata)>0?$ecomshippingaddressdata[0]->country:null;
                    $orderdetails->shipping_pincode = count($ecomshippingaddressdata)>0?$ecomshippingaddressdata[0]->pincode:null;
                    $orderdetails->billing_address = count($ecombillingaddressdata)>0?$ecombillingaddressdata[0]->address:null;
                    $orderdetails->billing_landmark = count($ecombillingaddressdata)>0?$ecombillingaddressdata[0]->landmark:null;
                    $orderdetails->billing_city = count($ecombillingaddressdata)>0?$ecombillingaddressdata[0]->city:null;
                    $orderdetails->billing_state = count($ecombillingaddressdata)>0?$ecombillingaddressdata[0]->state:null;
                    $orderdetails->billing_country = count($ecombillingaddressdata)>0?$ecombillingaddressdata[0]->country:null;
                    $orderdetails->billing_pincode = count($ecombillingaddressdata)>0?$ecombillingaddressdata[0]->pincode:null;
                    $orderdetails->created_by = $request->input('user_id');
                    $orderdetails->updated_by = $request->input('user_id');
                    $orderdetails->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $orderdetails->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $orderdetails->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $orderdetails->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $orderdetails->save();

                    /* Add Order Items to the Order */

                    $cartdata = array();
                    $cartdata['user_id'] = $request->input('user_id');
                    $cartdata['offset'] = 0;
                    $cartdata['limit'] = 100000;
                    $cartdata['status'] = 1;
                    $cartitems = $cart->getcart($cartdata);

                    /* Create an Order Items from order */

                    $totaltax = 0; $totalshipping = 0; $totalamount = 0; $totalitems = 0;

                    foreach ($cartitems as $item) {

                        $totalnooftax = 1;
                        $tax = 0;
                        $amount = round(floatval($item->cart_amount)*intval($item->cart_quantity) ,2);
                        $baseprice = $amount;

                        $ecomtax = new EcomTax();
                        $user_state = (count($ecombillingaddressdata)>0 && $ecombillingaddressdata[0]->state != null)?$ecombillingaddressdata[0]->state:"";
                        $business_state = (isset($businessdata->business_state))?$businessdata->business_state:"";

                        if(count($ecombillingaddressdata) > 0){

                            $taxobj = $ecomtax->gettaxbyproductid($item->product_id, $request->input('site_id'), $request->input('user_id'), $ecombillingaddressdata[0], $businessdata);
                            $totalnooftax = count($taxobj);

                            if(count($taxobj)>0){

                                $totalitemtax = 0;

                                foreach ($taxobj as $taxitem) {
                                    $totalitemtax = $totalitemtax + floatval($taxitem['tax_percentage']);
                                }

                                $baseprice = (floatval($amount)*100)/(floatval($totalitemtax)+100);

                                foreach ($taxobj as $taxitem) {
                                    //$currenttax = round($baseprice * (floatval($totalitemtax/count($taxobj))/100), 2);
                                    $currenttax = round((($baseprice * floatval($taxitem['tax_percentage']))/100), 2);
                                    //$currenttax = round($currenttax, 2);
                                    $tax = $tax + $currenttax;  
                                }
                            }

                        }
                        
                        /* Generate Order Item */
                        $orderitems = new EcomOrderItems();
                        $orderitems->order_id = $order_id;
                        $orderitems->product_id = $item->product_id;
                        $orderitems->attribute_id = $item->attribute_id;
                        $orderitems->attribute_term_id = $item->attribute_term_id;
                        $orderitems->attribute_term_name = $item->attribute_term_name;
                        $orderitems->order_product_price_with_tax = $item->cart_amount;
                        $orderitems->order_product_quantity = $item->cart_quantity;
                        $orderitems->order_product_amount = round($baseprice, 2);
                        $orderitems->order_product_tax = round($tax, 2);
                        $orderitems->order_product_total = $amount;
                        $orderitems->product_name = $item->product_name;
                        $orderitems->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $orderitems->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $orderitems->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $orderitems->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $orderitems->save();

                        $order_item_id = $orderitems->id;

                        if(count($ecombillingaddressdata) > 0){
                            /* Generate Order Item Tax */
                            if(count($taxobj)>0){
                                foreach ($taxobj as $taxitem) {
                                    $orderitemstax = new EcomOrderItemsTax();
                                    $orderitemstax->order_item_id = $order_item_id;
                                    $orderitemstax->tax_id = $taxitem['tax_id'];
                                    $orderitemstax->tax_name = $taxitem['tax_name'];
                                    $orderitemstax->tax_percentage = $taxitem['tax_percentage'];
                                    $orderitemstax->tax_amount = round(($baseprice * floatval($taxitem['tax_percentage']))/100, 2);
                                    $orderitemstax->status = 1;
                                    $orderitemstax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $orderitemstax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $orderitemstax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $orderitemstax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                    $orderitemstax->save();

                                    $orderitems = new EcomOrderItems();
                                    $orderitems->where("order_item_id", $order_item_id)->update(array(
                                        "tax_id"=>$taxitem['tax_id']
                                    ));
                                }
                            }
                        }

                        $totaltax = $totaltax + $tax;
                        $totalamount = $totalamount + $baseprice;
                        $totalitems = $totalitems + intval($item->cart_quantity);

                    }

                    $orderitems = new EcomOrderItems();
                    $ecomorderitemlist = $orderitems->where('order_id', $order_id)->where('status', 1)->pluck('order_item_id');

                    $orderitemstax = new EcomOrderItemsTax();
                    $orderitemstaxdata = $orderitemstax->select('tax_name', DB::raw("SUM(tax_amount) as tax_amount"))->whereIn('order_item_id', $ecomorderitemlist)->groupby('tax_name')->get();

                    if(count($orderitemstaxdata) > 0){
                        foreach ($orderitemstaxdata as $item) {
                            $ecomodertax = new EcomOrderTax();
                            $ecomodertax->order_id = $order_id;
                            $ecomodertax->tax_name = $item->tax_name;
                            $ecomodertax->tax_percentage = 0;
                            $ecomodertax->tax_amount = $item->tax_amount;
                            $ecomodertax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $ecomodertax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $ecomodertax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $ecomodertax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $ecomodertax->save();
                        }
                    }

                    $order_shipping_charge = 0; $shipping_id = null; $shipping_class_id = 1; $shipping_method_id=1;

                    if(count($ecomshippingaddressdata)>0){
                        $shippingmodel = new EcomShipping();
                        $shippingobj = $shippingmodel->getshippingforsiteid($shipping_class_id, $shipping_method_id=1, $request->input('site_id'), $ecomshippingaddressdata[0]);

                        if(count($shippingobj)>0){
                            foreach ($shippingobj as $item) {
                                $order_shipping_charge = isset($item->shipping_rate)?$item->shipping_rate:0;
                                $shipping_id = $item->shipping_id;
                            }
                        }
                    }

                    /* round up values */
                    /*$totalamount = round($totalamount, 2);
                    $totaltax = round($totaltax, 2);
                    $discount = round($discount, 2);*/
                    $order_shipping_charge = round($order_shipping_charge, 2);
                    $order_total = round(floatval($totalamount) + floatval($totaltax), 2);
                    $order_grandtotal = round(floatval($totalamount) + floatval($totaltax) - floatval($discount) + floatval($order_shipping_charge), 2);

                    $updatedata = array();
                    $updatedata['shipping_id'] = $shipping_id;
                    $updatedata['order_shipping_charge'] = $order_shipping_charge;
                    $updatedata['order_discount'] = $discount;
                    $updatedata['order_amount'] = $totalamount;
                    $updatedata['order_tax'] = $totaltax;
                    $updatedata['order_total'] = $order_total;
                    $updatedata['order_grandtotal'] = $order_grandtotal;
                    $updatedata['order_invoice_no'] = $order_invoice_no;
                    $order = new EcomOrder();
                    $result = $order->where('order_id', $order_id)->update($updatedata);
                        
                    $business = new EcomBusinessSetting();
                    $order = new EcomOrder();
                    $orderdetail = new EcomOrderDetails();
                    $orderitems = new EcomOrderItems();
                    $ecomordertax = new EcomOrderTax();
                    $data['businessinfo'] = $businessdata;
                    $data['order'] = $order->where('order_id', $order_id)->first();
                    $data['ordertax'] = $ecomordertax->where('order_id', $order_id)->get();
                    $data['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();

                    /* Generate Order Items */
                    $orderdata = $orderitems->where('order_id', $order_id)->get();
                    foreach($orderdata as $oitem){
                        $taxdata = array(
                            "order_item_id" => $oitem['order_item_id']
                        );
                        $ecomorderitemtax = new EcomOrderItemsTax();
                        $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
                    }
                    $data['orderitems'] = $orderdata;
                    $data['downloadinvoice'] = generateinvoice($order_id, $order_invoice_no, $data);

                    /* Record Order Activity */
                    $ecomorderactivity = new EcomOrderActivity();
                    $ecomorderactivity->order_id = $order_id;
                    $ecomorderactivity->order_activity_type = "Order Created";
                    $ecomorderactivity->order_activity_details = "Order has been created by customer. Order Amount:".$totalamount." Order Tax:".$totaltax." Order Total:".$order_grandtotal;
                    $ecomorderactivity->site_id = $request->input('site_id');
                    $ecomorderactivity->created_by = $request->input('user_id');
                    $ecomorderactivity->updated_by = $request->input('user_id');
                    $result = $ecomorderactivity->save();
                    
                    return response()->json(['status' => 200, 'base_price'=>$baseprice, 'data' => $data, 'result' => $result, 'orderitemstaxdata'=>$orderitemstaxdata, 'ecomorderitemlist'=>$ecomorderitemlist]);
                    
                } else {
                    return response()->json(['status' => 400, 'error' => 'There is no item in the cart.'], 400);
                }
            }
        }
    }

    public function getallorder(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $order = new Ecomorder();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('order_id') && $request->input('order_id') != "") {
                $data['order_id'] = $request->input('order_id');
            }

            if ($request->has('order_guid') && $request->input('order_guid') != "") {
                $data['order_guid'] = $request->input('order_guid');
            }

            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('from') && $request->input('from') != "") {
                $data['from'] = $request->input('from');
            }

            if ($request->has('from_date') && $request->input('from_date') != "" && $request->has('to_date') && $request->input('to_date') != "") {
                $data['from_date'] = $request->input('from_date');
                $data['to_date'] = $request->input('to_date');
            }

            if ($request->has('order_status') && $request->input('order_status') != "") {
                $data['order_status'] = $request->input('order_status');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $order = $order->getorder($data);
            $orderdata = array();
            $orderdetaildata = array();
            foreach($order as $item)
            {
                    $orderitems = new EcomOrderItems();
                    $tmpdata["order_id"]=$item->order_id;
                    $tmpdata["limit"]=9999999999;
                    $tmpdata["offset"]=0;
                    ////////////////
                    $orderitemsdata = $orderitems->getorderitems($tmpdata);
                    
                    $item->productinfo = $orderitems->getorderitemscount($tmpdata);
                    
                    
                   foreach($orderitemsdata as $item2)
                    {
                         $product_id = $item2->product_id;    
                        
                        //call for getting gallery image with product
                        $productgallery = new EcomProductGallery();
                        $gallerydata['product_id']=$product_id;
                        $gallerydata["limit"]=9999999999;
                        $gallerydata["offset"]=0;
                        // $data['isfeatured']=1;
                        $productgallery = $productgallery->getproductgallery($gallerydata);
                        $productgallerydata = array();
                        
                        foreach ($productgallery as $item1) {
                          
                            if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                            {
                                $item1->product_image_media_file =fixedproductgalleryimage;
                               
                            }
                            else
                            {
                                if(fileuploadtype=="local")
                                {
                                      if (File::exists(imagedisplaypath .$item1->product_image_media_file)) {
                                            $item1->product_image_media_file = imagedisplaypath.$item1->product_image_media_file;
                                        } else {
                                            $item1->product_image_media_file =fixedproductgalleryimage;
                                        }
                                    
                                }
                                else
                                {
                                    
                                    if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                                        $item1->product_image_media_file = imagedisplaypath. $item1->product_image_media_file;
                                    } else {
                                        $item1->product_image_media_file = fixedproductgalleryimage;
                                    }
                                }
                          
                            }  
                            array_push($productgallerydata, $item1);
                        }
                        $item2->product_gallery=$productgallerydata; 
                        
                        // array_push($orderdetaildata, $item2);
                    } 
                    
                    $item->orderdetail= $orderitemsdata;

                    array_push($orderdata, $item);
            }
            return response()->json(['status' => 200, 'count' => count($order), 'data' => $orderdata]);
        }
    }

    public function updateorder(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "order_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $order = new Ecomorder();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
          
            $newrequest = $request->except(['order_id','accesstoken','user_id']);
            $result = $order->where('order_id', $request->input('order_id'))->update($newrequest);

            if ($result) {

                /* Record Order Activity */
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $request->input('order_id');
                $ecomorderactivity->order_activity_type = "Order Updated";
                $ecomorderactivity->order_activity_details = "Order information has been updated.";
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();

                if($request->has('order_status')){
                    if($request->input('order_status')=="shipping"){
                        $business = new EcomBusinessSetting();
                        $order = new EcomOrder();
                        $orderdetail = new EcomOrderDetails();
                        $orderitems = new EcomOrderItems();
                        $ecomordertax = new EcomOrderTax();
                        $order_id = $request->input('order_id');
                        $data = array();
                        $data['businessinfo'] = $business->where('site_id',$request->input('site_id'))->first();
                        $data['order'] = $order->where('order_id', $order_id)->first();
                        $data['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();
                        $data['orderitems'] = $orderitems->where('order_id', $order_id)->get();
                        $data['ordertax'] = $ecomordertax->where('order_id', $order_id)->get();
                        /* Generate Order Items */
                        $orderdata = $orderitems->where('order_id', $order_id)->get();
                        foreach($orderdata as $oitem){
                            $taxdata = array(
                                "order_item_id" => $oitem['order_item_id']
                            );
                            $ecomorderitemtax = new EcomOrderItemsTax();
                            $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
                        }
                        $data['orderitems'] = $orderdata;
                        $data['downloadinvoice'] = generateinvoice($order_id, $data['order']->order_invoice_no, $data);

                        /* Record Order Activity */
                        $ecomorderactivity = new EcomOrderActivity();
                        $ecomorderactivity->order_id = $request->input('order_id');
                        $ecomorderactivity->order_activity_type = "Order Status Updated";
                        $ecomorderactivity->order_activity_details = "Order has been ".$request->input('order_status');
                        $ecomorderactivity->site_id = $request->input('site_id');
                        $ecomorderactivity->created_by = $request->input('user_id');
                        $ecomorderactivity->updated_by = $request->input('user_id');
                        $result = $ecomorderactivity->save();
                    }
                }
                return response()->json(['status' => 200, 'data' => "Order updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteorder(Request $request) {

        $valid = Validator::make($request->all(), [
                    "order_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $order = new Ecomorder();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['order_id','accesstoken','user_id']);
            $result = $order->where('order_id', $request->input('order_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Order deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function getorderdetails(Request $request){
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            if($request->input('order_guid') != "" && $request->input('order_id') != ""){
                return response()->json(['status' => 400, 'error' =>"order guid or order id missing."], 400);
            }

            $ecomordermodel = new EcomOrder();
            $ecomorderdetailsmodel = new EcomOrderDetails();
            $ecomorderitemmodel = new EcomOrderItems();
            $ecomordernotes = new EcomOrderNotes();
            $ecompayment = new EcomPayments();
            $ecomorderitemtax = new EcomOrderItemsTax();
            $ecomodertax = new EcomOrderTax();
            $ecomorderactivity = new EcomOrderActivity();

            $finalorderdetails = array();

            $data = array();
            $data['offset'] = 0;
            $data['limit'] = 100;

            if ($request->has('order_id') && $request->input('order_id') != "") {
                $data['order_id'] = $request->input('order_id');
            }

            if ($request->has('order_guid') && $request->input('order_guid') != "") {
                $data['order_guid'] = $request->input('order_guid');
            }

            $temp = $ecomordermodel->getorder($data);

            $finalorderdetails['orderdata'] = count($temp)>0?$temp[0]:null;
            $finalorderdetails['ordertax'] = $ecomodertax->getordertax($data);

            $temp = $ecomorderdetailsmodel->getorderdetails($data);
            $finalorderdetails['orderdetails'] = count($temp)>0?$temp[0]:null;

            $temp = $ecomorderitemmodel->getorderitems($data);
            foreach($temp as $item){

                $taxdata = array(
                    "order_item_id" =>$item->order_item_id
                );
                $item->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
                $tempdata = array();
                $tempdata['product_id'] = $item->product_id;
                $tempdata['offset'] = 0;
                $tempdata['limit'] = 100;
                $product = new EcomProduct();
                $products = $product->getproduct($tempdata);
                //$item->products = $products;
                if(count($products)>0){
                    
                    $productgallery = new EcomProductGallery();
                    $gallerydata = array();
                    $gallerydata['product_id']=$products[0]->product_id;
                    $gallerydata['offset'] = 0;
                    $gallerydata['limit'] = 1000;
                    $productgallery = $productgallery->getproductgallery($gallerydata);
                    $productgallerydata=array();
                    foreach ($productgallery as $item1) {
                    
                        if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                        {
                            $item1->product_image_path =fixedproductgalleryimage;
                        
                        }
                        else
                        {
                            if(fileuploadtype=="local")
                            {
                                if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                        $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                    } else {
                                        $item1->product_image_path =fixedproductgalleryimage;
                                    }
                                
                            }
                            else
                            {
                                if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                                    $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                                } else {
                                    $item1->product_image_path = fixedproductgalleryimage;
                                }
                            }
                        }  

                    
                        array_push($productgallerydata, $item1);
                    }
                    $products[0]->product_gallery=$productgallerydata;

                    //product category
                    $categorynamelist = DB::table('tbl_ecom_product_category as pc')
                    ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                    ->where('pc.status', '=', 1)
                    ->where('pc.product_id', '=', $products[0]->product_id)
                    ->orderBy('category.category_name', 'DESC')
                    ->pluck('category.category_name');

                    $products[0]->categorynamelist=$categorynamelist;

                    $item->productdata = count($products)>0?$products[0]:null;

                    /* Attribute Data */
                    $item->productattrdata = $product->getattributeandattrtermlistforproductid($products[0]->product_id);

                }else{
                    $item->productdata = array();
                }
            }
            $finalorderdetails['orderitems'] = count($temp)>0?$temp:null;

            $tempdata = array();
            $tempdata['offset'] = 0;
            $tempdata['limit'] = 100;
            $tempdata['order_id'] = $finalorderdetails['orderdata']->order_id;
            $temp = $ecomordernotes->getordernotes($tempdata);
            $finalorderdetails['ordernotes'] = count($temp)>0?$temp:null;
            
            $tempdata = array();
            $tempdata['offset'] = 0;
            $tempdata['limit'] = 100;
            $tempdata['order_id'] = $finalorderdetails['orderdata']->order_id;
            $temp = $ecompayment->getpayments($tempdata);
            $finalorderdetails['paymentinfo'] = count($temp)>0?$temp:null;

            $tempdata = array();
            $tempdata['offset'] = 0;
            $tempdata['limit'] = 100;
            $tempdata['order_id'] = $finalorderdetails['orderdata']->order_id;
            $temp = $ecomorderactivity->getecomorderactivity($tempdata);
            $finalorderdetails['orderactivity'] = count($temp)>0?$temp:null;

            $filename = $finalorderdetails['orderdata']->order_invoice_no.".pdf";
            $filename = url('/')."/storage/app/".$filename;
            if(does_url_exists($filename)) {
                $finalorderdetails['invoice'] = $filename;
            }else{
                $finalorderdetails['invoice'] = null;
            }

            return response()->json(['status' => 200, 'count' => count($finalorderdetails), 'data' => $finalorderdetails]);
        }
    }

    public function sendEmailInvoice(Request $request){
        $valid = Validator::make($request->all(), [
            "order_id" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $order_id = $request->input('order_id');
            $ecompayment = new EcomPayments();
            $ecomorderitemtax = new EcomOrderItemsTax();
            $user = new User();
            $business = new EcomBusinessSetting();
            $order = new EcomOrder();
            $orderdetail = new EcomOrderDetails();
            $orderitems = new EcomOrderItems();
            $ecomodertax = new EcomOrderTax();


            /* Create an Order Details from order */
            $data['order'] = $order->where('order_id', $order_id)->first();
            $data['ordertax'] = $ecomodertax->getordertax(
                array(
                    "order_id" => $order_id
                )
            );
            $userdata = $user->where('user_id', $data['order']->user_id)->first();
            $data['userinfo'] = $userdata;
            $data['businessinfo'] = $business->where('site_id', $request->input('site_id'))->first();;
            
            $data['orderdetail'] = $orderdetail->where('order_id', $order_id)->first();

            /* Generate Order Items */
            $orderdata = $orderitems->where('order_id', $order_id)->get();
            foreach($orderdata as $oitem){
                $taxdata = array(
                    "order_item_id" => $oitem['order_item_id']
                );
                $ecomorderitemtax = new EcomOrderItemsTax();
                $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
            }
            $data['orderitems'] = $orderdata;
            $tempdata = array();
            $tempdata['offset'] = 0;
            $tempdata['limit'] = 100;
            $tempdata['order_id'] =  $order_id;
            $temp = $ecompayment->getpayments($tempdata);
            $data['paymentinfo'] = count($temp)>0?$temp[0]:null;
            $order_invoice_no = isset($data['order']->order_invoice_no)?$data['order']->order_invoice_no:preg_replace('/\s*/', '', strtolower($data["businessinfo"]->business_name)).date('Y').date('m').$order_id;
            $data['downloadinvoice'] = generateinvoice($order_id, $order_invoice_no, $data);

            $emailstatus = sendEmailInvoice($data, $data['userinfo']->user_email);
            if($emailstatus){
                $ecomorderactivity = new EcomOrderActivity();
                $ecomorderactivity->order_id = $order_id;
                $ecomorderactivity->order_activity_type = "Order Invoice Sent";
                $ecomorderactivity->order_activity_details = "Order invoice has been sent.";
                $ecomorderactivity->site_id = $request->input('site_id');
                $ecomorderactivity->created_by = $request->input('user_id');
                $ecomorderactivity->updated_by = $request->input('user_id');
                $result = $ecomorderactivity->save();
            }else{
                 $ecomorderactivity = new EcomOrderActivity();
                    $ecomorderactivity->order_id = $order_id;
                    $ecomorderactivity->order_activity_type = "Order Invoice Not Sent";
                    $ecomorderactivity->order_activity_details = "Order invoice has been Not sent.";
                    $ecomorderactivity->site_id = $request->input('site_id');
                    $ecomorderactivity->created_by = $request->input('user_id');
                    $ecomorderactivity->updated_by = $request->input('user_id');
                    $result = $ecomorderactivity->save();

            }
            /* Record Order Activity */
            

            return response()->json(['status' => 200, 'data' => $data, 'result' => $result,'emailstatus'=>$emailstatus]);
        }
    }

    /* Update tax based on change of state */

    public function updateStateBasedTax(Request $request) {

        $cart = new EcomCart();
        $product = new EcomProduct();
        $user = new User(); 
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required",
            "order_id" => "required",
            "billing_country" => "required",
            "billing_state" => "required",
            "billing_city" => "required",
            "billing_pincode" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            /* Business Data */
            $business = new EcomBusinessSetting();
            $businessdata = $business->where('site_id',$request->input('site_id'))->first();

            /* Create an Order Details from order */
            $user = new User();
            $userdata = $user->where('user_id', $request->input('user_id'))->get();

            /* Prepare Summary */

            $total = 0;
            $tax = 0;
            $order_amount = 0;
            $discount = 0;
            $shipping_charge = 0;
			$baseprice = 0;


            /* Create an Order from cart */
            $order = new EcomOrder();
            $order_id = $request->input('order_id');
            $order_data = $order->where('order_id', $order_id)->first();

            /* Clone order details */

            $orderdetails = new EcomOrderDetails();
            $orderdetails->where('order_id', $order_id)->update(
                array(
                    "shipping_city"=>$request->has('shipping_city')?$request->input('shipping_city'):$request->input('billing_city'),
                    "shipping_state"=>$request->has('shipping_state')?$request->input('shipping_state'):$request->input('billing_state'),
                    "shipping_country"=>$request->has('shipping_country')?$request->input('shipping_country'):$request->input('billing_country'),
                    "shipping_pincode"=>$request->has('shipping_pincode')?$request->input('shipping_pincode'):$request->input('billing_pincode'),
                    "billing_city"=>$request->input('billing_city'),
                    "billing_state"=>$request->input('billing_state'),
                    "billing_country"=>$request->input('billing_country'),
                    "billing_pincode"=>$request->input('billing_pincode')
                )
            );
            
            /* Generate user billing info */
            $userbilling =  new stdClass();
            $userbilling->country = $request->input('billing_country');
            $userbilling->state = $request->input('billing_state');
            $userbilling->city = $request->input('billing_city');
            $userbilling->pincode = $request->input('billing_pincode');

            /* Create an Order Items from order */

            $totaltax = 0; $totalshipping = 0; $totalamount = 0; $totalitems = 0;

            
            
            /* Generate Order Item */
            $orderitems = new EcomOrderItems();
            $orderitemdata = DB::table('tbl_ecom_order_items as oi')->select('oi.*','tep.product_actual_price','tep.product_sale_price','tep.start_date','tep.end_date')
            ->leftJoin('tbl_ecom_product as tep', 'tep.product_id', '=', 'oi.product_id')
            ->where('oi.order_id', $order_id)->where('site_id',$request->input('site_id'))->where('oi.status', 1)->get();
			
            if(count($orderitemdata)>0){

                foreach($orderitemdata as $item){
                    $tax = 0;
                    $price = $item->order_product_total;
                    /*if($item->product_sale_price != null && $item->product_sale_price != 0){
                        $currentDate = date('Y-m-d');
                        $currentDate = date('Y-m-d', strtotime($currentDate));   
                        $startDate = date('Y-m-d', strtotime($item->start_date));
                        $endDate = date('Y-m-d', strtotime($item->end_date));   
                        if (($currentDate >= $startDate) && ($currentDate <= $endDate)){   
                            $price = $item->product_sale_price;
                        }
                    }*/

                    $order_item_id = $item->order_item_id;
                    $amount = $price;
                    $baseprice = $price;
                    $ecomtax = new EcomTax();
                    $user_state = $userbilling->state;
                    $business_state = (isset($businessdata->business_state))?$businessdata->business_state:"";
                    $taxobj = $ecomtax->gettaxbyproductid($item->product_id, $request->input('site_id'), $request->input('user_id'), $userbilling, $businessdata);
					
                    /*if(count($taxobj)>0){
                        foreach ($taxobj as $taxitem) {
                            $tax = $tax + floatval($amount) * (floatval($taxitem['tax_percentage'])/100);
                        }
                    }*/
                    
                    $totalitemtax = 0;

                    if(count($taxobj)>0){   

                        foreach ($taxobj as $taxitem) {
                            $totalitemtax = $totalitemtax + round(floatval($taxitem['tax_percentage']), 2);
                        }

                        //$baseprice = round(((floatval($amount)*100)/(floatval($totalitemtax)+100)), 2);
                        $baseprice = (floatval($amount)*100)/(floatval($totalitemtax)+100);

                        foreach ($taxobj as $taxitem) {
                            //$currenttax = round($baseprice * (floatval($totalitemtax/count($taxobj))/100), 2);
                            $currenttax = ($baseprice * floatval($taxitem['tax_percentage']))/100;
                            //$currenttax = round($currenttax, 2);
                            $tax = $tax + $currenttax;  
                        }
                    }

                    /* Generate Order Item Tax */
                    if(count($taxobj)>0){

                        $orderitemstax = new EcomOrderItemsTax();
                        $orderitemstax->where("order_item_id", $item->order_item_id)->update(
                            array("status"=>2)
                        );

                        foreach ($taxobj as $taxitem) {
                            $orderitemstax = new EcomOrderItemsTax();
                            $orderitemstax->order_item_id = $order_item_id;
                            $orderitemstax->tax_id = $taxitem['tax_id'];
                            $orderitemstax->tax_name = $taxitem['tax_name'];
                            $orderitemstax->tax_percentage = $taxitem['tax_percentage'];
                            $orderitemstax->tax_amount = round((($baseprice * floatval($taxitem['tax_percentage']))/100), 2);
                            $orderitemstax->status = 1;
                            $orderitemstax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $orderitemstax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $orderitemstax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $orderitemstax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $orderitemstax->save();

                            /*$orderitems = new EcomOrderItems();
                            $orderitems->where("order_item_id", $order_item_id)->update(array(
                                "tax_id"=>$taxitem['tax_id'],
                                "order_product_tax"=>$itemtax,
                                "order_product_total"=>floatval($amount)+floatval($itemtax),
                            ));*/


                        }
                    }

                    $totaltax = $totaltax + $tax;
                    $totalamount = $totalamount + $baseprice;

                }

            }

            
            $totalitems = $totalitems + count($orderitemdata);

            $orderitems = new EcomOrderItems();
            $ecomorderitemlist = $orderitems->where('order_id', $order_id)->where('status', 1)->pluck('order_item_id');

            /* Remove previous order tax */
            $ecomodertax = new EcomOrderTax();
            $ecomodertax->where('order_id', $order_id)->update(array('status'=>2));

            $orderitemstax = new EcomOrderItemsTax();
            $orderitemstaxdata = $orderitemstax->select('tax_name', DB::raw("SUM(tax_amount) as tax_amount"))->where('status', 1)
            ->whereIn('order_item_id', $ecomorderitemlist)->groupby('tax_name')->get();

            if(count($orderitemstaxdata)>0){
                
                foreach ($orderitemstaxdata as $item) {
                    $ecomodertax = new EcomOrderTax();
                    $ecomodertax->order_id = $order_id;
                    $ecomodertax->tax_name = $item->tax_name;
                    $ecomodertax->tax_percentage = 0;
                    $ecomodertax->tax_amount = round($item->tax_amount, 2);
                    $ecomodertax->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $ecomodertax->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $ecomodertax->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $ecomodertax->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $ecomodertax->save();
                }

            }

            $order_shipping_charge = 0; $shipping_id = null; $shipping_class_id = 1; $shipping_method_id=1;

            /* Generate user shipping info */
            $usershippingdata =  new stdClass();
            $usershippingdata->country = $request->has('shipping_country')?$request->input('shipping_country'):$request->input('billing_country');
            $usershippingdata->state = $request->has('shipping_state')?$request->input('shipping_state'):$request->input('billing_state');
            $usershippingdata->city = $request->has('shipping_city')?$request->input('shipping_city'):$request->input('billing_city');
            $usershippingdata->pincode = $request->has('shipping_pincode')?$request->input('shipping_pincode'):$request->input('billing_pincode');

            $shippingmodel = new EcomShipping();
            $shippingobj = $shippingmodel->getshippingforsiteid($shipping_class_id, $shipping_method_id=1, $request->input('site_id'), $usershippingdata);

            if(count($shippingobj)>0){
                foreach ($shippingobj as $item) {
                    $order_shipping_charge = isset($item->shipping_rate)?$item->shipping_rate:0;
                    $shipping_id = $item->shipping_id;
                }
            }

            /* round up values */
            $totalamount = round($totalamount, 2);
            $totaltax = round($totaltax, 2);
            $discount = round($discount, 2);
            $order_shipping_charge = round($order_shipping_charge, 2);
            $order_total = round(floatval($totalamount) + floatval($totaltax), 2);
            $order_grandtotal = round(floatval($totalamount) + floatval($totaltax) - floatval($discount) + floatval($order_shipping_charge), 2);

            $updatedata = array();
            $updatedata['shipping_id'] = $shipping_id;
            $updatedata['order_shipping_charge'] = $order_shipping_charge;
            $updatedata['order_discount'] = $discount;
            $updatedata['order_amount'] = $totalamount;
            $updatedata['order_tax'] = $totaltax;
            $updatedata['order_total'] = $order_total;
            $updatedata['order_grandtotal'] = $order_grandtotal;

            $order = new EcomOrder();
            $result = $order->where('order_id', $order_id)->update($updatedata);
                
            $business = new EcomBusinessSetting();
            $order = new EcomOrder();
            $orderdetail = new EcomOrderDetails();
            $orderitems = new EcomOrderItems();
            $ecomordertax = new EcomOrderTax();
            $data['businessinfo'] = $businessdata;
            $data['order'] = $order->where('order_id', $order_id)->first();
            $data['ordertax'] = $ecomordertax->where('order_id', $order_id)->where('status', 1)->get();
            $data['orderdetail'] = $orderdetail->where('order_id', $order_id)->where('status', 1)->first();

            /* Generate Order Items */
            $orderdata = $orderitems->where('order_id', $order_id)->where('status', 1)->get();
            foreach($orderdata as $oitem){
                $taxdata = array(
                    "order_item_id" => $oitem['order_item_id']
                );
                $ecomorderitemtax = new EcomOrderItemsTax();
                $oitem->tax_info = $ecomorderitemtax->getorderitemtax($taxdata);
            }
            $data['orderitems'] = $orderdata;
            $data['downloadinvoice'] = generateinvoice($order_id, $order_data->order_invoice_no, $data);

            /* Record Order Activity */
            $ecomorderactivity = new EcomOrderActivity();
            $ecomorderactivity->order_id = $order_id;
            $ecomorderactivity->order_activity_type = "Order Invoice Sent";
            $ecomorderactivity->order_activity_details = "Order invoice has been sent to ".isset($userdata[0]->user_email)?$userdata[0]->user_email:"customer email";
            $ecomorderactivity->site_id = $request->input('site_id');
            $ecomorderactivity->created_by = $request->input('user_id');
            $ecomorderactivity->updated_by = $request->input('user_id');
            $result = $ecomorderactivity->save();
            
            return response()->json(['status' => 200, 'base_price'=>number_format((float)$baseprice, 2, '.', ''), 'data' => $data, 'result' => $result]);

        }
    }

}
