<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomTaxStatus;
use Carbon\Carbon;
class EcomTaxStatusController extends Controller
{
    public function addtaxstatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "tax_status_name"=>"required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxstatus = new EcomTaxStatus();
            $taxstatus->tax_status_name = $request->input('tax_status_name');
            $taxstatus->created_by = $request->input('user_id');
            $taxstatus->status = 1;
            $taxstatus->site_id = $request->input('site_id');
            $taxstatus->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $taxstatus->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $taxstatus->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $taxstatus->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $taxstatus->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "taxstatus Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalltaxstatus(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxstatus = new EcomTaxStatus();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('tax_status_id') && $request->input('tax_status_id') != "") {
                $data['tax_status_id'] = $request->input('tax_status_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }
            $taxstatus = $taxstatus->gettaxstatus($data);

            return response()->json(['status' => 200, 'count' => count($taxstatus), 'data' => $taxstatus]);
        }

    }

    public function updatetaxstatus(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "tax_status_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxstatus = new EcomTaxStatus();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tax_status_id','user_id','accesstoken']);
            $result = $taxstatus->where('tax_status_id', $request->input('tax_status_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "taxstatus Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletetaxstatus(Request $request) {

        $valid = Validator::make($request->all(), [
                    "tax_status_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $taxstatus = new EcomTaxStatus();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tax_status_id','user_id','accesstoken']);
            $result = $taxstatus->where('tax_status_id', $request->input('tax_status_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Tax Status Deleted sUCCESSFULLY"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
