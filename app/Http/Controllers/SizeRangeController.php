<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\SizeRange;

class SizeRangeController extends Controller
{
     public function addsizerange(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "size_range_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $sizerange = new SizeRange();
            $sizerange->size_range_name = $request->input('size_range_name');
            $sizerange->created_by = $request->input('created_by');
            $sizerange->updated_by = $request->input('updated_by');
            $sizerange->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $sizerange->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $sizerange->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $sizerange->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $sizerange->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Size Range Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
     public function getallsizerange(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $sizerange = new SizeRange();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            
            if ($request->has('size_range_id') && $request->input('size_range_id') != "") {
                $data['size_range_id'] = $request->input('size_range_id');
            }
            $sizerange = $sizerange->getsizerange($data);
            return response()->json(['status' => 200, 'count' => count($sizerange), 'data' => $sizerange]);
        }
    }
     public function updatesizerange(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "size_range_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $sizerange = new SizeRange();
            $newrequest = $request->except(['size_range_id']);
            $result = $sizerange->where('size_range_id', $request->input('size_range_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Size Range Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function deletesizerange(Request $request) {
        $valid = Validator::make($request->all(), [
                    "size_range_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $sizerange = new SizeRange();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['size_range_id']);
            $result = $sizerange->where('size_range_id', $request->input('size_range_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function managesizerange(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "size_range_name" => "required|numeric",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->has('category_id') && $request->filled('category_id')){
                $sizerange = new SizeRange();
                $newrequest = $request->except(['size_range_id']);
                $result = $sizerange->where('size_range_id', $request->input('size_range_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Size Range Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $sizerange = new SizeRange();
                $sizerange->size_range_name = $request->input('size_range_name');
                $sizerange->created_by = $request->input('created_by');
                $sizerange->updated_by = $request->input('updated_by');
                $sizerange->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $sizerange->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $sizerange->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $sizerange->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $sizerange->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Size Range Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
        }
    }
}
