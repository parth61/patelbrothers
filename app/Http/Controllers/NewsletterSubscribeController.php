<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\NewsletterSubscribe;
use Validator;
class NewsletterSubscribeController extends Controller
{
    public function addsubscribenewsletter(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "status" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $newsletterSubscribe= new NewsletterSubscribe();
            $newsletterSubscribe->email = $request->has('email')?$request->input('email'):null;
            $newsletterSubscribe->status = $request->has('status')?$request->input('status'):null;
            $newsletterSubscribe->site_id = $request->has('site_id')?$request->input('site_id'):null;
            $newsletterSubscribe->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $newsletterSubscribe->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $newsletterSubscribe->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $newsletterSubscribe->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $newsletterSubscribe->save();
            $newsletterSubscribecategorydata = $newsletterSubscribe->latest()->first();
            if ($result) {
                return response()->json(['status' => 200, 'message' => "Newsletter Added Sucessfully",'data'=>$newsletterSubscribecategorydata]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallsubscribenewsletter(Request $request) {

        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $newsletterSubscribe= new NewsletterSubscribe();
            $data = array();
            if ($request->has('order_by') && $request->filled('order_by')) {
                $data["order_by"] = $request->input("order_by");
            }
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            if ($request->has('newsletter_subscriber_id') && $request->input('newsletter_subscriber_id') != "") {
                $data['newsletter_subscriber_id'] = $request->input('newsletter_subscriber_id');
            }

           
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            $newsletterSubscribedata= $newsletterSubscribe->getnewsletter($data);
           
            return response()->json(['status' => 200, 'count' => count($newsletterSubscribe), 'data' => $newsletterSubscribedata]);
           
            
        }

    }

    public function updatesubscribenewsletter(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "newsletter_subscriber_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $newsletterSubscribe= new NewsletterSubscribe();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['newsletter_subscriber_id','email']);

           
            $result = $newsletterSubscribe->where('newsletter_subscriber_id', $request->input('newsletter_subscriber_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "page Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletesubscribenewsletter(Request $request) {

        $valid = Validator::make($request->all(), [
                    "newsletter_subscriber_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $newsletterSubscribe= new NewsletterSubscribe();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['newsletter_subscriber_id']);
            $result = $newsletterSubscribe->where('newsletter_subscriber_id', $request->input('newsletter_subscriber_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
