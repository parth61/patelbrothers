<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductVariant;
use App\EcomProduct;
use Carbon\Carbon;
class EcomProductVariantController extends Controller
{

    public function addproductvariant(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $data = array();
                $product = new EcomProduct();
                $data['product_id'] = $request->input('product_id');
                $data['offset'] =0;
                $data['limit'] =1;
                //code for getting product data 
                $productdata = $product->getproduct($data);
                 if(count($productdata)>0)
                    {
                       //product data value
                       $product_name=$productdata[0]->product_name;
                       $slug_id=$productdata[0]->slug_id;
                          //entry in product table with is variation 1
                          $product = new EcomProduct();
                          $product->product_name = $product_name;  
                          $product->slug_id =  $slug_id;  
                          $product->is_variation = 1; 
                          $product->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $product->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $product->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $product->ip_address = $request->has('ip_address')?$request->input('ip_address'):null; 
                          $result = $product->save();
                          if ($result) {
                            $variant_product_id=$product->id;
                            $productvariant = new EcomProductVariant();
                            $productvariant->variant_product_id =  $variant_product_id; 
                            $productvariant->product_id = $request->input('product_id');
                            $productvariant->created_by = $request->input('user_id');
                            $productvariant->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $productvariant->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $productvariant->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $productvariant->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $result1 = $productvariant->save();
                            if ($result1) {
                                return response()->json(['status' => 200, 'data' => "productvariant Added Sucessfully"]);
                            } else {
                                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                            }

                          }
                    }
                    else
                    {
                        return response()->json(['status' => 400, 'error' => "No such product available."], 400);
                    }


           
        }
    }

    public function addproductvariantmaster(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariant = new EcomProductVariant();
            $productvariant->variant_product_id = $request->input('variant_product_id'); 
            $productvariant->product_id = $request->input('product_id');
            $productvariant->created_by = $request->input('user_id');
            $productvariant->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $productvariant->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $productvariant->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $productvariant->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $productvariant->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "productvariant Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallproductvariantmaster(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariant = new EcomProductVariant();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_variant_id') && $request->input('product_variant_id') != "") {
                $data['product_variant_id'] = $request->input('product_variant_id');
            }

            
            if ($request->has('variant_product_id') && $request->input('variant_product_id') != "") {
                $data['variant_product_id'] = $request->input('variant_product_id');
            }
        
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            $productvariant = $productvariant->getproductvariant($data);

            return response()->json(['status' => 200, 'count' => count($productvariant), 'data' => $productvariant]);
        }

    }

    public function updateproductvariantmaster(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_variant_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariant = new EcomProductVariant();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_variant_id','user_id','accesstoken']);
            $result = $productvariant->where('product_variant_id', $request->input('product_variant_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "productvariant Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproductvariantmaster(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_variant_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariant = new EcomProductVariant();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_variant_id','user_id','accesstoken']);
            $result = $productvariant->where('product_variant_id', $request->input('product_variant_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
  
}
