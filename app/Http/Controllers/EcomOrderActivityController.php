<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomOrderActivity;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Facades\File;

class EcomOrderActivityController extends Controller
{
    public function addecomorderactivity(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "order_id" => "required",
            "order_activity_type" => "required",
            "order_activity_details" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $ecomorderactivity = new EcomOrderActivity();
            $ecomorderactivity->order_id = $request->input('order_id');
            $ecomorderactivity->order_activity_type = $request->input('order_activity_type');
            $ecomorderactivity->order_activity_details = $request->input('order_activity_details');
            $ecomorderactivity->site_id = $request->input('site_id');
            $ecomorderactivity->created_by = $request->input('user_id');
            $ecomorderactivity->updated_by = $request->input('user_id');
            $result = $ecomorderactivity->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Ecom order activity added sucessfully."]);
            } else {
                return response()->json(['status' => 400, 'data' => "Something went wrong."], 400);
            }
        
        }
    }

    public function getallecomorderactivity(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $ecomorderactivity = new EcomOrderActivity();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('order_id') && $request->input('order_id') != "") {
                $data['order_id'] = $request->input('order_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $ecomorderactivitydata = $ecomorderactivity->getecomorderactivity($data);
            
            return response()->json(['status' => 200, 'count' =>count($ecomorderactivitydata), 'data' => $ecomorderactivitydata]);
           
        }

    }
    
    public function updateecomorderactivity(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "order_activity_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomorderactivity = new EcomOrderActivity();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
       
            $newrequest = $request->except(['order_activity_id','accesstoken','user_id','site_id']);
          
            $result = $ecomorderactivity->where('order_activity_id', $request->input('order_activity_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Order activity updated succesfully."]);
            } else {
                return response()->json(['status' => 400, 'data' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteecomorderactivity(Request $request) {

        $valid = Validator::make($request->all(), [
            "order_activity_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomorderactivity = new EcomOrderActivity();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['order_activity_id','accesstoken','user_id','site_id']);
            $result = $ecomorderactivity->where('order_activity_id', $request->input('order_activity_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Order activity deleted successfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
