<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Banks;

class BankController extends Controller
{

    public function addbank(Request $request) {
        //validations
        $bank = new Banks();
        $valid = Validator::make($request->all(), [
                    "bank_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $bank->bank_name = $request->input('bank_name');
            $bank->status = $request->input('status');
            $bank->created_by = $request->input('created_by');
            $bank->updated_by = $request->input('updated_by');
            $bank->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $bank->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $bank->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $bank->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $bank->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Bank added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function getallbanks(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $bank = new Banks();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('bankid') && $request->input('bankid') != "") {
                $data['bankid'] = $request->input('bankid');
            }
            $banklist = $bank->getbank($data);

            return response()->json(['status' => 200, 'count' => count($banklist), 'data' => $banklist]);
        }
    }
    public function updatebank(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "bankid" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $bank = new Banks();
            $newrequest = $request->except(['bankid']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $bank->where('bankid', $request->input('bankid'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Bank Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletebank(Request $request) {
        $valid = Validator::make($request->all(), [
                    "bankid" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $bank = new Banks();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['bankid']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $bank->where('bankid', $request->input('bankid'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
