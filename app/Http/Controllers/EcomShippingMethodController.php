<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomShippingClass;
use App\EcomShippingMethod;
use App\Slug;

class EcomShippingMethodController extends Controller
{
    public function addshippingmethod(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "shipping_method" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingmethod = new EcomShippingMethod();
            $shippingmethod->shipping_method = $request->input('shipping_method');
            $shippingmethod->created_by = $request->input('user_id');
            $shippingmethod->updated_by = $request->input('user_id');
            $shippingmethod->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $shippingmethod->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $shippingmethod->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $shippingmethod->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $shippingmethod->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Shipping Method Added Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallshippingmethod(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingmethod = new EcomShippingMethod();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('shipping_method_id') && $request->input('shipping_method_id') != "") {
                $data['shipping_method_id'] = $request->input('shipping_method_id');
            }
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $shippingmethodsdata = $shippingmethod->getshippingmethods($data);

            return response()->json(['status' => 200, 'count' => count($shippingmethodsdata), 'data' => $shippingmethodsdata]);
        }

    }

    public function updateshippingmethod(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "shipping_method_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingmethod = new EcomShippingMethod();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['site_id','shipping_method_id','accesstoken','user_id']);
            $result = $shippingmethod->where('shipping_method_id', $request->input('shipping_method_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Shipping Method Updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteshippingmethod(Request $request) {

        $valid = Validator::make($request->all(), [
            "shipping_method_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingmethod = new EcomShippingMethod();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['shipping_method_id','accesstoken','site_id','user_id']);
            $result = $shippingmethod->where('shipping_method_id', $request->input('shipping_method_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "shippingclass Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
