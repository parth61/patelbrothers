<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use Validator;
use App\ExotelCallLog;
use App\User;
use App\SalesExecutive;
use App\TouchPoint;
use App\Project;
use App\LeadTouchpoint;
use DB;
use PDF;
use File;

use Illuminate\Support\Facades\Mail;
class LeadController extends Controller {

    public function addlead(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
                    "project_id" => "required",
                    "sales_executive_id" => "required",
                    "touchpoint_id" => "required",
                    "source_id" => "required",
                    "lead_status_primary" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

			$usermodel = new User();
			$userdata = $usermodel->where('user_id',$request->input('user_id'))->first();
			
            $lead = new Lead();
            $query = DB::table('tbl_leads')->select('*')->where('project_id', $request->input('project_id'))->where('user_id',$request->input('user_id'))->get();

            if(count($query)==0)
            {
                $lead = new Lead();
                $lead->user_id = $request->input('user_id');
                $lead->project_id = $request->input('project_id');
                $lead->sales_executive_id = $request->input('sales_executive_id');
                $lead->lead_status_primary = $request->input('lead_status_primary');
                $lead->lead_status_secondary = $request->input('lead_status_secondary');
                $lead->lead_status_tertiary = $request->input('lead_status_tertiary');
                $lead->touchpoint_id = $request->input('touchpoint_id');
                $lead->source_id = $request->input('source_id');
                $lead->lead_remarks = $request->input('lead_remarks');
                $lead->created_by = $request->input('created_by');
                $lead->updated_by = $request->input('updated_by');
                $lead->usource = $request->input('USOURCE');
                $lead->umedium = $request->input('UMEDIUM');
                $lead->ucampaign = $request->input('UCAMPAIGN');
                $lead->ucontent = $request->input('UCONTENT');
                $lead->uterm = $request->input('UTERM');
                $lead->iusource = $request->input('IUSOURCE');
                $lead->iumedium = $request->input('IUMEDIUM');
                $lead->iucampaign = $request->input('IUCAMPAIGN');
                $lead->iucontent = $request->input('IUCONTENT');
                $lead->iuterm = $request->input('IUTERM');
                $lead->ireferrer = $request->input('IREFERRER');
                $lead->ilandpage = $request->input('ILANDPAGE');
                $lead->visits = $request->input('VISITS');
    			$lead->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
	            $lead->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
	            $lead->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
	            $lead->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $lead->save();
                $lead_id = $lead->id;
                if ($result) {
					
					$touchpoint_id = $request->input('touchpoint_id');
					$usermodel = new User();
                    $saleexecutivemodel = new SalesExecutive();
                    $touchpointmodel = new TouchPoint();
    
                    $userdata = $usermodel->where('user_id', $request->input('user_id'))->first();
                    $salesdata = $saleexecutivemodel->where('sales_executive_id', $request->input('sales_executive_id'))->first();
                    $touchpointdata = $touchpointmodel->where('touchpoint_id', $request->input('touchpoint_id'))->first();
    
                    $salesexecutiveemail = $salesdata->sales_executive_email;
					if($touchpoint_id == 2){
						$touchpointname = $touchpointdata->touchpoint_name." at ".$request->input('lead_remarks');
					}else{
						$touchpointname = $touchpointdata->touchpoint_name;
					}
                    
                    $customername = $userdata->user_firstname." ".$userdata->user_lastname;
                    $customeremail = $userdata->user_email;
                    $customermobile = $userdata->user_mobile;
                    $inquiredate = date('Y-m-d H:i:s');
            
                    sendTouchpointEmail($salesexecutiveemail, $touchpointname, $customername, $customeremail, $customermobile, $inquiredate);
					
                    
					
                    if ($touchpoint_id == 1) {
                        $sales_executive_phone = str_replace(' ', '', $request->input('sales_executive_phone'));
                        $user_mobile = str_replace(' ', '', $userdata->user_mobile);
						
						$project_model = new Project();
						$exotelcallerid = $project_model->getexotelcallerid($request->input('project_id'));
    
                        //code for calling salesexecutive by customer
                        if (isset($sales_executive_phone) && isset($user_mobile)) {
                            $post_data = array(
                                'From' => $sales_executive_phone,
                                'To' => $user_mobile,
                                'CallerId' => $exotelcallerid
                            );
                            $api_key = exotel_api_key;
                            $api_token = exotel_api_token;
                            $exotel_sid = exotel_sid;
                            $url = "https://" . $api_key . ":" . $api_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/Calls/connect.json";
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_VERBOSE, 1);
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                            $http_result = curl_exec($ch);
                            curl_close($ch);
                            if (isset($http_result)) {
                                $tmp = json_decode($http_result);
								$tmp = json_decode($http_result);
								if(isset($tmp->Call)){
									$exotelresponse = $tmp->Call;
									$exotelcalllog = new ExotelCallLog;
									$exotelcalllog->lead_id = $lead_id;
									$exotelcalllog->sid = $exotelresponse->Sid;
									$exotelcalllog->parentcallsid = $exotelresponse->ParentCallSid;
									$exotelcalllog->datecreated = $exotelresponse->DateCreated;
									$exotelcalllog->dateupdated = $exotelresponse->DateUpdated;
									$exotelcalllog->accountsid = $exotelresponse->AccountSid;
									$exotelcalllog->exotel_to = $exotelresponse->To;
									$exotelcalllog->exotel_from = $exotelresponse->From;
									$exotelcalllog->phonenumbersid = $exotelresponse->PhoneNumberSid;
									$exotelcalllog->status = $exotelresponse->Status;
									$exotelcalllog->starttime = $exotelresponse->StartTime;
									$exotelcalllog->endtime = $exotelresponse->EndTime;
									$exotelcalllog->duration = $exotelresponse->Duration;
									$exotelcalllog->price = $exotelresponse->Price;
									$exotelcalllog->direction = $exotelresponse->Direction;
									$exotelcalllog->answerdby = $exotelresponse->AnsweredBy;
									$exotelcalllog->forwardedfrom = $exotelresponse->ForwardedFrom;
									$exotelcalllog->callername = $exotelresponse->CallerName;
									$exotelcalllog->uri = $exotelresponse->Uri;
									$exotelcalllog->recordingurl = $exotelresponse->RecordingUrl;
									$exotelcalllog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
						            $exotelcalllog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
						            $exotelcalllog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
						            $exotelcalllog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
									$exotelcalllog->save();
								}else{
									return response()->json(['status' => 200, 'data' => "lead Added Sucessfully"]);
								}
                            }
                        }
                    } elseif ($touchpoint_id == 3) {
                        //$sales_executive_whatsapp=$request->input('sales_executive_whatsapp');
                        //$user_whatsapp_number=$request->input('user_whatsapp_number');
                        //code for whatsapp
                       /*  $defaultmessage = "Hello, \nKindly help us to know what information are interested in. \n*Reply with number of your choice*\n
                        *Type 1* for About the Project\n*Type 2* for Amenities/Highlights\n*Type 3* for Project Location\n*Type 4* for Download Brochure\n*Type 5* for Schedule a Site Visit\n*Type 6* for Call Now\n*Type 7* for Request a callback\n*Type 8* for Request a WhatsApp Video Call\n*Type 9* for Visit Website\nEnter # to go back to see the main menu";
    
    
                        if ($request->has('type')) {
                            $payload = $request->input('payload');
                            $sender = $request->input('sender');
                            if ($request->input('type') === "user-event") {
                                if ($payload['type'] === "opted-in") {
                                    $message = "Thank you for Subscribing in Worldhome Super. You may notedown the project code  WH101.";
                                }
                            } else if ($request->input('type') === "message") {
                                $payloadtext = $payload['payload'];
                                if ($payloadtext['text'] === "1") {
                                    $message = "Press 01 : Project description";
                                } elseif ($payloadtext['text'] === "2") {
                                    $message = "Press 02 : Send list of Amenities/Highlight";
                                } elseif ($payloadtext['text'] === "3") {
                                    $message = "Press 03 : Send Project location";
                                } elseif ($payloadtext['text'] === "4") {
                                    $message = "Press 04 : Return PDF of Brochure";
                                } elseif ($payloadtext['text'] === "5") {
                                    $message = "Press 05 : Enter Date in format of DD/MM/YYYY";
                                } elseif ($payloadtext['text'] === "6") {
    
                                    $message = "Press 06 : Thank you message and hit exotel API";
                                } elseif ($payloadtext['text'] === "7") {
                                    $message = "Press 07 : Thank you message and send SMS/Email to manager";
                                } elseif ($payloadtext['text'] === "8") {
                                    $message = "Press 08 : Thank you message and send SMS/Email to manager for initiate video call";
                                } elseif ($payloadtext['text'] === "9") {
                                    $message = "Press 09 : Thank you message and send website url";
                                } else {
                                    $message = $defaultmessage;
                                }
                            } else {
                                $message = $defaultmessage;
                            }
                        } else {
                            $message = "Invalid Input. Enter # to back to main menu";
                        }
    
                        //echo $message;
                        return response($message, 200)->header('Content-Type', 'text/plain'); */
                    } elseif ($touchpoint_id == 4) {
                        /* $sales_executive_whatsapp = $request->input('sales_executive_whatsapp');
                        $user_whatsapp_number = $request->input('user_whatsapp_number'); */
                        //code for whatsapp video
                    }
    
                    else if ($touchpoint_id == 5) {
                        $usermodel = new User();
						$Project = new Project();
						$data1 = array();
						$finaldata = array();
						$data1['offset'] = 0;
						$data1['limit'] = 1;
				
						$data1['project_id'] = $request->input("project_id");
				
						$Projects = $Project->getproject($data1);
						if (count($Projects) > 0) {
				
							foreach ($Projects as $item) {
								
								$userdata = $usermodel->where('user_id', $request->input('user_id'))->first();
								
								$customername = $userdata->user_firstname." ".$userdata->user_lastname;
								$customeremail = $userdata->user_email;
								$brochurelink = url('/')."/storage/app/".projectbrochureprefix.$item->project_id.".pdf";
								
								downloadBrochureEmail($customeremail, $brochurelink, $item->project_name, $item->project_id);
								
								/*$data=array();
								$customername = $userdata->user_firstname." ".$userdata->user_lastname;
								$customeremail = $userdata->user_email;
								$data["email"]=$customeremail;
								$data["client_name"]=$customername;
								$data["subject"]="Download Brochure | CREDAI365.COM";
								
								$projectdata = array();
								$project_id = $item->project_id;

								$item->project_id;

								$SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
										->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
										->where('pse.status', '=', 1)
										->where('se.sales_executive_status_id', '=', 1)
										->where('pse.project_id', '=', $project_id)
										->limit(1)
										->get();
										foreach ($SalesExecutive as $item1) {
											if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
												$item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
											} else {
												if (does_url_exists(mediapath .$item1->sales_executive_photo)) {
													$item1->sales_executive_photo_path = imagedisplaypath .$item1->sales_executive_photo;
												} else {
													$item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
												}
											}
										}
					
										$item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
										
								$Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
												->where('pp.status', '=', 1)
												->where('pp.project_id', '=', $project_id)
												->get();

												$photosarray=array();
												foreach ($Projectphotos as $item2) {
													if ($item2->project_photo == NULL || $item2->project_photo == "") {
														$item2->project_photo = fixedimagepathprojectfeaturedimage;
													} else {
														if (does_url_exists(mediapath .$item2->project_photo)) {
															$item2->project_photo = imagedisplaypath .$item2->project_photo;
														} else {
															$item2->project_photo = fixedimagepathprojectfeaturedimage;
														}
													}
													array_push($photosarray,$item2);
												}
												$item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

								$Amenities = DB::table('tbl_project_amenities as pa')
										->select('a.amenities_name')
										->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
										->where('pa.status', '=', 1)
										->where('pa.project_id', '=', $project_id)
										->get();
									   $amenitiesarray=array();
										foreach ($Amenities as $ame) {
										   array_push($amenitiesarray,$ame->amenities_name);
										}
										$item->amenities = (count($Amenities)>0) ? $amenitiesarray : array();
									  
							  $Highlight = DB::table('tbl_project_highlights as ph')
										->select('h.highlight_name')
										->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
										->where('ph.status', '=', 1)
										->where('ph.project_id', '=', $project_id)
										->get();
										$highlightarray=array();
										foreach ($Highlight as $high) {
										   array_push($highlightarray,$high->highlight_name);
										}
										$item->highlight = (count($Highlight)>0) ? $highlightarray : array();

								if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
									$item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
								} else {
									if (does_url_exists(mediapath .$item->project_featured_img)) {
										$item->project_featured_img_path = imagedisplaypath .$item->project_featured_img;
									} else {
										$item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
									}
								}

								if ($item->project_logo == NULL || $item->project_logo == "") {
									$item->project_logo_path = fixedimagepathbuilderlogo;
								} else {
									if (does_url_exists(mediapath .$item->project_logo)) {
										$item->project_logo_path = imagedisplaypath .$item->project_logo;
									} else {
										$item->project_logo_path = fixedimagepathbuilderlogo;
									}
								}
								
								$featuredimagelist = DB::table('tbl_project_photos as tpp')
												->where('tpp.status', '=', 1)
												->where('tpp.type', '=', 'featuredimage')
												->where('tpp.project_id', '=', $project_id)
												->get();
												
								$item->featuredimage = (count($featuredimagelist)>0) ? $featuredimagelist : array();
												
								$gallerylist = DB::table('tbl_project_photos as tpp')
												->where('tpp.status', '=', 1)
												->where('tpp.type', '=', 'gallery')
												->where('tpp.project_id', '=', $project_id)
												->get();
								$item->gallery = (count($gallerylist)>0) ? $gallerylist : array();
												
								$configdetailslist = DB::table('tbl_project_configuration_details as tpc')->select('tc.configuration_name','tpc.*')
									->leftJoin('tbl_configuration as tc', 'tc.configuration_id', '=', 'tpc.configuration_id')
												->where('tpc.status', '=', 1)
												->where('tpc.project_id', '=', $project_id)
												->get();
								$item->configdetails = (count($configdetailslist)>0) ? $configdetailslist : "";
												
								$banklist = DB::table('tbl_project_banks as tpb')
									->leftJoin('tbl_banks as tb', 'tb.bankid', '=', 'tpb.bank_id')
												->where('tpb.status', '=', 1)
												->where('tpb.project_id', '=', $project_id)
												->pluck('tb.bank_name')->implode(',');
								$item->banklist = ($banklist!="") ? $banklist : "";
								
								$maincertificateslist = DB::table('tbl_project_main_certificate as tpmc')->select('tpmc.certificate_no','tpmc.certificate_file')
												->where('tpmc.status', '=', 1)
												->where('tpmc.project_id', '=', $project_id)
												->get();
								$item->maincertificates = (count($maincertificateslist)>0) ? $maincertificateslist : "";
								
								$item->builderinfo = DB::table('tbl_builder as tb')->select('tb.*')
												->where('tb.status', '=', 1)
												->where('tb.builder_id', '=', $item->builder_id)
												->first();
							   
								array_push($finaldata, $item);
								
								$data['projectdata']=$finaldata;
								$data['brochurelink']=  brochurelink.$finaldata[0]->project_id;
								$data['projectname']=  $item->project_name;
								$data['projectid'] = $item->project_id;
								$pdf = PDF::loadView('emails.brochure', $data);
				
								try{
									Mail::send('emails.downloadbrochure', $data, function($message)use($data,$pdf) {
									$message->to($data["email"], $data["client_name"])
									->subject($data["subject"])
									->attachData($pdf->output(), "ProjectBrochure.pdf");
									});
								}catch(JWTException $exception){
									$this->serverstatuscode = "0";
									$this->serverstatusdes = $exception->getMessage();
								}
								if (Mail::failures()) {
									$this->statusdesc  =   "Error sending mail";
									$this->statuscode  =   "0";
						
								}else{
						
								$this->statusdesc  =   "Message sent Succesfully";
								$this->statuscode  =   "1";
								}*/
							}
						}
                
                
                    }
    
                    return response()->json(['status' => 200, 'data' => "lead Added Sucessfully",'count'=>count($query)]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong.",'count'=>count($query)], 400);
                }
            }
            else
            {
                $leadtouchpoint = new LeadTouchpoint();
                $lead_id= $query[0]->lead_id;
                $leadtouchpoint->touchpoint_id = $request->input('touchpoint_id');
                $leadtouchpoint->lead_id =$lead_id;
                $leadtouchpoint->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
	            $leadtouchpoint->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
	            $leadtouchpoint->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
	            $leadtouchpoint->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $leadtouchpoint->save();
                if($result)
                {
                    $touchpoint_id = $request->input('touchpoint_id');
                    if ($touchpoint_id == 1) {
                        $sales_executive_phone = str_replace(' ', '', $request->input('sales_executive_phone'));
                        $user_mobile = str_replace(' ', '', $userdata->user_mobile);
						$exotelcallerid = $project_model->getexotelcallerid($request->input('project_id'));
                        //code for calling salesexecutive by customer
                        if (isset($sales_executive_phone) && isset($user_mobile)) {
                            $post_data = array(
                                'From' => $sales_executive_phone,
                                'To' => $user_mobile,
                                'CallerId' => $exotelcallerid
                            );
                            $api_key = exotel_api_key;
                            $api_token = exotel_api_token;
                            $exotel_sid = exotel_sid;
                            $url = "https://" . $api_key . ":" . $api_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/Calls/connect.json";
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_VERBOSE, 1);
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                            $http_result = curl_exec($ch);
                            curl_close($ch);
                            if (isset($http_result)) {
								$tmp = json_decode($http_result);
								if(isset($tmp->Call)){
									$exotelresponse = $tmp->Call;
									$exotelcalllog = new ExotelCallLog;
									$exotelcalllog->lead_id = $lead_id;
									$exotelcalllog->sid = $exotelresponse->Sid;
									$exotelcalllog->parentcallsid = $exotelresponse->ParentCallSid;
									$exotelcalllog->datecreated = $exotelresponse->DateCreated;
									$exotelcalllog->dateupdated = $exotelresponse->DateUpdated;
									$exotelcalllog->accountsid = $exotelresponse->AccountSid;
									$exotelcalllog->exotel_to = $exotelresponse->To;
									$exotelcalllog->exotel_from = $exotelresponse->From;
									$exotelcalllog->phonenumbersid = $exotelresponse->PhoneNumberSid;
									$exotelcalllog->status = $exotelresponse->Status;
									$exotelcalllog->starttime = $exotelresponse->StartTime;
									$exotelcalllog->endtime = $exotelresponse->EndTime;
									$exotelcalllog->duration = $exotelresponse->Duration;
									$exotelcalllog->price = $exotelresponse->Price;
									$exotelcalllog->direction = $exotelresponse->Direction;
									$exotelcalllog->answerdby = $exotelresponse->AnsweredBy;
									$exotelcalllog->forwardedfrom = $exotelresponse->ForwardedFrom;
									$exotelcalllog->callername = $exotelresponse->CallerName;
									$exotelcalllog->uri = $exotelresponse->Uri;
									$exotelcalllog->recordingurl = $exotelresponse->RecordingUrl;
									$exotelcalllog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
						            $exotelcalllog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
						            $exotelcalllog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
						            $exotelcalllog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
									$exotelcalllog->save();
								}else{
									return response()->json(['status' => 200, 'data' => "lead Added Sucessfully"]);
								}
                                
                            }
                        }
                    } elseif ($touchpoint_id == 3) {
                        //$sales_executive_whatsapp=$request->input('sales_executive_whatsapp');
                        //$user_whatsapp_number=$request->input('user_whatsapp_number');
                        //code for whatsapp
                       /*  $defaultmessage = "Hello, \nKindly help us to know what information are interested in. \n*Reply with number of your choice*\n
                        *Type 1* for About the Project\n*Type 2* for Amenities/Highlights\n*Type 3* for Project Location\n*Type 4* for Download Brochure\n*Type 5* for Schedule a Site Visit\n*Type 6* for Call Now\n*Type 7* for Request a callback\n*Type 8* for Request a WhatsApp Video Call\n*Type 9* for Visit Website\nEnter # to go back to see the main menu";
    
    
                        if ($request->has('type')) {
                            $payload = $request->input('payload');
                            $sender = $request->input('sender');
                            if ($request->input('type') === "user-event") {
                                if ($payload['type'] === "opted-in") {
                                    $message = "Thank you for Subscribing in Worldhome Super. You may notedown the project code  WH101.";
                                }
                            } else if ($request->input('type') === "message") {
                                $payloadtext = $payload['payload'];
                                if ($payloadtext['text'] === "1") {
                                    $message = "Press 01 : Project description";
                                } elseif ($payloadtext['text'] === "2") {
                                    $message = "Press 02 : Send list of Amenities/Highlight";
                                } elseif ($payloadtext['text'] === "3") {
                                    $message = "Press 03 : Send Project location";
                                } elseif ($payloadtext['text'] === "4") {
                                    $message = "Press 04 : Return PDF of Brochure";
                                } elseif ($payloadtext['text'] === "5") {
                                    $message = "Press 05 : Enter Date in format of DD/MM/YYYY";
                                } elseif ($payloadtext['text'] === "6") {
    
                                    $message = "Press 06 : Thank you message and hit exotel API";
                                } elseif ($payloadtext['text'] === "7") {
                                    $message = "Press 07 : Thank you message and send SMS/Email to manager";
                                } elseif ($payloadtext['text'] === "8") {
                                    $message = "Press 08 : Thank you message and send SMS/Email to manager for initiate video call";
                                } elseif ($payloadtext['text'] === "9") {
                                    $message = "Press 09 : Thank you message and send website url";
                                } else {
                                    $message = $defaultmessage;
                                }
                            } else {
                                $message = $defaultmessage;
                            }
                        } else {
                            $message = "Invalid Input. Enter # to back to main menu";
                        }
    
                        //echo $message;
                        return response($message, 200)->header('Content-Type', 'text/plain'); */
                    } elseif ($touchpoint_id == 4) {
                        /* $sales_executive_whatsapp = $request->input('sales_executive_whatsapp');
                        $user_whatsapp_number = $request->input('user_whatsapp_number'); */
                        //code for whatsapp video
                    }
    
                    $usermodel = new User();
                    $saleexecutivemodel = new SalesExecutive();
                    $touchpointmodel = new TouchPoint();
    
                    $userdata = $usermodel->where('user_id', $request->input('user_id'))->first();
                    $salesdata = $saleexecutivemodel->where('sales_executive_id', $request->input('sales_executive_id'))->first();
                    $touchpointdata = $touchpointmodel->where('touchpoint_id', $request->input('touchpoint_id'))->first();
    
                    $salesexecutiveemail = $salesdata->sales_executive_email;
                    $touchpointname = $touchpointdata->touchpoint_name;
                    $customername = $userdata->user_firstname." ".$userdata->user_lastname;
                    $customeremail = $userdata->user_email;
                    $customermobile = $userdata->user_mobile;
                    $inquiredate = date('Y-m-d H:i:s');
					
					if($touchpoint_id == 2){
						$touchpointname = $touchpointdata->touchpoint_name." at ".$request->input('lead_remarks');
					}else{
						$touchpointname = $touchpointdata->touchpoint_name;
					}
            
                    sendTouchpointEmail($salesexecutiveemail, $touchpointname, $customername, $customeremail, $customermobile, $inquiredate);
            
					if ($touchpoint_id == 5) {
						
						
                        $usermodel = new User();
						$Project = new Project();
						$data1 = array();
						$finaldata = array();
						$data1['offset'] = 0;
						$data1['limit'] = 1;
				
						$data1['project_id'] = $request->input("project_id");
				
						$Projects = $Project->getproject($data1);
						if (count($Projects) > 0) {
				
							foreach ($Projects as $item) {
								
								$userdata = $usermodel->where('user_id', $request->input('user_id'))->first();
								
								$customername = $userdata->user_firstname." ".$userdata->user_lastname;
								$customeremail = $userdata->user_email;
								$brochurelink = url('/')."/storage/app/".projectbrochureprefix.$item->project_id.".pdf";
								
								downloadBrochureEmail($customeremail, $brochurelink, $item->project_name, $item->project_id);
								
								/*$projectdata = array();
								$project_id = $item->project_id;

								$item->project_id;

								$SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
										->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
										->where('pse.status', '=', 1)
										->where('se.sales_executive_status_id', '=', 1)
										->where('pse.project_id', '=', $project_id)
										->limit(1)
										->get();
										foreach ($SalesExecutive as $item1) {
											if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
												$item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
											} else {
												if (does_url_exists(mediapath .$item1->sales_executive_photo)) {
													$item1->sales_executive_photo_path = imagedisplaypath .$item1->sales_executive_photo;
												} else {
													$item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
												}
											}
										}
					
										$item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
										
								$Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
												->where('pp.status', '=', 1)
												->where('pp.project_id', '=', $project_id)
												->get();

												$photosarray=array();
												foreach ($Projectphotos as $item2) {
													if ($item2->project_photo == NULL || $item2->project_photo == "") {
														$item2->project_photo = fixedimagepathprojectfeaturedimage;
													} else {
														if (does_url_exists(mediapath .$item2->project_photo)) {
															$item2->project_photo = imagedisplaypath .$item2->project_photo;
														} else {
															$item2->project_photo = fixedimagepathprojectfeaturedimage;
														}
													}
													array_push($photosarray,$item2);
												}
												$item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

								$Amenities = DB::table('tbl_project_amenities as pa')
										->select('a.amenities_name')
										->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
										->where('pa.status', '=', 1)
										->where('pa.project_id', '=', $project_id)
										->get();
									   $amenitiesarray=array();
										foreach ($Amenities as $ame) {
										   array_push($amenitiesarray,$ame->amenities_name);
										}
										$item->amenities = (count($Amenities)>0) ? $amenitiesarray : array();
									  
							  $Highlight = DB::table('tbl_project_highlights as ph')
										->select('h.highlight_name')
										->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
										->where('ph.status', '=', 1)
										->where('ph.project_id', '=', $project_id)
										->get();
										$highlightarray=array();
										foreach ($Highlight as $high) {
										   array_push($highlightarray,$high->highlight_name);
										}
										$item->highlight = (count($Highlight)>0) ? $highlightarray : array();

								if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
									$item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
								} else {
									if (does_url_exists(mediapath .$item->project_featured_img)) {
										$item->project_featured_img_path = imagedisplaypath .$item->project_featured_img;
									} else {
										$item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
									}
								}

								if ($item->project_logo == NULL || $item->project_logo == "") {
									$item->project_logo_path = fixedimagepathbuilderlogo;
								} else {
									if (does_url_exists(mediapath .$item->project_logo)) {
										$item->project_logo_path = imagedisplaypath .$item->project_logo;
									} else {
										$item->project_logo_path = fixedimagepathbuilderlogo;
									}
								}
								
								$featuredimagelist = DB::table('tbl_project_photos as tpp')
												->where('tpp.status', '=', 1)
												->where('tpp.type', '=', 'featuredimage')
												->where('tpp.project_id', '=', $project_id)
												->get();
												
								$item->featuredimage = (count($featuredimagelist)>0) ? $featuredimagelist : array();
												
								$gallerylist = DB::table('tbl_project_photos as tpp')
												->where('tpp.status', '=', 1)
												->where('tpp.type', '=', 'gallery')
												->where('tpp.project_id', '=', $project_id)
												->get();
								$item->gallery = (count($gallerylist)>0) ? $gallerylist : array();
												
								$configdetailslist = DB::table('tbl_project_configuration_details as tpc')->select('tc.configuration_name','tpc.*')
									->leftJoin('tbl_configuration as tc', 'tc.configuration_id', '=', 'tpc.configuration_id')
												->where('tpc.status', '=', 1)
												->where('tpc.project_id', '=', $project_id)
												->get();
								$item->configdetails = (count($configdetailslist)>0) ? $configdetailslist : "";
												
								$banklist = DB::table('tbl_project_banks as tpb')
									->leftJoin('tbl_banks as tb', 'tb.bankid', '=', 'tpb.bank_id')
												->where('tpb.status', '=', 1)
												->where('tpb.project_id', '=', $project_id)
												->pluck('tb.bank_name')->implode(',');
								$item->banklist = ($banklist!="") ? $banklist : "";
								
								$maincertificateslist = DB::table('tbl_project_main_certificate as tpmc')->select('tpmc.certificate_no','tpmc.certificate_file')
												->where('tpmc.status', '=', 1)
												->where('tpmc.project_id', '=', $project_id)
												->get();
								$item->maincertificates = (count($maincertificateslist)>0) ? $maincertificateslist : "";
								
								$item->builderinfo = DB::table('tbl_builder as tb')->select('tb.*')
												->where('tb.status', '=', 1)
												->where('tb.builder_id', '=', $item->builder_id)
												->first();
							   
								array_push($finaldata, $item);
								
								$data['projectdata']=$finaldata;
								$data['brochurelink']=  brochurelink.$finaldata[0]->project_id;
								$data['projectname']=  $item->project_name;
								$data['projectid'] = $item->project_id;
								$pdf = PDF::loadView('emails.brochure', $data);
				
								try{
									Mail::send('emails.downloadbrochure', $data, function($message)use($data,$pdf) {
									$message->to($data["email"], $data["client_name"])
									->subject($data["subject"])
									->attachData($pdf->output(), "ProjectBrochure.pdf");
									});
								}catch(JWTException $exception){
									$this->serverstatuscode = "0";
									$this->serverstatusdes = $exception->getMessage();
								}
								if (Mail::failures()) {
									$this->statusdesc  =   "Error sending mail";
									$this->statuscode  =   "0";
						
								}else{
						
								$this->statusdesc  =   "Message sent Succesfully";
								$this->statuscode  =   "1";
								}*/
							}
							
						}
                
                
                    
						
					}
					return response()->json(['status' => 200, 'data' => "lead Added Sucessfully"]);
                }
               
            }
            
        }
    }

    public function getalllead(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $lead = new Lead();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('lead_id') && $request->input('lead_id') != "") {
                $data['lead_id'] = $request->input('lead_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            if ($request->has('sales_executive_id') && $request->input('sales_executive_id') != "") {
                $data['sales_executive_id'] = $request->input('sales_executive_id');
            }
            if ($request->has('source_id') && $request->input('source_id') != "") {
                $data['source_id'] = $request->input('source_id');
            }
            if ($request->has('builder_id') && $request->input('builder_id') != "") {
                $data['builder_id'] = $request->input('builder_id');
            }
            if ($request->has('touchpoint_id') && $request->input('touchpoint_id') != "") {
                $data['touchpoint_id'] = $request->input('touchpoint_id');
            }
            $leads = $lead->getlead($data);
            $leadtouchpoint = new LeadTouchpoint();
            foreach($leads as $item){
                
                $data['lead_id'] = $item->lead_id;
                $touchpointlist = $leadtouchpoint->gettouchpoints($data);
                $item->touchpointslist = count($touchpointlist)>0?$touchpointlist:array("No Touch Points"); 
              }
            return response()->json(['status' => 200, 'count' => count($leads), 'data' => $leads]);
        }
    }

    public function updatelead(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_id" => "required",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $lead = new Lead();
            $newrequest = $request->except(['lead_id']);
            $result = $lead->where('lead_id', $request->input('lead_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "lead Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletelead(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_id" => "required",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $lead = new Lead();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['lead_id']);
            $result = $lead->where('lead_id', $request->input('lead_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
