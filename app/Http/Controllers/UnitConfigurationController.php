<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\UnitConfiguration;

class UnitConfigurationController extends Controller
{
     public function addmasterunitconfiguration(Request $request) {
        //validations
       $UnitConfiguration = new UnitConfiguration();
        $valid = Validator::make($request->all(), [
                    "unit_configuration_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $UnitConfiguration->unit_configuration_name = $request->input('unit_configuration_name');
            $UnitConfiguration->unit_id = $request->input('unit_id');
            $UnitConfiguration->category_id = $request->input('category_id');
            $UnitConfiguration->status = $request->input('status');
            $UnitConfiguration->created_by = $request->input('created_by');
            $UnitConfiguration->updated_by = $request->input('updated_by');
            $UnitConfiguration->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $UnitConfiguration->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $UnitConfiguration->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $UnitConfiguration->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $UnitConfiguration->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Unit Configuration added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallmasterunitconfiguration(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $UnitConfiguration = new UnitConfiguration();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $UnitConfigurationdata = $UnitConfiguration->getunitconfiguration($data);
            
            return response()->json(['status' => 200, 'count' => count($UnitConfigurationdata), 'data' => $UnitConfigurationdata]);
        }
    }
    public function updatemasterunitconfiguration(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "unit_configuration_id" => "required|numeric",
                    "category_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $UnitConfiguration = new UnitConfiguration();
            $newrequest = $request->except(['unit_configuration_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $UnitConfiguration->where('unit_configuration_id', $request->input('unit_configuration_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Unit Configuration Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletemasterunitconfiguration(Request $request) {
        $valid = Validator::make($request->all(), [
                    "unit_configuration_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           $UnitConfiguration = new UnitConfiguration();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['unit_configuration_id']);
            $result = $UnitConfiguration->where('unit_configuration_id', $request->input('unit_configuration_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
