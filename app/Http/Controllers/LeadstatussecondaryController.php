<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Leadstatussecondary;

class LeadstatussecondaryController extends Controller
{
     public function addleadstatussecondary(Request $request) {
        //validations
       $leadstatussecondary = new Leadstatussecondary();
        $valid = Validator::make($request->all(), [
                    "lead_status_secondary_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $leadstatussecondary->lead_status_secondary_name = $request->input('lead_status_secondary_name');
            $leadstatussecondary->lead_status_primary_id = $request->input('lead_status_primary_id');
            $leadstatussecondary->status = $request->input('status');
            $leadstatussecondary->created_by = $request->input('created_by');
            $leadstatussecondary->updated_by = $request->input('updated_by');
            $leadstatussecondary->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $leadstatussecondary->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $leadstatussecondary->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $leadstatussecondary->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $leadstatussecondary->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadstatussecondary added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallleadstatussecondary(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadstatussecondary = new Leadstatussecondary();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('lead_status_primary_id') && $request->input('lead_status_primary_id') != "") {
                $data['lead_status_primary_id'] = $request->input('lead_status_primary_id');
            }
            $leadstatussecondarydata = $leadstatussecondary->getleadsecondary($data);
            
            return response()->json(['status' => 200, 'count' => count($leadstatussecondarydata), 'data' => $leadstatussecondarydata]);
        }
    }
    public function updateleadstatussecondary(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_status_secondary_id" => "required|numeric",
                    
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $leadstatussecondary = new Leadstatussecondary();
            $newrequest = $request->except(['lead_status_secondary_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $leadstatussecondary->where('lead_status_secondary_id', $request->input('lead_status_secondary_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadstatussecondary Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteleadstatussecondary(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_status_secondary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           $leadstatussecondary = new Leadstatussecondary();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['lead_status_secondary_id']);
            $result = $leadstatussecondary->where('lead_status_secondary_id', $request->input('lead_status_secondary_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
