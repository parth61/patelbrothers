<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Slug;
use App\PostCategory;
class PostCategoryController extends Controller
{
    public function addpostcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "postcategory_name"=>"required",
               
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $postcategory = new PostCategory();
            $postcategory->postcategory_name = $request->input('postcategory_name');
            if ($request->has('postcategory_slug') && $request->input('postcategory_slug') != "")
            {
                $slug_name =$request->input('postcategory_slug');
            }
            else
            {
                $slug_name =$request->input('postcategory_name');
            }

            $slug_name=checkslugavailability($slug_name, "postcategory",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="postcategory";
            $slug->created_by = $request->input('user_id');
            $result = $slug->save();

            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $postcategory->slug_id = $slug_id;*/
            $postcategory->slug_name = $slug_name;
            $postcategory->postcategory_description = $request->input('postcategory_description');
            $postcategory->parentcategory_id = $request->input('parentcategory_id');  
            $postcategory->created_by = $request->input('user_id');
            $postcategory->site_id = $request->input('site_id');
            $postcategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $postcategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $postcategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $postcategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $postcategory->save();
            $postcategorydata = $postcategory->latest()->first();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $postcategorydata,"message"=>"added successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallpostcategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $postcategory = new PostCategory();
            $data = array();
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            if ($request->has('postcategory_id') && $request->input('postcategory_id') != "") {
                $data['postcategory_id'] = $request->input('postcategory_id');
            }
          
            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $postcategory = $postcategory->getpostcategory($data);
            if($postcategory)
            {
                
                return response()->json(['status' => 200, 'count' => count($postcategory), 'data' => $postcategory]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updatepostcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "postcategory_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $postcategory = new PostCategory();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['postcategory_id','user_id','accesstoken','slug_name']);

            if($request->has('slug_name')){
                $postcategory = $postcategory->where('postcategory_id', $request->input('postcategory_id'))->first();
                if($postcategory->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "postcategory",$request->input('site_id'));
                }
            }
      
            $result = $postcategory->where('postcategory_id', $request->input('postcategory_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "postcategory Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletepostcategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "postcategory_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $postcategory = new PostCategory();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['postcategory_id','user_id','accesstoken']);
            $result = $postcategory->where('postcategory_id', $request->input('postcategory_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
