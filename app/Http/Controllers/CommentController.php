<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use App\Comment;
class CommentController extends Controller
{
     

    public function addcomment(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "message"=>"required",
            "post_id"=>"required"
          //  "admin_password"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $comment = new Comment();
            $comment->message = $request->input('message');
            $comment->name = $request->input('name');
            $comment->post_id = $request->input('post_id');
            $comment->country_code = $request->input('country_code');
            $comment->country_iso = $request->input('country_iso');
            $comment->site_id = $request->input('site_id');
            $comment->mobile = $request->input('mobile');
            $comment->status = $request->input('status');
            $comment->parent_comment_id = $request->has('parent_comment_id') ? $request->input('parent_comment_id') : null;
            $comment->umedium = $request->has('UMEDIUM') ? $request->input('UMEDIUM') : null;
            $comment->usource  = $request->has('USOURCE') ? $request->input('USOURCE') : null;
            $comment->ucampaign  = $request->has('UCAMPAIGN') ? $request->input('UCAMPAIGN') : null;
            $comment->ucontent  = $request->has('UCONTENT') ? $request->input('UCONTENT') : null;
            $comment->uterm = $request->has('UTERM') ? $request->input('UTERM') : null;
            $comment->iusource = $request->has('IUSOURCE') ? $request->input('IUSOURCE') : null;
            $comment->iumedium = $request->has('IUMEDIUM') ? $request->input('IUMEDIUM') : null;
            $comment->iucampaign = $request->has('IUCAMPAIGN') ? $request->input('IUCAMPAIGN') : null;
            $comment->iucontent = $request->has('IUCONTENT') ? $request->input('IUCONTENT') : null;
            $comment->iuterm = $request->has('IUTERM') ? $request->input('IUTERM') : null;
            $comment->ireferrer = $request->has('IREFERRER') ? $request->input('IREFERRER') : null;
            $comment->lreferrer = $request->has('LREFERRER') ? $request->input('LREFERRER') : null;
            $comment->ilandingpage = $request->has('ILANDPAGE') ? $request->input('ILANDPAGE') : null;
            $comment->visits = $request->has('VISITS') ? $request->input('VISITS') : null;
            $comment->user_id = $request->has('user_id') ? $request->input('user_id') : null;

            $comment->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $comment->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $comment->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $comment->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $comment->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Comment Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

   //Get all admin code
   public function getallcomment(Request $request) {
        
    $valid = Validator::make($request->all(), [
                "limit" => "required|numeric",
                "offset" => "required|numeric",
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $comment = new Comment();
        $data = array();
        $data['offset'] = $request->input('offset');
        $data['limit'] = $request->input('limit');
        if ($request->has('comment_id') && $request->input('comment_id') != "") {
            $data['comment_id'] = $request->input('comment_id');
        }
        
        if ($request->has('sortby') && $request->input('sortby') != "") {
            $data['sortby'] = $request->input('sortby');
        }
        if ($request->has('parent_comment_id') && $request->input('parent_comment_id') != "") {
            $data['parent_comment_id'] = $request->input('parent_comment_id');
        }
        if ($request->has('sorttype') && $request->input('sorttype') != "") {
            $data['sorttype'] = $request->input('sorttype');
        }
        if ($request->has('site_id') && $request->input('site_id') != "") {
            $data['site_id'] = $request->input('site_id');
        }
        if ($request->has('post_id') && $request->input('post_id') != "") {
            $data['post_id'] = $request->input('post_id');
        }
        
        if ($request->has('status') && $request->input('status') != "") {
            $data['status'] = $request->input('status');
        }
        $comment = $comment->getcomment($data);
        
        return response()->json(['status' => 200, 'count' => count($comment), 'data' => $comment]);
    }
}
    public function getallparentcategorywithcomment(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $comment = new Comment();
            
            $data = array();
            if ($request->has('comment_id') && $request->input('comment_id') != "") {
                $data['comment_id'] = $request->input('comment_id');
            }
            
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('parent_comment_id') && $request->input('parent_comment_id') != "") {
                $data['parent_comment_id'] = $request->input('parent_comment_id');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
          
            
            
          

            $categorys = $comment->getalllcomentwithparentcategory($data);
            
            if($categorys)
            {
                return response()->json(['status' => 200, 'count' =>count($categorys), 'data' => $categorys]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        
        }

    }


public function updatecomment(Request $request) {
    //validations
    $valid = Validator::make($request->all(), [
             "comment_id" => "required",
             
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $comment = new Comment();
        $request->request->add(['updated_by' =>  $request->input('comment_id')]);
        $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
        $newrequest = $request->except(['comment_id']);
        $newrequest['admin_password'] =md5($request->input('admin_password'));
        $result = $comment->where('comment_id', $request->input('comment_id'))->update($newrequest);
        //md5($request->input('newpassword'));
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Comment information updated succesfully."]);
        } else {
            return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
        }
    }

}

public function deletecomment(Request $request) {

    $valid = Validator::make($request->all(), [
                "comment_id" => "required",       
    ]);

    if ($valid->fails()) {
        return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
    } else {
        $comment = new Comment();
        $request->request->add(['status' => 0]);
        $request->request->add(['updated_by' =>  $request->input('comment_id')]);
        $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
        $newrequest = $request->except(['comment_id']);
        $result = $comment->where('comment_id', $request->input('comment_id'))->update($newrequest);
        if ($result) {
            return response()->json(['status' => 200, 'data' => "Comment deleted successfully"]);
        } else {
            return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
        }
    }

}
}
