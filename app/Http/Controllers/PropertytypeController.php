<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Propertytype;
class PropertytypeController extends Controller
{
    public function addpropertytype(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                    "property_type_name" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $propertytype = new Propertytype();
            $propertytype->property_type_name = $request->input('property_type_name');
            
            $propertytype->created_by = $request->input('created_by');
            $propertytype->updated_by = $request->input('updated_by');
            $propertytype->status = $request->input('status');
            $propertytype->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $propertytype->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $propertytype->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $propertytype->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $propertytype->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Propertytype added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallpropertytype(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $propertytype = new Propertytype();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('property_type_id') && $request->input('property_type_id') != "") {
                $data['property_type_id'] = $request->input('property_type_id');
            }
            $propertytype = $propertytype->getrerastate($data);

            return response()->json(['status' => 200, 'count' => count($propertytype), 'data' => $propertytype]);
        }
    }

    public function updatepropertytype(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "property_type_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $propertytype = new Propertytype();
            $newrequest = $request->except(['property_type_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $propertytype->where('property_type_id', $request->input('property_type_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Propertytype updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletepropertytype(Request $request) {
        $valid = Validator::make($request->all(), [
                    "property_type_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $propertytype = new Propertytype();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['property_type_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $propertytype->where('property_type_id', $request->input('property_type_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
