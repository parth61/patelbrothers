<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomProduct;
use App\EcomProductTag;
use App\EcomProductCategory;
use App\Slug;
use App\EcomProductGallery;
use App\EcomProductAttribute;
use App\EcomProductAttributeTerm;
use App\EcomProductVariantAttributeTerm;
use Illuminate\Support\Facades\File;
use App\EcomProductVariant;
use App\EcomBusinessSetting;
use App\EcomWishlist;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Model;
use DB;

class EcomProductController extends Controller
{

    public function addproductexcel(Request $request) {
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            
            if ($request->hasFile('import_file')) {
                $files = $request->file('import_file');
                $data = Excel::toArray([],$files);
                $a1 = ["Tshr","SDds","SD"];
                $a2 = $data[0][0];
                $finalarray = [];
                foreach($data as $i=>$item){
                    $arr3 = [];
                       foreach($item as $i2=>$item2){
                        $array = [];
                            if($i2 != 0){
                                array_push($array,$item2);
                                if(sizeof($item2) == sizeof($a2)){
                                    $arr3 =  array_combine($a2,$item2);
                                    array_push($finalarray,$arr3);
                                }
                            }
                        }
                }
                foreach($finalarray as $i3=>$item3){
                    if($item3["product_name"] !=  null && $item3["product_name"] !=  "" ){
                        if(isset($item3["product_id"])){
                            $count =  $product->where('product_id', $item3["product_id"])->count(); 
                            if($count == 0){
                                    $product = new EcomProduct();
                                    $product->created_by = $request->input('user_id');
                                    $product->site_id = $request->input('site_id');
                                    $product->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $product->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $product->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $product->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                    $product->product_guid =generateAccessToken(25);
                                    $result = $product->save();
                                    $productid =  $product->id;
                                    $result = $product->where('product_id', $productid)->update($item3);
                            }else{
                                $result = $product->where('product_id', $item3["product_id"])->update($item3); 
                        }
                        }else{
                                $product = new EcomProduct();
                                $product->created_by = $request->input('user_id');
                                $product->site_id = $request->input('site_id');
                                $product->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $product->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $product->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $product->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $product->product_guid =generateAccessToken(25);
                                $result = $product->save();
                                $productid =  $product->id;
                                $result = $product->where('product_id', $productid)->update($item3);
                        }
                    }
                }
                return response()->json(["rows"=>$finalarray ,'count' => count($finalarray)]);
            }
        }
    }

    
    public function addproduct(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_name"=>"required",
            "unique:tbl_ecom_product"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            $product->product_name = $request->input('product_name');
            $product->product_short_description = $request->input('product_short_description');
            $product->product_description = $request->input('product_description');
            $product->product_stock = $request->input('product_stock');
            $product->product_guid = generateAccessToken(25);
            $product->site_id = $request->input('site_id');
            $product->has_variation = $request->input('has_variation');
            $product->product_type = $request->input('has_variation')=="1"?"master":"simple";
            if ($request->has('product_slug') && $request->input('product_slug') != "")
            {
                $slug_name =$request->input('product_slug');
            }
            else
            {
                $slug_name =$request->input('product_name');
            }

            $slug_name=checkslugavailability($slug_name, "product",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="product";

            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $product->slug_id = $slug_id;*/
            $product->slug_name = $slug_name;
            $product->product_actual_price = $request->input('product_actual_price');
            $product->product_height = $request->input('product_height');
            $product->product_length = $request->input('product_length');
            $product->product_weight = $request->input('product_weight');
            $product->product_wide = $request->input('product_wide');
            $product->is_featured = $request->input('is_featured');
            $product->is_variation = 0;
            $product->is_searchable =1;
            $product->length_unit = $request->input('length_unit');
            $product->wide_unit = $request->input('wide_unit');
            $product->height_unit = $request->input('height_unit');
            $product->status = 2;
            $product->isvisible = $request->input('isvisible');
            $product->created_by = $request->input('user_id');
			$product->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $product->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $product->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $product->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $product->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            
            $result = $product->save();
            if ($result) {
                $product_id=$product->product_guid;
              
                 /*   //product tag list
                   if ($request->has('taglist')) {
                    $taglist = json_decode($request->input('taglist'));
                    if (count($taglist) > 0) {
                        for ($i = 0; $i < count($taglist); $i++) {
                            $producttag = new EcomProductTag();
                            $checkcount = count($producttag->where('tag_id', $taglist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $producttag->tag_id = $taglist[$i];
                                $producttag->product_id = $product_id;
                                $producttag->save();
                            }
                        }
                        
                    } 
                }
                  //product tag list
                  if ($request->has('categorylist')) {
                      
                    $categorylist = json_decode($request->input('categorylist'));
                    if (count($categorylist) > 0) {
                        for ($i = 0; $i < count($categorylist); $i++) {
                            $productcategory = new EcomProductCategory();
                            $checkcount = count($productcategory->where('category_id', $categorylist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $productcategory->category_id = $categorylist[$i];
                                $productcategory->product_id = $product_id;
                                $productcategory->save();
                            }
                        }
                        
                    } 
                } */

                return response()->json(['status' => 200, 'data' =>$product_id ]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallproduct(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            if ($request->has('product_guid') && $request->input('product_guid') != "") {
                $data['product_guid'] = $request->input('product_guid');
            }

            if ($request->has('product_guid') && $request->input('product_guid') != "") {
                $data['product_guid'] = $request->input('product_guid');
            }
        
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('is_variation') && $request->input('is_variation') != "") {
                $data['is_variation'] = $request->input('is_variation');
            }
            
            if ($request->has('product_type') && $request->filled('product_type')) {
                $data['product_type'] = $request->input('product_type');
            }
            if ($request->has('product_name') && $request->filled('product_name')) {
                $data['product_name'] = $request->input('product_name');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $product = $product->getproduct($data);

            $wishlist = new EcomWishlist();
            $wishlistold=array();
            $wishlist1=array();
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            
                
                if ($request->has('user_id') && $request->input('user_id') != "") {
                    $data['user_id'] = $request->input('user_id');
                    $wishlist1=$wishlist->where('user_id','=',$request->input('user_id'))->where('site_id','=',$request->input('site_id'))->where('status','=','1')->pluck('product_id');
                }
                for($i=0;$i<count($wishlist1);$i++){
                    if(count($wishlist1)>0 ){
                        if($wishlist1 != null &&  $wishlist1 != ""){
                            array_push($wishlistold, $wishlist1[$i]);
                        }
                    }

                }
            }

            $productdata = array();
            // print_r($wishlistold);exit;
            //  print_r($product);exit;
            
            foreach($product as $item)
            {
                $product_id = $item->product_id;


                if(count($wishlistold)>0 && isset($wishlistold)){
                    $var = in_array($item->product_id,$wishlistold );
                    if($var){
                        $item->is_wishlist=true;
                    }else{
                        $item->is_wishlist=false;
                    }
                }else{
                    $item->is_wishlist=false;
                }
                //product tag 
                $tagnamelist = DB::table('tbl_ecom_product_tag as pt')
                ->leftJoin('tbl_ecom_tag as tag', 'pt.tag_id', '=', 'tag.tag_id')
                ->where('pt.status', '=', 1)
                ->where('pt.product_id', '=', $product_id)
                ->orderBy('tag.tag_name', 'ASC')
                ->pluck('tag.tag_name');

                $tagidlist = DB::table('tbl_ecom_product_tag as pt')
                ->where('pt.status', '=', 1)
                ->where('pt.product_id', '=', $product_id)
                ->pluck('pt.tag_id');

              
                //product attribute
                $attributenamelist = DB::table('tbl_ecom_product_attribute as proattr')
                ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->orderBy('attr.attribute_name', 'ASC')
                ->pluck('attr.attribute_name');

                $attributeidlist = DB::table('tbl_ecom_product_attribute as proattr')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->pluck('proattr.attribute_id');

                $attributetermidlist = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                ->select('proattrterm.*','teat.attribute_term','teat.attribute_term_colorcode')
                ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                ->where('proattrterm.status', '=', 1)
                ->where('proattrterm.product_id', '=', $product_id)
                ->groupBy('attribute_id')->get();

                /* Attribute List */
                $attributenamelist = DB::table('tbl_ecom_product_attribute as proattr')
                ->select('attr.*')
                ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->orderBy('attr.attribute_name', 'ASC')
                /*->pluck('attr.attribute_name');*/
                ->get();

                $item->attributenamelist=$attributenamelist;

                if($item->is_variation){
                    $item->attributetermlist = DB::table('tbl_ecom_product_variant_attribute_term as proattrterm')
                        ->select('tea.attribute_name','teat.*')
                        ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                        ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'proattrterm.attribute_id')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.variant_product_id', '=', $product_id)
                        ->orderBy('tea.attribute_id')
                        /*->pluck('teat.attribute_term')*/
                        ->get();
                }else{
                    $item->attributetermlist = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                        ->select('tea.attribute_name','teat.*')
                        ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                        ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'proattrterm.attribute_id')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.product_id', '=', $product_id)
                        ->orderBy('tea.attribute_id')
                        /*->pluck('teat.attribute_term')*/
                        ->get();
                }

                //product category
                $categorynamelist = DB::table('tbl_ecom_product_category as pc')
                ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->orderBy('category.category_name', 'ASC')
                ->pluck('category.category_name');

                $categoryidlist = DB::table('tbl_ecom_product_category as pc')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->pluck('pc.category_id');

                if($item->seo_media_file=="" ||$item->seo_media_file==null||$item->seo_media_file=="null")
                {
                    $item->seo_img_path = fixedseofeaturedimage;
                }
                else
                {
                    if(fileuploadtype=="local")
                    { 
                        //code for seo image
                        if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        }
                    }
                    else
                    {
                        //seo image
                        if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        } 
                    }
                }

                //call for getting gallery image with product
                $productgallery = new EcomProductGallery();
                $data['product_id']=$product_id;
                $productgallery = $productgallery->getproductgallery($data);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                 
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                    $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                } else {
                                    $item1->product_image_path =fixedproductgalleryimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                            } else {
                                $item1->product_image_path = fixedproductgalleryimage;
                            }
                        }
                    }  

                   
                    array_push($productgallerydata, $item1);
                }
                $item->product_gallery=$productgallerydata;
                $item->tagidlist= $tagidlist;
                $item->tagnamelist = $tagnamelist;
                $item->categorynamelist= $categorynamelist;
                $item->categoryidlist= $categoryidlist;
                $item->attributenamelist= $attributenamelist;
                $item->attributetermidlist= $attributetermidlist;
                $item->attributeidlist= $attributeidlist;
                array_push($productdata, $item);
            }
            return response()->json(['status' => 200, 'count' => count($product), 'data' => $productdata]);
        }

    }

    public function getallproductvariant(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required",
                    "product_id"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            if ($request->has('product_guid') && $request->input('product_guid') != "") {
                $data['product_guid'] = $request->input('product_guid');
            }
        
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $product = $product->getproductvariant($data);

            $productdata = array();
            foreach($product as $item)
            {
                $product_id = $item->product_id;  

                //call for getting gallery image with product
                $productgallery = new EcomProductGallery();
                $data['product_id']=$product_id;
                $productgallery = $productgallery->getproductgallery($data);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                 
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath.$item1->product_image_media_file)) {
                                    $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                } else {
                                    $item1->product_image_path =fixedproductgalleryimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath.$item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                            } else {
                                $item1->product_image_path = fixedproductgalleryimage;
                            }
                        }
                    }  

                   
                    array_push($productgallerydata, $item1);
                }

                
                $item->product_gallery=$productgallerydata;
                $productattribute = DB::table('tbl_ecom_product_attribute as proatt')
                 ->select('proatt.*','att.attribute_name')
                ->leftJoin('tbl_ecom_attribute as att', 'att.attribute_id', '=', 'proatt.attribute_id')
                ->where('proatt.status', '=', 1)
                ->where('proatt.product_id', '=',$request->input('product_id'))
                ->get();
                $productattributearray=array();
                foreach($productattribute as $item22)
                {
                    $selectedattributetermlist = DB::table('tbl_ecom_product_variant_attribute_term as provarattr')
                    ->where('provarattr.status', '=', 1)
                    ->where('provarattr.variant_product_id', '=',$product_id )
                    ->where('provarattr.attribute_id', '=', $item22->attribute_id)
                    ->pluck('provarattr.attribute_term_id');

                    $attributetermlist = DB::table('tbl_ecom_attribute_term as attrterm')
                    ->select('attrterm.attribute_term_id','attrterm.attribute_term')
                    ->where('attrterm.status', '=', 1)
                    ->where('attrterm.attribute_id', '=', $item22->attribute_id)
                    ->get();
                    $item22->selectedattributetermlist= $selectedattributetermlist;
                    $item22->attributetermlist= $attributetermlist;
                    array_push($productattributearray,$item22);
                }

                $item->attributedata=$productattributearray;
            
                array_push($productdata, $item);
            }
            return response()->json(['status' => 200, 'count' => count($product), 'data' => $productdata]);
        }

    }

    public function updateproduct(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required",
                 // "has_variation"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            $ecomproductvariant = new EcomProductVariant();
            $product_id=$request->input('product_id');
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['slug_id','product_id','user_id','accesstoken','taglist','categorylist','slug_name']);

            if($request->has('slug_name')){
                $product = $product->where('product_id', $request->input('product_id'))->first();
                if($product->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "product",$request->input('site_id'));
                }
            }

            $result = $product->where('product_id',$product_id)->update($newrequest);

            if ($result) {
                $product_id=$request->input('product_id');

                /* Update Name of product then update to variant as well */
                $productdata = $product->where('product_id',$product_id)->where('has_variation', 1)->where('status', 1)->get();

                if ($request->has('product_name')) {
                    if(count($productdata)>0){
                        
                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->pluck('variant_product_id');
                        $result = $product->whereIn('product_id',$variantlist)->where('status', 1)->update(
                            array(
                                'product_name'=>$request->input('product_name')
                            )
                        );
                    }
                }

                if ($request->has('status')) {
                    if(count($productdata)>0){
                        
                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->whereIn('status', [1,2])->pluck('variant_product_id');
                        $result = $product->whereIn('product_id',$variantlist)->whereIn('status', [1,2])->update(
                            array(
                                'status'=>$request->input('status')
                            )
                        );
                    }
                }

                if ($request->has('shippingandreturn')) {
                    if(count($productdata)>0){
                        
                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->pluck('variant_product_id');
                        $result = $product->whereIn('product_id',$variantlist)->where('status', 1)->update(
                            array(
                                'shippingandreturn'=>$request->input('shippingandreturn')
                            )
                        );
                    }
                }


                if ($request->has('tax_class_id')) {
                    if(count($productdata)>0){
                        
                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->pluck('variant_product_id');
                        $result = $product->whereIn('product_id',$variantlist)->where('status', 1)->update(
                            array(
                                'tax_class_id'=>$request->input('tax_class_id')
                            )
                        );
                    }
                }

                
                if ($request->has('tax_status_id')) {
                    if(count($productdata)>0){
                        
                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->pluck('variant_product_id');
                        $result = $product->whereIn('product_id',$variantlist)->where('status', 1)->update(
                            array(
                                'tax_status_id'=>$request->input('tax_status_id')
                            )
                        );
                    }
                }

                /* End Update */

                if ($request->has('taglist')) {
                     //for updating product tag relation entry
                     $taglist = $request->input('taglist');

                     if (count($taglist) > 0) {
                        $producttag = new EcomProductTag();
                       
                       //status 0 code                
                        $newrequest = array();
                        $newrequest['status'] = 0;
                        $newrequest['updated_by'] = $request->input('user_id');
                        $result = $producttag->where('product_id', $request->input('product_id'))->whereNotIn('tag_id', $taglist)->update($newrequest);

                        for ($i = 0; $i < count($taglist); $i++) {
                            $checkcount = count($producttag->where('tag_id', $taglist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $producttag = new EcomProductTag();
                                $producttag->tag_id = $taglist[$i];
                                $producttag->product_id = $product_id;
                                $producttag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $producttag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $producttag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $producttag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $producttag->save();
                            }
                        }

                        /* Update category in variant product */

                       if(count($productdata)>0){
                            $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->get();
                            if(count($variantlist)>0){
                                foreach($variantlist as $varitem){
                                    $result = $producttag->where('product_id', $varitem->variant_product_id)->whereNotIn('tag_id', $taglist)->update($newrequest);
                                    $producttag->where('product_id', $product_id)->whereNotIn('tag_id', $taglist)->delete();
                                    for ($i = 0; $i < count($taglist); $i++) {
                                        $checkcount = count($producttag->where('tag_id', $taglist[$i])->where('product_id', $varitem->variant_product_id)->where('status', '1')->get());
                                        if ($checkcount === 0) {
                                            $producttag = new EcomProductTag();
                                            $producttag->tag_id = $taglist[$i];
                                            $producttag->product_id = $varitem->variant_product_id;
                                            $producttag->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                            $producttag->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                            $producttag->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                            $producttag->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                            $producttag->save();
                                        }
                                   }
                                }
                            }
                        }
                   
                    } else {
                        $producttag = new EcomProductTag();
                        $producttag->where('product_id', $product_id)
                               ->update(array('status' => 0)); 
                        //$producttag->where('product_id', $product_id)->delete();

                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->get();
                        if(count($variantlist)>0){
                            foreach($variantlist as $varitem){
                                $productcategory->where('product_id', $varitem->variant_product_id)
                                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));
                            }
                        }

                    }
                }else {
                    $producttag = new EcomProductTag();
                    $producttag->where('product_id', $product_id)->update(array('status' => 0)); 
                        //  $producttag->where('product_id', $product_id)->delete();

                    $producttag = new EcomProductTag();
                    $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->get();
                    if(count($variantlist)>0){
                        foreach($variantlist as $varitem){
                            $producttag->where('product_id', $varitem->variant_product_id)
                            ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));
                        }
                    }

                }

                if($request->has('categorylist') && $request->filled('categorylist')) {
                    //for updating product tag relation entry
                    $categorylist = explode (",", $request->input('categorylist'));

                    $ecomproductvariant = new EcomProductVariant();
                    

                    if (count($categorylist) > 0) {
                        
                        $productcategory = new EcomProductCategory();
                        $newrequest = array();
                        $newrequest['updated_at'] =Carbon::now()->toDateTimeString();
                        $newrequest['status'] = 0;
                        $newrequest['updated_by'] = $request->input('user_id');
                        //$newrequest = $request->except(['product_id','categorylist','categorylist','user_id','accesstoken','taglist']);
                        $result = $productcategory->where('product_id', $request->input('product_id'))->whereNotIn('category_id', $categorylist)->update($newrequest);

                      // $productcategory->where('product_id', $product_id)->whereNotIn('category_id', $categorylist)->delete();
                       for ($i = 0; $i < count($categorylist); $i++) {
                           $checkcount = count($productcategory->where('category_id', $categorylist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                           if ($checkcount === 0) {
                               $productcategory = new EcomProductCategory();
                               $productcategory->category_id = $categorylist[$i];
                               $productcategory->product_id = $product_id;
                               $productcategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $productcategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $productcategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $productcategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                               $productcategory->save();
                           }
                       }

                       /* Update category in variant product */

                       if(count($productdata)>0){
                            $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->get();
                            if(count($variantlist)>0){
                                foreach($variantlist as $varitem){
                                    $result = $productcategory->where('product_id', $varitem->variant_product_id)->whereNotIn('category_id', $categorylist)->update($newrequest);
                                    for ($i = 0; $i < count($categorylist); $i++) {
                                        $checkcount = count($productcategory->where('category_id', $categorylist[$i])->where('product_id', $varitem->variant_product_id)->where('status', '1')->get());
                                        if ($checkcount === 0) {
                                            $productcategory = new EcomProductCategory();
                                            $productcategory->category_id = $categorylist[$i];
                                            $productcategory->product_id = $varitem->variant_product_id;
                                            $productcategory->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                            $productcategory->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                            $productcategory->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                            $productcategory->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                            $productcategory->save();
                                        }
                                   }
                                }
                            }
                        }
                  
                    } else {
                        $productcategory = new EcomProductCategory();
                        $productcategory->where('product_id', $product_id)
                              ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));
                        $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->get();
                        if(count($variantlist)>0){
                            foreach($variantlist as $varitem){
                                $productcategory->where('product_id', $varitem->variant_product_id)
                                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));
                            }
                        }
                   }
                }else {

                    $productcategory = new EcomProductCategory();
                    $productcategory->where('product_id', $product_id)
                        ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));

                    $ecomproductvariant = new EcomProductVariant();
                    $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->get();
                    if(count($variantlist)>0){
                        foreach($variantlist as $varitem){
                            $productcategory->where('product_id', $varitem->variant_product_id)
                            ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));
                        }
                    }

                }

               //code for simple and variable product
                if($request->has('has_variation')){
                if($request->input('has_variation')==0)
                {
                       //=========code for simple product==========//

                    //code for getting old variation value from db
                    $old_has_variation = DB::table('tbl_ecom_product as pro')
                    ->select('pro.has_variation')
                    ->where('pro.status', '!=', 0)
                    ->where('pro.product_id', '=', $product_id)
                    ->first();

                   $oldhasvariation=$old_has_variation->has_variation;

                   //if(old variation was variable product than we need to delete all entries)
                    if($oldhasvariation==1)
                    {
                        //code for deleting the variation
                        $product = new EcomProduct();

                        $productvariant = new EcomProductVariant();

                        $productvariantattributeterm = new EcomProductVariantAttributeTerm();
                        
                        //code for getting old variation value from db
                        $variableproductidlist = DB::table('tbl_ecom_product_variant as provariant')
                        ->where('provariant.status', '=', 1)
                        ->where('provariant.product_id', '=', $product_id)
                        ->pluck('provariant.variant_product_id');

                        if(count($variableproductidlist)>0)
                        {
                                //delete product
                                $product->whereIn('product_id', $variableproductidlist)
                                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 

                                //delete attribute term
                                $productvariantattributeterm->whereIn('variant_product_id', $variableproductidlist)
                                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));          

                                //delete product variant relation entries
                                $productvariant->where('product_id', $product_id)
                                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                        }
                    }
     
                       //for updating product attribute relation entry
                    if ($request->has('productattributelist')) {
                     
                        $productattributelist = $request->input('productattributelist');
                        if (count($productattributelist) > 0) {
                           $productattribute = new EcomProductAttribute();
                          
                          //status 0 code
                           /*$request->request->add(['status' => 0]);
                           $request->request->add(['updated_by' =>  $request->input('user_id')]);
                           $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);*/
                           $newrequest = array();
                           $newrequest['status'] = 0;

                            $newrequest['updated_by'] = $request->input('user_id');
                           $result = $productattribute->where('product_id', $request->input('product_id'))->whereNotIn('attribute_id', $productattributelist)->update($newrequest);
    
                          // $productattribute->where('product_id', $product_id)->whereNotIn('attribute_id', $productattributelist)->delete();
                           for ($i = 0; $i < count($productattributelist); $i++) {
                               $checkcount = count($productattribute->where('attribute_id', $productattributelist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                               if ($checkcount === 0) {
                                   $productattribute = new Ecomproductattribute();
                                   $productattribute->attribute_id = $productattributelist[$i];
                                   $productattribute->product_id = $product_id;
                                   $productattribute->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $productattribute->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $productattribute->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $productattribute->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                   $productattribute->save();
                               }
                           }
                      
                    } else {
                       $productattribute = new EcomProductAttribute();
                       $productattribute->where('product_id', $product_id)
                                  ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                         //  $productattribute->where('product_id', $product_id)->delete();
                       }
                   }

                       //for updating product attribute term relation entry
                       if ($request->has('productattributetermlist')) {
                     
                        $productattributetermlist = $request->input('productattributetermlist');
                        if (count($productattributetermlist) > 0) {
                           $productattributeterm = new EcomProductAttributeTerm();
                          
                          //status 0 code
                           /*$request->request->add(['status' => 0]);
                           $request->request->add(['updated_by' =>  $request->input('user_id')]);
                           $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                           $newrequest = $request->except(['product_id','productattributetermlist','user_id','accesstoken']);*/
                           $newrequest = array();
                           $newrequest['status'] = 0;

                            $newrequest['updated_by'] = $request->input('user_id');
                           $result = $product->where('product_id', $request->input('product_id'))->whereNotIn('attribute_term_id', $productattributetermlist)->update($newrequest);
    
                          // $productattributeterm->where('product_id', $product_id)->whereNotIn('attribute_term_id', $productattributetermlist)->delete();
                           for ($i = 0; $i < count($productattributetermlist); $i++) {
                               $checkcount = count($productattributeterm->where('attribute_term_id', $productattributetermlist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                               if ($checkcount === 0) {
                                   $productattributeterm = new EcomproductattributeTerm();
                                   $productattributeterm->attribute_term_id = $productattributetermlist[$i];
                                   $productattributeterm->product_id = $product_id;
                                   $productattributeterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $productattributeterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $productattributeterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $productattributeterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                   $productattributeterm->save();
                               }
                           }
                      
                        } else {
                       $productattributeterm = new EcomProductAttributeTerm();
                       $productattributeterm->where('product_id', $product_id)
                                  ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                         //  $productattribute->where('product_id', $product_id)->delete();
                       }
                   }


                }
                else
                {
                       //for updating product attribute relation entry
                       if ($request->has('productattributelist')) {
                     
                        $productattributelist = $request->input('productattributelist');
                        if (count($productattributelist) > 0) {
                            $productattribute = new EcomProductAttribute();

                            $newrequest = array();
                            $newrequest['status'] = 0;
                            $newrequest['updated_by'] = $request->input('user_id');
                            $result = $productattribute->where('product_id', $request->input('product_id'))->whereNotIn('attribute_id', $productattributelist)->update($newrequest);
    
                          // $productattribute->where('product_id', $product_id)->whereNotIn('attribute_id', $productattributelist)->delete();
                            for ($i = 0; $i < count($productattributelist); $i++) {
                               $checkcount = count($productattribute->where('attribute_id', $productattributelist[$i])->where('product_id', $product_id)->where('status', '1')->get());
                               if ($checkcount === 0) {
                                   $productattribute = new Ecomproductattribute();
                                   $productattribute->attribute_id = $productattributelist[$i];
                                   $productattribute->product_id = $product_id;
                                   $productattribute->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $productattribute->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $productattribute->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $productattribute->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                   $productattribute->save();
                               }
                            }
                      
                        } else {
                            $productattribute = new EcomProductAttribute();
                           $productattribute->where('product_id', $product_id)
                                      ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                       }
                    }    

                   /*      //update in product attribute term variant table
                    if ($request->has('productvariantattributetermlist')) {
                     
                        $productvariantattributetermlist = $request->input('productvariantattributetermlist');
                        if (count($productvariantattributetermlist) > 0) {
                           $productattributevariantterm = new EcomProductVariantAttributeTerm();
                           $newrequest = array();
                           $newrequest['status'] = 0;

                            $newrequest['updated_by'] = $request->input('user_id');
                           $result = $product->where('variant_product_id', $product_id)->whereNotIn('attribute_term_id', $productvariantattributetermlist)->update($newrequest);
    
                          // $productattributevariantterm->where('product_id', $product_id)->whereNotIn('attribute_term_id', $productvariantattributetermlist)->delete();
                           for ($i = 0; $i < count($productvariantattributetermlist); $i++) {
                               $checkcount = count($productattributevariantterm->where('attribute_term_id', $productvariantattributetermlist[$i])->where('variant_product_id', $product_id)->where('status', '1')->get());
                               if ($checkcount === 0) {
                                   $productattributevariantterm = new EcomProductVariantAttributeTerm();
                                   $productattributevariantterm->attribute_term_id = $productvariantattributetermlist[$i];
                                   $productattributevariantterm->product_id = $product_id;
                                   $productattributevariantterm->save();
                               }
                           }
                      
                    } else {
                       $productattributevariantterm = new EcomProductVariantAttributeTerm();
                       $productattributevariantterm->where('product_id', $product_id)
                                  ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                         //  $productattribute->where('product_id', $product_id)->delete();
                       }
                   } */
                    //code for variable product
                }
                }
                
                return response()->json(['status' => 200, 'data' => "Product updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproduct(Request $request) {

        $valid = Validator::make($request->all(), [
            "product_id" => "required",
            "accesstoken" => "required",
            "site_id" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            $ecomproductvariant = new EcomProductVariant();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_id','user_id','accesstoken']);
            $result = $product->where('product_id', $request->input('product_id'))->update($newrequest);
            if ($result) {

                $productdata = $product->where('product_id',$request->input('product_id'))->where('has_variation', 1)->where('status', 1)->get();
                if(count($productdata)>0){
                    $variantlist = $ecomproductvariant->where('product_id', $product_id)->where('status', 1)->pluck('variant_product_id');
                    $result = $product->whereIn('product_id',$variantlist)->update(
                        array(
                            'status' => 0,
                            'updated_by' =>  $request->input('user_id')
                        )
                    );
                }

                return response()->json(['status' => 200, 'data' => "Product deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

    public function deletevariantproduct(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
                    "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product = new EcomProduct();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_id','user_id','accesstoken']);
            $result = $product->where('product_id', $request->input('product_id'))->update($newrequest);
            if ($result) {
                $productvariant = new EcomProductVariant();

                $productvariantattributeterm = new EcomProductVariantAttributeTerm();
                
                //delete attribute term
                $productvariantattributeterm->where('variant_product_id',$request->input('product_id'))
                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString()));          

                //delete product variant relation entries
                $productvariant->where('variant_product_id',$request->input('product_id'))
                ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 

                return response()->json(['status' => 200, 'data' => "Product deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

    public function generateallvariantforproduct(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $generatedvariant = 0;
            $data = array();
            $product = new EcomProduct();
            $data['product_id'] = $request->input('product_id');
            $data['offset'] =0;
            $data['limit'] =1;
            //code for getting product data 
            $productdata = $product->getproduct($data);
            
            if(count($productdata)>0)
            {
                //product data value
                $product_name=$productdata[0]->product_name;
                $slug_id=$productdata[0]->slug_id;
                
                    //   return response()->json(['status' => 200,'data' => $product_name]);

                    //get all product related attribute
                $productrelatedattributelist = DB::table('tbl_ecom_product_attribute as proattr')
                ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
                ->where('proattr.status', '=', 1)
                ->where('proattr.use_for_variation', '=', 1)
                ->where('proattr.product_id', '=', $request->input('product_id'))
                ->get();
                $masterarray=array();

                //code for getting array of different attribute term
                foreach($productrelatedattributelist as $attr)
                {
                    $productattributeterm = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.product_id', '=', $request->input('product_id'))
                        ->where('proattrterm.attribute_id', '=',  $attr->attribute_id)
                        ->pluck('proattrterm.attribute_term_id');

                    if(count($productattributeterm) > 0){
                        array_push($masterarray,$productattributeterm);
                    }else{
                        return response()->json(['status' => 400, 'error' => "Please select attribute term for ".$attr->attribute_name], 400);
                    }
                    
                }

                // check for already exist variable products
                $productvariant = new EcomProductVariant();
                $generatedproductvariantcount = $productvariant->where('product_id', $request->input('product_id'))->where('status', 1)->orderBy('product_variant_id')->get();

                //code for getting all permutaion of ther different attribute term
                $finalarray=array();
                $finalarray=combinations($masterarray);
                // return response()->json(['status' => 200, "data"=>"ALL Product variant Generated", "data" => $finalarray]);

                $i=0;
                $matchedata = array();

                foreach($finalarray as $item)
                {   
                    
                    /* check for product with variation exists or not */
                    if(count($generatedproductvariantcount) > $i){

                        $producttermdata = DB::table('tbl_ecom_product_variant_attribute_term as proattrterm')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.variant_product_id', '=', $generatedproductvariantcount[$i]->variant_product_id)
                        ->first();

                        $temp = array();
                        $existing_comb = DB::table('tbl_ecom_product_variant_attribute_term as proattrterm')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.variant_product_id', '=', $generatedproductvariantcount[$i]->variant_product_id)
                        ->pluck('proattrterm.attribute_term_id');
                        $new_comb = $item;

                        for($comb_index=0; $comb_index<count($existing_comb); $comb_index++){
                            for($new_comb_index=0; $new_comb_index<count($new_comb); $new_comb_index++){
                                if($existing_comb[$comb_index] != $new_comb[$new_comb_index]){

                                    $productvariantattributeterm = new EcomProductVariantAttributeTerm();
                                    $productvariantattributeterm->attribute_id = $producttermdata->attribute_id;
                                    $productvariantattributeterm->variant_product_id = $producttermdata->variant_product_id;
                                    $productvariantattributeterm->attribute_term_id = $producttermdata->attribute_term_id;
                                    $productvariantattributeterm->created_by = $request->input('user_id');
                                    $productvariantattributeterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $productvariantattributeterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $productvariantattributeterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $productvariantattributeterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                    $result = $productvariantattributeterm->save();

                                }
                            } 
                        }

                    }
                    else{

                        $temp = array();
                        $temp['existing_comb'] = array();
                        $temp['new_comb'] = $item;
                        array_push($matchedata, $temp);

                        
                        //entry in product table with is variation 1
                        
                        $product = new EcomProduct();
                        $product->product_name = $product_name;  
                        $product->slug_id =  $slug_id;  
                        $product->is_variation = 1;
                        $product->has_variation = 0;

                        $product->tax_class_id = $productdata[0]->tax_class_id;
                        // $product->slug_name = $productdata[0]->tax_class_id;
                        $product->shipping_class_id = $productdata[0]->shipping_class_id;
                        $product->is_stockmanagement = $productdata[0]->is_stockmanagement;
                        $product->stock_status = $productdata[0]->stock_status;
                        $product->product_stock = $productdata[0]->product_stock;
                        $product->low_stock_threshold = $productdata[0]->low_stock_threshold;
                        $product->product_height = $productdata[0]->product_height;
                        $product->product_length = $productdata[0]->product_length;
                        $product->product_weight = $productdata[0]->product_weight;
                        $product->product_wide = $productdata[0]->product_wide;
                        $product->product_actual_price = $productdata[0]->product_actual_price;
                        $product->product_sale_price = $productdata[0]->product_sale_price;
                        $product->start_date = $productdata[0]->start_date;
                        $product->end_date = $productdata[0]->end_date;
                        $product->status = $productdata[0]->status;
                        $product->product_guid = generateAccessToken(25);
                        $product->shippingandreturn = $productdata[0]->shippingandreturn;
                        $product->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $product->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $product->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $product->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $product->site_id = $productdata[0]->site_id;
                        $product->created_by = $productdata[0]->created_by;
                        $product->updated_by = $productdata[0]->updated_by;
                        $product->product_type = "variant";
                        $result = $product->save();
                        $variant_product_id = $product->id;

                        if ($result) {

                            //entry in product variant
                            $variant_product_id=$product->id;

                            $productvariant = new EcomProductVariant();
                            $productvariantcount = $productvariant->where('product_id', $request->input('product_id'))->where('variant_product_id', $variant_product_id)->where('status', 1)->count();

                            if($productvariantcount==0){
                                $productvariant = new EcomProductVariant();
                                $productvariant->variant_product_id = $variant_product_id;
                                $productvariant->product_id = $request->input('product_id');
                                $productvariant->created_by = $request->input('user_id');
                                $productvariant->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $productvariant->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $productvariant->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $productvariant->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $result = $productvariant->save();
                                $generatedvariant++;
                                if (!$result) {
                                    return response()->json(['status' => 400, 'error' => "Error in adding product variant."], 400);
                                }
                            }

                            $termname = "";

                            if(is_array($item)){
                                foreach($item as $item1)
                                {
                                    //code for getting old variation value from db
                                    $attributedata = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                                    ->select('proattrterm.attribute_id','attr.attribute_term')
                                    ->leftJoin('tbl_ecom_attribute_term as attr', 'attr.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                                    ->where('proattrterm.status', '=', 1)
                                    ->where('proattrterm.attribute_term_id', '=',$item1)
                                    ->first();

                                    $attribute_id=$attributedata->attribute_id;
                                    $termname = $termname . " " . $attributedata->attribute_term;

                                    //entry in product variant attribute term
                                    $productvariatterm = new EcomProductVariantAttributeTerm();                                     
                                    $productvariattermdata =new EcomProductVariantAttributeTerm();

                                    $productvariatterm->variant_product_id = $variant_product_id;
                                    $productvariatterm->attribute_term_id =$item1;
                                    $productvariatterm->attribute_id = $attribute_id;
                                    $productvariatterm->created_by = $request->input('user_id');
                                    $productvariatterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                    $productvariatterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                    $productvariatterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                    $productvariatterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                    $result = $productvariatterm->save();

                                    
                                   
                                    $request->request->add(['updated_by' =>  $request->input('user_id')]);
                                    $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                                    $newrequest = $request->except(['slug_id','product_id','user_id','accesstoken','taglist','categorylist','slug_name','limit','offset','accesstoken']);
                                    $product_slug = $productdata[0]->product_name .$termname;
                                    $newrequest['slug_name'] = checkslugavailability($product_slug, "product",$request->input('site_id'));
                        
                                    $result = $product->where('product_id',$variant_product_id)->update($newrequest);

                                }
                            }else{
                                //code for getting old variation value from db
                                $attributedata = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                                ->select('proattrterm.attribute_id','attr.attribute_term')
                                ->leftJoin('tbl_ecom_attribute_term as attr', 'attr.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                                ->where('proattrterm.status', '=', 1)
                                ->where('proattrterm.attribute_term_id', '=',$item)
                                ->first();

                                $attribute_id=$attributedata->attribute_id;
                                $termname = $termname . " " . $attributedata->attribute_term;

                                //entry in product variant attribute term
                                $productvariatterm = new EcomProductVariantAttributeTerm();                                     
                                $productvariattermdata =new EcomProductVariantAttributeTerm();

                                $productvariatterm->variant_product_id = $variant_product_id;
                                $productvariatterm->attribute_term_id =$item;
                                $productvariatterm->attribute_id = $attribute_id;
                                $productvariatterm->created_by = $request->input('user_id');
                                $productvariatterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $productvariatterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $productvariatterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $productvariatterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $result = $productvariatterm->save();
                                $request->request->add(['updated_by' =>  $request->input('user_id')]);
                                $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                                $newrequest = $request->except(['slug_id','product_id','user_id','accesstoken','taglist','categorylist','slug_name','limit','offset','accesstoken']);
                                $product_slug = $productdata[0]->product_name .$termname;
                                $newrequest['slug_name'] = checkslugavailability($product_slug, "product",$request->input('site_id'));
                    
                                $result = $product->where('product_id',$variant_product_id)->update($newrequest);
                            }                     
                           
                        } 
                    }
                    
                    $i++;

                }
                
                return response()->json(['status' => 200, "data"=>"ALL Product variant Generated", "matcheddata"=>$matchedata , "generatedvariants" => $generatedvariant , "masterarray" => $masterarray, "data" => $finalarray]);

            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'No such product available.'], 400);
            }
       }

    }

    public function getproductlist(Request $request){
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $product = new EcomProduct();
            $businesssettings = new EcomBusinessSetting();

            $data = array();
            $data['offset'] = 0;
            $data['limit'] = 1;
            $data['site_id'] = $request->input('site_id');
            $businessdata = $businesssettings->getbusinesssetting($data);

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $data['site_id'] = $request->input('site_id');

            if (count($businessdata)>0) {
                $data['is_variation'] = $businessdata[0]->is_show_variation;
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }

            if ($request->has('product_guid') && $request->input('product_guid') != "") {
                $data['product_guid'] = $request->input('product_guid');
            }
        
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('product_name') && $request->filled('product_name')) {
                $data['product_name'] = $request->input('product_name');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }


            if ($request->has('minprice') && $request->input('minprice') != "" && $request->has('maxprice') && $request->input('maxprice') != "") {
                $data['minprice'] = $request->input('minprice');
                $data['maxprice'] = $request->input('maxprice');
            }

            if ($request->has('categoryidlist') && $request->input('categoryidlist') != "") {
                $data['categoryidlist'] = $request->input('categoryidlist');
            }

            if ($request->has('attributeidlist') && $request->input('attributeidlist') != "") {
                $data['attributeidlist'] = $request->input('attributeidlist');
            }

            if ($request->has('attributetermidlist') && $request->input('attributetermidlist') != "") {
                $data['attributetermidlist'] = $request->input('attributetermidlist');
            }

            if ($request->has('category_id') && $request->input('category_id') != "") {
                $data['category_id'] = $request->input('category_id');
            }

            if ($request->has('productidlist') && $request->input('productidlist') != "") {
                $data['productidlist'] = $request->input('productidlist');
            }

            if ($request->has('stock_status') && $request->input('stock_status') != "") {
                $data['stock_status'] = $request->input('stock_status');
            }

            if ($request->has('is_assured') && $request->input('is_assured') != "") {
                $data['is_assured'] = $request->input('is_assured');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $wishlist = new EcomWishlist();
            $wishlistold=array();
            $wishlist1=array();
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            
                
                if ($request->has('user_id') && $request->input('user_id') != "") {
                    $data['user_id'] = $request->input('user_id');
                    $wishlist1=$wishlist->where('user_id','=',$request->input('user_id'))->where('status','=','1')->where('site_id','=',$request->input('site_id'))->pluck('product_id');
                }
                for($i=0;$i<count($wishlist1);$i++){
                    if(count($wishlist1)>0 ){
                        if($wishlist1 != null &&  $wishlist1 != ""){
                            array_push($wishlistold, $wishlist1[$i]);
                        }
                    }

                }

            }
            $products = $product->getproductlist($data);
            $productdata = array();

            foreach($products['result'] as $item)
            {
                $product_id = $item->product_id;

                if(count($wishlistold)>0 && isset($wishlistold)){
                    $var = in_array($item->product_id,$wishlistold );
                    if($var){
                        $item->is_wishlist=true;
                    }else{
                        $item->is_wishlist=false;
                    }
                }else{
                    $item->is_wishlist=false;
                }
                /* Category List */
                $categorynamelist = DB::table('tbl_ecom_product_category as pc')
                ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->orderBy('category.category_name', 'ASC')
                ->pluck('category.category_name');
                $item->categorynamelist= $categorynamelist;

                if($item->seo_media_file=="" ||$item->seo_media_file==null || $item->seo_media_file=="null"){
                    $item->seo_img_path = fixedseofeaturedimage;
                }
                else
                {
                    if (does_url_exists( imagedisplaypath.$item->seo_media_file )) {
                        $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                    } else {
                        $item->seo_img_path = fixedseofeaturedimage;
                    } 
                }

                /* Product Gallery */
                $productgallery = new EcomProductGallery();
                $tempdata['product_id']=$product_id;
                $tempdata['offset']=0;
                $tempdata['limit']=10000;
                $productgallery = $productgallery->getproductgallery($tempdata);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }  
                    array_push($productgallerydata, $item1->product_image_path);
                }

                $item->product_gallery=$productgallerydata;

                /* Attribute List */
                $attributenamelist = DB::table('tbl_ecom_product_attribute as proattr')
                ->select('attr.*')
                ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->orderBy('attr.attribute_name', 'ASC')
                /*->pluck('attr.attribute_name');*/
                ->get();

                $item->attributenamelist=$attributenamelist;

                if($item->is_variation){
                    $item->attributetermlist = DB::table('tbl_ecom_product_variant_attribute_term as proattrterm')
                        ->select('tea.attribute_name','teat.*')
                        ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                        ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'proattrterm.attribute_id')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.variant_product_id', '=', $product_id)
                        ->orderBy('tea.attribute_id')
                        ->get();
                }else{
                    $item->attributetermlist = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                        ->select('tea.attribute_name','teat.*')
                        ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                        ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'proattrterm.attribute_id')
                        ->where('proattrterm.status', '=', 1)
                        ->where('proattrterm.product_id', '=', $product_id)
                        ->orderBy('tea.attribute_id')
                        ->get();
                }
                if($item->has_variation){
                    $productmodel = new EcomProduct();
                    $item->pricerange = $productmodel->getminmaxforproductid($product_id);
                }
                array_push($productdata, $item);
            }

            return response()->json(['status' => 200, 'totalcount' => $products['fullcount'], 'count' => count($products['result']), 'data' => $productdata,'business_data'=>$data]);
        }
    }

    public function getproductdetails(Request $request){
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            if(!$request->has('product_id') && !$request->has('slug_name')){
                return response()->json(['status' => 400, 'error' => 'product_id or slug_name must required.'], 400);
            }

            $product = new EcomProduct();
            $data = array();
            $data['offset'] = 0;
            $data['limit'] = 1;

            if ($request->has('product_guid') && $request->input('product_guid') != "") {
                $data['product_guid'] = $request->input('product_guid');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            $wishlist = new EcomWishlist();
            $wishlistold=array();
            $wishlist1=array();
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            
                
                if ($request->has('user_id') && $request->input('user_id') != "") {
                    $data['user_id'] = $request->input('user_id');
                    $wishlist1=$wishlist->where('user_id','=',$request->input('user_id'))->where('status','=','1')->where('site_id','=',$request->input('site_id'))->pluck('product_id');
                }
                for($i=0;$i<count($wishlist1);$i++){
                    if(count($wishlist1)>0 ){
                        if($wishlist1 != null &&  $wishlist1 != ""){
                            array_push($wishlistold, $wishlist1[$i]);
                        }
                    }

                }

            }
            $product = $product->getproduct($data);
            $productdata = array();
            foreach($product as $item)
            {
                $product_id = $item->product_id;
                if(count($wishlistold)>0 && isset($wishlistold)){
                    $var = in_array($item->product_id,$wishlistold );
                    if($var){
                        $item->is_wishlist=true;
                    }else{
                        $item->is_wishlist=false;
                    }
                }else{
                    $item->is_wishlist=false;
                }
                //product tag 
                $tagnamelist = DB::table('tbl_ecom_product_tag as pt')
                ->leftJoin('tbl_ecom_tag as tag', 'pt.tag_id', '=', 'tag.tag_id')
                ->where('pt.status', '=', 1)
                ->where('pt.product_id', '=', $product_id)
                ->orderBy('tag.tag_name', 'ASC')
                ->pluck('tag.tag_name');

                $tagidlist = DB::table('tbl_ecom_product_tag as pt')
                ->where('pt.status', '=', 1)
                ->where('pt.product_id', '=', $product_id)
                ->pluck('pt.tag_id');

              
                //product attribute
                $attributenamelist = DB::table('tbl_ecom_product_attribute as proattr')
                ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
                ->where('proattr.status', '=', 1)
                ->where('attr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->orderBy('attr.attribute_name', 'ASC')
                /*->pluck('attr.attribute_name');*/
                ->get();

                $attributeidlist = DB::table('tbl_ecom_product_attribute as proattr')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->pluck('proattr.attribute_id');

                $attributetermidlist = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                ->select('proattrterm.*','teat.attribute_term','teat.attribute_term_colorcode')
                ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                ->where('teat.status', '=', 1)
                ->where('proattrterm.status', '=', 1)
                ->where('proattrterm.product_id', '=', $product_id)
                ->get(); /*->groupBy('attribute_id')*/



                foreach($attributenamelist as $tempitem){
                    $attributefinallist = array();
                    foreach($attributetermidlist as $tempitem1){
                        if($tempitem1->attribute_id === $tempitem->attribute_id){
                            array_push($attributefinallist, $tempitem1);
                        }
                    }
                    $tempitem->attributeitemslist = $attributefinallist;
                }

                //product category
                $categorynamelist = array();
                $categoryidlist = array();

                $categorylist = DB::table('tbl_ecom_product_category as pc')
                ->select('category.*')
                ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->orderBy('category.category_name', 'ASC')
                ->get();

                foreach($categorylist as $catitem){
                    array_push($categorynamelist, $catitem->category_name);
                    array_push($categoryidlist, $catitem->category_id);
                }

                /*$categorynamelist = DB::table('tbl_ecom_product_category as pc')
                ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->orderBy('category.category_name', 'ASC')
                ->pluck('category.category_name');

                $categoryidlist = DB::table('tbl_ecom_product_category as pc')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->pluck('pc.category_id');*/

                if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null")
                    {
                        $item->seo_img_path = fixedseofeaturedimage;
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        { 
                            //code for seo image
                            if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            }
                            
                        }
                        else
                        {
                             //seo image
                            if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                                $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                            } else {
                                $item->seo_img_path = fixedseofeaturedimage;
                            } 
                        }
                    }

                //call for getting gallery image with product
                $productgallery = new EcomProductGallery();
                $data['product_id']=$product_id;
                $data['offset'] = 0;
                $data['limit'] = 1000;
                $productgallery = $productgallery->getproductgallery($data);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                 
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                    $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                } else {
                                    $item1->product_image_path =fixedproductgalleryimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                            } else {
                                $item1->product_image_path = fixedproductgalleryimage;
                            }
                        }
                    }  

                   
                    array_push($productgallerydata, $item1);
                }
                $item->product_gallery=$productgallerydata;
                $item->tagidlist= $tagidlist;
                $item->tagnamelist = $tagnamelist;
                $item->categorynamelist= $categorynamelist;
                $item->categoryidlist= $categoryidlist;
                $item->attributenamelist= $attributenamelist;
                $item->attributetermidlist= $attributetermidlist;
                $item->attributeidlist= $attributeidlist;
                $item->categorylist = $categorylist;

                /* get all variants if its variable product */
                $productvariant = new EcomProductVariant();
                if($item->has_variation){
                    $data = array();
                    $data['offset'] = 0;
                    $data['limit'] = 1000;
                    $data['product_id'] = $item->product_id;
                    $data['site_id'] = $request->input('site_id');
                    $data['user_id'] = $request->input('user_id');
                    
                    $item->variantslist= $productvariant->getproductvariant($data);
                    $productmodel = new EcomProduct();
                    $item->pricerange = $productmodel->getminmaxforproductid($product_id);
                }else{
                    $data = array();
                    $item->variantslist= $data;
                }
                array_push($productdata, $item);
            }

            return response()->json(['status' => 200, 'count' => count($product), 'data' => $productdata]);
        }
    }

    public function getrelatedproductlist(Request $request){
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            if(!$request->has('product_id') && !$request->has('slug_name')){
                return response()->json(['status' => 400, 'error' => 'product_id or slug_name must required.'], 400);
            }

            $product = new EcomProduct();
            $wishlist = new EcomWishlist();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }

            if ($request->has('product_guid') && $request->input('product_guid') != "") {
                $data['product_guid'] = $request->input('product_guid');
            }
            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
        
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('is_variation') && $request->input('is_variation') != "") {
                $data['is_variation'] = $request->input('is_variation');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $product = $product->getrelatedproductlist($data);
            $wishlistold=array();
            $wishlist1=array();
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            
                
                if ($request->has('user_id') && $request->input('user_id') != "") {
                    $data['user_id'] = $request->input('user_id');
                    $wishlist1=$wishlist->where('user_id','=',$request->input('user_id'))->where('status','=','1')->where('site_id','=',$request->input('site_id'))->pluck('product_id');
                }
                for($i=0;$i<count($wishlist1);$i++){
                    if(count($wishlist1)>0 ){
                        if($wishlist1 != null &&  $wishlist1 != ""){
                            array_push($wishlistold, $wishlist1[$i]);
                        }
                    }

                }

            }
            $productdata = array();
            foreach($product as $item)
            {
                $product_id = $item->product_id;

                
                if(count($wishlistold)>0 && isset($wishlistold)){
                    $var = in_array($item->product_id,$wishlistold );
                    if($var){
                        $item->is_wishlist=true;
                    }else{
                        $item->is_wishlist=false;
                    }
                }else{
                    $item->is_wishlist=false;
                }
                //product category
                $categorynamelist = DB::table('tbl_ecom_product_category as pc')
                ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->orderBy('category.category_name', 'ASC')
                ->pluck('category.category_name');

                if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null"){
                    $item->seo_img_path = fixedseofeaturedimage;
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                        $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                    } else {
                        $item->seo_img_path = fixedseofeaturedimage;
                    } 
                }

                //call for getting gallery image with product
                $productgallery = new EcomProductGallery();
                $data['product_id']=$product_id;
                $productgallery = $productgallery->getproductgallery($data);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }  
                    array_push($productgallerydata, $item1->product_image_path);
                }
                $item->product_gallery=$productgallerydata;
                $item->categorynamelist= $categorynamelist;
                array_push($productdata, $item);
            }

            return response()->json(['status' => 200, 'count' => count($product), 'data' => $productdata]);
        }
    }

    public function gettrendyproduct(Request $request){
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $product = new EcomProduct();
            $data = array();
            $offset = $request->input('offset');
            $limit = $request->input('limit');
            $i = 0;
            up:

            $query = DB::table('tbl_ecom_order_items as teoi')
                ->select('teoi.product_id',DB::Raw('count(*) as total'))
                ->leftJoin('tbl_ecom_order as teo', 'teo.order_id', '=', 'teoi.order_id')
                ->leftJoin('tbl_ecom_product as tep', 'tep.product_id', '=', 'teoi.product_id')
                ->whereNotIn('teo.order_status', ['pending','onhold','failed','cancelled'])
                ->where('teoi.status', 1)
                ->where('teo.status',1)
                ->where('tep.status',1)
                ->whereNotNull('tep.product_actual_price');

            if ($request->has('site_id') && $request->input('site_id') != "0") {
                $query = $query->where('teo.site_id', '=', $request->input('site_id'));
            }

            if ($request->has('days') && $request->input('days') != "") {
                $query = $query->whereRaw('DATE(teo.created_at) > DATE_SUB(CURDATE(), INTERVAL '.$request->input('days').' DAY)');
            }else{
                $query = $query->whereRaw('DATE(teoi.created_at) > DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
            }

            $trendyproducts = $query->groupBy('teoi.product_id')->offset($offset)->limit($limit)->orderBy('total', 'DESC')->get();

            if(count($trendyproducts)==0 && $i<5){
                $limit = $limit + 30;
                $i++;
                goto up;
            }

            $productlist = array();
            $productcount = array();

            foreach($trendyproducts as $item){
                array_push($productlist, $item->product_id);
                array_push($productcount, $item);
            }

            $products = DB::table('tbl_ecom_product as tep')->whereIn('tep.product_id', $productlist)->get();

            $productdata = array();
            foreach($products as $item)
            {
                $product_id = $item->product_id;

                foreach($trendyproducts as $item1){
                    if($product_id ==$item1->product_id){
                        $item->total = $item1->total;
                    }
                }

                /* Category List */
                $categorynamelist = DB::table('tbl_ecom_product_category as pc')
                ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
                ->where('pc.status', '=', 1)
                ->where('pc.product_id', '=', $product_id)
                ->orderBy('category.category_name', 'ASC')
                ->pluck('category.category_name');
                $item->categorynamelist= $categorynamelist;

                /* Product Gallery */
                $productgallery = new EcomProductGallery();
                $tempdata['product_id']=$product_id;
                $tempdata['offset']=0;
                $tempdata['limit']=10000;
                $productgallery = $productgallery->getproductgallery($tempdata);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }  
                    array_push($productgallerydata, $item1->product_image_path);
                }

                $item->product_gallery=$productgallerydata;

                /* Attribute List */
                $attributenamelist = DB::table('tbl_ecom_product_attribute as proattr')
                ->select('attr.*')
                ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=', $product_id)
                ->orderBy('attr.attribute_name', 'ASC')
                /*->pluck('attr.attribute_name');*/
                ->get();

                $item->attributenamelist=$attributenamelist;

                if($item->is_variation){
                    $item->attributetermlist = DB::table('tbl_ecom_product_variant_attribute_term as proattrterm')
                        ->select('tea.attribute_name','teat.*')
                        ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                        ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'proattrterm.attribute_id')
                        ->where('proattrterm.status', '=', 1)
                        ->where('tea.status', '=', 1)
                        ->where('proattrterm.variant_product_id', '=', $product_id)
                        ->orderBy('tea.attribute_id')
                        /*->pluck('teat.attribute_term')*/
                        ->get();
                }else{
                    $item->attributetermlist = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                        ->select('tea.attribute_name','teat.*')
                        ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                        ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'proattrterm.attribute_id')
                        ->where('proattrterm.status', '=', 1)
                        ->where('tea.status', '=', 1)
                        ->where('proattrterm.product_id', '=', $product_id)
                        ->orderBy('tea.attribute_id')
                        /*->pluck('teat.attribute_term')*/
                        ->get();
                }

                if($item->has_variation){
                    $productmodel = new EcomProduct();
                    $item->pricerange = $productmodel->getminmaxforproductid($product_id);
                }

                array_push($productdata, $item);
            }

            return response()->json(['status' => 200, 'count' => count($productdata), 'data' => $productdata, 'trendy product'=>$trendyproducts]);
        }
    }
}
?>
