<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Package;

class PackageController extends Controller
{
    
    public function managepackage(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "package_name" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $package = new package();
            $packagedata = $package->where('package_id', $request->input('package_id'))->count();

            if($packagedata == 0){
                $package = new Package();
                $package->package_name = $request->input('package_name');
                $package->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $package->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $package->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $package->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $package->save();
            }else{
            	 $Package = new Package();
            // $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['site_id','package_id','accesstoken','user_id']);
            $result = $Package->where('package_id', $request->input('package_id'))->update($newrequest);
            }
            
            return response()->json(['status' => 200, 'data' => 'Package registered.']);
        }
    }

    //Get all admin code
    public function getAllpackage(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $package = new Package();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if($request->has('package_id') && $request->filled('package_id')){
                $data['package_id'] = $request->input('package_id');
            }
            $packagedata = $package->getPackage($data);
            return response()->json(['status' => 200, 'count' => count($packagedata), 'data' => $packagedata]);
        }
    }
}
