<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomBusinessSetting;
use Illuminate\Support\Facades\File;

class EcomBusinessSettingController extends Controller
{
    public function addbusinesssetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            /* check any setting exists or not same site_id */
            $businesssetting = new EcomBusinessSetting();
            $businesssettingdata = $businesssetting->where('site_id',$request->input('site_id'))->where('status', 1)->get();

            if(count($businesssettingdata)>0){

                $updateddata = array();
                $updateddata = $request->except(['site_id','accesstoken','user_id']);

                if ($request->hasFile('business_dark_logo_svg')) {
                    
                    $files = $request->file('business_dark_logo_svg');
                    $filename =$files->getClientOriginalName();
                    $imgpath= fileuploads($filename, $files);
                    $updateddata['business_dark_logo_svg'] = $imgpath;

                } 

                if ($request->hasFile('business_light_logo_svg')) {
                    
                    $files = $request->file('business_light_logo_svg');
                    $filename =$files->getClientOriginalName();
                    $imgpath= fileuploads($filename, $files);
                    $updateddata['business_light_logo_svg'] = $imgpath;

                }

                if($request->has('business_dark_logo') && $request->filled('business_dark_logo'))
                {
                    $media_file_base64= $request->input('business_dark_logo');
                    if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                    {
                        $responsedata = fileUploadSlim($media_file_base64, "business_dark_logo");
                        $filepath = $responsedata['folderpath'];
                        $updateddata['business_dark_logo'] = $filepath;
                    }
                }

                /* Upload Light Logo */
                if($request->has('business_light_logo') && $request->filled('business_light_logo'))
                {
                    $media_file_base64= $request->input('business_light_logo');
                    if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                    {
                        $responsedata = fileUploadSlim($media_file_base64, "business_light_logo");
                        $filepath = $responsedata['folderpath'];
                        $updateddata['business_light_logo'] = $filepath;
                    }
                }

                /* Upload Favicon Logo */
                if($request->has('business_favicon') && $request->filled('business_favicon'))
                {
                    $media_file_base64= $request->input('business_favicon');
                    if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                    {
                        $responsedata = fileUploadSlim($media_file_base64, "business_favicon");
                        $filepath = $responsedata['folderpath'];
                        $updateddata['business_favicon'] = $filepath;
                    }
                }

                /* Upload Light Logo */
                if($request->has('business_email_header') && $request->filled('business_email_header'))
                {
                    $media_file_base64= $request->input('business_email_header');
                    if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                    {
                        $responsedata = fileUploadSlim($media_file_base64, "business_email_header");
                        $filepath = $responsedata['folderpath'];
                        $updateddata['business_email_header'] = $filepath;
                    }
                }

                $businesssetting = new EcomBusinessSetting();
                $result = $businesssetting->where('business_setting_id',$businesssettingdata[0]->business_setting_id)->update($updateddata);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Business setting added sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }

            }else{
            
                $businesssetting = new EcomBusinessSetting();
                $businesssetting->business_name = $request->has('business_name')?$request->input('business_name'):null;
                $businesssetting->business_type = $request->has('business_type')?$request->input('business_type'):null;
                $businesssetting->business_category = $request->has('business_category')?$request->input('business_category'):null;
                $businesssetting->business_city = $request->has('business_city')?$request->input('business_city'):null;
                $businesssetting->business_state = $request->has('business_state')?$request->input('business_state'):null;
                $businesssetting->business_country = $request->has('business_country')?$request->input('business_country'):null;
                $businesssetting->business_pincode = $request->has('business_pincode')?$request->input('business_pincode'):null;
                $businesssetting->business_address1 = $request->has('business_address1')?$request->input('business_address1'):null;
                $businesssetting->business_address2 = $request->has('business_address2')?$request->input('business_address2'):null;
                $businesssetting->business_admin_email = $request->has('business_admin_email')?$request->input('business_admin_email'):null;
                $businesssetting->business_tax_no = $request->has('business_tax_no')?$request->input('business_tax_no'):null;
                $businesssetting->business_tax_no = $request->has('business_site_url')?$request->input('business_site_url'):null;
                $businesssetting->business_dark_logo = null;
                $businesssetting->business_light_logo = null;
                $businesssetting->business_favicon = null;
                $businesssetting->business_email_header = null;
                $businesssetting->business_phone_iso = $request->has('business_phone_iso')?$request->input('business_phone_iso'):null;
                $businesssetting->business_phone_code = $request->has('business_phone_code')?$request->input('business_phone_code'):null;
                $businesssetting->business_phone = $request->has('business_phone')?$request->input('business_phone'):null;
                $businesssetting->business_mobile_iso = $request->has('business_mobile_iso')?$request->input('business_mobile_iso'):null;
                $businesssetting->business_mobile_code = $request->has('business_mobile_code')?$request->input('business_mobile_code'):null;
                $businesssetting->business_mobile = $request->has('business_mobile')?$request->input('business_mobile'):null;
                $businesssetting->created_by = $request->has('created_by')?$request->input('created_by'):null;
                $businesssetting->site_id = $request->has('site_id')?$request->input('site_id'):null;
                $businesssetting->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $businesssetting->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $businesssetting->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $businesssetting->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $businesssetting->save();
                if ($result) {

                    $newrequest = array();

                    if ($request->hasFile('business_dark_logo_svg')) {
                    
                        $files = $request->file('business_dark_logo_svg');
                        $filename =$files->getClientOriginalName();
                        $imgpath= fileuploads($filename, $files);
                        $newrequest['business_dark_logo_svg'] = $imgpath;

                    } 

                    if ($request->hasFile('business_light_logo_svg')) {
                        
                        $files = $request->file('business_light_logo_svg');
                        $filename =$files->getClientOriginalName();
                        $imgpath= fileuploads($filename, $files);
                        $newrequest['business_light_logo_svg'] = $imgpath;

                    }

                    /* Upload Dark Logo */
                    if($request->has('business_dark_logo') && $request->filled('business_dark_logo'))
                    {
                        $media_file_base64= $request->input('business_dark_logo');
                        if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                        {
                            $responsedata = fileUploadSlim($media_file_base64, "business_dark_logo");
                            $filepath = $responsedata['folderpath'];
                            $newrequest['business_dark_logo'] = $filepath;
                        }
                    }

                    /* Upload Light Logo */
                    if($request->has('business_light_logo') && $request->filled('business_light_logo'))
                    {
                        $media_file_base64= $request->input('business_light_logo');
                        if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                        {
                            $responsedata = fileUploadSlim($media_file_base64, "business_light_logo");
                            $filepath = $responsedata['folderpath'];
                            $newrequest['business_light_logo'] = $filepath;
                        }
                    }

                    /* Upload Favicon Logo */
                    if($request->has('business_favicon') && $request->filled('business_favicon'))
                    {
                        $media_file_base64= $request->input('business_favicon');
                        if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                        {
                            $responsedata = fileUploadSlim($media_file_base64, "business_favicon");
                            $filepath = $responsedata['folderpath'];
                            $newrequest['business_favicon'] = $filepath;
                        }
                    }

                    /* Upload Light Logo */
                    if($request->has('business_email_header') && $request->filled('business_email_header'))
                    {
                        $media_file_base64= $request->input('business_email_header');
                        if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                        {
                            $responsedata = fileUploadSlim($media_file_base64, "business_email_header");
                            $filepath = $responsedata['folderpath'];
                            $newrequest['business_email_header'] = $filepath;
                        }
                    }

                    if(count($newrequest)>0){
                        $businesssetting = new EcomBusinessSetting();
                        $businesssetting->where('site_id', $request->input('site_id'))->update($newrequest);
                    }

                    return response()->json(['status' => 200, 'data' => "Business setting added sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }

            
        }
    }

    public function getallbusinesssetting(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $businesssetting = new EcomBusinessSetting();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('business_setting_id') && $request->input('business_setting_id') != "") {
                $data['business_setting_id'] = $request->input('business_setting_id');
            }
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $businesssettingdata = $businesssetting->getbusinesssetting($data);

            foreach($businesssettingdata as $item){

                if(fileuploadtype=="local"){
                    if($item->business_dark_logo_svg==""||$item->business_dark_logo_svg==null||$item->business_dark_logo_svg=="null")
                    {
                        $item->business_dark_logo_svg =fixedpagefeaturedimage;
                    }else{
                        if (File::exists(baseimagedisplaypath .$item->business_dark_logo_svg)) {
                            $item->business_dark_logo_svg = imagedisplaypath.$item->business_dark_logo_svg;
                        } else {
                            $item->business_dark_logo_svg =fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->business_dark_logo_svg)) {
                        $item->business_dark_logo_svg = imagedisplaypath.$item->business_dark_logo_svg;
                    } else {
                        $item->business_dark_logo_svg =fixedpagefeaturedimage;
                    } 
                }

                if(fileuploadtype=="local"){
                    if($item->business_light_logo_svg==""||$item->business_light_logo_svg==null||$item->business_light_logo_svg=="null")
                    {
                        $item->business_light_logo_svg =fixedpagefeaturedimage;
                    }else{
                        if (File::exists(baseimagedisplaypath .$item->business_light_logo_svg)) {
                            $item->business_light_logo_svg = imagedisplaypath.$item->business_light_logo_svg;
                        } else {
                            $item->business_light_logo_svg =fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->business_light_logo_svg)) {
                        $item->business_light_logo_svg = imagedisplaypath.$item->business_light_logo_svg;
                    } else {
                        $item->business_light_logo_svg =fixedpagefeaturedimage;
                    } 
                }

                if(fileuploadtype=="local"){
                    if($item->business_dark_logo==""||$item->business_dark_logo==null||$item->business_dark_logo=="null")
                    {
                        $item->business_dark_logo =fixedpagefeaturedimage;
                    }else{
                        if (File::exists(baseimagedisplaypath .$item->business_dark_logo)) {
                            $item->business_dark_logo = imagedisplaypath.$item->business_dark_logo;
                        } else {
                            $item->business_dark_logo =fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->business_dark_logo)) {
                        $item->business_dark_logo = imagedisplaypath.$item->business_dark_logo;
                    } else {
                        $item->business_dark_logo =fixedpagefeaturedimage;
                    } 
                }

                if(fileuploadtype=="local"){
                    if($item->business_light_logo==""||$item->business_light_logo==null||$item->business_light_logo=="null")
                    {
                        $item->business_light_logo =fixedpagefeaturedimage;
                    }else{
                        if (File::exists(baseimagedisplaypath .$item->business_light_logo)) {
                            $item->business_light_logo = imagedisplaypath.$item->business_light_logo;
                        } else {
                            $item->business_light_logo =fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->business_light_logo)) {
                        $item->business_light_logo = imagedisplaypath.$item->business_light_logo;
                    } else {
                        $item->business_light_logo =fixedpagefeaturedimage;
                    } 
                }


                if(fileuploadtype=="local"){
                    if($item->business_favicon==""||$item->business_favicon==null||$item->business_favicon=="null")
                    {
                        $item->business_favicon =fixedpagefeaturedimage;
                    }else{
                        if (File::exists(baseimagedisplaypath .$item->business_favicon)) {
                            $item->business_favicon = imagedisplaypath.$item->business_favicon;
                        } else {
                            $item->business_favicon =fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->business_favicon)) {
                        $item->business_favicon = imagedisplaypath.$item->business_favicon;
                    } else {
                        $item->business_favicon =fixedpagefeaturedimage;
                    } 
                }

                
                if(fileuploadtype=="local"){
                    if($item->business_email_header==""||$item->business_email_header==null||$item->business_email_header=="null")
                    {
                        $item->business_email_header =fixedpagefeaturedimage;
                    }else{
                        if (File::exists(baseimagedisplaypath .$item->business_email_header)) {
                            $item->business_email_header = imagedisplaypath.$item->business_email_header;
                        } else {
                            $item->business_email_header =fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->business_email_header)) {
                        $item->business_email_header = imagedisplaypath.$item->business_email_header;
                    } else {
                        $item->business_email_header =fixedpagefeaturedimage;
                    } 
                }


            }
            return response()->json(['status' => 200, 'count' => count($businesssettingdata), 'data' => $businesssettingdata]);
        }

    }

    public function updatebusinesssetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "business_setting_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $businesssetting = new EcomBusinessSetting();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['business_setting_id','accesstoken','user_id']);

            /* Upload Dark Logo */
            if($request->has('business_dark_logo') && $request->filled('business_dark_logo'))
            {
                $media_file_base64= $request->input('business_dark_logo');
                if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                {
                    $responsedata = fileUploadSlim($media_file_base64, "business_dark_logo");
                    $filepath = $responsedata['folderpath'];
                    $newrequest['business_dark_logo'] = $filepath;
                }
            }

            /* Upload Light Logo */
            if($request->has('business_light_logo') && $request->filled('business_light_logo'))
            {
                $media_file_base64= $request->input('business_light_logo');
                if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                {
                    $responsedata = fileUploadSlim($media_file_base64, "business_light_logo");
                    $filepath = $responsedata['folderpath'];
                    $newrequest['business_light_logo'] = $filepath;
                }
            }

            /* Upload Favicon Logo */
            if($request->has('business_favicon') && $request->filled('business_favicon'))
            {
                $media_file_base64= $request->input('business_favicon');
                if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                {
                    $responsedata = fileUploadSlim($media_file_base64, "business_favicon");
                    $filepath = $responsedata['folderpath'];
                    $newrequest['business_favicon'] = $filepath;
                }
            }

            /* Upload Light Logo */
            if($request->has('business_email_header') && $request->filled('business_email_header'))
            {
                $media_file_base64= $request->input('business_email_header');
                if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                {
                    $responsedata = fileUploadSlim($media_file_base64, "business_email_header");
                    $filepath = $responsedata['folderpath'];
                    $newrequest['business_email_header'] = $filepath;
                }
            }

            $result = $businesssetting->where('business_setting_id', $request->input('business_setting_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Business setting updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletebusinesssetting(Request $request) {

        $valid = Validator::make($request->all(), [
                    "business_setting_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $businesssetting = new EcomBusinessSetting();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['business_setting_id','accesstoken','user_id']);
            $result = $businesssetting->where('business_setting_id', $request->input('business_setting_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' =>"Delete business setting"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
