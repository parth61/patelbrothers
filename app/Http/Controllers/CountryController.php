<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Country;

class CountryController extends Controller
{
    

     public function addmastercountry(Request $request) {
        //validations
        $country = new Country();
        $valid = Validator::make($request->all(), [
                    "country_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $country->country_name = $request->input('country_name');
            $country->country_phonecode = $request->input('country_phonecode');
            $country->country_shortcode = $request->input('country_shortcode');
            $country->created_by = $request->input('created_by');
            $country->updated_by = $request->input('updated_by');
            $country->status = $request->input('status');
            $country->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $country->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $country->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $country->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $country->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Country added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallmastercountry(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $country = new Country();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $countrydata = $country->getcountry($data);
            
            return response()->json(['status' => 200, 'count' => count($countrydata), 'data' => $countrydata]);
        }
    }
    public function updatemastercountry(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "country_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $country = new Country();
            $newrequest = $request->except(['country_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $country->where('country_id', $request->input('country_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Country Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletemastercountry(Request $request) {
        $valid = Validator::make($request->all(), [
                    "country_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $country = new Country();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['country_id']);
            $result = $country->where('country_id', $request->input('country_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
