<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Buildingtype;
class BuildingTypeController extends Controller
{
    public function addbuildingtype(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                   
                    "building_type_name" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $buildingtype = new Buildingtype();
            $buildingtype->building_type_name = $request->input('building_type_name');
            
            $buildingtype->created_by = $request->input('created_by');
            $buildingtype->updated_by = $request->input('updated_by');
            $buildingtype->status = $request->input('status');
            $buildingtype->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $buildingtype->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $buildingtype->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $buildingtype->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $buildingtype->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Buildingtype added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallbuildingtype(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $buildingtype = new Buildingtype();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('building_type_id') && $request->input('building_type_id') != "") {
                $data['building_type_id'] = $request->input('building_type_id');
            }
            $buildingtype = $buildingtype->getbuilding($data);

            return response()->json(['status' => 200, 'count' => count($buildingtype), 'data' => $buildingtype]);
        }
    }

    public function updatebuildingtype(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "building_type_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $buildingtype = new Buildingtype();
            $newrequest = $request->except(['building_type_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $buildingtype->where('building_type_id', $request->input('building_type_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Buildingtype updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletebuildingtype(Request $request) {
        $valid = Validator::make($request->all(), [
                    "building_type_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $buildingtype = new Buildingtype();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['building_type_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $buildingtype->where('building_type_id', $request->input('building_type_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
