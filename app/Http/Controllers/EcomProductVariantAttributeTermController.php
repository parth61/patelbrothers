<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductVariantAttributeTerm;
use Carbon\Carbon;
class EcomProductVariantAttributeTermController extends Controller
{
    public function addproductvariantattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "variant_product_id" => "required",
            "attribute_term_id" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             //code for getting old variation value from db
             $attributedata = DB::table('tbl_ecom_product_attribute_term as proattrterm')
             ->select('proattrterm.attribute_id')
             ->where('proattrterm.status', '=', 1)
             ->where('proattrterm.attribute_term_id', '=',$request->input('attribute_term_id'))
             ->first();

            $attribute_id=$attributedata->attribute_id;

            $productvariantattributeterm = new EcomProductVariantAttributeTerm();
            $productvariantattributeterm->attribute_id = $request->input('attribute_id');
            $productvariantattributeterm->variant_product_id = $request->input('variant_product_id');
            $productvariantattributeterm->attribute_term_id = $request->input('attribute_term_id');
            $productvariantattributeterm->created_by = $request->input('user_id');
            $productvariantattributeterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $productvariantattributeterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $productvariantattributeterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $productvariantattributeterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $productvariantattributeterm->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "productvariantattributeterm Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallproductvariantattributeterm(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariantattributeterm = new EcomProductVariantAttributeTerm();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_attribute_term_id') && $request->input('product_attribute_term_id') != "") {
                $data['product_attribute_term_id'] = $request->input('product_attribute_term_id');
            }
            if ($request->has('attribute_term_id') && $request->input('attribute_term_id') != "") {
                $data['attribute_term_id'] = $request->input('attribute_term_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('variant_product_id') && $request->input('variant_product_id') != "") {
                $data['variant_product_id'] = $request->input('variant_product_id');
            }
            $productvariantattributeterm = $productvariantattributeterm->getproductvariantattributeterm($data);

            return response()->json(['status' => 200, 'count' => count($productvariantattributeterm), 'data' => $productvariantattributeterm]);
        }

    }

    public function updateproductvariantattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_attribute_term_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariantattributeterm = new EcomProductVariantAttributeTerm();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_attribute_term_id','user_id','accesstoken']);
            $result = $productvariantattributeterm->where('product_attribute_term_id', $request->input('product_attribute_term_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "productvariantattributeterm Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproductvariantattributeterm(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_attribute_term_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productvariantattributeterm = new EcomProductVariantAttributeTerm();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_attribute_term_id','user_id','accesstoken']);
            $result = $productvariantattributeterm->where('product_attribute_term_id', $request->input('product_attribute_term_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  
}
