<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\ProjectSalesExecutive;
class ProjectSaleExecutiveController extends Controller
{
    public function getallprojectbysalesexecutiveid(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "sales_executive_id" => "required|numeric",
           ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectSalesExecutive = new ProjectSalesExecutive();
            $data = array();
          
            if ($request->has('sales_executive_id') && $request->input('sales_executive_id') != "") {
                $data['sales_executive_id'] = $request->input('sales_executive_id');
            }
            $ProjectSalesExecutive = $ProjectSalesExecutive->getprojectbysalesexecutiveid($data);
            return response()->json(['status' => 200, 'count' => count($ProjectSalesExecutive), 'data' => $ProjectSalesExecutive]);
        }
    }
}
