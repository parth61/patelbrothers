<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Layouts;
class LayoutController extends Controller
{
    public function addlayout(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                    "layout_name" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $layout = new Layouts();
            $layout->layout_name = $request->input('layout_name');
            
            $layout->created_by = $request->input('created_by');
            $layout->updated_by = $request->input('updated_by');
            $layout->status = $request->input('status');
            $layout->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $layout->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $layout->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $layout->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $layout->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Layout added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalllayout(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $layout = new Layouts();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('layout_id') && $request->input('layout_id') != "") {
                $data['layout_id'] = $request->input('layout_id');
            }
            $layout = $layout->getlayout($data);

            return response()->json(['status' => 200, 'count' => count($layout), 'data' => $layout]);
        }
    }

    public function updatelayout(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "layout_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $layout = new Layouts();
            $newrequest = $request->except(['layout_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $layout->where('layout_id', $request->input('layout_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Layout updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletelayout(Request $request) {
        $valid = Validator::make($request->all(), [
                    "layout_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $layout = new Layouts();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['layout_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $layout->where('layout_id', $request->input('layout_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
