<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Stores3Controller extends Controller
{
    public function store(Request $request)
    {
        if($request->hasFile('profile_image')) {
     
            $file = $request->file('profile_image');
            //get filename with extension
            $filenamewithextension = $request->file('profile_image')->getClientOriginalName();
     
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
     
            //get file extension
            $extension = $request->file('profile_image')->getClientOriginalExtension();
     
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $filePath = 'images/' . $filenametostore;
            //Upload File to s3
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
     
            //Store $filenametostore in the database
        }
}
}
