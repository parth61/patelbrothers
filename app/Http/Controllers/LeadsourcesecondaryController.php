<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Leadsourcesecondary;

class LeadsourcesecondaryController extends Controller
{
     public function addleadsourcesecondary(Request $request) {
        //validations
       $leadsourcesecondary = new Leadsourcesecondary();
        $valid = Validator::make($request->all(), [
                    "lead_source_secondary_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $leadsourcesecondary->lead_source_secondary_name = $request->input('lead_source_secondary_name');
            $leadsourcesecondary->lead_source_primary_id = $request->input('lead_source_primary_id');
            $leadsourcesecondary->status = $request->input('status');
            $leadsourcesecondary->created_by = $request->input('created_by');
            $leadsourcesecondary->updated_by = $request->input('updated_by');
            $leadsourcesecondary->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $leadsourcesecondary->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $leadsourcesecondary->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $leadsourcesecondary->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $leadsourcesecondary->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadsourcesecondary added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallleadsourcesecondary(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadsourcesecondary = new Leadsourcesecondary();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('lead_source_primary_id') && $request->input('lead_source_primary_id') != "") {
                $data['lead_source_primary_id'] = $request->input('lead_source_primary_id');
            }
            $leadsourcesecondarydata = $leadsourcesecondary->getleadsourcesecondary($data);
            
            return response()->json(['status' => 200, 'count' => count($leadsourcesecondarydata), 'data' => $leadsourcesecondarydata]);
        }
    }
    public function updateleadsourcesecondary(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_source_secondary_id" => "required|numeric",
                    
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $leadsourcesecondary = new Leadsourcesecondary();
            $newrequest = $request->except(['lead_source_secondary_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $leadsourcesecondary->where('lead_source_secondary_id', $request->input('lead_source_secondary_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadsourcesecondary Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteleadsourcesecondary(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_source_secondary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           $leadsourcesecondary = new Leadsourcesecondary();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['lead_source_secondary_id']);
            $result = $leadsourcesecondary->where('lead_source_secondary_id', $request->input('lead_source_secondary_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
