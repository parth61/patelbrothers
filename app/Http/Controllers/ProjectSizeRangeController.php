<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\ProjectSizeRange;
use DB;
class ProjectSizeRangeController extends Controller
{
    public function addprojectsizerange(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
                    "size_range_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectSizeRange = new ProjectSizeRange();
            $ProjectSizeRange->project_id = $request->input('project_id');
            $ProjectSizeRange->size_range_id = $request->input('size_range_id');
            $ProjectSizeRange->created_by = $request->input('created_by');
            $ProjectSizeRange->updated_by = $request->input('updated_by');
            $ProjectSizeRange->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ProjectSizeRange->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ProjectSizeRange->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ProjectSizeRange->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ProjectSizeRange->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Size Range Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function getallprojectsizerange(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectSizeRange = new ProjectSizeRange();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('project_size_range_id') && $request->input('project_size_range_id') != "") {
                $data['project_size_range_id'] = $request->input('project_size_range_id');
            }
            if ($request->has('size_range_id') && $request->input('size_range_id') != "") {
                $data['size_range_id'] = $request->input('size_range_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $ProjectSizeRange = $ProjectSizeRange->getprojectsizerange($data);
            return response()->json(['status' => 200, 'count' => count($ProjectSizeRange), 'data' => $ProjectSizeRange]);
        }
    }
   
    
    public function updateprojectsizerange(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_size_range_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectSizeRange = new ProjectSizeRange();
            $newrequest = $request->except(['project_size_range_id']);
            $result = $ProjectSizeRange->where('project_size_range_id', $request->input('project_size_range_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "ProjectSizeRange Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function deleteprojectsizerange(Request $request) {
        $valid = Validator::make($request->all(), [
            "project_size_range_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectSizeRange = new ProjectSizeRange();
            $result = $ProjectSizeRange->where('project_size_range_id', $request->input('project_size_range_id'))->delete();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
