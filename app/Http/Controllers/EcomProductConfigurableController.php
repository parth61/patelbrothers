<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductConfigurable;
use Carbon\Carbon;
class EcomProductConfigurableController extends Controller
{
    public function addproductconfigurable(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required",
            "configurable_product_id" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productconfigurable = new EcomProductConfigurable();
           
            $productconfigurable->configurable_product_id = $request->input('configurable_product_id');
            $productconfigurable->product_id = $request->input('product_id');
            $productconfigurable->created_by = $request->input('user_id');
            $productconfigurable->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $productconfigurable->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $productconfigurable->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $productconfigurable->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $productconfigurable->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Configurable product added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallproductconfigurable(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productconfigurable = new EcomProductConfigurable();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_configurable_id') && $request->input('product_configurable_id') != "") {
                $data['product_configurable_id'] = $request->input('product_configurable_id');
            }
            if ($request->has('configurable_product_id') && $request->input('configurable_product_id') != "") {
                $data['configurable_product_id'] = $request->input('configurable_product_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            $productconfigurable = $productconfigurable->getproductconfigurable($data);

            return response()->json(['status' => 200, 'count' => count($productconfigurable), 'data' => $productconfigurable]);
        }

    }

    public function updateproductconfigurable(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_configurable_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productconfigurable = new EcomProductConfigurable();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_configurable_id','user_id','accesstoken']);
            $result = $productconfigurable->where('product_configurable_id', $request->input('product_configurable_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Configurable product updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproductconfigurable(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_configurable_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productconfigurable = new EcomProductConfigurable();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_configurable_id','user_id','accesstoken']);
            $result = $productconfigurable->where('product_configurable_id', $request->input('product_configurable_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
  
}
