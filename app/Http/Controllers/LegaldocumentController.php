<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Legaldocuments;
class LegaldocumentController extends Controller
{
    public function addlegaldocument(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                    "legal_document_type" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $legaldocuments = new Legaldocuments();
            $legaldocuments->legal_document_type = $request->input('legal_document_type');
            
            $legaldocuments->created_by = $request->input('created_by');
            $legaldocuments->updated_by = $request->input('updated_by');
            $legaldocuments->status = $request->input('status');
            $legaldocuments->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $legaldocuments->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $legaldocuments->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $legaldocuments->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $legaldocuments->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "LegalDocuments added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalllegaldocument(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $legaldocuments = new Legaldocuments();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('legal_document_type_id') && $request->input('legal_document_type_id') != "") {
                $data['legal_document_type_id'] = $request->input('legal_document_type_id');
            }
            $legaldocuments = $legaldocuments->getlegaldocuments($data);

            return response()->json(['status' => 200, 'count' => count($legaldocuments), 'data' => $legaldocuments]);
        }
    }

    public function updatelegaldocument(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "legal_document_type_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $legaldocuments = new Legaldocuments();
            $newrequest = $request->except(['legal_document_type_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $legaldocuments->where('legal_document_type_id', $request->input('legal_document_type_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "LegalDocuments updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletelegaldocument(Request $request) {
        $valid = Validator::make($request->all(), [
                    "legal_document_type_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $legaldocuments = new Legaldocuments();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['legal_document_type_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $legaldocuments->where('legal_document_type_id', $request->input('legal_document_type_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
