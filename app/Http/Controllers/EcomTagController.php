<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomTag;
use App\Slug;
class EcomTagController extends Controller
{
    public function addtag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "tag_name" => "required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tags = new Ecomtag();
            $tags->tag_name = $request->input('tag_name');
           if ($request->has('tag_slug') && $request->input('tag_slug') != "")
            {
                $slug_name =$request->input('tag_slug');
            }
            else
            {
                $slug_name =$request->input('tag_name');
            }

            $slug_name=checkslugavailability($slug_name, "producttag",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="producttag";
            $slug->site_id =$request->input('site_id');
            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $tags->slug_id = $slug_id;*/
            $tags->slug_name = $slug_name;
            $tags->tag_description = $request->input('tag_description');
            $tags->created_by = $request->input('user_id');
            $tags->site_id = $request->input('site_id');
            $tags->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $tags->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $tags->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $tags->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $tags->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "tag Added Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalltag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tag = new Ecomtag();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('tag_id') && $request->input('tag_id') != "") {
                $data['tag_id'] = $request->input('tag_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            
            $tag = $tag->gettag($data);

            return response()->json(['status' => 200, 'count' => count($tag), 'data' => $tag]);
        }

    }

    public function updatetag(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "tag_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tag = new Ecomtag();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['slug_id','tag_id','accesstoken','user_id','slug_name']);

            if($request->has('slug_name')){
                $tag = $tag->where('tag_id', $request->input('tag_id'))->first();
                if($tag->slug_name != $request->input('slug_name')){
                   $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "producttag",$request->input('site_id'));
                }
            }

            $result = $tag->where('tag_id', $request->input('tag_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "tag Updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletetag(Request $request) {

        $valid = Validator::make($request->all(), [
                    "tag_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $tag = new Ecomtag();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['tag_id','accesstoken','user_id']);
            $result = $tag->where('tag_id', $request->input('tag_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
