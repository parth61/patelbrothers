<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductGallery;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
class EcomProductGalleryController extends Controller
{
    public function addproductgallery(Request $request) {

        //validations
        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
                    "user_id" => "required",
                    "accesstoken" => "required",       
                    "product_id" => "required",
                    "product_image" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $product_id=$request->input('product_id');
            if ($request->has('isfeatured') && $request->input('isfeatured') ==1) {
                $featuredcount = DB::table('tbl_ecom_product_gallery as pg')
                ->where('pg.status', '=', 1)
                ->where('pg.product_id', '=', $product_id)
                ->where('pg.isfeatured', '=', 1)
                ->get();
                if(count($featuredcount)>0)
                {
                    $update = DB::table('tbl_ecom_product_gallery as pg')
                    ->where('pg.product_id', '=', $product_id)
                    ->where('pg.isfeatured', '=', 1)
                        ->update([
                            'status' => 0,
                            'updated_by'=>$request->input('user_id'),
                            'updated_at'=>Carbon::now()->toDateTimeString(),
                            ]);
                            if($update)
                            {
                                $productgallery = new EcomProductGallery();
                                $productgallery->product_image = $request->input('product_image');
                                $productgallery->product_id = $product_id;
                                $productgallery->created_by = $request->input('user_id');
                                $productgallery->isfeatured = 1;
                                $productgallery->site_id = $request->input('site_id');
                                $productgallery->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $productgallery->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $productgallery->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $productgallery->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $result = $productgallery->save();
                                
                                    if($result) {
                                        return response()->json(['status' => 200, 'data' =>$productgallery->id]);
                                    } else {
                                        return response()->json(['status' => 400, 'error' => "Something went wrong.", 400]);
                                    }
                            }
                            else
                            {
                                return response()->json(['status' => 400, 'error' => "Something went wrong product deleting previous entry.", 400]);
                            }
                }
                else
                {
                    $productgallery = new EcomProductGallery();
                    $productgallery->product_image = $request->input('product_image');
                    $productgallery->product_id = $product_id;
                    $productgallery->created_by = $request->input('user_id');
                    $productgallery->isfeatured = 1;
                    $productgallery->site_id = $request->input('site_id');
                    $productgallery->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $productgallery->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $productgallery->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $productgallery->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $productgallery->save();
                        
                    if($result) {
                        return response()->json(['status' => 200, 'data' => $productgallery->id]);
                    } else {
                        return response()->json(['status' => 400, 'error' => "Something went wrong.", 400]);
                    }
                }
     
                
            }
            else
            {
                $productgallery = new EcomProductGallery();
                $productgallery->product_image = $request->input('product_image');
                $productgallery->product_id = $product_id;
                $productgallery->created_by = $request->input('user_id');
                $productgallery->site_id = $request->input('site_id');
                $productgallery->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $productgallery->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $productgallery->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $productgallery->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $productgallery->save();

                if($result) {
                    return response()->json(['status' => 200, 'data' => $productgallery->id]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong.", 400]);
                }
            }      
            
        }
    }
   
    public function getallproductgallery(Request $request) {
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productgallery = new EcomProductGallery();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_gallery_id') && $request->input('product_gallery_id') != "") {
                $data['product_gallery_id'] = $request->input('product_gallery_id');
            }
            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            if ($request->has('isfeatured') && $request->input('isfeatured') != "") {
                $data['isfeatured'] = $request->input('isfeatured');
            }
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }
            $productgallery = $productgallery->getproductgallery($data);
            
            if ($productgallery) {
                $productgallerydata = array();
                foreach ($productgallery as $item) {
                 
   
                    if($item->product_image_media_file==""||$item->product_image_media_file==null||$item->product_image_media_file=="null")
                    {
                        $item->product_image_path =fixedproductgalleryimage;
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath.$item->product_image_media_file)) {
                                    $item->product_image_path = imagedisplaypath.$item->product_image_media_file;
                                } else {
                                    $item->product_image_path =fixedproductgalleryimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath.$item->product_image_media_file)) {
                                $item->product_image_path = imagedisplaypath.$item->product_image_media_file;
                            } else {
                                $item->product_image_path = fixedproductgalleryimage;
                            }
                        }
                    }  
                    array_push($productgallerydata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($productgallery), 'data' => $productgallerydata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    
    public function deleteproductgallery(Request $request) {
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
                    "product_gallery_id" => "required",
                    "updated_by"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $productgallery = new EcomProductGallery();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_gallery_id','user_id','accesstoken']);
            $result = $productgallery->where('product_gallery_id', $request->input('product_gallery_id'))->update($newrequest);
            if ($result) {
               return response()->json(['status' => 200, 'data' => "Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}