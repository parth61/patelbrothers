<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use App\Subscription;

class SubscriptionController extends Controller
{
    
    public function manageSubscription(Request $request) {
        
        $valid = Validator::make($request->all(), [
            // "subscription_name" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $subscription = new Subscription();
            $subscriptiondata = $subscription->where('subscription_id', $request->input('subscription_id'))->count();

            if($subscriptiondata == 0){
                $subscription = new Subscription();
                // $subscription->subscription_name = $request->input('subscription_name');
                $subscription->package_id = $request->input('package_id');
                $subscription->user_id = $request->input('user_id');
                $subscription->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $subscription->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $subscription->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $subscription->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $subscription->save();
            }else{
            $subscription = new Subscription();
            // $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['site_id','subscription_id','accesstoken','user_id']);
            $result = $subscription->where('subscription_id', $request->input('subscription_id'))->update($newrequest);
            }
            
            return response()->json(['status' => 200, 'data' => 'subscription registered.']);
        }
    }

    //Get all admin code
    public function getAllsubscription(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $subscription = new subscription();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if($request->has('subscription_id') && $request->filled('subscription_id')){
                $data['subscription_id'] = $request->input('subscription_id');
            }
            if($request->has('package_id') && $request->filled('package_id')){
                $data['package_id'] = $request->input('package_id');
            }
            if($request->has('user_id') && $request->filled('user_id')){
                $data['user_id'] = $request->input('user_id');
            }
            $subscriptiondata = $subscription->getsubscription($data);
            return response()->json(['status' => 200, 'count' => count($subscriptiondata), 'data' => $subscriptiondata]);
        }
    }
}
