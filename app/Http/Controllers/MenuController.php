<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Menu;
use App\MenuItem;
class MenuController extends Controller
{
    public function addmenu(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "menu_name"=>"required",
            "menulocation_id"=>"required",
           // "menu_menuitems"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $menu = new menu();
            $menu->menu_name = $request->input('menu_name');
          
            $menu->menulocation_id = $request->input('menulocation_id');
            $menu->menu_menuitems = $request->input('menu_menuitems');
            
            $menu->created_by = $request->input('user_id');
            $menu->site_id = $request->input('site_id');
            $menu->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $menu->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $menu->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $menu->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $menu->save();
            if ($result) {
               
                return response()->json(['status' => 200, 'data' => "menu Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallmenu(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menu = new menu();
            $data = array();
          
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('menu_id') && $request->input('menu_id') != "") {
                $data['menu_id'] = $request->input('menu_id');
            }

            if ($request->has('menulocation_id') && $request->input('menulocation_id') != "") {
                $data['menulocation_id'] = $request->input('menulocation_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $menu = $menu->getmenu($data);
            if($menu)
            {
                
                return response()->json(['status' => 200, 'count' => count($menu), 'data' => $menu]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updatemenu(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "menu_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menu = new menu();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['menu_id','user_id','accesstoken']);
            $result = $menu->where('menu_id', $request->input('menu_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "menu Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletemenu(Request $request) {

        $valid = Validator::make($request->all(), [
                    "menu_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menu = new menu();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['menu_id','user_id','accesstoken']);
            $result = $menu->where('menu_id', $request->input('menu_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
