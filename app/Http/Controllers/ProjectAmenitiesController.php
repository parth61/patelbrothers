<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\ProjectAmenities;
use DB;
class ProjectAmenitiesController extends Controller
{
    public function addprojectamenities(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
                    "amenities_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectamenities = new ProjectAmenities();
            $projectamenities->project_id = $request->input('project_id');
            $projectamenities->amenities_id = $request->input('amenities_id');
            $projectamenities->project_amenities_alias = $request->input('project_amenities_alias');
            $projectamenities->created_by = $request->input('created_by');
            $projectamenities->updated_by = $request->input('updated_by');
            $projectamenities->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $projectamenities->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $projectamenities->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $projectamenities->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $projectamenities->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Amenities Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function getallprojectamenities(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectamenities = new ProjectAmenities();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('project_amenities_id') && $request->input('project_amenities_id') != "") {
                $data['project_amenities_id'] = $request->input('project_amenities_id');
            }
            if ($request->has('amenities_id') && $request->input('amenities_id') != "") {
                $data['amenities_id'] = $request->input('amenities_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $projectamenities = $projectamenities->getprojectamenities($data);
            return response()->json(['status' => 200, 'count' => count($projectamenities), 'data' => $projectamenities]);
        }
    }
    
    public function updateprojectamenities(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_amenities_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectamenities = new ProjectAmenities();
            $newrequest = $request->except(['project_amenities_id']);
            $result = $projectamenities->where('project_amenities_id', $request->input('project_amenities_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "projectamenities Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function deleteprojectamenities(Request $request) {
        $valid = Validator::make($request->all(), [
            "project_amenities_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectamenities = new ProjectAmenities();
            $result = $projectamenities->where('project_amenities_id', $request->input('project_amenities_id'))->delete();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
