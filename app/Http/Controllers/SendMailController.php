<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use PDF;
use DB;
use File;
use Illuminate\Support\Facades\Mail;
class SendMailController extends Controller
{
    public function sendmail(Request $request){
        $data=array();
        
        $data["email"]=$request->get("user_email");
        $data["client_name"]=$request->get("user_name");
        $data["subject"]="Download Brochure | CREDAI365.COM";
        $Project = new Project();
        $data1 = array();
     
        $finaldata = array();
        $data1['offset'] = 0;
        $data1['limit'] = 1;

        $data1['project_id'] = $request->get("project_id");

        $Projects = $Project->getproject($data1);
        if (count($Projects) > 0) {

            foreach ($Projects as $item) {
                $projectdata = array();
                $project_id = $item->project_id;

                $item->project_id;

                //code for getting sales-executive info

                $SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
                        ->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
                        ->where('pse.status', '=', 1)
                        ->where('se.sales_executive_status_id', '=', 1)
                        ->where('pse.project_id', '=', $project_id)
                        ->limit(1)
                        ->get();
                        foreach ($SalesExecutive as $item1) {
                            if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
                                $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                            } else {
                                if (does_url_exists(mediapath . '/' . $item1->sales_executive_photo)) {
                                    $item1->sales_executive_photo_path = imagedisplaypath . '/' . $item1->sales_executive_photo;
                                } else {
                                    $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                                }
                            }
                        }
    
                        $item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
                        
                $Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
                                ->where('pp.status', '=', 1)
                                ->where('pp.project_id', '=', $project_id)
                                ->get();

                                $photosarray=array();
                                foreach ($Projectphotos as $item2) {
                                    if ($item2->project_photo == NULL || $item2->project_photo == "") {
                                        $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                    } else {
                                        if (does_url_exists(mediapath . '/' . $item2->project_photo)) {
                                            $item2->project_photo = imagedisplaypath . '/' . $item2->project_photo;
                                        } else {
                                            $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                        }
                                    }
                                    array_push($photosarray,$item2);
                                }
                                $item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

                $Amenities = DB::table('tbl_project_amenities as pa')
                        ->select('a.amenities_name')
                        ->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
                        ->where('pa.status', '=', 1)
                        ->where('pa.project_id', '=', $project_id)
                        ->get();
                       $amenitiesarray=array();
                        foreach ($Amenities as $ame) {
                           array_push($amenitiesarray,$ame->amenities_name);
                        }
                        $item->amenities = (count($Amenities)>0) ? $amenitiesarray : "";
                      
              $Highlight = DB::table('tbl_project_highlights as ph')
                        ->select('h.highlight_name')
                        ->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
                        ->where('ph.status', '=', 1)
                        ->where('ph.project_id', '=', $project_id)
                        ->get();
                        $highlightarray=array();
                        foreach ($Highlight as $high) {
                           array_push($highlightarray,$high->highlight_name);
                        }
                        $item->highlight = (count($Highlight)>0) ? $highlightarray : "";
                      
                $Budget = DB::table('tbl_project_budget as pb')->select(DB::raw("MIN(budget.budget_min_amount) AS minbudget, MAX(budget.budget_max_amount) AS maxbudget"))
                        ->leftJoin('tbl_budget as budget', 'budget.budget_id', '=', 'pb.budget_id')
                        ->where('pb.status', '=', 1)
                        ->where('pb.project_id', '=', $project_id)
                        ->get();

                $Configurations = DB::table('tbl_project_configuration as pc')
                                ->leftJoin('tbl_configuration as conf', 'pc.configuration_id', '=', 'conf.configuration_id')
                                ->where('pc.status', '=', 1)
                                ->where('pc.project_id', '=', $project_id)
                                ->pluck('conf.configuration_name')->implode(',');

                $SizeRange = DB::table('tbl_project_size_range as psr')
                                ->leftJoin('tbl_size_range as sr', 'sr.size_range_id', '=', 'psr.size_range_id')
                                ->where('psr.status', '=', 1)
                                ->where('psr.project_id', '=', $project_id)
                                ->pluck('sr.size_range_name')->implode(',');

                if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                    $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                } else {
                    if (does_url_exists(mediapath . '/' . $item->project_featured_img)) {
                        $item->project_featured_img_path = imagedisplaypath . '/' . $item->project_featured_img;
                    } else {
                        $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                    }
                }

                if ($item->project_logo == NULL || $item->project_logo == "") {
                    $item->project_logo_path = fixedimagepathbuilderlogo;
                } else {
                    if (does_url_exists(mediapath . '/' . $item->project_logo)) {
                        $item->project_logo_path = imagedisplaypath . '/' . $item->project_logo;
                    } else {
                        $item->project_logo_path = fixedimagepathbuilderlogo;
                    }
                }
                $item->configuration_info = ($Configurations != "") ? $Configurations : "";
                $item->size_range_info = ($SizeRange != "") ? $SizeRange : "";
                $item->budget_info = (count($Budget) > 0) ? $Budget[0] : $Budget;
              
               
                
               
                array_push($finaldata, $item);
            }
           
            $data['projectdata']=$finaldata;
             //code for generating pdf and send as attachment
           $data['brochurelink']=  brochurelink."{{$finaldata[0]->project_id}}";
             $pdf = PDF::loadView('emails.brochure', $data);

             try{
                 Mail::send('emails.downloadbrochure', $data, function($message)use($data,$pdf) {
                 $message->to($data["email"], $data["client_name"])
                 ->subject($data["subject"])
                 ->attachData($pdf->output(), "ProjectBrochure.pdf");
                 });
             }catch(JWTException $exception){
                 $this->serverstatuscode = "0";
                 $this->serverstatusdes = $exception->getMessage();
             }
             if (Mail::failures()) {
                  $this->statusdesc  =   "Error sending mail";
                  $this->statuscode  =   "0";
     
             }else{
     
                $this->statusdesc  =   "Message sent Succesfully";
                $this->statuscode  =   "1";
             }
             return response()->json(compact('this'));
           // return view('brochure', $data);
        } else {
            return response()->json(['status' => 200, 'count' => count($Projects), 'data' => $finaldata]);
        }
   
 }
}
