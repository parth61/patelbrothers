<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\ProjectConfiguration;
class ProjectConfigurationController extends Controller
{
    public function addprojectconfiguration(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
                    "configuration_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectConfiguration = new ProjectConfiguration();
            $ProjectConfiguration->project_id = $request->input('project_id');
            $ProjectConfiguration->configuration_id = $request->input('configuration_id');
           
            $ProjectConfiguration->created_by = $request->input('created_by');
            $ProjectConfiguration->updated_by = $request->input('updated_by');
            $ProjectConfiguration->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $ProjectConfiguration->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $ProjectConfiguration->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $ProjectConfiguration->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $ProjectConfiguration->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project Amenities Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function getallprojectconfiguration(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectConfiguration = new ProjectConfiguration();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('project_configuration_id') && $request->input('project_configuration_id') != "") {
                $data['project_configuration_id'] = $request->input('project_configuration_id');
            }
            if ($request->has('configuration_id') && $request->input('configuration_id') != "") {
                $data['configuration_id'] = $request->input('configuration_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $ProjectConfiguration = $ProjectConfiguration->getprojectconfiguration($data);
            return response()->json(['status' => 200, 'count' => count($ProjectConfiguration), 'data' => $ProjectConfiguration]);
        }
    }
    
    public function updateprojectconfiguration(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_configuration_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectConfiguration = new ProjectConfiguration();
            $newrequest = $request->except(['project_configuration_id']);
            $result = $ProjectConfiguration->where('project_configuration_id', $request->input('project_configuration_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "ProjectConfiguration Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function deleteprojectconfiguration(Request $request) {
        $valid = Validator::make($request->all(), [
            "project_configuration_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ProjectConfiguration = new ProjectConfiguration();
            $result = $ProjectConfiguration->where('project_configuration_id', $request->input('project_configuration_id'))->delete();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
