<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Category;

class CategoryController extends Controller
{
    //Add Category Code
    public function addcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "category_name" => "required",
                    "category_colorcode"=>"required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $category = new Category();
            $category->category_name = $request->input('category_name');
            $category->category_colorcode = $request->input('category_colorcode');
            $category->created_by = $request->input('created_by');
            $category->updated_by = $request->input('updated_by');
            $category->status = $request->input('status');
            
            $category->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $category->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $category->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $category->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $category->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Category added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all category code
    public function getallcategory(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $category = new Category();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('category_id') && $request->input('category_id') != "") {
                $data['category_id'] = $request->input('category_id');
            }
            $category = $category->getcategory($data);
            return response()->json(['status' => 200, 'count' => count($category), 'data' => $category]);
        }
    }
    //update category code
    public function updatecategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "category_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $category = new Category();
            $newrequest = $request->except(['category_id']);
              $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $category->where('category_id', $request->input('category_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Category updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //delete category code
    public function deletecategory(Request $request) {
        $valid = Validator::make($request->all(), [
            "category_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $category = new Category();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['category_id']);
              $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $category->where('category_id', $request->input('category_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function managecategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "category_name" => "required",
            "category_colorcode"=>"required",
            "created_by" => "required",
            "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            if($request->has('category_id') && $request->filled('category_id')){
                $category = new Category();
                $newrequest = $request->except(['category_id']);
                $request->request->add(['updated_by' =>  $request->input('updated_by')]);
                $result = $category->where('category_id', $request->input('category_id'))->update($newrequest);
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Category updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $category = new Category();
                $category->category_name = $request->input('category_name');
                $category->category_colorcode = $request->input('category_colorcode');
                $category->created_by = $request->input('created_by');
                $category->updated_by = $request->input('updated_by');
                $category->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $category->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $category->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $category->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $category->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Category added sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
            
        }
    }
}
