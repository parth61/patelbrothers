<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\SeoGlobal;
use DB;
class SeoGlobalController extends Controller
{
    public function addseoglobal(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $seoglobal = new SeoGlobal();
            $seoglobal->seoglobal_sitename = $request->has('seoglobal_sitename')?$request->input('seoglobal_sitename'):null;
            $seoglobal->seoglobal_googlesiteverification = $request->has('seoglobal_googlesiteverification')?$request->input('seoglobal_googlesiteverification'):null;
            $seoglobal->seoglobal_facebook_url = $request->has('seoglobal_facebook_url')?$request->input('seoglobal_facebook_url'):null;
            $seoglobal->seoglobal_twitter_url = $request->has('seoglobal_twitter_url')?$request->input('seoglobal_twitter_url'):null;
            $seoglobal->seoglobal_linkedin_url = $request->has('seoglobal_linkedin_url')?$request->input('seoglobal_linkedin_url'):null;
            $seoglobal->seoglobal_instagram_url = $request->has('seoglobal_instagram_url')?$request->input('seoglobal_instagram_url'):null;
            $seoglobal->seoglobal_facebook_pixelcode = $request->has('seoglobal_facebook_pixelcode')?$request->input('seoglobal_facebook_pixelcode'):null;
            $seoglobal->seoglobal_youtube_url = $request->has('seoglobal_youtube_url')?$request->input('seoglobal_youtube_url'):null;
            $seoglobal->created_by = $request->input('user_id');
            $seoglobal->updated_by = $request->input('updated_by');
            $seoglobal->site_id = $request->input('site_id');
            $seoglobal->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $seoglobal->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $seoglobal->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $seoglobal->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $seoglobal->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "SEO Global  Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallseoglobal(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seoglobal = new SeoGlobal();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('seoglobal_id') && $request->input('seoglobal_id') != "") {
                $data['seoglobal_id'] = $request->input('seoglobal_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $seoglobal = $seoglobal->getseoglobal($data);
            if($seoglobal)
            {
                
                return response()->json(['status' => 200, 'count' => count($seoglobal), 'data' => $seoglobal]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updateseoglobal(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "seoglobal_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seoglobal = new SeoGlobal();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['seoglobal_id','user_id','accesstoken']);
            $result = $seoglobal->where('seoglobal_id', $request->input('seoglobal_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "SEO Global  Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteseoglobal(Request $request) {

        $valid = Validator::make($request->all(), [
                    "seoglobal_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seoglobal = new SeoGlobal();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['seoglobal_id','user_id','accesstoken']);
            $result = $seoglobal->where('seoglobal_id', $request->input('seoglobal_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "SEO Global Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

    public function manageseoglobal(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $query = DB::table('tbl_seoglobal as seoglobal')
                    ->select('seoglobal.*')->where('seoglobal.site_id', '=' ,$request->input('site_id'))->where('seoglobal.status', '=' ,1)->get();
            if(count($query)>0)
            {
                    $seoglobal = new SeoGlobal();
                    $request->request->add(['updated_by' =>  $request->input('user_id')]);
                    $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                    $newrequest = $request->except(['seoglobal_id','user_id','accesstoken']);
                    $result = $seoglobal->where('seoglobal_id', $request->input('seoglobal_id'))->update($newrequest);

                    if ($result) {
                        return response()->json(['status' => 200, 'data' => "SEO Global  Updated succesfully"]);
                    } else {
                        return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                    }
            }
            else
            {
                $seoglobal = new SeoGlobal();
                $seoglobal->seoglobal_sitename = $request->has('seoglobal_sitename')?$request->input('seoglobal_sitename'):null;
                $seoglobal->seoglobal_googlesiteverification = $request->has('seoglobal_googlesiteverification')?$request->input('seoglobal_googlesiteverification'):null;
                $seoglobal->seoglobal_facebook_url = $request->has('seoglobal_facebook_url')?$request->input('seoglobal_facebook_url'):null;
                $seoglobal->seoglobal_twitter_url = $request->has('seoglobal_twitter_url')?$request->input('seoglobal_twitter_url'):null;
                $seoglobal->seoglobal_linkedin_url = $request->has('seoglobal_linkedin_url')?$request->input('seoglobal_linkedin_url'):null;
                $seoglobal->seoglobal_instagram_url = $request->has('seoglobal_instagram_url')?$request->input('seoglobal_instagram_url'):null;
                $seoglobal->seoglobal_youtube_url = $request->has('seoglobal_youtube_url')?$request->input('seoglobal_youtube_url'):null;
                $seoglobal->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $seoglobal->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $seoglobal->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $seoglobal->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $seoglobal->site_id = $request->has('site_id')?$request->input('site_id'):null;
                $result = $seoglobal->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "SEO Global  Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
       
    }
    }
}
