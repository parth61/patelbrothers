<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomShippingClass;
use App\Slug;
class EcomShippingClassController extends Controller
{
    public function addshippingclass(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "shipping_class_name" => "required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingclasss = new EcomShippingClass();
            $shippingclasss->shipping_class_name = $request->input('shipping_class_name');
     
            if ($request->has('shipping_class_slug') && $request->input('shipping_class_slug') != "")
            {
                $slug_name =$request->input('shipping_class_slug');
            }
            else
            {
                $slug_name =$request->input('shipping_class_name');
            }

            $slug_name=checkslugavailability($slug_name, "productshippingclass",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="productshippingclass";
            $slug->site_id =$request->input('site_id');
            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $shippingclasss->slug_id = $slug_id;*/
            $shippingclasss->slug_name = $slug_name;
            $shippingclasss->site_id = $request->input('site_id');
            $shippingclasss->shipping_class_description = $request->input('shipping_class_description');
            $shippingclasss->created_by = $request->input('user_id');
            $shippingclasss->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $shippingclasss->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $shippingclasss->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $shippingclasss->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $shippingclasss->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "shippingclass Added Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallshippingclass(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingclass = new EcomShippingClass();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('shipping_class_id') && $request->input('shipping_class_id') != "") {
                $data['shipping_class_id'] = $request->input('shipping_class_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }

            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            
            $shippingclass = $shippingclass->getshippingclass($data);

            return response()->json(['status' => 200, 'count' => count($shippingclass), 'data' => $shippingclass]);
        }

    }

    public function updateshippingclass(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "shipping_class_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingclass = new EcomShippingClass();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['slug_id','shipping_class_id','accesstoken','user_id','slug_name']);

            if($request->has('slug_name')){
                $shippingclass = $shippingclass->where('shipping_class_id', $request->input('shipping_class_id'))->first();
                if($shippingclass->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "productshippingclass", $request->input('site_id'));
                }
            }

            $result = $shippingclass->where('shipping_class_id', $request->input('shipping_class_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "shippingclass Updated successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteshippingclass(Request $request) {

        $valid = Validator::make($request->all(), [
                    "shipping_class_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $shippingclass = new EcomShippingClass();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['shipping_class_id','accesstoken','user_id']);
            $result = $shippingclass->where('shipping_class_id', $request->input('shipping_class_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "shippingclass Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
