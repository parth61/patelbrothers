<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomRelatedProduct;
use Carbon\Carbon;
use App\EcomProductGallery;
use Illuminate\Support\Facades\File;
class EcomRelatedProductController extends Controller
{
    public function addrelatedproduct(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required",
            "related_id" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->input('product_id') != $request->input('related_id')){
                $relatedproduct = new EcomRelatedProduct();
                $checkcount = count($relatedproduct->where('related_id', $request->input('related_id'))->where('product_id', $request->input('product_id'))->where('status', '1')->get());
                if ($checkcount === 0) {
                $relatedproduct->product_id = $request->input('product_id');
                $relatedproduct->related_id = $request->input('related_id');
                $relatedproduct->created_by = $request->input('user_id');
                $relatedproduct->site_id = $request->input('site_id');
                $relatedproduct->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $relatedproduct->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $relatedproduct->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $relatedproduct->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $relatedproduct->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "relatedproduct Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                return response()->json(['status' => 400, 'error' => "Product and Related Product should not be same."], 400);
            }
        }
        else
        {
           return response()->json(['status' => 400, 'error' => "Already Exist."], 400);
        }
        }
    }

    public function getallrelatedproduct(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relatedproduct = new EcomRelatedProduct();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('related_product_id') && $request->input('related_product_id') != "") {
                $data['related_product_id'] = $request->input('related_product_id');
            }

            if ($request->has('related_id') && $request->input('related_id') != "") {
                $data['related_id'] = $request->input('related_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            $relatedproduct = $relatedproduct->getrelatedproduct($data);
  
                foreach ($relatedproduct as $item) {
                    //call for getting gallery image with product
                    $productgallery = new EcomProductGallery();
                    $data['product_id']=$item->product_id;
                    $data['isfeatured']=1;
                    $productgallery = $productgallery->getproductgallery($data);
                    $productgallerydata=array();
                    foreach ($productgallery as $item1) {
                    
                        if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                        {
                            $item1->related_product_image_path =fixedproductgalleryimage;
                            
                        }
                        else
                        {
                            if(fileuploadtype=="local")
                            {
                                    if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                        $item1->related_product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                    } else {
                                        $item1->related_product_image_path =fixedproductgalleryimage;
                                    }
                                
                            }
                            else
                            {
                                if (does_url_exists(imagedisplaypath.'/'. $item1->product_image_media_file)) {
                                    $item1->related_product_image_path = imagedisplaypath.'/'. $item1->product_image_media_file;
                                } else {
                                    $item1->related_product_image_path = fixedproductgalleryimage;
                                }
                            }
                        }  

                        
                        array_push($productgallerydata, $item1);
                    }
                    $item->product_gallery=$productgallerydata;

        }
            return response()->json(['status' => 200, 'count' => count($relatedproduct), 'data' => $relatedproduct]);
        }

    }

    public function updaterelatedproduct(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "related_product_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relatedproduct = new EcomRelatedProduct();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['related_product_id','user_id','accesstoken']);
            $result = $relatedproduct->where('related_product_id', $request->input('related_product_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "relatedproduct Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleterelatedproduct(Request $request) {

        $valid = Validator::make($request->all(), [
                    "related_product_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $relatedproduct = new EcomRelatedProduct();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['related_product_id','user_id','accesstoken']);
            $result = $relatedproduct->where('related_product_id', $request->input('related_product_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' =>"Related Product Deleted"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }


   
}
