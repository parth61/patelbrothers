<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\GeneralSetting;
use DB;
class GeneralSettingController extends Controller
{
    
    public function addgeneralsetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "site_title"=>"required",
            "tag_line"=>"required",
            "time_zone"=>"required",
            "date_format"=>"required",
            "search_engine_visibility"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $generalsetting = new Generalsetting();
            $generalsetting->site_title = $request->input('site_title');
            $generalsetting->tag_line = $request->input('tag_line');
            $generalsetting->time_zone = $request->input('time_zone');
            $generalsetting->date_format = $request->input('date_format');
            $generalsetting->search_engine_visibility = $request->input('search_engine_visibility');
            $generalsetting->site_id = $request->input('user_id');
            $generalsetting->site_id = $request->input('site_id');
            $generalsetting->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $generalsetting->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $generalsetting->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $generalsetting->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $generalsetting->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "generalsetting Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallgeneralsetting(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $generalsetting = new Generalsetting();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('generalsetting_id') && $request->input('generalsetting_id') != "") {
                $data['generalsetting_id'] = $request->input('generalsetting_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $generalsetting = $generalsetting->getgeneralsetting($data);
            if($generalsetting)
            {
                
                return response()->json(['status' => 200, 'count' => count($generalsetting), 'data' => $generalsetting]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updategeneralsetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "generalsetting_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $generalsetting = new Generalsetting();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['generalsetting_id','user_id','accesstoken']);
            $result = $generalsetting->where('generalsetting_id', $request->input('generalsetting_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "generalsetting Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletegeneralsetting(Request $request) {

        $valid = Validator::make($request->all(), [
                    "generalsetting_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $generalsetting = new Generalsetting();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['generalsetting_id','user_id','accesstoken']);
            $result = $generalsetting->where('generalsetting_id', $request->input('generalsetting_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "General Setting Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
    public function managegeneralsetting(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $query = DB::table('tbl_generalsetting as gs')
                    ->select('gs.*')->where('gs.site_id', '=' ,$request->input('site_id'))->where('gs.status', '=' ,1)->get();
            if(count($query)>0)
            {
                $generalsetting = new Generalsetting();
                $request->request->add(['updated_by' =>  $request->input('user_id')]);
                $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                $newrequest = $request->except(['generalsetting_id','user_id','accesstoken']);
                $result = $generalsetting->where('generalsetting_id', $request->input('generalsetting_id'))->update($newrequest);
    
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "generalsetting Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
            else
            {
                $generalsetting = new Generalsetting();
                $generalsetting->site_title = $request->input('site_title');
                $generalsetting->tag_line = $request->input('tag_line');
                $generalsetting->time_zone = $request->input('time_zone');
                $generalsetting->date_format = $request->input('date_format');
                $generalsetting->search_engine_visibility = $request->input('search_engine_visibility');
                $generalsetting->created_by = $request->input('user_id');
                $generalsetting->site_id = $request->input('site_id');
                $generalsetting->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $generalsetting->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $generalsetting->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $generalsetting->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $generalsetting->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "generalsetting Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
       
    }
    }
    

}
