<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\MenuLocation;
class MenuLocationController extends Controller
{
    public function addmenulocation(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "menulocation_name"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $menulocation = new menulocation();
            $menulocation->menulocation_name = $request->input('menulocation_name');
            $menulocation->created_by = $request->input('user_id');
            $menulocation->site_id = $request->input('site_id');
            $menulocation->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $menulocation->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $menulocation->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $menulocation->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $menulocation->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "menulocation Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallmenulocation(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menulocation = new menulocation();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('menulocation_id') && $request->input('menulocation_id') != "") {
                $data['menulocation_id'] = $request->input('menulocation_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $menulocation = $menulocation->getmenulocation($data);
            if($menulocation)
            {
                
                return response()->json(['status' => 200, 'count' => count($menulocation), 'data' => $menulocation]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updatemenulocation(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "menulocation_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menulocation = new menulocation();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['menulocation_id','user_id','accesstoken']);
            $result = $menulocation->where('menulocation_id', $request->input('menulocation_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "menulocation Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletemenulocation(Request $request) {

        $valid = Validator::make($request->all(), [
                    "menulocation_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menulocation = new menulocation();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['menulocation_id','user_id','accesstoken']);
            $result = $menulocation->where('menulocation_id', $request->input('menulocation_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
