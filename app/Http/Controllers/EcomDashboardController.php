<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Validator;
use DB;
use App\EcomOrder;
use App\EcomProduct;
use App\EcomPayments;
use App\EcomOrderItems;
use App\EcomTax;
use \stdClass;
use App\EcomProductGallery;

class EcomDashboardController extends Controller
{

	public function getDashboardStats(Request $request) {

		$valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

        	$ecomproduct = new EcomProduct();
        	$ecomorder = new EcomOrder();
        	$ecompayment = new EcomPayments();
        	$ecomtax = new EcomTax();

			$finaldata = array(); 

		    /* Load Dashboard Stats */

		    if($request->input('site_id')=="0"){
		    	$finaldata['totalproducts'] = $ecomproduct->where('status', 1)->where('site_id', $request->input('site_id'))->count();
         		$finaldata['totalorders'] = $ecomorder->where('status', 1)->whereNotIn('order_status', ['pending','onhold','failed','cancelled'])->count();
		    }else{
		    	$finaldata['totalproducts'] = $ecomproduct->where('status', 1)->where('site_id', $request->input('site_id'))->count();
          		$finaldata['totalorders'] = $ecomorder->where('status', 1)->whereNotIn('order_status', ['pending','onhold','failed','cancelled'])->where('site_id', $request->input('site_id'))->count();
		    }

		    $query = "";
		    $query = DB::table('tbl_ecom_order as tec')->select(DB::raw('SUM(order_amount) AS nettotal'),DB::raw('SUM(order_tax) AS taxtotal'),DB::raw('SUM(order_grandtotal) AS grosstotal'))->where('status', 1)->whereNotIn('tec.site_id', ['pending','failed','cancelled']);
		    if ($request->has('site_id') && $request->input('site_id') != 0) {
	            $query = $query->where('tec.site_id', '=' , $request->input('site_id'));
	        }
	    	$finaldata['stats'] = $query->get();

	    	/* End Load Dashboard Stats */

	    	/* Get All Low Threshold Product */

		    if($request->input('site_id')=="0"){
		    	$finaldata['lowthresholdproduct'] = $ecomproduct->where('is_stockmanagement', 1)->whereRaw('product_stock <  low_stock_threshold')->where('status', 1)->get();
		    }else{
		    	$finaldata['lowthresholdproduct'] = $ecomproduct->where('site_id', $request->input('site_id'))->whereRaw('product_stock < low_stock_threshold')->where('status', 1)->get();
		    }

		    $productdata = array();
            foreach($finaldata['lowthresholdproduct'] as $item)
            {
            	$product_id = $item->product_id;

            	$productgallery = new EcomProductGallery();
                $data['product_id']=$product_id;
				$data['offset']=0;
				$data['limit']=1;
                $productgallery = $productgallery->getproductgallery($data);
                $productgallerydata=array();
                foreach ($productgallery as $item1) {
                 
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                    $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                                } else {
                                    $item1->product_image_path =fixedproductgalleryimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                            } else {
                                $item1->product_image_path = fixedproductgalleryimage;
                            }
                        }
                    }  

                   
                    array_push($productgallerydata, $item1);
                }
                
                $item->product_gallery=$productgallerydata;
            }

	    	/* End Get All Low Threshold Product */

			/* Get Productwise Selling */

	    	if($request->input('site_id')=="0"){
		    	$finaldata['highestsellingproduct'] = DB::table('tbl_ecom_order_items as orditem')->select('pro.product_id','pro.product_name','pro.product_guid','pro.product_name', DB::raw('SUM(orditem.order_product_quantity) AS totalsales'))
		    	->leftJoin('tbl_ecom_order as eo','eo.order_id','=','orditem.order_id')
		    	->leftJoin('tbl_ecom_product as pro','pro.product_id','=','orditem.product_id')
		    	->whereNotIn('eo.order_status', ['pending','onhold','failed','cancelled'])
		    	->where('orditem.status', 1)->where('pro.status', 1)->where('eo.status', 1)->limit(20)->groupby('orditem.product_id')->orderby('totalsales', 'DESC')->get();
		    }else{
		    	$finaldata['highestsellingproduct'] = DB::table('tbl_ecom_order_items as orditem')->select('pro.product_id','pro.product_name', 'pro.product_guid',DB::raw('SUM(orditem.order_product_quantity) AS totalsales'))
		    	->leftJoin('tbl_ecom_order as eo','eo.order_id','=','orditem.order_id')
		    	->leftJoin('tbl_ecom_product as pro','pro.product_id','=','orditem.product_id')
		    	->whereNotIn('eo.order_status', ['pending','onhold','failed','cancelled'])->where('eo.site_id', $request->input('site_id'))
		    	->where('orditem.status', 1)->where('pro.status', 1)->where('eo.status', 1)->limit(20)->groupby('pro.product_id')->orderby('totalsales', 'DESC')->get();
		    }

		    	/* Get Image of product */

		    	foreach($finaldata['highestsellingproduct'] as $productitem){
		    		$productgallery = new EcomProductGallery();
	                $data['product_id']=$productitem->product_id;
	                $data['offset'] = 0;
	                $data['limit'] = 1000;
	                $productgallery = $productgallery->getproductgallery($data);
	                $productgallerydata=array();
	                foreach ($productgallery as $item1) {
	                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
	                    {
	                        $item1->product_image_path =fixedproductgalleryimage;
	                       
	                    }
	                    else
	                    {
	                        if(fileuploadtype=="local")
	                        {
	                              if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
	                                    $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
	                                } else {
	                                    $item1->product_image_path =fixedproductgalleryimage;
	                                }
	                            
	                        }
	                        else
	                        {
	                            if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
	                                $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
	                            } else {
	                                $item1->product_image_path = fixedproductgalleryimage;
	                            }
	                        }
	                    }  
	                    array_push($productgallerydata, $item1);
	                }
	                $productitem->product_gallery = $productgallerydata;
		    	}

		    /* End Productwise Selling */

		    /* Get Order by Status */

		    $finaldata['orderstats'] = $ecomorder->select('order_status', DB::raw('COUNT(order_id) AS totalorders'))->where('status', 1)->groupby('order_status')->get();

		    /* End Get Order by Status */

		    return response()->json(['status' => 200, 'data' => $finaldata]);
        }

    }

    public function getDaywiseOrderStats(Request $request) {

		$valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

        	$fromdate = $request->has('from_date') ? $request->input('from_date') : date("Y-m-d");
        	$todate = $request->input('to_date')? $request->input('to_date') : date("Y-m-d");
        	$duration = $request->input('duration');

			$orderdata = array();
			$finaldata = array();

			$netamount = array(); $taxamount = array(); $grossamount = array();$orderdates = array();

	        if($duration == "year"){

	        	$yearlist = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

	        	$query = DB::table('tbl_ecom_order as tec')->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw('SUM(order_amount) AS nettotal'), DB::raw('SUM(tec.order_tax) AS taxtotal'), DB::raw('SUM(tec.order_grandtotal) AS grosstotal'))->where('tec.status', 1)->whereNotIn('tec.site_id', ['pending','failed','cancelled'])->whereYear('created_at', date('Y'));

			    if ($request->has('site_id') && $request->input('site_id') != 0) {
		            $query = $query->where('tec.site_id', '=' , $request->input('site_id'));
		        }

		        $orderdata = $query->groupby('month')->get();

	        	for($i=0; $i<12; $i++){
	        		$isfound = 0;
	        		foreach($orderdata as $item){
	        			if($yearlist[$i] == $item->month){
	        				array_push($netamount, sprintf('%.2f', $item->nettotal));
				    		array_push($taxamount, sprintf('%.2f', $item->taxtotal));
				    		array_push($grossamount, sprintf('%.2f', $item->grosstotal));
		                    array_push($orderdates, $item->month);
		                    $isfound = 1;
	        			}
			    	}
			    	if($isfound===0){ 
		            	array_push($netamount, sprintf('%.2f', 0));
			    		array_push($taxamount, sprintf('%.2f', 0));
			    		array_push($grossamount, sprintf('%.2f', 0));
			    		array_push($orderdates, $yearlist[$i]);
		            }
		            
	        	}
		        
		        
	        }else{

	        	$query = DB::table('tbl_ecom_order as tec')->select(DB::raw('DATE(tec.created_at) AS orderdate'), DB::raw('SUM(order_amount) AS nettotal'), DB::raw('SUM(tec.order_tax) AS taxtotal'), DB::raw('SUM(tec.order_grandtotal) AS grosstotal'))->where('tec.status', 1)->whereNotIn('tec.site_id', ['pending','failed','cancelled']);

			    if ($request->has('site_id') && $request->input('site_id') != 0) {
		            $query = $query->where('tec.site_id', '=' , $request->input('site_id'));
		        }

	        	if($request->has('from_date') && $request->has('to_date') && $request->filled('to_date') && $request->filled('to_date')) {
		            $query = $query->whereBetween(DB::raw('DATE(tec.created_at)'), array($request->input('from_date'), $request->input('to_date')));
		        }

		        $orderdata = $query->groupby('orderdate')->get();

		        while (strtotime($fromdate) <= strtotime($todate)) {
		    		$isfound = 0;            
		            foreach($orderdata as $item){
		            	if($item->orderdate == $fromdate){
		                    array_push($netamount, sprintf('%.2f', $item->nettotal));
				    		array_push($taxamount, sprintf('%.2f', $item->taxtotal));
				    		array_push($grossamount, sprintf('%.2f', $item->grosstotal));
		                    $isfound = 1;
		                }
			    	}
		            if($isfound===0){ 
		            	array_push($netamount, sprintf('%.2f', 0));
			    		array_push($taxamount, sprintf('%.2f', 0));
			    		array_push($grossamount, sprintf('%.2f', 0));
		            }
		            array_push($orderdates, $fromdate);
			    	$fromdate = date ("Y-m-d", strtotime("+1 day", strtotime($fromdate)));
		        }
	        }

	    	$orderdataset = array();


            $orderinfo =  new stdClass();
            $orderinfo->name = "Gross Amount";
            $orderinfo->data = $grossamount;
            array_push($orderdataset, $orderinfo);

	    	$orderinfo =  new stdClass();
            $orderinfo->name = "Net Amount";
            $orderinfo->data = $netamount;
            array_push($orderdataset, $orderinfo);

            $orderinfo =  new stdClass();
            $orderinfo->name = "Tax";
            $orderinfo->data = $taxamount;
            array_push($orderdataset, $orderinfo);



	    	$finaldata['orderdates'] = $orderdates;
	    	$finaldata['orderdata'] = $orderdataset;

		    return response()->json(['status' => 200, 'data' => $finaldata]);
        }

    }

}