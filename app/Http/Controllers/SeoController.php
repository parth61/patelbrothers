<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Seo;
use DB;
class SeoController extends Controller
{
    public function manageseo(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "seo_title"=>"required",
                "seo_for"=>"required",
                "seo_for_id"=>"required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $query = DB::table('tbl_seo as seo')
                    ->select('seo.*')
                    ->where('seo.seo_for_id', '=' ,$request->input('seo_for_id'))
                    ->where('seo.seo_for', '=' ,$request->input('seo_for'))->where('seo.status', '=' ,1)->get();
            if(count($query)>0)
            {
                //code for updating seo
                $seo_id=$query[0]->seo_id;
                $seo = new Seo();
                $request->request->add(['updated_by' =>  $request->input('user_id')]);
                $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                $newrequest = $request->except(['seo_for','seo_for_id','seo_id','user_id','accesstoken']);
                $result = $seo->where('seo_id', $seo_id)->update($newrequest);
    
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "seo Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
            else
            {

                    //code for addding seo
                        $seo = new Seo();
                        $seo->seo_title = $request->input('seo_title');
                        $seo->seo_keywords = $request->input('seo_keywords');
                        $seo->seo_description = $request->input('seo_description');
                        $seo->seo_image = $request->input('seo_image');
                        $seo->seo_type = $request->input('seo_type');
                        $seo->seo_canonical = $request->input('seo_canonical');
                        $seo->created_by = $request->input('user_id');
                        $seo->seo_for_id = $request->input('seo_for_id');
                        $seo->seo_for = $request->input('seo_for');
                        $seo->site_id = $request->input('site_id');
                        $seo->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $seo->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $seo->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $seo->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $result = $seo->save();
                        if ($result) {
                            if($request->input('seo_for')=="page")
                            {
                                DB::table('tbl_page')
                                ->where('page_id', $request->input('seo_for_id')) 
                                ->limit(1)  
                                ->update(array('seo_id' => $seo->id));
                            }
                            elseif($request->input('seo_for')=="post")
                            {
                                DB::table('tbl_post')
                                ->where('post_id',$request->input('seo_for_id'))  
                                ->limit(1)  
                                ->update(array('seo_id' => $seo->id));
                            }
                            elseif($request->input('seo_for')=="posttag")
                            {
                                DB::table('tbl_posttag')
                                ->where('posttag_id',$request->input('seo_for_id'))  
                                ->limit(1)  
                                ->update(array('seo_id' => $seo->id));
                            }
                            elseif($request->input('seo_for')=="postcategory")
                            {
                                DB::table('tbl_postcategory')
                                ->where('postcategory_id',$request->input('seo_for_id'))  
                                ->limit(1)  
                                ->update(array('seo_id' => $seo->id));
                            }
                            elseif($request->input('seo_for')=="product")
                            {
                                DB::table('tbl_ecom_product')
                                ->where('product_id',$request->input('seo_for_id'))  
                                ->limit(1)  
                                ->update(array('seo_id' => $seo->id));
                            }
                            elseif($request->input('seo_for')=="productcategory")
                            {
                                DB::table('tbl_ecom_category')
                                ->where('category_id',$request->input('seo_for_id'))  
                                ->limit(1)  
                                ->update(array('seo_id' => $seo->id));
                            }
                            return response()->json(['status' => 200, 'data' => "seo Added Sucessfully"]);
                        } else {
                            return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                        }
                }


        }
    }

    public function getallseo(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seo = new Seo();
            $data = array();
           
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('seo_id') && $request->input('seo_id') != "") {
                $data['seo_id'] = $request->input('seo_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('seo_for_id') && $request->input('seo_for_id') != "") {
                $data['seo_for_id'] = $request->input('seo_for_id');
            }

            if ($request->has('seo_for') && $request->input('seo_for') != "") {
                $data['seo_for'] = $request->input('seo_for');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $seo = $seo->getseo($data);
            if($seo)
            {
                $seodata = array();
                foreach ($seo as $item) {
                    if (does_url_exists(imagedisplaypath.$item->media_file)) {
                        $item->seo_image_path = imagedisplaypath.$item->media_file;
                    } else {
                        $item->seo_image_path = fixedseofeaturedimage;
                    }
                    array_push($seodata, $item);
                }
                return response()->json(['status' => 200, 'count' => count($seo), 'data' => $seodata]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updateseo(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "seo_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seo = new Seo();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['seo_id','user_id','accesstoken']);
            $result = $seo->where('seo_id', $request->input('seo_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "seo Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteseo(Request $request) {

        $valid = Validator::make($request->all(), [
                    "seo_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seo = new Seo();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['seo_id','user_id','accesstoken']);
            $result = $seo->where('seo_id', $request->input('seo_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
