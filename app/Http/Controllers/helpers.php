<?php

use Illuminate\Support\Facades\File;
use App\Slim;
use App\Project;
use App\ProjectAmenities;
use App\ProjectCertificate;
use App\ProjectHighlight;
use App\ProjectSizeRange;
use App\ProjectPhoto;
use App\ProjectBudget;
use App\ProjectConfiguration;
use App\Banks;
use App\ProjectBank;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

if (!function_exists('registrationEmail')) {
  function round_up( $value, $precision ) { 
      $pow = pow ( 10, $precision ); 
      return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
  } 
}
/* Send Registration Email */
if (!function_exists('registrationEmail')) {
  function registrationEmail($content, $email) {

    /* Set business logo */
    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_dark_logo==""||$content["businessinfo"]->business_dark_logo==null||$content["businessinfo"]->business_dark_logo=="null")
        {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_dark_logo)) {
                $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
            } else {
                $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_dark_logo)) {
            $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
        } else {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        } 
    }

    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_light_logo==""||$content["businessinfo"]->business_light_logo==null||$content["businessinfo"]->business_light_logo=="null")
        {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_light_logo)) {
                $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
            } else {
                $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_light_logo)) {
            $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
        } else {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        } 
    }

    $emailcontent = $content;
    $userinfo = $emailcontent['userinfo'];
    $businessinfo = $emailcontent['businessinfo'];
    $subject = "Thanks for Registration | ".$businessinfo->business_name;
    $name = $userinfo->user_firstname." ".$userinfo->user_lastname;
	
  
try{
     Mail::send('ecom_emails.registeremail', $emailcontent, function($message) use ($email, $name, $subject) 
    {
        $message->to($email, $name)->subject($subject);
    });
}
catch(\Exception $e){
	return false;
}

 return true;
  }
}

/* Send Forgot Password Email */
if (!function_exists('forgotPasswordEmail')) {
  function forgotPasswordEmail($content, $email) {

    /* Set business logo */
    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_dark_logo==""||$content["businessinfo"]->business_dark_logo==null||$content["businessinfo"]->business_dark_logo=="null")
        {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_dark_logo)) {
                $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
            } else {
                $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_dark_logo)) {
            $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
        } else {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        } 
    }

    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_light_logo==""||$content["businessinfo"]->business_light_logo==null||$content["businessinfo"]->business_light_logo=="null")
        {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_light_logo)) {
                $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
            } else {
                $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_light_logo)) {
            $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
        } else {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        } 
    }
      
    $emailcontent = $content;
    $userinfo = $emailcontent['userinfo'];
    $businessinfo = $emailcontent['businessinfo'];
    
    $subject = "Reset Your Password | ".$businessinfo->business_name;
    $name = $userinfo->user_firstname." ".$userinfo->user_lastname;

    Mail::send('ecom_emails.forgotpassword', $emailcontent, function($message) use ($email, $name, $subject) 
    {
        $message->to($email, $name)->subject($subject);
    });
       
    return true;
  }
}

/* Send Forgot Password Email */
if (!function_exists('orderConfirmEmail')) {
  function orderConfirmEmail($content, $email) {

    /* Set business logo */
    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_dark_logo==""||$content["businessinfo"]->business_dark_logo==null||$content["businessinfo"]->business_dark_logo=="null")
        {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_dark_logo)) {
                $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
            } else {
                $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_dark_logo)) {
            $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
        } else {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        } 
    }

    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_light_logo==""||$content["businessinfo"]->business_light_logo==null||$content["businessinfo"]->business_light_logo=="null")
        {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_light_logo)) {
                $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
            } else {
                $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_light_logo)) {
            $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
        } else {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        } 
    }
      
    $emailcontent = $content;
    $userinfo = $emailcontent['userinfo'];
    $businessinfo = $emailcontent['businessinfo'];
    $orderinfo = $emailcontent['order'];
    $orderdetail = $emailcontent['orderdetail'];
    $orderitems = $emailcontent['orderitems'];
    $billingaddress = $emailcontent['billingaddress'];
    $shippingaddress = $emailcontent['shippingaddress'];

    $subject = "Order Placed Successfully | ".$businessinfo->business_name;
    $name = $userinfo->user_firstname." ".$userinfo->user_lastname;

    Mail::send('ecom_emails.orderemail', $emailcontent, function($message) use ($email, $name, $subject) 
    {
        $message->to($email, $name)->subject($subject);
    });
       
    return true;
  }
}


/* Send Forgot Password Email */
if (!function_exists('sendEmailInvoice')) {
  function sendEmailInvoice($content, $email) {

    /* Set business logo */
    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_dark_logo==""||$content["businessinfo"]->business_dark_logo==null||$content["businessinfo"]->business_dark_logo=="null")
        {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_dark_logo)) {
                $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
            } else {
                $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_dark_logo)) {
            $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
        } else {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        } 
    }

    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_light_logo==""||$content["businessinfo"]->business_light_logo==null||$content["businessinfo"]->business_light_logo=="null")
        {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_light_logo)) {
                $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
            } else {
                $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_light_logo)) {
            $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
        } else {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        } 
    }
      
    $emailcontent = $content;
    $invoicepath = $emailcontent['downloadinvoice'];
    $subject = "Order Invoice : ".$emailcontent['order']->order_id." | ".$emailcontent['businessinfo']->business_name;
    $name = $emailcontent['userinfo']->user_firstname." ".$emailcontent['userinfo']->user_lastname;
    $invoicefilename =$emailcontent['businessinfo']->business_name."-".$emailcontent['order']->order_id.".pdf";

    Mail::send('ecom_emails.emailinvoice', $emailcontent, function($message) use ($email, $name, $subject, $invoicepath, $invoicefilename) 
    {
        $message->to($email, $name)->subject($subject);
        $message->attach($invoicepath,[
                              'as' => $invoicefilename,
                              'mime' => 'application/pdf'
                            ]);
    });
       
    return true;
  }
}


//for making all combination from arrays attributes 
function combinations($arrays, $i = 0) {
  if (!isset($arrays[$i])) {
      return array();
  }
  if ($i == count($arrays) - 1) {
      return $arrays[$i];
  }

  // get combinations from subsequent arrays
  $tmp = combinations($arrays, $i + 1);

  $result = array();

  // concat each array from tmp with each element from $arrays[$i]
  foreach ($arrays[$i] as $v) {
      foreach ($tmp as $t) {
          $result[] = is_array($t) ? 
              array_merge(array($v), $t) :
              array($v, $t);
      }
  }

  return $result;
}
//code for converting .jpg/.png to webp
if (!function_exists('convert_to_webp')) {
  function convert_to_webp($filepath, $name) {
    $webp = $filepath;
    $im = imagecreatefromstring(file_get_contents($webp));  
    $new_webp = preg_replace('"\.(jpg|jpeg|png|webp)$"', '.webp', $webp);
    imagewebp($im, $new_webp, 50);
    if(file_exists($new_webp)){
        $name = preg_replace('"\.(jpg|jpeg|png|webp)$"', '.webp', $name);
        $fullpath = 'media/'.date("Y") . "/" . date('m').'/'.$name;
        if(fileuploadtype!="local")
        {
            $result = Storage::disk('s3')->put($fullpath, file_get_contents($new_webp), 'public');
            unlink($filepath);
            unlink($new_webp);
        }
    }
    return true;
  }
}

//code for checking does url exist for dispalying images
if (!function_exists('does_url_exists')) {
  function does_url_exists($url) {
    $headers=get_headers($url);
    return stripos($headers[0],"200 OK")?true:false;
  }
}

/* Check slugn in the table */
if (!function_exists('checkslugavailability')) {
  function checkslugavailability($slug_names, $slug_type, $site_id) {

      /* Generate Slug */
      $slug_name= Str::slug($slug_names);

      /* Get the Table Name */
      $table_name = "";
      if($slug_type == "productattribute"){ $table_name = "tbl_ecom_attribute"; }
      if($slug_type == "productattributeterm"){ $table_name = "tbl_ecom_attribute_term"; }
      if($slug_type == "productcategory"){ $table_name = "tbl_ecom_category"; }
      if($slug_type == "product"){ $table_name = "tbl_ecom_product"; }
      if($slug_type == "productshippingclass"){ $table_name = "tbl_ecom_shipping_class"; }
      if($slug_type == "producttag"){ $table_name = "tbl_ecom_tag"; }
      if($slug_type == "page"){ $table_name = "tbl_page"; }
      if($slug_type == "postcategory"){ $table_name = "tbl_postcategory"; }
      if($slug_type == "post"){ $table_name = "tbl_post"; }
      if($slug_type == "posttag"){ $table_name = "tbl_posttag"; }

      /* Check Avaialability */
      $slugcount = DB::table($table_name)
      ->where('site_id', '=', $site_id)
      ->where('status', 1)
      ->where('slug_name', '=',Str::slug($slug_name))
      ->get();

      if(count($slugcount)==0)
      {
        $slug_name=$slug_name;
      }
      else
      {
        up:
        $slug_name1=$slug_name."-".getthreestring();
        $slugcounter = DB::table($table_name)
        ->where('site_id', '=', $site_id)
        ->where('status', 1)
        ->where('slug_name', '=',Str::slug($slug_name1))
        ->get();
        if(count($slugcounter)==0)
        {
          $slug_name=$slug_name1;
        }
        else
        {
          goto up;
        }
      }
      return $slug_name;
  }
}

//code for checking slug is available or not and if not than returning by adding -3 random sting behind it
if (!function_exists('checkslugavailability1')) {
  function checkslugavailability1($slug_names, $slug_type, $site_id) {
      $slug_name= Str::slug($slug_names);
      $slugcount = DB::table('tbl_slug as slug')
      ->where('slug.site_id', '=', $site_id)
      ->where('slug.status', '=', 1)
      ->where('slug.slug_name', '=',Str::slug($slug_name))
      ->where('slug.slug_type', '=',$slug_type)
      ->get();

      if(count($slugcount)==0)
      {
        $slug_name=$slug_name;
      }
      else
      {
        up:
        $slug_name1=$slug_name."-".getthreestring();
        $slugcounter = DB::table('tbl_slug as slug')
        ->where('slug.site_id', '=', $site_id)
        ->where('slug.status', '=', 1)
        ->where('slug.slug_name', '=',Str::slug($slug_name1))
        ->where('slug.slug_type', '=',$slug_type)
        ->get();
        if(count($slugcounter)==0)
        {
          $slug_name=$slug_name1;
        }
        else
        {
            goto up;
        }
      }
      return $slug_name;
  }
}

//for getting  length three string 
if (!function_exists('getthreestring')) {
  function getthreestring($length = 3) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
  }
}

if (!function_exists('sendContactInquiry')) {

  function sendContactInquiry($content, $email, $name) {

    /* Set business logo */
    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_dark_logo==""||$content["businessinfo"]->business_dark_logo==null||$content["businessinfo"]->business_dark_logo=="null")
        {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_dark_logo)) {
                $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
            } else {
                $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_dark_logo)) {
            $content["businessinfo"]->business_dark_logo = imagedisplaypath.$content["businessinfo"]->business_dark_logo;
        } else {
            $content["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        } 
    }

    if(fileuploadtype=="local"){
        if($content["businessinfo"]->business_light_logo==""||$content["businessinfo"]->business_light_logo==null||$content["businessinfo"]->business_light_logo=="null")
        {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$content["businessinfo"]->business_light_logo)) {
                $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
            } else {
                $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$content["businessinfo"]->business_light_logo)) {
            $content["businessinfo"]->business_light_logo = imagedisplaypath.$content["businessinfo"]->business_light_logo;
        } else {
            $content["businessinfo"]->business_light_logo =fixedpagefeaturedimage;
        } 
    }
      
    $emailcontent = $content;

    Mail::send('ecom_emails.contactemail', $emailcontent, function($message) use ($email, $name) 
    {
        $message->to($email, $name)->subject('Inquiry Received');
    });
       
    return true;
  }
}

if (!function_exists('requestForMasterData')) {

  function requestForMasterData($buildername, $type, $value) {
      $emailcontent = array (
          'builder_name' => $buildername,
          'type' => $type,
          'value' => $value
      );

      $to_email= adminemail;
      $to_name = 'CREDAI 365 | CREDAI CHENNAI';

      $response = Mail::send('emails.newmasterdatarequest', $emailcontent, function($message) use ($to_email, $to_name) 
      {
         $message->to($to_email, $to_name)->subject('New Master Entry Request Received | CREDAI365.COM');
      });
       
      return true;
  }

}

if (!function_exists('sendResetPasswordLinkinOTP')) {

  function sendResetPasswordLinkinOTP($link, $email) {
      $emailcontent = array (
        'link' => $link
      );

      $to_email= $email;
      $to_name = 'CREDAI 365 | CREDAI CHENNAI';

      Mail::send('emails.resetpassword', $emailcontent, function($message) use ($to_email, $to_name) 
      {
         $message->to($to_email, $to_name)->subject('Reset Your Password | CREDAI365.COM');
      });
       
      return true;
  }
}

if (!function_exists('verifyOTP')) {

  function verifyOTP($otp, $email) {
      $emailcontent = array (
        'otp' => $otp
      );

      $to_email= $email;
      $to_name = 'CREDAI 365 | CREDAI CHENNAI';

      Mail::send('emails.sendotp', $emailcontent, function($message) use ($to_email, $to_name) 
      {
         $message->to($to_email, $to_name)->subject('Verify Your OTP | CREDAI365.COM');
      });
       
      return true;
  }
}

if (!function_exists('sendTouchpointEmail')) {

    function sendTouchpointEmail($salesexecutiveemail, $touchpoint, $name, $email, $mobile, $inquiredate) {
        $emailcontent = array (
          'touchpoint' => $touchpoint,
          'name' => $name,
          'email' => $email,
          'mobile' => $mobile,
          'inquiredate' => $inquiredate
        );
	
		$to_email= $salesexecutiveemail;
		$to_name = 'CREDAI 365 | CREDAI CHENNAI';

        $response = Mail::send('emails.touchpoint', $emailcontent, function($message) use ($to_email, $to_name) 
        {
           $message->to($to_email, $to_name)->subject('A New Lead Received from CREDAI365.COM');
        });
		
		/*Log::useDailyFiles(storage_path().'/logs/email.log');
		Log::info(['Response'=>$response]);*/
         
        return true;
    }
  
}

if (!function_exists('downloadBrochureEmail')) {

    function downloadBrochureEmail($customeremail, $brochurelink, $projectname, $projectid) {
        $emailcontent = array (
          'brochurelink' => $brochurelink,
          'projectname' => $projectname,
	    	  'projectid' => $projectid
        );
	
		$to_email= $customeremail;
		$to_name = 'CREDAI 365 | CREDAI CHENNAI';
		$to_attachment = url('/')."/storage/app/".projectbrochureprefix.$projectid.".pdf";

		Mail::send('emails.downloadbrochure', $emailcontent, function($message) use ($to_email, $to_name) 
        {
           $message->to($to_email, $to_name)->subject('Download Brochure | CREDAI365.COM');
        });
         
        return true;
    }
	/*
	Log::useDailyFiles(storage_path().'/logs/email.log');
	Log::info(['Response'=>$response]);*/	
  
}

//generate accesstoken
if (!function_exists('generateAccessToken')) {

  function generateAccessToken($n=30) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $randomString = '';

      for ($i = 0; $i < $n; $i++) {
          $index = rand(0, strlen($characters) - 1);
          $randomString .= $characters[$index];
      }

      return $randomString;
  }

}

//for uploading file from the base64 crop string(used when we were using cropper)
if (!function_exists('fileupload')) {
	function fileupload($file){
		  $constantpath =  date("Y") . "/" . date('m');
		  $mediapath =mediapath."/". $constantpath;
		  if (!does_url_exists($mediapath)) {
			File::makeDirectory( $mediapath ,0777,true);
		  }
	 
		  $image_parts = explode(";base64,", $file);
		  $image_type_aux = explode("image/", $image_parts[0]);
		  $image_type = $image_type_aux[1];
		  $image_base64 = base64_decode($image_parts[1]);
		  $randomstring= randomnumbergenerator(25);//uniqid();/*rand(1111,9999);*/
		  $filename=date("Y").date('m').$randomstring;
		  $extension='.jpg';
		  $filenamewithextension= $filename.$extension;
		  $storagepath = $mediapath."/" . $filenamewithextension;
		  $result=file_put_contents($storagepath, $image_base64);
			 
		  if($result)
		  {
			  return $constantpath.'/'. $filenamewithextension;   
		  }
		  else
		  {
		   return NULL;
		  }
	}
}

//code for uploading simple file 
if (!function_exists('fileuploads')) {

     function fileuploads($file_name,$file)
    {
        
        //for making custom image name by removing the spaces and adding dash
        $customfilename= strtolower($file_name);
         //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $customfilename);
          //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        //for separating the name and extension
        $file_info = pathinfo($string);
        $filename = $file_info['filename'];
        $fileextension=$file_info['extension'];
        //get constant path

        $constantpath =  date("Y") . "/" . date('m');

        //making 8 digit random string for making custom image name
        $randomstring= randomnumbergenerator(25);/*uniqid();/*rand(1111,9999);*/
        $filenamewithoutextension=$filename."-".$randomstring;
        //dynamic path
        $fullfilename=$filename."-".$randomstring.".".$fileextension;
       
       

        // $mediapath =mediapath.$constantpath ;
        $mediapath =mediapath ;
   
        
      if(fileuploadtype=="local")
      {

        if (!File::exists($mediapath)) {
            

	    	$status= File::makeDirectory( $mediapath ,0777,true);
            
		}

        $result = $file->move($mediapath, $fullfilename);
      }
      else
      {
        $fullpath = 'beta/media/'.date("Y") . "/" . date('m').'/'.$fullfilename;
        $result = Storage::disk('s3')->put($fullpath, file_get_contents($file), 'public');
      }
     
       if($result)
       {
        return $constantpath.'/'. $fullfilename;   
       }
       else
       {
        return NULL;
       }
         
    }
}

//random number generator
if (!function_exists('randomnumbergenerator')) {
    function randomnumbergenerator($n){
      // Variable which store final string 
      $generated_string = ""; 
        
      // Create a string with the help of  
      // small letters, capital letters and 
      // digits. 
      $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
        
      // Find the length of created string 
      $len = strlen($domain); 
        
      // Loop to create random string 
      for ($i = 0; $i < $n; $i++) 
      { 
        // Generate a random index to pick 
        // characters 
        $index = rand(0, $len - 1); 
          
        // Concatenating the character  
        // in resultant string 
        $generated_string = $generated_string . $domain[$index]; 
      } 
        
      // Return the random generated string 
      return uniqid().$generated_string;
    }
}

if (!function_exists('sendsms')) {
    function sendsms($mobile, $message,$countrycode=91){
  
        $postData = "authkey=138138AnhKsTbNSUQK5e888b2fP1&mobiles=".$mobile."&message=".$message."&sender=CRDCHN&route=4&country=".$countrycode;
      
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $postData/*"authkey=138138AnhKsTbNSUQK5e888b2fP1&mobiles=9925743801&message=test%20teste&sender=CREDAI&route=4"*/,
          CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/x-www-form-urlencoded",
          "postman-token: 9611c626-a5d5-c2f1-e553-ce1b11ddb249"
          ),
        ));
      
        $response = curl_exec($curl);
        $err = curl_error($curl);
      
        curl_close($curl);
      
        if ($err) {
          $response = "cURL Error #:" . $err;
        }
        
        return $response;
    }
}

//code for uploading file using slim
if (!function_exists('fileUploadSlim')) {
  function fileUploadSlim($request, $keyname='slim'){
      // Get posted data, if something is wrong, exit
      try {
        $images = Slim::getImages($keyname);
      }
      catch (Exception $e) {

        // Possible solutions
        // ----------
        // Make sure you're running PHP version 5.6 or higher

        Slim::outputJSON(array(
          'status' => "failure",
          'message' => 'Unknown',
          'Exception' => $e
        ));

        return;
      }

      // No image found under the supplied input name
      if ($images === false) {

        // Possible solutions
        // ----------
        // Make sure the name of the file input is "slim[]" or you have passed your custom
        // name to the getImages method above like this -> Slim::getImages("myFieldName")

        Slim::outputJSON(array(
          'status' => "failure",
          'message' => 'No data posted'
        ));

        return;
      }

      // Should always be one image (when posting async), so we'll use the first on in the array (if available)
      $image = array_shift($images);

      // Something was posted but no images were found
      if (!isset($image)) {

        // Possible solutions
        // ----------
        // Make sure you're running PHP version 5.6 or higher

        Slim::outputJSON(array(
          'status' => "failure",
          'message' => 'No images found'
        ));

        return;
      }

      // If image found but no output or input data present
      if (!isset($image['output']['data']) && !isset($image['input']['data'])) {

        // Possible solutions
        // ----------
        // If you've set the data-post attribute make sure it contains the "output" value -> data-post="actions,output"
        // If you want to use the input data and have set the data-post attribute to include "input", replace the 'output' String above with 'input'
        // The submitted files are checked to see if they are images, if determined they are not images the values are null

        Slim::outputJSON(array(
          'status' => "failure",
          'message' => 'No image data'
        ));

        return;
      }



      // if we've received output data save as file
      if (isset($image['output']['data'])) {

        // get the name of the file
        $name = $image['output']['name'];

        // get the crop data for the output image
        $data = $image['output']['data'];

        // If you want to store the file in another directory pass the directory name as the third parameter.
        // $output = Slim::saveFile($data, $name, 'my-directory/');

        // If you want to prevent Slim from adding a unique id to the file name add false as the fourth parameter.
        // $output = Slim::saveFile($data, $name, 'tmp/', false);

        // Default call for saving the output data
        $output = Slim::saveFile($data, $name);
      }

      // if we've received input data (do the same as above but for input data)
      if (isset($image['input']['data'])) {

        // get the name of the file
        $name = $image['input']['name'];

        // get the crop data for the output image
        $data = $image['input']['data'];

        // If you want to store the file in another directory pass the directory name as the third parameter.
        // $input = Slim::saveFile($data, $name, 'my-directory/');

        // If you want to prevent Slim from adding a unique id to the file name add false as the fourth parameter.
        // $input = Slim::saveFile($data, $name, 'tmp/', false);

        // Default call for saving the input data
        $input = Slim::saveFile($data, $name);
      }

      //
      // Build response to client
      //
      $response = array(
        'status' => 'success'
      );

      if (isset($output) && isset($input)) {

        $response['output'] = array(
          'file' => $output['name'],
          'path' => $output['path'],
          'folderpath' => $output['folderpath']
        );

        $response['input'] = array(
          'file' => $input['name'],
          'path' => $input['path'],
          'folderpath' => $output['folderpath']
        );

      }
      else {
        $response['file'] = isset($output) ? $output['name'] : $input['name'];
        $response['path'] = isset($output) ? $output['path'] : $input['path'];
        $response['folderpath'] = isset($output) ? $output['folderpath'] : $input['folderpath'];
      }

      return $response;
  }
}

if (!function_exists('generateBrochurePDF')) {
  function generateBrochurePDF($project_id){

    $data=array();
		$finaldata = array();

		$data["subject"]="Download Brochure | CREDAI365.COM";

		$Project = new Project();

		$data1 = array();
		$data1['offset'] = 0;
		$data1['limit'] = 1;
		$data1['project_id'] = $project_id;

		$Projects = $Project->getproject($data1);

		if (count($Projects) > 0) {
			
			foreach ($Projects as $item) {
                    $projectdata = array();
                    $project_id = $item->project_id;

                    $item->project_id;

                    //code for getting sales-executive info

                    $SalesExecutive = DB::table('tbl_project_sales_executive as pse')->select('pse.sales_executive_id', 'se.sales_executive_status_id', 'se.sales_executive_firstname','se.sales_executive_lastname', 'se.sales_executive_photo', 'se.sales_executive_phone', 'sales_executive_whatsapp')
                            ->leftJoin('tbl_sales_executive as se', 'pse.sales_executive_id', '=', 'se.sales_executive_id')
                            ->where('pse.status', '=', 1)
                            ->where('se.sales_executive_status_id', '=', 1)
                            ->where('pse.project_id', '=', $project_id)
                            ->limit(1)
                            ->get();
                            foreach ($SalesExecutive as $item1) {
                                if ($item1->sales_executive_photo == NULL || $item1->sales_executive_photo == "") {
                                    $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                                } else {
                                    if (does_url_exists(mediapath .$item1->sales_executive_photo)) {
                                        $item1->sales_executive_photo_path = imagedisplaypath .$item1->sales_executive_photo;
                                    } else {
                                        $item1->sales_executive_photo_path = fixedimagepathsalesexecutive;
                                    }
                                }
                            }
        
                            $item->sales_executive_info = (count($SalesExecutive)) > 0 ? $SalesExecutive[0] : "";
                            
                    $Projectphotos = DB::table('tbl_project_photos as pp')->select('pp.project_photo')
                                    ->where('pp.status', '=', 1)
                                    ->where('pp.project_id', '=', $project_id)
                                    ->get();

                                    $photosarray=array();
                                    foreach ($Projectphotos as $item2) {
                                        if ($item2->project_photo == NULL || $item2->project_photo == "") {
                                            $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                        } else {
                                            if (does_url_exists(mediapath .$item2->project_photo)) {
                                                $item2->project_photo = imagedisplaypath .$item2->project_photo;
                                            } else {
                                                $item2->project_photo = fixedimagepathprojectfeaturedimage;
                                            }
                                        }
                                        array_push($photosarray,$item2);
                                    }
                                    $item->gallery = (count($Projectphotos)) > 0 ? $photosarray : "";

                    $Amenities = DB::table('tbl_project_amenities as pa')
                            ->select('a.amenities_name')
                            ->leftJoin('tbl_amenities as a', 'a.amenities_id', '=', 'pa.amenities_id')
                            ->where('pa.status', '=', 1)
                            ->where('pa.project_id', '=', $project_id)
                            ->get();
                           $amenitiesarray=array();
                            foreach ($Amenities as $ame) {
                               array_push($amenitiesarray,$ame->amenities_name);
                            }
                            $item->amenities = (count($Amenities)>0) ? $amenitiesarray : array();
                          
                  $Highlight = DB::table('tbl_project_highlights as ph')
                            ->select('h.highlight_name')
                            ->leftJoin('tbl_highlights as h', 'h.highlight_id', '=', 'ph.highlight_id')
                            ->where('ph.status', '=', 1)
                            ->where('ph.project_id', '=', $project_id)
                            ->get();
                            $highlightarray=array();
                            foreach ($Highlight as $high) {
                               array_push($highlightarray,$high->highlight_name);
                            }
                            $item->highlight = (count($Highlight)>0) ? $highlightarray : array();

                    if ($item->project_featured_img == NULL || $item->project_featured_img == "") {
                        $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                    } else {
                        if (does_url_exists(mediapath .$item->project_featured_img)) {
                            $item->project_featured_img_path = imagedisplaypath .$item->project_featured_img;
                        } else {
                            $item->project_featured_img_path = fixedimagepathprojectfeaturedimage;
                        }
                    }

                    if ($item->project_logo == NULL || $item->project_logo == "") {
                        $item->project_logo_path = fixedimagepathbuilderlogo;
                    } else {
                        if (does_url_exists(mediapath .$item->project_logo)) {
                            $item->project_logo_path = imagedisplaypath .$item->project_logo;
                        } else {
                            $item->project_logo_path = fixedimagepathbuilderlogo;
                        }
                    }
					
					$featuredimagelist = DB::table('tbl_project_photos as tpp')
                                    ->where('tpp.status', '=', 1)
									->where('tpp.type', '=', 'featuredimage')
                                    ->where('tpp.project_id', '=', $project_id)
                                    ->get();
									
					$item->featuredimage = (count($featuredimagelist)>0) ? $featuredimagelist : array();
									
					$gallerylist = DB::table('tbl_project_photos as tpp')
                                    ->where('tpp.status', '=', 1)
									->where('tpp.type', '=', 'gallery')
                                    ->where('tpp.project_id', '=', $project_id)
                                    ->get();
					$item->gallery = (count($gallerylist)>0) ? $gallerylist : array();
									
					$configdetailslist = DB::table('tbl_project_configuration_details as tpc')->select('tc.configuration_name','tpc.*')
						->leftJoin('tbl_configuration as tc', 'tc.configuration_id', '=', 'tpc.configuration_id')
                                    ->where('tpc.status', '=', 1)
                                    ->where('tpc.project_id', '=', $project_id)
                                    ->get();
					$item->configdetails = (count($configdetailslist)>0) ? $configdetailslist : "";
									
					$banklist = DB::table('tbl_project_banks as tpb')
						->leftJoin('tbl_banks as tb', 'tb.bankid', '=', 'tpb.bank_id')
                                    ->where('tpb.status', '=', 1)
                                    ->where('tpb.project_id', '=', $project_id)
                                    ->pluck('tb.bank_name')->implode(',');
					$item->banklist = ($banklist!="") ? $banklist : "";
					
					$maincertificateslist = DB::table('tbl_project_main_certificate as tpmc')->select('tpmc.certificate_no','tpmc.certificate_file')
                                    ->where('tpmc.status', '=', 1)
                                    ->where('tpmc.project_id', '=', $project_id)
                                    ->get();
					$item->maincertificates = (count($maincertificateslist)>0) ? $maincertificateslist : "";
					
					$item->builderinfo = DB::table('tbl_builder as tb')->select('tb.*')
                                    ->where('tb.status', '=', 1)
                                    ->where('tb.builder_id', '=', $item->builder_id)
                                    ->first();
                   
                    array_push($finaldata, $item);
                }
			
			$data['projectdata']=$finaldata;
			
			//code for generating pdf and send as attachment
			$data['brochurelink']=  brochurelink.$project_id;
			
			/*try {*/
				PDF::setOptions(['isPhpEnabled' => true]);
				$pdf = PDF::loadView('emails.brochure', $data);
				$filename = projectbrochureprefix.$project_id.".pdf";
				Storage::delete($filename);
				Storage::put($filename, $pdf->output());
				return $filename;
			/*} catch (Exception $e){
				echo $e->getMessage() ;
			}*/
		
		}
  }
}

function count_digit($number) {
  return strlen($number);
}

function divider($number_of_digits) {
    $tens="1";

  if($number_of_digits>8)
    return 10000000;

  while(($number_of_digits-1)>0)
  {
    $tens.="0";
    $number_of_digits--;
  }
  return $tens;
}

function getnumbers($num){
	$ext="";//thousand,lac, crore
	$number_of_digits = count_digit($num); //this is call :)
		if($number_of_digits>3)
	{
		if($number_of_digits%2!=0)
			$divider=divider($number_of_digits-1);
		else
			$divider=divider($number_of_digits);
	}
	else
		$divider=1;

	$fraction=$num/$divider;
	$fraction=number_format($fraction,2);
	if($number_of_digits==4 ||$number_of_digits==5)
		$ext="k";
	if($number_of_digits==6 ||$number_of_digits==7)
		$ext="Lacs";
	if($number_of_digits==8 ||$number_of_digits==9)
		$ext="Crs";
	return $fraction." ".$ext;
}

if (!function_exists('generateinvoice')) {
  function generateinvoice($orderid, $invoiceno, $data){

    /* Set business logo */
    if(fileuploadtype=="local"){
        if($data["businessinfo"]->business_dark_logo==""||$data["businessinfo"]->business_dark_logo==null||$data["businessinfo"]->business_dark_logo=="null")
        {
            $data["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        }else{
            if (File::exists(baseimagedisplaypath .$data["businessinfo"]->business_dark_logo)) {
                $data["businessinfo"]->business_dark_logo = imagedisplaypath.$data["businessinfo"]->business_dark_logo;
            } else {
                $data["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
            }
        }
    }
    else
    {
        if (does_url_exists(imagedisplaypath.$data["businessinfo"]->business_dark_logo)) {
            $data["businessinfo"]->business_dark_logo = imagedisplaypath.$data["businessinfo"]->business_dark_logo;
        } else {
            $data["businessinfo"]->business_dark_logo =fixedpagefeaturedimage;
        } 
    }

		$finaldata = array();
    //try {
      PDF::setOptions(['isPhpEnabled' => true]);
      $pdf = PDF::loadView('invoice.invoice', $data);
      $guid = generateAccessToken(5);
      $filename = $invoiceno.".pdf";
      //Storage::delete($filename);
      Storage::put($filename, $pdf->output());
      $filename = url('/')."/storage/app/".$filename;
      return $filename;
    //} catch (Exception $e){
      //return $e->getMessage() ;
    //}

  }
}

if (!function_exists('getIndianCurrency')) {
  function getIndianCurrency(float $number)
  {
      $decimal = round($number - ($no = floor($number)), 2) * 100;
      $hundred = null;
      $digits_length = strlen($no);
      $i = 0;
      $str = array();
      $words = array(0 => '', 1 => 'One', 2 => 'Two',
          3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
          7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
          10 => 'ten', 11 => 'Eleven', 12 => 'Twelve',
          13 => 'Thirteen', 14 => 'fourteen', 15 => 'Fifteen',
          16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
          19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
          40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
          70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
      $digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
      while( $i < $digits_length ) {
          $divider = ($i == 2) ? 10 : 100;
          $number = floor($no % $divider);
          $no = floor($no / $divider);
          $i += $divider == 10 ? 1 : 2;
          if ($number) {
              $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
              $hundred = ($counter == 1 && $str[0]) ? ' And ' : null;
              $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
          } else $str[] = null;
      }
      $Rupees = implode('', array_reverse($str));
      $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
      return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
  }
}

if (!function_exists('moneyFormatIndia')) {
  function moneyFormatIndia($num) {
    
    $explrestunits = "" ;
    $num = preg_replace('/,+/', '', $num);
    $words = explode(".", $num);
    $des = "00";
    if(count($words)<=2){
        $num=$words[0];
        if(count($words)>=2){$des=$words[1];}
        if(strlen($des)<2){$des="$des";}else{$des=substr($des,0,2);}
    }
    if(strlen($num)>3){
        $lastthree = substr($num, strlen($num)-3, strlen($num));
        $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
        $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
        $expunit = str_split($restunits, 2);
        for($i=0; $i<sizeof($expunit); $i++){
            // creates each of the 2's group and adds a comma to the end
            if($i==0)
            {
                $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
            }else{
                $explrestunits .= $expunit[$i].",";
            }
        }
        $thecash = $explrestunits.$lastthree;
    } else {
        $thecash = $num;
    }
    return "$thecash.$des";
  }
}

?>
