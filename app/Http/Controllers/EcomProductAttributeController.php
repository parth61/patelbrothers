<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductAttribute;
use App\EcomProductVariantAttributeTerm;
use App\EcomProductVariant;
use Carbon\Carbon;
use DB;

class EcomProductAttributeController extends Controller
{
    public function addproductattribute(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required",
            "attribute_id" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } 
        else {
            
            $productattribute = new EcomProductAttribute();
            $productvariant = new EcomProductVariant();
            $checkcount = count($productvariant->where('product_id', $request->input('product_id'))->where('status', '1')->get());

            if($checkcount === 0){
                $checkcount = count($productattribute->where('attribute_id', $request->input('attribute_id'))->where('product_id', $request->input('product_id'))->where('status', '1')->get());

                if ($checkcount === 0) {
                        $productattribute->product_id = $request->input('product_id');
                        $productattribute->attribute_id = $request->input('attribute_id');
                        $productattribute->is_visible_on_product_page = $request->input('is_visible_on_product_page');
                        $productattribute->use_for_variation = $request->input('use_for_variation');
                        $productattribute->created_by = $request->input('user_id');
                        $productattribute->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $productattribute->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $productattribute->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $productattribute->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                        $result = $productattribute->save();
                        if ($result) {
                            return response()->json(['status' => 200, 'data' => "Product attribute added sucessfully"]);
                        } else {
                            return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                        }
                }
                else
                {
                    return response()->json(['status' => 400, 'error' => "Already exist."], 400);
                }
            }else{
                return response()->json(['status' => 400, 'error' => "Please remove older variation to add new."], 400);
            }
           
        }
    }

    public function getallproductattribute(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $productattribute = new EcomProductAttribute();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_attribute_id') && $request->input('product_attribute_id') != "") {
                $data['product_attribute_id'] = $request->input('product_attribute_id');
            }
            if ($request->has('attribute_id') && $request->input('attribute_id') != "") {
                $data['attribute_id'] = $request->input('attribute_id');
            }
            if ($request->has('use_for_variation') && $request->input('use_for_variation') != "") {
                $data['use_for_variation'] = $request->input('use_for_variation');
            }
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            $productattribute = $productattribute->getproductattribute($data);
            $productattributedata=array();
            foreach($productattribute as $item)
            {
                //return response()->json(['status' => 200,'data' => $item->attribute_id]);

                //getting list of selected attribute term of that product
                 $selectedattributetermlist = DB::table('tbl_ecom_product_attribute_term as proattr')
                ->where('proattr.status', '=', 1)
                ->where('proattr.product_id', '=',$request->input('product_id'))
                ->where('proattr.attribute_id', '=', $item->attribute_id)
                ->pluck('proattr.attribute_term_id');

                
                $attributetermlist = DB::table('tbl_ecom_attribute_term as attrterm')
                ->select('attrterm.attribute_term_id','attrterm.attribute_term')
                ->where('attrterm.status', '=', 1)
                ->where('attrterm.attribute_id', '=', $item->attribute_id)
                ->get();

                $item->selectedattributetermlist=$selectedattributetermlist;
                $item->attributetermlist=$attributetermlist;
               

                array_push($productattributedata, $item);
            }
            return response()->json(['status' => 200, 'count' => count($productattribute), 'data' => $productattribute]);
        }

    }

    public function updateproductattribute(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_attribute_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $productattribute = new EcomProductAttribute();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_attribute_id','user_id','accesstoken']);
            $result = $productattribute->where('product_attribute_id', $request->input('product_attribute_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product attribute updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproductattribute(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_attribute_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
                    "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            /* Get attribute based data */
            $productattribute = new EcomProductAttribute();
            $productattributedata = $productattribute->where("product_attribute_id", $request->input('product_attribute_id'))->where('status', 1)->get();

            if(count($productattributedata) > 0){

                /* Get Variable Products */
                $ecomproductvariant = new EcomProductVariant();
                $ecomproductvariantdata = $ecomproductvariant->where('product_id', $productattributedata[0]->product_id)->where('status',1)->pluck('variant_product_id');

                /* Check attribute term dependency */
                $ecomproductvariantattributeterm = new EcomProductVariantAttributeTerm();
                $ecomproductvariantattributetermdata = $ecomproductvariantattributeterm->whereIn('variant_product_id', $ecomproductvariantdata)->where('attribute_id', $productattributedata[0]->attribute_id)->get();

                if(count($ecomproductvariantattributetermdata) == 0){
                    $request->request->add(['status' => 0]);
                    $request->request->add(['updated_by' =>  $request->input('user_id')]);
                    $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
                    $newrequest = $request->except(['product_attribute_id','user_id','site_id','accesstoken']);
                    $result = $productattribute->where('product_attribute_id', $request->input('product_attribute_id'))->update($newrequest);

                    if ($result) {

                        $ecomproductvariantattributeterm = new EcomProductVariantAttributeTerm();
                        $ecomproductvariantattributeterm->where('variant_product_id', $productattributedata[0]->product_id)->where('attribute_id', $productattributedata[0]->attribute_id)->update(array('status'=>0));

                        $productattribute = new EcomProductAttribute();
                        $productattributedatacounter = $productattribute->where("product_id", $productattributedata[0]->product_id)->where('status', 1)->count();

                        if($productattributedatacounter == 0){
                            $ecomproductvariant = new EcomProductVariant();
                           $ecomproductvariant->where('product_id', $productattributedata[0]->product_id)->update(array('status'=>0));
                        }

                        return response()->json(['status' => 200, 'data' => $request->all()]);

                    } else {
                        return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
                    }
                }else{
                    return response()->json(['status' => 400, 'error' => 'Please delete product dependency first.'], 400);
                }
            }else{
                return response()->json(['status' => 400, 'error' => 'Invalid attribute data.'], 400);
            }
            
        }

    }

}
