<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Budget;
class BudgetController extends Controller
{
    public function addbudget(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                    "created_by" => "required",
                    "updated_by" => "required",
                    "budget_min_amount" => "required",
                    "budget_max_amount" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $budget = new Budget();
            $budget->budget_min_amount = $request->input('budget_min_amount');
            $budget->budget_max_amount = $request->input('budget_max_amount');
            $budget->created_by = $request->input('created_by');
            $budget->updated_by = $request->input('updated_by');
            $budget->status = $request->input('status');
            $budget->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $budget->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $budget->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $budget->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $budget->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Budget added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallbudget(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $budget = new Budget();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('budget_id') && $request->input('budget_id') != "") {
                $data['budget_id'] = $request->input('budget_id');
            }
            $budget = $budget->getbudget($data);

            return response()->json(['status' => 200, 'count' => count($budget), 'data' => $budget]);
        }
    }

    public function updatebudget(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "budget_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $budget = new Budget();
            $newrequest = $request->except(['budget_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $budget->where('budget_id', $request->input('budget_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Budget updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletebudget(Request $request) {
        $valid = Validator::make($request->all(), [
                    "budget_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $budget = new Budget();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['budget_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $budget->where('budget_id', $request->input('budget_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
