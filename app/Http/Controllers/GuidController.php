<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Builder;
use Illuminate\Support\Facades\File;
class GuidController extends Controller
{
    public function loginwithguid(Request $request) {
       if(isset($_GET['guid']))
       {
            $guid=$_GET['guid'];
            $builder= new Builder;
            $builderdata=array();
            $res = $builder->where('guid','=', $guid)->get();
           $userinfo=array();
            
              if (count($res) > 0) {
                foreach ($res as $item) {
                    if($item->builder_logo==NULL|| $item->builder_logo=="")
                    {
                        $item->path = fixedimagepathbuilderlogo;
                    }
                    else
                    {
                            if (does_url_exists(mediapath.$item->builder_logo)) {
                                $item->logopath = imagedisplaypath.$item->builder_logo;
                            } else {
                                $item->logopath = fixedimagepathbuilderlogo;
                            }                               
                    }
                    if($item->builder_featured_img==NULL|| $item->builder_featured_img=="")
                    {
                        $item->path = fixedimagepathbuilderfeaturedimage;
                    }
                    else
                    {
                            if (does_url_exists(mediapath.$item->builder_featured_img)) {
                                $item->featuredpath = imagedisplaypath.$item->builder_featured_img;
                            } else {
                                $item->featuredpath = fixedimagepathbuilderfeaturedimage;
                            }
                   
                    }
                    array_push($builderdata, $item);
                  
                }
                $builderdata[0]['type']="builder";
                 $userinfo["id"] =$builderdata[0]->builder_id;
		$userinfo["type"] = "builder";
		
		$userinfo["id"] = $builderdata[0]["builder_id"];
            	$userinfo["name"] = $builderdata[0]["builder_name"];
            	$userinfo["email"] = $builderdata[0]["builder_email"];
            	$userinfo["guid"] = $builderdata[0]["guid"];

                    
              
                $request->session()->put('userdata',$userinfo);
             return redirect("dashboard");
             
        }
    }
    
}
}
