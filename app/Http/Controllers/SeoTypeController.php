<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\SeoType;
class SeoTypeController extends Controller
{
    public function addseotype(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "seo_type"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $seotype = new Seotype();
            $seotype->seo_type = $request->input('seo_type');
            $seotype->created_by = $request->input('user_id');
            $seotype->site_id = $request->input('site_id');
            $seotype->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $seotype->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $seotype->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $seotype->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $seotype->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "seotype Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallseotype(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seotype = new Seotype();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('seotype_id') && $request->input('seotype_id') != "") {
                $data['seotype_id'] = $request->input('seotype_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            $seotype = $seotype->getseotype($data);
            if($seotype)
            {
                
                return response()->json(['status' => 200, 'count' => count($seotype), 'data' => $seotype]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updateseotype(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "seotype_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seotype = new Seotype();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['seotype_id','user_id','accesstoken']);
            $result = $seotype->where('seotype_id', $request->input('seotype_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "seotype Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteseotype(Request $request) {

        $valid = Validator::make($request->all(), [
                    "seotype_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $seotype = new Seotype();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['seotype_id','user_id','accesstoken']);
            $result = $seotype->where('seotype_id', $request->input('seotype_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Seo Type Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
