<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Slider;
use App\SliderItems;
use Illuminate\Support\Facades\File;
use App\Media;
use DB;

class SliderItemsController extends Controller
{
    public function addslideritem(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required", 
            "slider_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slideritemmodel = new SliderItems();
            $slideritemmodel->slider_id = $request->input('slider_id');
            $slideritemmodel->slider_title = ($request->has('slider_title'))?$request->input('slider_title'):null;
            $slideritemmodel->slider_subtitle = ($request->has('slider_subtitle'))?$request->input('slider_subtitle'):null;
            $slideritemmodel->slider_description = ($request->has('slider_description'))?$request->input('slider_description'):null;
            $slideritemmodel->slider_image = $request->input('slider_image');
            $slideritemmodel->slider_action = ($request->has('slider_action'))?$request->input('slider_action'):null;$request->input('slider_action');
            $slideritemmodel->slider_action_title = ($request->has('slider_action_title'))?$request->input('slider_action_title'):null;
            $slideritemmodel->is_full_image = ($request->has('is_full_image'))?$request->input('is_full_image'):0;
            $slideritemmodel->slider_large_img = ($request->has('slider_large_img'))?$request->input('slider_large_img'):null;
            $slideritemmodel->slider_medium_img = ($request->has('slider_medium_img'))?$request->input('slider_medium_img'):null;
            $slideritemmodel->slider_small_img = ($request->has('slider_small_img'))?$request->input('slider_small_img'):null;
            $slideritemmodel->created_by = $request->input('user_id');
            $slideritemmodel->updated_by = $request->input('user_id');
            $slideritemmodel->site_id = $request->input('site_id');
            $slideritemmodel->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $slideritemmodel->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $slideritemmodel->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $slideritemmodel->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $slideritemmodel->save();

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Slider item Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }

    public function getallslideritem(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('slider_id') && $request->input('slider_id') != "") {
                $data['slider_id'] = $request->input('slider_id');
            }

            if ($request->has('slider_name') && $request->input('slider_name') != "") {
                $data['slider_name'] = $request->input('slider_name');
            }

            if ($request->has('site_id') && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('status')) {
                $data['status'] = $request->input('status');
            }

            $slideritemmodel = new SliderItems();
            $sliderdata = $slideritemmodel->getslideritems($data);

            foreach($sliderdata as $item) {
                 
                /* Slider Image */
                if($item->slider_image!="" || $item->slider_image!=null || $item->slider_image!="null")
                {
                    $mediamodel = new Media();
                    $media_file_data = $mediamodel->where('media_id', $item->slider_image)->first();
                    if($media_file_data != null  ){
                        if(fileuploadtype=="local")
                            {
                                if (File::exists(baseimagedisplaypath.$media_file_data["media_file"])) {
                                    $item->media_file = imagedisplaypath.$media_file_data["media_file"];
                                } else {
                                    $item->media_file =null;
                                }
                            }
                            else
                            {
                                if (does_url_exists(imagedisplaypath.$media_file_data["media_file"])) {
                                    $item->media_file = imagedisplaypath.$media_file_data["media_file"];
                                } else {
                                    $item->media_file = null;
                                }
                            }
                    }else{
                         $item->media_file = null;
                    }
                    $item->media_info = $media_file_data;

                }else{
                    $item->media_file = null;
                }

                /* Slider Large Image */
                if($item->slider_large_img!="" || $item->slider_large_img!=null || $item->slider_large_img!="null")
                {
                    $mediamodel = new Media();
                    $media_file_data = $mediamodel->where('media_id', $item->slider_large_img)->first();
                    if($media_file_data != null  ){
                         if(fileuploadtype=="local")
                        {
                            if (File::exists(baseimagedisplaypath.$media_file_data["media_file"])) {
                                $item->large_media_file = imagedisplaypath.$media_file_data["media_file"];
                            } else {
                                $item->large_media_file =null;
                            }
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath.$media_file_data["media_file"])) {
                                $item->large_media_file = imagedisplaypath.$media_file_data["media_file"];
                            } else {
                                $item->large_media_file = null;
                            }
                        }   
                    }else{
                        $item->large_media_file = null;
                    }
                    $item->large_media_info = $media_file_data;

                }else{
                    $item->large_media_file = null;
                }

                /* Slider Medium Image */
                if($item->slider_medium_img!="" || $item->slider_medium_img!=null || $item->slider_medium_img!="null")
                {
                    $mediamodel = new Media();
                    $media_file_data = $mediamodel->where('media_id', $item->slider_medium_img)->first();
                    if($media_file_data != null  ){
                        if(fileuploadtype=="local")
                        {
                            if (File::exists(baseimagedisplaypath.$media_file_data["media_file"])) {
                                $item->medium_media_file = imagedisplaypath.$media_file_data["media_file"];
                            } else {
                                $item->medium_media_file =null;
                            }
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath.$media_file_data["media_file"])) {
                                $item->medium_media_file = imagedisplaypath.$media_file_data["media_file"];
                            } else {
                                $item->medium_media_file = null;
                            }
                        }
                    }else{
                        $item->medium_media_file = null;
                    }
                    $item->medium_media_info = $media_file_data;
                }else{
                    $item->medium_media_file = null;   
                }
                    

                /* Slider Small Image */
                if($item->slider_small_img!="" || $item->slider_small_img!=null || $item->slider_small_img!="null")
                {
                    $mediamodel = new Media();
                    $media_file_data = $mediamodel->where('media_id', $item->slider_small_img)->first();
                    if($media_file_data != null  ){
                        if(fileuploadtype=="local")
                        {
                            if (File::exists(baseimagedisplaypath.$media_file_data["media_file"])) {
                                $item->small_media_file = imagedisplaypath.$media_file_data["media_file"];
                            } else {
                                $item->small_media_file =null;
                            }
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath.$media_file_data["media_file"])) {
                                $item->small_media_file = imagedisplaypath.$media_file_data["media_file"];
                            } else {
                                $item->small_media_file = null;
                            }
                        }

                    }else{
                        $item->small_media_file = null;
                    }
                    $item->small_media_info = $media_file_data;
                }else{
                   $item->small_media_file = null;  
                }
                    


            }
            
            return response()->json(['status' => 200, 'data' => $sliderdata]);

        }
    }
    
    public function updateslideritem(Request $request) {

        $valid = Validator::make($request->all(), [
            "slider_item_id" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slidermodel = new SliderItems();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['site_id','accesstoken','user_id','slider_item_id']);
            $result = $slidermodel->where('slider_item_id', $request->input('slider_item_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Slider item Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }

    public function deleteslideritem(Request $request) {

        $valid = Validator::make($request->all(), [
            "slider_item_id" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required",
            "user_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $slidermodel = new SliderItems();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['slider_item_id','slider_id','site_id','accesstoken','user_id']);
            $newrequest['status'] = 0;
            $result = $slidermodel->where('slider_item_id', $request->input('slider_item_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Slider item deleted succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }
}
