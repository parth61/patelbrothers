<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\State;

class StateController extends Controller
{
     public function addmasterstate(Request $request) {
        //validations
       $state = new State();
        $valid = Validator::make($request->all(), [
                    "state_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $state->state_name = $request->input('state_name');
            $state->country_id = $request->input('country_id');
            $state->status = $request->input('status');
            $state->created_by = $request->input('created_by');
            $state->updated_by = $request->input('updated_by');
            $state->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $state->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $state->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $state->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $state->rera_link_url = $request->input('rera_link_url');
            $result = $state->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "State added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallmasterstate(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $state = new State();
            $data = array();
            if ($request->has('country_id') && $request->input('country_id') != "") {
                $data['country_id'] = $request->input('country_id');
            }
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            $statedata = $state->getstate($data);
            
            return response()->json(['status' => 200, 'count' => count($statedata), 'data' => $statedata]);
        }
    }
    public function updatemasterstate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "state_id" => "required|numeric",
                    "country_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $state = new State();
            $newrequest = $request->except(['state_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $state->where('state_id', $request->input('state_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "State Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletemasterstate(Request $request) {
        $valid = Validator::make($request->all(), [
                    "state_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           $state = new State();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['state_id']);
            $result = $state->where('state_id', $request->input('state_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
