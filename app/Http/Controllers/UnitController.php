<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Units;

class UnitController extends Controller
{
     public function addmasterunit(Request $request) {
        //validations
       $unit = new Units();
        $valid = Validator::make($request->all(), [
                    "unit_type_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $unit->unit_type_name = $request->input('unit_type_name');
            $unit->category_id = $request->input('category_id');
            $unit->status = $request->input('status');
            $unit->created_by = $request->input('created_by');
            $unit->updated_by = $request->input('updated_by');
            $unit->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $unit->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $unit->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $unit->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $unit->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Units added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallmasterunits(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $unit = new Units();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('category_id') && $request->input('category_id') != "") {
                $data['category_id'] = $request->input('category_id');
            }
            $unitdata = $unit->getunits($data);
            
            return response()->json(['status' => 200, 'count' => count($unitdata), 'data' => $unitdata]);
        }
    }
    public function updatemasterunit(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "unit_id" => "required|numeric",
                    "category_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $unit = new Units();
            $newrequest = $request->except(['unit_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $unit->where('unit_id', $request->input('unit_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Unit Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletemasterunit(Request $request) {
        $valid = Validator::make($request->all(), [
                    "unit_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           $unit = new Units();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['unit_id']);
            $result = $unit->where('unit_id', $request->input('unit_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
