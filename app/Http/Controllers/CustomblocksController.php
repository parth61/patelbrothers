<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Customblocks;

class CustomblocksController extends Controller
{
    

     public function addmastercustomblocks(Request $request) {
        //validations
        $customblocks = new Customblocks();
        $valid = Validator::make($request->all(), [
                    // "custom_blocks_title" => "required",
                    // "site_id" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            //$slug_name=checkslugavailability($request->input('slug_name'), "customblocks",$request->input('site_id'));
            
            
            $customblocks->custom_blocks_title = $request->input('custom_blocks_title');
            $customblocks->custom_blocks_description = $request->input('custom_blocks_description');
            $customblocks->created_by = $request->input('created_by');
            $customblocks->post_id = $request->input('post_id');
            $customblocks->updated_by = $request->input('created_by');
            $customblocks->status = $request->input('status');
            $customblocks->slug_name = $request->has('slug_name')?$request->input('slug_name'):null;
            $customblocks->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $customblocks->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $customblocks->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $customblocks->custom_blocks_featured_img = $request->has('custom_blocks_featured_img')?$request->input('custom_blocks_featured_img'):null;
            $customblocks->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $customblocks->site_id = $request->has('site_id')?$request->input('site_id'):null;
            $customblocks->custom_block_guid = generateAccessToken(20); 
            $result = $customblocks->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Customblocks added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallmastercustomblocks(Request $request) {
        
        $valid = Validator::make($request->all(), [
                  
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $customblocks = new Customblocks();

            $data = array();
            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
             if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }
             if ($request->has('custom_block_guid') && $request->filled('custom_block_guid')) {
                $data["custom_block_guid"] = $request->input("custom_block_guid");
            }
            if ($request->has('post_id') && $request->input('post_id') != "") {
                $data['post_id'] = $request->input('post_id');
            }
            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }
            $customblocksdata = $customblocks->getcustomblocks($data);
            if($customblocksdata)
            {
                $customblocksdatadata = array();
                foreach ($customblocksdata as $item) {
                    if(fileuploadtype=="local")
                    {
                        
                        if($item->custom_blocks_media_file==""||$item->custom_blocks_media_file==null||$item->custom_blocks_media_file=="null")
                        {
                            $item->custom_blocks_featured_path = fixedpagefeaturedimage;
                        }
                        else
                        {
                            if (File::exists(baseimagedisplaypath .$item->custom_blocks_media_file)) {
                                $item->custom_blocks_featured_path = imagedisplaypath.$item->custom_blocks_media_file;
                            } else {
                                $item->custom_blocks_featured_path = fixedpagefeaturedimage;
                            }
                        }
                    }
                    else
                    {
                            //page featured 
                            if (does_url_exists(imagedisplaypath.$item->custom_blocks_media_file)) {
                                $item->custom_blocks_featured_path = imagedisplaypath.$item->custom_blocks_media_file;
                            } else {
                                $item->custom_blocks_featured_path = fixedpagefeaturedimage;
                            }
                    }


                    array_push($customblocksdatadata, $item);
                }

                
                return response()->json(['status' => 200, 'count' => count($customblocksdata), 'data' => $customblocksdata]);
            }else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
            
            
        }
    }
    public function updatemastercustomblocks(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "custom_blocks_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $customblocks = new Customblocks();
            $newrequest = $request->except(['custom_blocks_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);

            /*if($request->has('slug_name')){
                $customblockdata = $customblocks->where('custom_blocks_id', $request->input('custom_blocks_id'))->first();
                if($customblockdata->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "customblocks", $request->input('site_id'));
                }
            }*/
            $result = $customblocks->where('custom_blocks_id', $request->input('custom_blocks_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Customblocks Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deletemastercustomblocks(Request $request) {
        $valid = Validator::make($request->all(), [
                    "custom_blocks_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $customblocks = new Customblocks();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['custom_blocks_id']);
            $result = $customblocks->where('custom_blocks_id', $request->input('custom_blocks_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
    
}