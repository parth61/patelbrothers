<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\EcomWishlist;
use App\EcomProductCategory;
use App\EcomProductGallery;
use App\EcomProduct;
use App\EcomCart;
use DB;

class EcomWishlistController extends Controller
{
    public function addwishlist(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "product_id" => "required",
            "user_id" => "required",    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $wishlist = new EcomWishlist();
            $wishlistdata = $wishlist->where('user_id', $request->input('user_id'))->where('product_id', $request->input('product_id'))->where('status', 1)->get();
            if(count($wishlistdata)==0){
                $wishlist->product_id = $request->input('product_id');
                $wishlist->user_id =$request->input('user_id');
                $wishlist->attribute_id_list = $request->has('attributeidlist') ? $request->input('attributeidlist') : null ;
                $wishlist->attribute_term_id_list = $request->has('attributetermidlist') ? $request->input('attributetermidlist') : null;
                $wishlist->attribute_term_name_list = $request->has('attributetermnamelist') ? $request->input('attributetermnamelist') : null;
                $wishlist->site_id = $request->input('site_id');
                $wishlist->created_by = $request->input('user_id');
                $wishlist->updated_by = $request->input('user_id');
                $wishlist->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $wishlist->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $wishlist->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $wishlist->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $wishlist->save();
                if ($result) {
                    if($request->has('from')){
                        if($request->input('from')=="cart"){
                            $updatedata = array();
                            $updatedata['status'] = '0';
                            $carts = new Ecomcart();
                            $updateresult = $carts->where('product_id', $request->input('product_id'))->where('user_id', $request->input('user_id'))->update($updatedata);
                            
                        }
                    }
                    return response()->json(['status' => 200, 'data' => "Product added to wishlist."]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                return response()->json(['status' => 200, 'data' => "Product added to wishlist."]);
            }
            
        }
    }

    public function getallwishlist(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "user_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $wishlist = new EcomWishlist();
            $productcategory = new EcomProductCategory();
            $productgallery = new EcomProductGallery();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('wishlist_id') && $request->input('wishlist_id') != "") {
                $data['wishlist_id'] = $request->input('wishlist_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
            
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $wishlist = $wishlist->getwishlist($data);
            foreach ($wishlist as $item) {

                $temp = array();
                $temp['product_id'] = $item->product_id;
                $item->categorylist = $productcategory->getproductcategorylist($temp);
                $temp['offset'] = 0;
                $temp['limit'] = 1;
                $productgalleryobj = $productgallery->getproductgallery($temp);
                $productgallerydata=array();
                foreach ($productgalleryobj as $item1) {
                    if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                    {
                        $item1->product_image_path =fixedproductgalleryimage;
                       
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }  
                    array_push($productgallerydata, $item1->product_image_path);
                }

                $item->attribute_terms = DB::table('tbl_ecom_product_attribute_term as proattrterm')
                ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
                ->where('proattrterm.status', '=', 1)
                ->where('proattrterm.product_id', '=', $item->product_id)
                ->pluck('teat.attribute_term');
                
                $item->product_gallery=$productgallerydata;
                if($item->has_variation){
                    $productmodel = new EcomProduct();
                    $item->pricerange = $productmodel->getminmaxforproductid($item->product_id);
                }
            }
            return response()->json(['status' => 200, 'count' => count($wishlist), 'data' => $wishlist]);
        }

    }

    public function updatewishlist(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "wishlist_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $wishlist = new EcomWishlist();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['wishlist_id','accesstoken','user_id']);
            $result = $wishlist->where('wishlist_id', $request->input('wishlist_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product updated to wishlist"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletewishlist(Request $request) {

        $valid = Validator::make($request->all(), [
                    "wishlist_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $wishlist = new EcomWishlist();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['wishlist_id','accesstoken','user_id']);
            $result = $wishlist->where('wishlist_id', $request->input('wishlist_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product removed from wishlist"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
    public function deletewishlistbyproductid(Request $request) {

        $valid = Validator::make($request->all(), [
                    "user_id" => "required",
                    "product_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $wishlist = new EcomWishlist();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['wishlist_id','accesstoken','user_id']);
            $result = $wishlist->where('user_id', $request->input('user_id'))->where('product_id', $request->input('product_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product removed from wishlist"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function getallwishlistandcartcount(Request $request) {

        $valid = Validator::make($request->all(), [
                    "user_id" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $wishlist = new EcomWishlist();
            
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
           
            $wishlistdata = $wishlist->getwishlistandcartcount($data);
            return response()->json(['status' => 200, 'count' => count($wishlistdata), 'data' => $wishlistdata]);
        }

    }
}
