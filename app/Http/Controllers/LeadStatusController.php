<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeadStatus;
use DB;
use Validator;
class LeadStatusController extends Controller
{
    public function getallprimarystatus(Request $request) {
        $valid = Validator::make($request->all(), [
                       "limit" => "required|numeric",
                       "offset" => "required|numeric",
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
               $LeadStatus = new LeadStatus();
               $data = array();
               $data['offset'] = $request->input('offset');
               $data['limit'] = $request->input('limit');

               $LeadStatus = $LeadStatus->getprimarystatus($data);
   
               return response()->json(['status' => 200, 'count' => count($LeadStatus), 'data' => $LeadStatus]);
           }
       }

       public function getallsecondarystatus(Request $request) {
        $valid = Validator::make($request->all(), [
                       "limit" => "required|numeric",
                       "offset" => "required|numeric",
                      
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
               $LeadStatus = new LeadStatus();
               $data = array();
               $data['offset'] = $request->input('offset');
               $data['limit'] = $request->input('limit');
               if ($request->has('primarystatus') && $request->input('primarystatus') != "") {
               $data['primarystatus'] = $request->input('primarystatus');
              
            }
            $LeadStatus = $LeadStatus->getsecondarystatus($data);
               return response()->json(['status' => 200, 'count' => count($LeadStatus), 'data' => $LeadStatus]);
           }
       }
       public function getalltertiarystatus(Request $request) {
        $valid = Validator::make($request->all(), [
                       "limit" => "required|numeric",
                       "offset" => "required|numeric",
                     
           ]);
   
           if ($valid->fails()) {
               return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
           } else {
               $LeadStatus = new LeadStatus();
               $data = array();
               $data['offset'] = $request->input('offset');
               $data['limit'] = $request->input('limit');
               if ($request->has('secondarystatus') && $request->input('secondarystatus') != "") {
               $data['secondarystatus'] = $request->input('secondarystatus');
             
               }
               $LeadStatus = $LeadStatus->gettertiarystatus($data);
               return response()->json(['status' => 200, 'count' => count($LeadStatus), 'data' => $LeadStatus]);
           }
       }                                                 
}
