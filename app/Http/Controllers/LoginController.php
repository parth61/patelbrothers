<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
use URL;
use App\User;
use App\Builder;
use App\SalesExecutive;
use App\LoginLog;
use App\EcomCart;
use Illuminate\Support\Facades\File;

class LoginController extends Controller {

    public function login(Request $request) {
        $valid = Validator::make($request->all(), [
                    "username" => "required",
                    "password" => "required",
                    "type" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if ($request->input('type') == "salesexecutive") {

                $sales_executive = new SalesExecutive;
                $salesexecutivedata = array();
                $res = $sales_executive->where('sales_executive_email', $request->input('username'))
                        ->where('sales_executive_password', md5($request->input('password')))
                        ->where('status', 1)
                        ->get();

                if (count($res) > 0) {

                    foreach ($res as $item) {
                        if ($item->sales_executive_photo == NULL || $item->sales_executive_photo == "") {
                            $item->salesexecutivepath = fixedimagepathsalesexecutive;
                        } else {
                            if (does_url_exists(mediapath . '/' . $item->sales_executive_photo)) {
                                $item->salesexecutivepath = imagedisplaypath . '/' . $item->sales_executive_photo;
                            } else {
                                $item->salesexecutivepath = fixedimagepathsalesexecutive;
                            }
                        }


                        array_push($salesexecutivedata, $item);
                    }
                    $salesexecutivedata[0]['type'] = "salesexecutive";

                       //code for maintaining user login history  
                    $LoginLog= new LoginLog;
                    $LoginLog->user_id =   $res[0]->sales_executive_id;
                    $LoginLog->site_id =   $res[0]->site_id;
		            $LoginLog->login_type= "login";
                    $LoginLog->user_type = "salesexecutive";
                    $LoginLog->domain_name = $request->has('domain_name')?$request->input('domain_name'):null;
                    $LoginLog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $LoginLog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $LoginLog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $LoginLog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $LoginLog->save();
                    return response()->json(['status' => 200, 'data' => $salesexecutivedata[0]]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } elseif ($request->input('type') == "builder") {
                $builder = new Builder;
                $builderdata = array();
                $res = $builder->where('builder_email', $request->input('username'))
                        ->where('builder_password', md5($request->input('password')))
                        ->get();


                if (count($res) > 0) {
                    foreach ($res as $item) {
                        if ($item->builder_logo == NULL || $item->builder_logo == "") {
                            $item->path = fixedimagepathbuilderlogo;
                        } else {
                            if (does_url_exists(mediapath . '/' . $item->builder_logo)) {
                                $item->logopath = imagedisplaypath . '/' . $item->builder_logo;
                            } else {
                                $item->logopath = fixedimagepathbuilderlogo;
                            }
                        }
                        if ($item->builder_featured_img == NULL || $item->builder_featured_img == "") {
                            $item->path = fixedimagepathbuilderfeaturedimage;
                        } else {
                            if (does_url_exists(mediapath . '/' . $item->builder_featured_img)) {
                                $item->featuredpath = imagedisplaypath . '/' . $item->builder_featured_img;
                            } else {
                                $item->featuredpath = fixedimagepathbuilderfeaturedimage;
                            }
                        }
                        array_push($builderdata, $item);
                    }
                    $builderdata[0]['type'] = "builder";

    		    //code for maintaining user login history  
                    $LoginLog= new LoginLog;
                    $LoginLog->user_id =  $res[0]->builder_id;
                    $LoginLog->site_id =  $res[0]->site_id;
                    $LoginLog->login_type= "login";
                    $LoginLog->user_type = "builder";
                    $LoginLog->domain_name = $request->has('domain_name')?$request->input('domain_name'):null;
                    $LoginLog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $LoginLog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $LoginLog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $LoginLog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $LoginLog->save();

                    return response()->json(['status' => 200, 'data' => $builderdata[0]]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } elseif (($request->input('type') == "customer")) {
                $user = new User();
                $res = $user->where('status', 1)
                                ->where('user_email', $request->input('username'))
                                ->where('site_id', $request->input('site_id'))
                                ->where('user_password', md5($request->input('password')))->get();

                if (count($res) >= 1) {
                    $res[0]['type'] = "customer";
                    $LoginLog= new LoginLog;
                    $LoginLog->user_id =  $res[0]->user_id;
                    $LoginLog->site_id =  $res[0]->site_id;
                    $LoginLog->login_type= "login";
                    $LoginLog->user_type = "customer";
                     $LoginLog->domain_name = $request->has('domain_name')?$request->input('domain_name'):null;
                    $LoginLog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $LoginLog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $LoginLog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $LoginLog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $LoginLog->save();

                    if(fileuploadtype=="local")
                    {
                        if($res[0]->user_photo==""||$res[0]->user_photo==null||$res[0]->user_photo=="null")
                        {
                            $res[0]->user_photo =fixedpagefeaturedimage;
                        }
                        else
                        {
                            if (File::exists(baseimagedisplaypath .$res[0]->user_photo)) {
                                $res[0]->user_photo = imagedisplaypath.$res[0]->user_photo;
                            } else {
                                $res[0]->user_photo =fixedpagefeaturedimage;
                            }
                        }
                    }
                    else
                    {

                        if (does_url_exists(imagedisplaypath.$res[0]->user_photo)) {
                            $res[0]->user_photo = imagedisplaypath.$res[0]->user_photo;
                        } else {
                            $res[0]->user_photo =fixedpagefeaturedimage;
                        } 
                    }

                    if($request->has('visitor_tag') && $request->input('visitor_tag') != null){
                        $ecomcartmodel = new EcomCart();
                        $result = $ecomcartmodel->syncCartFromVisitorTag($res[0]->user_id, $request->input('visitor_tag'));
                    }
                    $result = false;

                    return response()->json(['status' => 200, 'data' => $res[0], 'sync_status'=>$result]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } else {

                $res = DB::table('tbl_admin as ta')->select('ta.*','ts.site_name','ts.accesstoken','tst.site_type')
                ->leftJoin('tbl_sites as ts', 'ts.site_id', '=', 'ta.site_id')
                ->leftJoin('tbl_site_type as tst', 'tst.site_type_id', '=', 'ts.site_type_id')
                ->where('ta.admin_email', $request->input('username'))->where('ta.admin_password', md5($request->input('password')))->where('ta.status', 1)->get();

                if (count($res) >= 1) {
                    $res[0]->type = "admin";
                    //code for maintaining user login history  
                    $LoginLog= new LoginLog;
                    $LoginLog->user_id = $res[0]->admin_id;
                    $LoginLog->site_id = $res[0]->site_id;
                    $LoginLog->login_type= "login";
                    $LoginLog->user_type = "admin";
                     $LoginLog->domain_name = $request->has('domain_name')?$request->input('domain_name'):null;
                    $LoginLog->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $LoginLog->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $LoginLog->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $LoginLog->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $LoginLog->save();
                    return response()->json(['status' => 200, 'data' => $res[0]]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email or Password'], 400);
                }
            }
        }
    }

    // set user session
    public function setsession(Request $request) {


        //$request->session()->put('userdata',json_decode($request->input('userdata'),true));
        $userinfo = json_decode($request->input('userdata'), TRUE);

        if ($userinfo["type"] === "salesexecutive") {
            $salesexecutivefullname = $userinfo["sales_executive_firstname"] . " " . $userinfo["sales_executive_lastname"];
            $userinfo["id"] = $userinfo["sales_executive_id"];
            $userinfo["name"] = $salesexecutivefullname;
            $userinfo["email"] = $userinfo["sales_executive_email"];
            $userinfo["phone"] = $userinfo["sales_executive_phone"];
            $userinfo["guid"] = $userinfo["guid"];
        }

        if ($userinfo["type"] === "builder") {
            $userinfo["id"] = $userinfo["builder_id"];
            $userinfo["name"] = $userinfo["builder_name"];
            $userinfo["email"] = $userinfo["builder_email"];
            $userinfo["guid"] = $userinfo["guid"];
            // $userinfo["phone"] = $userinfo["builder_phone"];
        }

        if ($userinfo["type"] === "admin") {
            $userinfo["id"] = $userinfo["admin_id"];
            $userinfo["name"] = $userinfo["admin_firstname"]." ".$userinfo["admin_lastname"];
            $userinfo["email"] = $userinfo["admin_email"];
            $userinfo["guid"] = $userinfo["guid"];
            $userinfo["site_type"] = $userinfo["site_type"];
        }

        $request->session()->put('userdata', $userinfo);

        //return response()->json(['status' => 200, 'data' => "Session set successfull"]);
        return response()->json(['status' => 200, 'data' => $userinfo]);
    }

    public function sendResetPasswordLink(Request $request) {
        $valid = Validator::make($request->all(), [
            "email" => "required",
            "type" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if ($request->input('type') == "salesexecutive") {

                $sales_executive = new SalesExecutive;
                $salesexecutivedata = array();
                $res = $sales_executive->where('sales_executive_email', $request->input('email'))->where('status', 1)->get();
                if (count($res) > 0) {
                    $link = URL::asset('reset-password')."/?guid=".$res[0]['guid']."&type=salesexecutive";
                    $email = $res[0]['sales_executive_email'];
                    $responsedata = sendResetPasswordLinkinOTP($link, $email);
                    return response()->json(['status' => 200, 'data' => "Email Sent", 'link'=>$link]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } elseif ($request->input('type') == "builder") {
                $builder = new Builder;
                $builderdata = array();
                $res = $builder->where('builder_email', $request->input('email'))->where('status',1)->get();
                if (count($res) > 0) {
                    $link = URL::asset('reset-password')."/?guid=".$res[0]['guid']."&type=builder";
                    $email = $res[0]['builder_email'];
                    $responsedata = sendResetPasswordLinkinOTP($link, $email);
                    return response()->json(['status' => 200, 'data' => "Email Sent", 'link'=>$link]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } else {
                $res = DB::table('tbl_admin as ta')->where('admin_email', $request->input('email'))->get();
                if (count($res) >= 1) {
                    $link = URL::asset('reset-password')."/?guid=".$res[0]['guid']."&type=admin";
                    $email = $res[0]['admin_email'];
                    $responsedata = sendResetPasswordLinkinOTP($link, $email);
                    return response()->json(['status' => 200, 'data' => "Email Sent", 'link'=>$link]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email or Password'], 400);
                }
            }
        }
    }

    public function resetPassword(Request $request) {
        $valid = Validator::make($request->all(), [
            "guid" => "required",
            "type" => "required",
            "password" => "required"
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $newdata = array();
            if ($request->input('type') == "salesexecutive") {
                $sales_executive = new SalesExecutive;
                $newdata['sales_executive_password'] = md5($request->input('password'));
                $res = $sales_executive->where('guid', $request->input('guid'))->update($newdata);
                if ($res) {
                    return response()->json(['status' => 200, 'data' => "Password Updated Successfully."]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } elseif ($request->input('type') == "builder") {
                $builder = new Builder;
                $newdata['builder_password'] = md5($request->input('password'));
                $res = $builder->where('guid', $request->input('guid'))->update($newdata);
                if ($res) {
                    return response()->json(['status' => 200, 'data' => "Password Updated Successfully."]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email Or Password'], 400);
                }
            } else {
                $newdata['admin_password'] = md5($request->input('password'));
                $res = DB::table('tbl_admin as ta')->where('guid', $request->input('guid'))->update($newdata);
                if ($res) {
                    return response()->json(['status' => 200, 'data' => "Password Updated Successfully."]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Invalid Email or Password'], 400);
                }
            }
        }
    }

      public function getallloginhistory(Request $request) {
        
            $valid = Validator::make($request->all(), [
                        "limit" => "required|numeric",
                        "offset" => "required|numeric",
                        "user_type" => "required",
            ]);

            if ($valid->fails()) {
                return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
            } else {
                $loginLog = new LoginLog();
                $data = array();
                $data['offset'] = $request->input('offset');
                $data['limit'] = $request->input('limit');
                
                if ($request->has('user_type') && $request->input('user_type') != "") {
                    $data['user_type'] = $request->input('user_type');
                }
                if ($request->has('sortby') && $request->input('sortby') != "") {
                    $data['sortby'] = $request->input('sortby');
                }
                if ($request->has('sorttype') && $request->input('sorttype') != "") {
                    $data['sorttype'] = $request->input('sorttype');
                }
                if ($request->has('login_log_id') && $request->input('login_log_id') != "") {
                    $data['login_log_id'] = $request->input('login_log_id');
                }
                if ($request->has('site_id') && $request->input('site_id') != "") {
                    $data['site_id'] = $request->input('site_id');
                }
                
                $loginLog = $loginLog->getloginlog($data);
                return response()->json(['status' => 200, 'count' => count($loginLog), 'data' => $loginLog]);
            }
        }


}
