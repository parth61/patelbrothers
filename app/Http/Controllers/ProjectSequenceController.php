<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Project;
use App\ProjectAmenities;
use App\ProjectCertificate;
use App\ProjectHighlight;
use App\ProjectSizeRange;
use App\ProjectPhoto;
use App\ProjectBudget;
use App\ProjectConfiguration;
use App\ProjectSalesExecutive;
use App\SalesExecutive;
use App\ProjectConfigurationDetails;
use Storage;

class ProjectSequenceController extends Controller
{
    public function generateSequence() {
		
		$finaldata = array();

		$projectmodel = new Project();
		$projectlist = $projectmodel->where('status',1)->get();
		
		foreach($projectlist as $projectobj){
			
			$ispriority = 1; $isvisible = 1;
			
			/* All checks for project visibility */
			
			if(is_null($projectobj->category_id) && is_null($projectobj->builder_id) && is_null($projectobj->location_id) && is_null($projectobj->project_name) && is_null($projectobj->project_logo) && is_null($projectobj->project_featured_img))
			{
				$isvisible = 0;
			}
			
			$projectsalesexecutive = new ProjectSalesExecutive();
			$salesexecutivelist = $projectsalesexecutive->where('project_id', $projectobj->project_id)->pluck('sales_executive_id');
			
			if(count($salesexecutivelist)>0){
				$salesexecutive = new SalesExecutive();
				$salesexecutiveinfo = $salesexecutive->whereIn('sales_executive_id', $salesexecutivelist)->where('sales_executive_status_id', 1)->where('status', 1)->whereNotnull('sales_executive_photo')->whereNotnull('sales_executive_whatsapp')->whereNotnull('sales_executive_phone')->whereNotnull('sales_executive_email')->get();
				
				if(count($salesexecutiveinfo)==0){
					$isvisible = 0;
				}
				
			}else{
				$isvisible = 0;
			}
			
			/* All checks for project priority */
			
			/*$projectconfig = new ProjectConfiguration();
			$totalconfigurations = $projectconfig->where('project_id', $projectobj->project_id)->where('status', 1)->count();
			
			$projectbudget = new ProjectBudget();
			$totalbudgets = $projectbudget->where('project_id', $projectobj->project_id)->where('status', 1)->count();
			
			$projectsizerange = new ProjectSizeRange();
			$totalsizeranges = $projectsizerange->where('project_id', $projectobj->project_id)->where('status', 1)->count();*/

			$projectconfigurationdetails = new ProjectConfigurationDetails();
			$projectconfigurationdetailslist = $projectconfigurationdetails->where('project_id', $projectobj->project_id)->where('status', 1)->count();
			
			$projectamenitites = new ProjectAmenities();
			$totalamenities = $projectamenitites->where('project_id', $projectobj->project_id)->where('status', 1)->count();
			
			if($projectconfigurationdetailslist == 0 || $totalamenities == 0){
				$ispriority = 0;
			}
			
			/* update final priority and visibility in table */
			
			$updateddata = array();
			$updateddata['isvisible'] = $isvisible;
			$updateddata['ispriority'] = $ispriority;
			
			$projectmodel = new Project();
			$projectmodel->where('project_id', $projectobj->project_id)->update($updateddata);
			
			$temp = array();
			$temp['project_id'] = $projectobj->project_id;
			$temp['isvisible'] = $isvisible;
			$temp['ispriority'] = $ispriority;
			array_push($finaldata, $temp);
			
		}
		
	}
}

