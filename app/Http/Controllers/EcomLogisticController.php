<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomLogistics;
use App\EcomFaq;
use Carbon\Carbon;
use App\Slug;
use Illuminate\Support\Facades\File;

class EcomLogisticController extends Controller
{
    public function addecomlogistic(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",       
            "logistic_name" => "required",
            "logistic_tracking_link" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $ecomlogistics = new EcomLogistics();
            $ecomlogistics->logistic_name = $request->input('logistic_name');
            $ecomlogistics->logistic_tracking_link = $request->input('logistic_tracking_link');
            $ecomlogistics->site_id = $request->input('site_id');
            $ecomlogistics->created_by = $request->input('user_id');
            $ecomlogistics->updated_by = $request->input('user_id');
            $result = $ecomlogistics->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Logistics added sucessfully."]);
            } else {
                return response()->json(['status' => 400, 'data' => "Something went wrong."], 400);
            }
        
        }
    }

    public function getallecomlogistic(Request $request) {

        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
            "site_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomlogistics = new EcomLogistics();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('logistic_id') && $request->input('logistic_id') != "") {
                $data['logistic_id'] = $request->input('logistic_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }

            $ecomlogisticsdata = $ecomlogistics->getecomlogistics($data);
            
            return response()->json(['status' => 200, 'count' =>count($ecomlogisticsdata), 'data' => $ecomlogisticsdata]);
           
        }

    }
    
    public function updateecomlogistic(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "logistic_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomlogistics = new EcomLogistics();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
       
            $newrequest = $request->except(['logistic_id','accesstoken','user_id','site_id']);
          
            $result = $ecomlogistics->where('logistic_id', $request->input('logistic_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Logistic updated succesfully."]);
            } else {
                return response()->json(['status' => 400, 'data' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteecomlogistic(Request $request) {

        $valid = Validator::make($request->all(), [
            "logistic_id" => "required",
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomlogistics = new EcomLogistics();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $newrequest = $request->except(['logistic_id','accesstoken','user_id','site_id']);
            $result = $ecomlogistics->where('logistic_id', $request->input('logistic_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Logistic deleted successfully."]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
