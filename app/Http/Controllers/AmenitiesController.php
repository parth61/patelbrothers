<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Amenities;

class AmenitiesController extends Controller {

    public function addamenities(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "amenities_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Amenities();
            $amenities->amenities_name = $request->input('amenities_name');
            $amenities->created_by = $request->input('created_by');
            $amenities->updated_by = $request->input('updated_by');
            $amenities->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $amenities->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $amenities->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $amenities->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $amenities->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Amenities added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallamenities(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Amenities();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('amenities_id') && $request->input('amenities_id') != "") {
                $data['amenities_id'] = $request->input('amenities_id');
            }
            $amenities = $amenities->getamenities($data);

            return response()->json(['status' => 200, 'count' => count($amenities), 'data' => $amenities]);
        }

    }

    public function updateamenities(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "amenities_id" => "required",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Amenities();
            $newrequest = $request->except(['amenities_id']);
            $result = $amenities->where('amenities_id', $request->input('amenities_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Amenities updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteamenities(Request $request) {

        $valid = Validator::make($request->all(), [
                    "amenities_id" => "required",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Amenities();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['amenities_id']);
            $result = $amenities->where('amenities_id', $request->input('amenities_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

    public function manageamenities(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "amenities_name" => "required",
            "created_by" => "required",
            "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            if($request->has('amenities_id') && $request->filled('amenities_id')){
                $amenities = new Amenities();
                $newrequest = $request->except(['amenities_id']);
                $result = $amenities->where('amenities_id', $request->input('amenities_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Amenities updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $amenities = new Amenities();
                $amenities->amenities_name = $request->input('amenities_name');
                $amenities->created_by = $request->input('created_by');
                $amenities->updated_by = $request->input('updated_by');
                $amenities->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $amenities->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $amenities->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $amenities->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $amenities->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Amenities added sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
            
        }
    }

}
