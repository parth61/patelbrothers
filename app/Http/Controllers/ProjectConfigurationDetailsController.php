<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ProjectConfigurationDetails;
use Validator;
class ProjectConfigurationDetailsController extends Controller
{
    public function addconfigurationdetail(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                    "created_by" => "required",
                    "updated_by" => "required",
                    "budget_min" => "required",
                    "budget_max" => "required",
                    "size_range_min" => "required",
                    "size_range_max" => "required",
                    "project_id" => "required",
                    "configuration_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $configurationdetail = new ProjectConfigurationDetails();
            $query = DB::table('tbl_project_configuration_details as pcd')
            ->select('pcd.*')
            ->where('pcd.status', '=' ,1)
            ->where('pcd.configuration_id', '=' ,$request->input('configuration_id'))
            ->where('pcd.project_id', '=' ,$request->input('project_id'))
			->get();
            if(count($query)==0)
            {
                    $configurationdetail->project_id = $request->input('project_id');
                    $configurationdetail->configuration_id = $request->input('configuration_id');
                    $configurationdetail->budget_min = $request->input('budget_min');
                    $configurationdetail->budget_max = $request->input('budget_max');
                    $configurationdetail->size_range_max = $request->input('size_range_max');
                    $configurationdetail->size_range_min = $request->input('size_range_min');
                    $configurationdetail->created_by = $request->input('created_by');
                    $configurationdetail->updated_by = $request->input('updated_by');
                    $configurationdetail->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $configurationdetail->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $configurationdetail->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $configurationdetail->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $configurationdetail->save();
                    if ($result) {
                        return response()->json(['status' => 200, 'data' => "Configuration Details Added Sucessfully"]);
                    } else {
                        return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                    }
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "You have already added"], 400);
            }
            
           
        }
    }

    public function getallconfigurationdetail(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $configurationdetail = new ProjectConfigurationDetails();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('pcdid') && $request->input('pcdid') != "") {
                $data['pcdid'] = $request->input('pcdid');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $configurationdetail = $configurationdetail->getconfigurationdetail($data);

            return response()->json(['status' => 200, 'count' => count($configurationdetail), 'data' => $configurationdetail]);
        }
    }

    public function updateconfigurationdetail(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "pcdid" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $configurationdetail = new ProjectConfigurationDetails();
            $newrequest = $request->except(['pcdid']);
            $result = $configurationdetail->where('pcdid', $request->input('pcdid'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "configurationdetail Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteconfigurationdetail(Request $request) {
        $valid = Validator::make($request->all(), [
                    "pcdid" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $configurationdetail = new ProjectConfigurationDetails();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['pcdid']);
            $result = $configurationdetail->where('pcdid', $request->input('pcdid'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
