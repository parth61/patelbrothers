<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\ProjectBudget;
class ProjectBudgetController extends Controller
{
    public function addprojectbudget(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_id" => "required",
                    "budget_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectbudget = new ProjectBudget();
            $projectbudget->project_id = $request->input('project_id');
            $projectbudget->budget_id = $request->input('budget_id');
            $projectbudget->created_by = $request->input('created_by');
            $projectbudget->updated_by = $request->input('updated_by');
            $projectbudget->status = $request->input('status');
            $projectbudget->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $projectbudget->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $projectbudget->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $projectbudget->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $projectbudget->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Project budget Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function getallprojectbudget(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectbudget = new ProjectBudget();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('project_budget_id') && $request->input('project_budget_id') != "") {
                $data['project_budget_id'] = $request->input('project_budget_id');
            }
            if ($request->has('budget_id') && $request->input('budget_id') != "") {
                $data['budget_id'] = $request->input('budget_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $projectbudget = $projectbudget->getprojectbudget($data);
            return response()->json(['status' => 200, 'count' => count($projectbudget), 'data' => $projectbudget]);
        }
    }
    public function getallprojectbudgetid(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "project_id" => "required|numeric"
                   
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectbudget = new ProjectBudget();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
           
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            $projectbudget = $projectbudget->getprojectbudgetid($data);
            return response()->json(['status' => 200, 'count' => count($projectbudget), 'data' => $projectbudget]);
        }
    }
    
    public function updateprojectbudget(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "project_budget_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectbudget = new ProjectBudget();
            $newrequest = $request->except(['project_budget_id']);
            $result = $projectbudget->where('project_budget_id', $request->input('project_budget_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "projectbudget Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
    public function deleteprojectbudget(Request $request) {
        $valid = Validator::make($request->all(), [
            "project_budget_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $projectbudget = new ProjectBudget();
            $result = $projectbudget->where('project_budget_id', $request->input('project_budget_id'))->delete();
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function manageprojectbudget(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "project_id" => "required",
            "budget_id" => "required",
            "created_by" => "required",
            "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->has('project_budget_id') && $request->filled('project_budget_id')){
                $projectbudget = new ProjectBudget();
                $projectbudget->project_id = $request->input('project_id');
                $projectbudget->budget_id = $request->input('budget_id');
                $projectbudget->created_by = $request->input('created_by');
                $projectbudget->updated_by = $request->input('updated_by');
                $projectbudget->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $projectbudget->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $projectbudget->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $projectbudget->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $projectbudget->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Project budget Added Sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $projectbudget = new ProjectBudget();
                $newrequest = $request->except(['project_budget_id']);
                $result = $projectbudget->where('project_budget_id', $request->input('project_budget_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "projectbudget Updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
        }
    }
}
