<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomProductAttributeTerm;
use App\EcomProductAttribute;
use App\EcomProductVariantAttributeTerm;
use Carbon\Carbon;
use DB;
class EcomProductAttributeTermController extends Controller
{
    public function addproductattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required",
            "attribute_term_id" => "required"
                 
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $productattributeterm = new EcomProductAttributeTerm();
            $productattributetermdata = $productattributeterm->where('product_id', $request->input('product_id'))->where('status', 1)->get();

            if(count($productattributetermdata) == 0){

                $checkcount = count($productattributeterm->where('attribute_term_id', $request->input('attribute_term_id'))->where('attribute_id', $request->input('attribute_id'))->where('product_id', $request->input('product_id'))->where('status', '1')->get());

                if ($checkcount === 0) {
                    $productattributeterm->attribute_id = $request->input('attribute_id');
                    $productattributeterm->product_id = $request->input('product_id');
                    $productattributeterm->attribute_term_id = $request->input('attribute_term_id');
                    $productattributeterm->created_by = $request->input('user_id');
                    $productattributeterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                    $productattributeterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                    $productattributeterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                    $productattributeterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $result = $productattributeterm->save();
                    if ($result) {
                        return response()->json(['status' => 200, 'data' => "Product attribute added sucessfully"]);
                    } else {
                        return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                    }
                }else{
                    return response()->json(['status' => 400, 'error' => "Please remove existing variations."], 400);
                }
            
            } else {
               return response()->json(['status' => 400, 'error' => "Already Exist."], 400);
            }

        }
    }
    public function manageproductattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "product_id" => "required",
            "attributetermlist" => "required",
        
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
          
            //dynamic entries in tbl_product_attribute_term
            if ($request->has('attributetermlist')) {

                 $attributetermlist = $request->input('attributetermlist');

                if (count($attributetermlist) > 0) {

                    foreach($attributetermlist as $item)
                    {
                        //update attribute table
                        $attribute_id=$item['attribute_id'];
                        $is_visible_on_product_page=$item['is_visible_on_product_page'];
                        $use_for_variation=$item['use_for_variation'];
                        $product_id=$request->input('product_id');
                        $newrequest = array();
                        if($is_visible_on_product_page!="" && $is_visible_on_product_page!=null)
                        {
                            $newrequest['is_visible_on_product_page'] = $is_visible_on_product_page;
                        }
                      
                        if($use_for_variation!="" && $use_for_variation!=null )
                        {
                            $newrequest['use_for_variation'] = $use_for_variation;
                            if($use_for_variation==0)
                            {
                                           //code for getting variant products id
                                $variableproductidlist = DB::table('tbl_ecom_product_variant as provariant')
                                ->where('provariant.status', '=', 1)
                                ->where('provariant.product_id', '=', $product_id)
                                ->pluck('provariant.variant_product_id');

                                if(count($variableproductidlist)>0)
                                {
                                         $attribute_id=$item['attribute_id'];

                                         $productvariantattributeterm = new EcomProductVariantAttributeTerm();
                                       
                                         //delete product variant attribute term
                                        $productvariantattributeterm->where('attribute_id', $attribute_id)
                                                ->whereIn('variant_product_id', $variableproductidlist)
                                        ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                                }
                               
                            }

                        }
                        
                        $productattribute = new EcomProductAttribute();
                        //update product attribute table(flags)
                        $newrequest['updated_by'] = $request->input('user_id');
                        $result = $productattribute->where('product_id', $product_id)
                        ->where('attribute_id', $attribute_id)
                        ->update($newrequest);
                        if(count($item)>0){
                            //check attribute term are there in attribute 
                            if (array_key_exists("attributetermlist",$item))
                             {

                                        //dynamic entries of product attribute term
                                        $attributetermlist =$item['attributetermlist'];
                                        if (count($attributetermlist) > 0) {
                                            $productattributeterm = new EcomProductAttributeTerm();
                                            
                                            $newrequest1 = array();
                                            $newrequest1['status'] = 0;
                                            $newrequest1['updated_by'] = $request->input('user_id');

                                            $result = $productattributeterm->where('attribute_id',$attribute_id)->where('product_id', $product_id)->whereNotIn('attribute_term_id', $attributetermlist)->update($newrequest1);

                                            // $productattributeterm->where('attribute_id',$attribute_id)->where('product_id', $product_id)->whereNotIn('attribute_term_id', $attributetermlist)->delete();
                                            foreach($attributetermlist as $item1)
                                            {
                                                $checkcount = count($productattributeterm->where('attribute_id', $attribute_id)->where('attribute_term_id', $item1)->where('product_id', $product_id)->where('status', '1')->get());
                                                if ($checkcount === 0) {
                                                    $productattributeterm = new EcomProductAttributeTerm();
                                                    $productattributeterm->attribute_id = $attribute_id;
                                                    $productattributeterm->product_id = $product_id;
                                                    $productattributeterm->attribute_term_id = $item1;
                                                    $productattributeterm->created_by = $request->input('user_id');
                                                    $productattributeterm->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                                    $productattributeterm->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                                    $productattributeterm->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                                    $productattributeterm->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                                    $result = $productattributeterm->save();
                                                }
                                            }
                                        
                                        } else {
                                        $productattributeterm = new EcomProductAttributeTerm();
                                        $productattributeterm->where('product_id',  $product_id)->where('attribute_id', $attribute_id)
                                                    ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                                            //  $productattributeterm->where('product_id', $product_id)->where('attribute_id', $attribute_id)->delete();
                                        }
                            }
                            else
                            {
                                $productattributeterm = new EcomProductAttributeTerm();
                                $productattributeterm->where('product_id',  $product_id)->where('attribute_id', $attribute_id)
                                            ->update(array('status' => 0,'updated_by' =>  $request->input('user_id'),'updated_at' =>Carbon::now()->toDateTimeString())); 
                             //   return response()->json(['status' => 400, 'error' => "no such key exist"]);
                                return response()->json(['status' => 400, 'error' => "Please select atlease one attribute term"], 400);
                            }

                        }
                      
                    }
                    return response()->json(['status' => 200, 'data' => "Product attribute term added sucessfully"]);
                }else{
                    
                }
            }
            else{
                    return response()->json(['status' => 400, 'error' => "Please select atlease one attribute term"], 400);
            }

             /* 
                if (count($attributetermlist) > 0) {
                    foreach ($attributetermlist as $tmmp) {
                        $productattributeterm = new EcomProductAttributeTerm();
                        $checkcount = count($productattributeterm->where('attribute_id', $tmmp['attribute_id'])->where('attribute_term_id', $tmmp['attribute_term_id'])->where('product_id', $request->input('product_id'))->where('status', '1')->get());
                        if ($checkcount === 0) {
                            $productattributeterm->attribute_id = $tmmp['attribute_id'];
                            $productattributeterm->product_id =$request->input('product_id');
                            $productattributeterm->attribute_term_id = $tmmp['attribute_term_id'];
                            $productattributeterm->save();
                        }
                    }
                } 
            */
        
            
        }
    }   

    public function getallproductattributeterm(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $productattributeterm = new EcomProductAttributeTerm();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('product_attribute_term_id') && $request->input('product_attribute_term_id') != "") {
                $data['product_attribute_term_id'] = $request->input('product_attribute_term_id');
            }
            if ($request->has('attribute_term_id') && $request->input('attribute_term_id') != "") {
                $data['attribute_term_id'] = $request->input('attribute_term_id');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('product_id') && $request->input('product_id') != "") {
                $data['product_id'] = $request->input('product_id');
            }
            $productattributeterm = $productattributeterm->getproductattributeterm($data);

            return response()->json(['status' => 200, 'count' => count($productattributeterm), 'data' => $productattributeterm]);
        }

    }

    public function updateproductattributeterm(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "product_attribute_term_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $productattributeterm = new EcomProductAttributeTerm();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_attribute_term_id','user_id','accesstoken']);
            $result = $productattributeterm->where('product_attribute_term_id', $request->input('product_attribute_term_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Product attribute term updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deleteproductattributeterm(Request $request) {

        $valid = Validator::make($request->all(), [
                    "product_attribute_term_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $productattributeterm = new EcomProductAttributeTerm();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['product_attribute_term_id','site_id','user_id','accesstoken']);
            $result = $productattributeterm->where('product_attribute_term_id', $request->input('product_attribute_term_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

}
