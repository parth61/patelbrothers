<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Configuration;

class ConfigurationController extends Controller {

    public function addconfiguration(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "configuration_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Configuration();
            $amenities->configuration_name = $request->input('configuration_name');
            $amenities->created_by = $request->input('created_by');
            $amenities->updated_by = $request->input('updated_by');
            $amenities->status = $request->input('status');
            $amenities->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $amenities->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $amenities->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $amenities->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $amenities->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Configuration added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallconfiguration(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Configuration();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('configuration_id') && $request->input('configuration_id') != "") {
                $data['configuration_id'] = $request->input('configuration_id');
            }

            $amenities = $amenities->getconfiguration($data);

            return response()->json(['status' => 200, 'count' => count($amenities), 'data' => $amenities]);
        }
    }

    public function updateconfiguration(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "configuration_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Configuration();
            $newrequest = $request->except(['configuration_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $amenities->where('configuration_id', $request->input('configuration_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Configuration updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteconfiguration(Request $request) {
        $valid = Validator::make($request->all(), [
            "configuration_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $amenities = new Configuration();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['configuration_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $amenities->where('configuration_id', $request->input('configuration_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function manageconfiguration(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "configuration_name" => "required",
                    "created_by" => "required",
                    "updated_by" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            if($request->has('configuration_id') && $request->filled('configuration_id')){
                $configuration = new Configuration();
                $newrequest = $request->except(['configuration_id']);
                 $request->request->add(['updated_by' =>  $request->input('updated_by')]);
                $result = $configuration->where('configuration_id', $request->input('configuration_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Configuration updated succesfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }else{
                $configuration = new Configuration();
                $configuration->configuration_name = $request->input('configuration_name');
                $configuration->created_by = $request->input('created_by');
                $configuration->updated_by = $request->input('updated_by');
                $configuration->status = $request->input('status');
                $configuration->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                $configuration->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                $configuration->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                $configuration->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                $result = $configuration->save();
                if ($result) {
                    return response()->json(['status' => 200, 'data' => "Configuration added sucessfully"]);
                } else {
                    return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
                }
            }
        }
    }

}
