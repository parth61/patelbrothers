<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class NewsletterSubscribe extends Model
{  
 
    protected $table = 'tbl_newsletter_subscribe';   
    protected $fillable=['newsletter_subscriber_id','email','status','site_id','created_at','updated_at','browser_name','browser_version','browser_platform','ip_address'];

    public static function getnewsletter($data)
    {
         
         $query = DB::table('tbl_newsletter_subscribe as newsletter')->select('newsletter.*');
         
         if (array_key_exists('newsletter_subscriber_id', $data) && isset($data['newsletter_subscriber_id'])) {
            $query = $query->where('newsletter_subscriber_id', '=' ,$data['newsletter_subscriber_id']);
           }
                          
         if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('status', '=' ,$data['status']);
           }
           if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('order_by', $data) && isset($data['order_by'])) {
            $query = $query->orderBy('newsletter_subscriber_id',$data['order_by'] );
        }
                          
         
                  
         
        
         $result = $query->get();
                            
         return $result;
    }
}
