<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Leadstatusprimary;

class LeadstatusprimaryController extends Controller
{
    

     public function addleadstatusprimary(Request $request) {
        //validations
        $leadstatusprimary = new Leadstatusprimary();
        $valid = Validator::make($request->all(), [
                    "lead_status_primary_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $leadstatusprimary->lead_status_primary_name = $request->input('lead_status_primary_name');
            $leadstatusprimary->created_by = $request->input('created_by');
            $leadstatusprimary->updated_by = $request->input('updated_by');
            $leadstatusprimary->status = $request->input('status');
            $leadstatusprimary->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $leadstatusprimary->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $leadstatusprimary->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $leadstatusprimary->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $leadstatusprimary->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadstatusprimary added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallleadstatusprimary(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadstatusprimary = new Leadstatusprimary();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $leadstatusprimarydata = $leadstatusprimary->getleadstatusprimary($data);
            
            return response()->json(['status' => 200, 'count' => count($leadstatusprimarydata), 'data' => $leadstatusprimarydata]);
        }
    }
    public function updateleadstatusprimary(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_status_primary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
              $leadstatusprimary = new Leadstatusprimary();
            $newrequest = $request->except(['lead_status_primary_id']);
             $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $leadstatusprimary->where('lead_status_primary_id', $request->input('lead_status_primary_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadstatusprimary Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteleadstatusprimary(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_status_primary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadstatusprimary = new Leadstatusprimary();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['lead_status_primary_id']);
            $result = $leadstatusprimary->where('lead_status_primary_id', $request->input('lead_status_primary_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
