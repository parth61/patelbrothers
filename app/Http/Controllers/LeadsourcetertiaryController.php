<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\File;
use App\Leadsourcetertiary;

class LeadsourcetertiaryController extends Controller
{
    
     public function addleadsourcetertiary(Request $request) {
        //validations
        $leadsourcetertiary = new Leadsourcetertiary();
        $valid = Validator::make($request->all(), [
                    "lead_source_tertiary_name" => "required",
                    "created_by" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $leadsourcetertiary->lead_source_tertiary_name = $request->input('lead_source_tertiary_name');
            $leadsourcetertiary->lead_source_secondary_id = $request->input('lead_source_secondary_id');
            $leadsourcetertiary->lead_source_primary_id = $request->input('lead_source_primary_id');
            $leadsourcetertiary->status = $request->input('status');
             $leadsourcetertiary->created_by = $request->input('created_by');
            $leadsourcetertiary->updated_by = $request->input('updated_by');
            $leadsourcetertiary->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $leadsourcetertiary->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $leadsourcetertiary->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $leadsourcetertiary->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $leadsourcetertiary->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadsourcetertiary added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    //Get all admin code
    public function getallleadsourcetertiary(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadsourcetertiary = new Leadsourcetertiary();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            if ($request->has('lead_source_secondary_id') && $request->input('lead_source_secondary_id') != "") {
                $data['lead_source_secondary_id'] = $request->input('lead_source_secondary_id');
            }
            if ($request->has('lead_source_primary_id') && $request->input('lead_source_primary_id') != "") {
                $data['lead_source_primary_id'] = $request->input('lead_source_primary_id');
            }
            $leadsourcetertiarydata = $leadsourcetertiary->getleadsourcetertiary($data);
            
            return response()->json(['status' => 200, 'count' => count($leadsourcetertiarydata), 'data' => $leadsourcetertiarydata]);
        }
    }
    public function updateleadsourcetertiary(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "lead_source_tertiary_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadsourcetertiary = new Leadsourcetertiary();
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $newrequest = $request->except(['lead_source_tertiary_id']);
            $result = $leadsourcetertiary->where('lead_source_tertiary_id', $request->input('lead_source_tertiary_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Leadsourcetertiary Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function deleteleadsourcetertiary(Request $request) {
        $valid = Validator::make($request->all(), [
                    "lead_source_tertiary_id" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $leadsourcetertiary = new Leadsourcetertiary();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['lead_source_tertiary_id']);
            $result = $leadsourcetertiary->where('lead_source_tertiary_id', $request->input('lead_source_tertiary_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
