<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Role;
use Carbon\Carbon;
class RoleController extends Controller
{
    public function addrole(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "role_name" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $role = new Role();
            $role->role_name = $request->input('role_name');
            $role->created_by = $request->input('user_id');
            $role->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $role->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $role->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $role->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $role->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Role Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
      public function getallrole(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $role = new Role();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            
            if ($request->has('role_id') && $request->input('role_id') != "") {
                $data['role_id'] = $request->input('role_id');
            }
            $role = $role->getrole($data);
            return response()->json(['status' => 200, 'count' => count($role), 'data' => $role]);
        }
    }
    
     public function updaterole(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "role_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $role = new Role();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['role_id','user_id','accesstoken']);
            
            $result = $role->where('role_id', $request->input('role_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Role Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
     public function deleterole(Request $request) {
        $valid = Validator::make($request->all(), [
                    "role_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $category = new Role();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['role_id','accesstoken','user_id']);
            $result = $category->where('role_id', $request->input('role_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
    
    
}
