<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EcomCategory;
use App\EcomProductCategory;
use Carbon\Carbon;
use App\Slug;
use App\EcomBusinessSetting;
use Illuminate\Support\Facades\File;

class EcomCategoryController extends Controller
{
    public function addcategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",       
            "category_name" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           
            $category = new EcomCategory();
            $category->category_name = $request->input('category_name');
            $category->category_description = $request->input('category_description');
            $category->tax_id = $request->has('tax_id')?$request->input('tax_id'):null;
            $category->tax_percentage = $request->input('tax_percentage');
            if ($request->has('category_slug') && $request->input('category_slug') != "")
            {
                $slug_name =$request->input('category_slug');
            }
            else
            {
                $slug_name =$request->input('category_name');
            }

            $slug_name=checkslugavailability($slug_name, "productcategory",$request->input('site_id'));
          
            /*$slug = new Slug();
            $slug->slug_name = $slug_name;
            $slug->slug_type ="productcategory";
            $slug->site_id =$request->input('site_id');
            $slug->created_by = $request->input('user_id');
            $result = $slug->save();
            if($result)
            {
                $slug_id=$slug->id;
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Something went wrong in adding slug."], 400);
            }
           
            $category->slug_id = $slug_id;*/
            $category->slug_name = $slug_name;
            $category->site_id = $request->input('site_id');
            $category->parent_category_id = $request->input('parent_category_id');
            $category->category_thumbnail = $request->input('category_thumbnail');
            $category->created_by = $request->input('user_id');
            $category->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $category->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $category->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $category->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $category->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Category added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        
        }
    }

    public function getallcategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $category = new EcomCategory();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('category_id') && $request->input('category_id') != "") {
                $data['category_id'] = $request->input('category_id');
            }

            if ($request->has('slug_name') && $request->input('slug_name') != "") {
                $data['slug_name'] = $request->input('slug_name');
            }

            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            if ($request->has('parent_category_id') && $request->input('parent_category_id') != "") {
                $data['parent_category_id'] = $request->input('parent_category_id');
            }
            
            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $categorys = $category->getcategory($data);
            if($categorys)
            {
                $categorydata = array();
                foreach ($categorys as $item) {

                    //FOR GETTING PARENT CATEGORY NAME
                   
                    $parent_category_id=$item->parent_category_id;
                    if( $parent_category_id==0)
                    {
                        $item->parent_category_name ="Master Category";
                    }
                    else
                    { 
                        
                        $category = new EcomCategory();

                        $categoryname=$category->where('category_id',$parent_category_id)->where('status', '1')->get();
                       if(count($categoryname)>0)
                        {
                            $item->parent_category_name =$categoryname[0]->category_name;
                       }
                       else
                        {
                            $item->parent_category_name =null;
                        }
                     

                    }
                   
                    if($item->cat_media_file==""||$item->cat_media_file==null||$item->cat_media_file=="null")
                    {
                        $item->category_thumbnail_path =fixedcategorythumbnailimage;
                    }
                    else
                    {
                        if(fileuploadtype=="local")
                        {
                              if (File::exists(baseimagedisplaypath .$item->cat_media_file)) {
                                    $item->category_thumbnail_path = imagedisplaypath.$item->cat_media_file;
                                } else {
                                    $item->category_thumbnail_path =fixedcategorythumbnailimage;
                                }
                            
                        }
                        else
                        {
                            if (does_url_exists(imagedisplaypath. $item->cat_media_file)) {
                                $item->category_thumbnail_path = imagedisplaypath.$item->cat_media_file;
                            } else {
                                $item->category_thumbnail_path = fixedcategorythumbnailimage;
                            }
                        }
                    }  
                    array_push($categorydata, $item);
                }

                return response()->json(['status' => 200, 'count' =>count($categorys), 'data' => $categorydata]);

            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
        }

    }

    public function getallcategorywithcount(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $ecomproductcategory = new EcomProductCategory();
            $businesssettings = new EcomBusinessSetting();

            $data = array();
            $data['offset'] = 0;
            $data['limit'] = 1;
            $data['site_id'] = $request->input('site_id');
            $businessdata = $businesssettings->getbusinesssetting($data);

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if (count($businessdata)>0) {
                $data['is_variation'] = $businessdata[0]->is_show_variation;
            }

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }
            
            if ($request->has('has_variation') ) {
                $data['has_variation'] = $request->input('has_variation');
            }

            $categorys = $ecomproductcategory->getallcategorywithcount($data);
            
            foreach ($categorys as $item) {

                //FOR GETTING PARENT CATEGORY NAME
               
               
               
                if($item->cat_media_file==""||$item->cat_media_file==null||$item->cat_media_file=="null")
                {
                    $item->category_thumbnail_path =fixedcategorythumbnailimage;
                }
                else
                {
                    if(fileuploadtype=="local")
                    {
                          if (File::exists(baseimagedisplaypath .$item->cat_media_file)) {
                                $item->category_thumbnail_path = imagedisplaypath.$item->cat_media_file;
                            } else {
                                $item->category_thumbnail_path =fixedcategorythumbnailimage;
                            }
                        
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item->cat_media_file)) {
                            $item->category_thumbnail_path = imagedisplaypath. $item->cat_media_file;
                        } else {
                            $item->category_thumbnail_path = fixedcategorythumbnailimage;
                        }
                    }
                }  
                
                
            }

            if($categorys)
            {
                return response()->json(['status' => 200, 'count' =>count($categorys), 'data' => $categorys]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
        }

    }

    public function updatecategory(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "category_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
           $category = new EcomCategory();
           $request->request->add(['updated_by' =>  $request->input('user_id')]);
           // $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
       
            $newrequest = $request->except(['category_id','accesstoken','user_id','slug_name']);

            if($request->has('slug_name')){
                $category = $category->where('category_id', $request->input('category_id'))->first();
                if($category->slug_name != $request->input('slug_name')){
                    $newrequest['slug_name'] = checkslugavailability($request->input('slug_name'), "productcategory",$request->input('site_id'));
                }
            }
          
            $result = $category->where('category_id', $request->input('category_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Category updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletecategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "category_id" => "required",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $category = new EcomCategory();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['category_id','accesstoken','user_id']);
            $result = $category->where('category_id', $request->input('category_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Category deleted successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }


    public function getmenucategory(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $ecomcategory = new EcomCategory();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('site_id') && $request->input('site_id') != "" && $request->input('site_id') != "0") {
                $data['site_id'] = $request->input('site_id');
            }

            $categorydata = $ecomcategory->getmenucategory($data);
            
            if($categorydata)
            {
                return response()->json(['status' => 200, 'count' =>count($categorydata), 'data' => $categorydata]);
            }
            else
            {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
        }

    }
}
