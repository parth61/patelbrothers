<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Carbon\Carbon;
use App\MenuItem;
class MenuItemController extends Controller
{
    public function addmenuitem(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required",
            "menuitem_name"=>"required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            
            $menuitem = new MenuItem();
            $menuitem->menuitem_name = $request->input('menuitem_name');
            $menuitem->menuitem_target = $request->input('menuitem_target');
            $menuitem->menuitem_title = $request->input('menuitem_title'); 
            $menuitem->menuitem_url = $request->input('menuitem_url'); 
            $menuitem->menu_id = $request->input('menu_id');
            $menuitem->created_by = $request->input('user_id');
            $menuitem->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $menuitem->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $menuitem->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $menuitem->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $menuitem->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "menuitem Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallmenuitem(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menuitem = new MenuItem();
            $data = array();
            
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }

            if ($request->has('menuitem_id') && $request->input('menuitem_id') != "") {
                $data['menuitem_id'] = $request->input('menuitem_id');
            }

            if ($request->has('menu_id') && $request->input('menu_id') != "") {
                $data['menu_id'] = $request->input('menu_id');
            }
          
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }

            if ($request->has('sortby') && $request->input('sortby') != "") {
                $data['sortby'] = $request->input('sortby');
            }
            if ($request->has('sorttype') && $request->input('sorttype') != "") {
                $data['sorttype'] = $request->input('sorttype');
            }
            $menuitem = $menuitem->getmenuitem($data);
            if($menuitem)
            {
                
                return response()->json(['status' => 200, 'count' => count($menuitem), 'data' => $menuitem]);
            }
            else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
           
            
        }

    }

    public function updatemenuitem(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "menuitem_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menuitem = new MenuItem();
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['menuitem_id','user_id','accesstoken']);
            $result = $menuitem->where('menuitem_id', $request->input('menuitem_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "menuitem Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletemenuitem(Request $request) {

        $valid = Validator::make($request->all(), [
                    "menuitem_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $menuitem = new MenuItem();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['menuitem_id','user_id','accesstoken']);
            $result = $menuitem->where('menuitem_id', $request->input('menuitem_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Post Tag Deleted Successfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }
}
