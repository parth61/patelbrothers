<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\SalesExecutive;
use App\Builder;
use App\ProjectSalesExecutive;
use DB;
use Illuminate\Support\Facades\File;

class SalesExecutiveController extends Controller {

    public function addsalesexecutive(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "sales_executive_firstname" => "required",
                    "sales_executive_lastname" => "required",
                    "sales_executive_email" => "required|unique:tbl_sales_executive,sales_executive_email",
                    "builder_id" => "required",
                    "created_by" => "required",
                    "updated_by" => "required"

        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $SalesExecutive = new SalesExecutive();

            $sales_executive_photo_base64 = $request->input('sales_executive_photo');
            if ($sales_executive_photo_base64 != "" && $sales_executive_photo_base64 != null && $sales_executive_photo_base64 != "null") {
                $imgpath = fileupload($sales_executive_photo_base64);
            }
            $SalesExecutive->sales_executive_firstname = $request->input('sales_executive_firstname');
            $SalesExecutive->sales_executive_lastname = $request->input('sales_executive_lastname');
            $SalesExecutive->sales_executive_password = md5($request->input('sales_executive_password'));
            $SalesExecutive->sales_executive_email = $request->input('sales_executive_email');
            $SalesExecutive->sales_executive_status_id = 1;
            $SalesExecutive->created_by = $request->input('created_by');
            $SalesExecutive->updated_by = $request->input('updated_by');
            $SalesExecutive->builder_id = $request->input('builder_id');
            $SalesExecutive->guid = generateAccessToken(16);
            $SalesExecutive->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $SalesExecutive->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $SalesExecutive->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $SalesExecutive->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $SalesExecutive->save();

            if ($request->has('projectlist')) {
                $projectlist = json_decode($request->input('projectlist'));
                if (count($projectlist) > 0) {
                    for ($i = 0; $i < count($projectlist); $i++) {
                        $projectsalesexecutive = new ProjectSalesExecutive();
                        $checkcount = count($projectsalesexecutive->where('project_id', $projectlist[$i])->where('sales_executive_id', $SalesExecutive->id)->where('status', '1')->get());
                        if ($checkcount === 0) {
                            $projectsalesexecutive->project_id = $projectlist[$i];
                            $projectsalesexecutive->created_by = $request->input('created_by');
                            $projectsalesexecutive->updated_by = $request->input('updated_by');
                            $projectsalesexecutive->sales_executive_id = $SalesExecutive->id;
                            $projectsalesexecutive->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                            $projectsalesexecutive->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                            $projectsalesexecutive->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                            $projectsalesexecutive->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                            $projectsalesexecutive->save();
                        }
                    }
                    return response()->json(['status' => 200, 'data' => $SalesExecutive->guid]);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Something went wrong']);
                }
            }
            if ($result) {
                return response()->json(['status' => 200, 'data' => $SalesExecutive->guid]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong"], 400);
            }
        }
    }

    public function getsalesexecutivebyid($salesexecutive_id) {
        $data = array();
        $query = DB::table('tbl_sales_executive as se')->select('se.*', 'tbl_builder.*', 'ses.sales_executive_status_name')
                ->leftJoin('tbl_builder', 'tbl_builder.builder_id', '=', 'se.builder_id')
                ->leftJoin('tbl_sales_executive_status as ses', 'ses.sales_executive_status_id', '=', 'se.sales_executive_status_id')
                ->where('se.status', '=', 1)
                ->where('se.guid', '=', $salesexecutive_id)
                ->get();
        $salesexecutivedata = array();
        foreach ($query as $item) {
            if ($item->sales_executive_photo == NULL || $item->sales_executive_photo == "") {
                $item->salesexecutivepath = fixedimagepathsalesexecutive;
            } else {
                if (does_url_exists(mediapath . $item->sales_executive_photo)) {
                    $item->salesexecutivepath = mediapath . $item->sales_executive_photo;
                } else {
                    $item->salesexecutivepath = fixedimagepathsalesexecutive;
                }
            }

            if ($item->builder_featured_img == NULL || $item->builder_featured_img == "") {
                $item->builder_featured_img = fixedimagepathbuilderfeaturedimage;
            } else {
                if (does_url_exists(mediapath . $item->builder_featured_img)) {
                    $item->builder_featured_img = mediapath . $item->builder_featured_img;
                } else {
                    $item->builder_featured_img = fixedimagepathbuilderfeaturedimage;
                }
            }

            $item->project_list = DB::table('tbl_project_sales_executive as tpse')
                    ->leftJoin('tbl_project as tp','tp.project_id','=','tpse.project_id')
                    ->where('tpse.sales_executive_id', '=',$item->sales_executive_id)
                    ->where('tpse.status', 1)
                    ->pluck('tp.project_name');
                    
            array_push($salesexecutivedata, $item);
        }

        $data['salesexecutivedata'] = $salesexecutivedata;
        return view('editsalesexecutive', $data);
    }

    public function getallsalesexecutive(Request $request) {


        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesExecutive = new SalesExecutive();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('sales_executive_id') && $request->input('sales_executive_id') != "") {
                $data['sales_executive_id'] = $request->input('sales_executive_id');
            }

            if ($request->has('builder_id') && $request->input('builder_id') != "") {
                $data['builder_id'] = $request->input('builder_id');
            }

            $salesExecutive = $salesExecutive->getsalesexecutive($data);

            if ($salesExecutive) {
                $salesExecutivedata = array();
                foreach ($salesExecutive as $item) {
                    if ($item->sales_executive_photo == NULL || $item->sales_executive_photo == "") {
                        $item->salesexecutivepath = fixedimagepathsalesexecutive;
                    } else {
                        if (does_url_exists(mediapath . $item->sales_executive_photo)) {
                            $item->salesexecutivepath = mediapath . $item->sales_executive_photo;
                        } else {
                            $item->salesexecutivepath = fixedimagepathsalesexecutive;
                        }
                    }
                    array_push($salesExecutivedata, $item);
                }

                return response()->json(['status' => 200, 'count' => count($salesExecutive), 'data' => $salesExecutivedata]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updatesalesexecutive(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "sales_executive_id" => "required",
                    "updated_by" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesExecutive = new SalesExecutive();
            $imgpath = null;
            $sales_executive_id = $request->input('sales_executive_id');


            $sales_executive_photo_base64 = $request->input('sales_executive_photo');
            if ($sales_executive_photo_base64 != "" && $sales_executive_photo_base64 != null && $sales_executive_photo_base64 != "null") {
                $imgpath = fileupload($sales_executive_photo_base64);
                $query = DB::table('tbl_sales_executive')->select('tbl_sales_executive.sales_executive_photo')->where('sales_executive_id', $sales_executive_id)->get();
                if (count($query) > 0) {
                    $img = $query[0]->sales_executive_photo;
                    if ($img != NULL || $img != "") {
                        if (does_url_exists(mediapath . '/' . $img)) {
                            $path = mediapath . '/' . $img;
                        //    unlink($path);
                        }
                    }
                }
            }


            $newrequest = $request->except(['builder_id','sales_executive_id', 'projectlist', 'sales_executive_photo','sales_executive_password']);
            if($request->input('sales_executive_password')!="" && $request->input('sales_executive_password')!=null)
            {
                $newrequest['sales_executive_password']=md5($request->input('sales_executive_password'));
            }

            if ($sales_executive_photo_base64 != "" && $sales_executive_photo_base64 != null) {
                $newrequest['sales_executive_photo'] = $imgpath;
            }

            $result = $salesExecutive->where('sales_executive_id', $sales_executive_id)->update($newrequest);
            if ($result) {
                //update project_salesexecutive table
                if ($request->has('projectlist')) {
                    $projectlist = json_decode($request->input('projectlist'));
                    if (count($projectlist) > 0) {
                        $projectsalesexecutive = new ProjectSalesExecutive();
                        $projectsalesexecutive->where('sales_executive_id', $sales_executive_id)->whereNotIn('project_id', $projectlist)->delete();
                        for ($i = 0; $i < count($projectlist); $i++) {
                            $checkcount = count($projectsalesexecutive->where('project_id', $projectlist[$i])->where('sales_executive_id', $request->input('sales_executive_id'))->where('status', '1')->get());
                            if ($checkcount === 0) {
                                $projectsalesexecutive = new ProjectSalesExecutive();
                                $projectsalesexecutive->project_id = $projectlist[$i];
                                $projectsalesexecutive->updated_by = $request->input('updated_by');
                                $projectsalesexecutive->sales_executive_id = $request->input('sales_executive_id');
                                $projectsalesexecutive->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $projectsalesexecutive->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $projectsalesexecutive->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $projectsalesexecutive->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $projectsalesexecutive->save();
                            }
                        }
                    } else {
                        $projectsalesexecutive = new ProjectSalesExecutive();
                        $projectsalesexecutive->where('sales_executive_id', $request->input('sales_executive_id'))->delete();
                    }
                }
                return response()->json(['status' => 200, 'data' => "Sales Executive Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletesalesexecutive(Request $request) {
        $valid = Validator::make($request->all(), [
                    "sales_executive_id" => "required",
                    "updated_by" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesexecutive = new SalesExecutive();
            $sales_executive_id = $request->input('sales_executive_id');
            $query = DB::table('tbl_sales_executive')->select('tbl_sales_executive.sales_executive_photo')->where('sales_executive_id', $sales_executive_id)->get();
            if (count($query) > 0) {
                $img = $query[0]->sales_executive_photo;
                if ($img != NULL || $img != "") {
                    if (does_url_exists(mediapath . $img)) {
                        $path = mediapath . $img;
                       // unlink($path);
                    }
                }
            }

            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['sales_executive_id']);
            $result = $salesexecutive->where('sales_executive_id', $request->input('sales_executive_id'))->update($newrequest);
            if ($result) {
                $projectsalesexecutive = new ProjectSalesExecutive();
                $projectsalesexecutive->where('sales_executive_id', $request->input('sales_executive_id'))->delete();
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function updatesalesexecutivephoto(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "sales_executive_id" => "required",
                    "updated_by" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $salesExecutive = new SalesExecutive();
            $imgpath = null;
            $sales_executive_id = $request->input('sales_executive_id');


            $sales_executive_photo_base64 = $request->input('sales_executive_photo');
            if ($sales_executive_photo_base64 != "" && $sales_executive_photo_base64 != null && $sales_executive_photo_base64 != "null") {
                
                /*$imgpath = fileupload($sales_executive_photo_base64);*/
                $responsedata = fileUploadSlim($request, "sales_executive_photo");
                $imgpath = $responsedata['folderpath'];

                $query = DB::table('tbl_sales_executive')->select('tbl_sales_executive.sales_executive_photo')->where('sales_executive_id', $sales_executive_id)->get();

                if (count($query) > 0) {
                    $img = $query[0]->sales_executive_photo;
                    if ($img != NULL || $img != "") {
                        if (does_url_exists(mediapath . '/' . $img)) {
                            $path = mediapath . '/' . $img;
                        //    unlink($path);
                        }
                    }
                }

            }


            $newrequest = $request->except(['sales_executive_id', 'sales_executive_photo']);

            if ($sales_executive_photo_base64 != "" && $sales_executive_photo_base64 != null) {
                $newrequest['sales_executive_photo'] = $imgpath;
            }

            $result = $salesExecutive->where('sales_executive_id', $sales_executive_id)->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Sales Executive Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

}
