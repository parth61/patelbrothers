<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Template;
class TemplateController extends Controller
{
    public function addtemplate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "accesstoken" => "required"
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $template = new Template();
             if ($request->hasFile('template_file')) 
             {
             
                 $files = $request->file('template_file');
                
                 $filename =$files->getClientOriginalName();
                 $filepath= fileuploads($filename, $files);
             } 
            $template->template_name = $request->input('template_name');
            $template->template_file = $filename;
            $template->created_by = $request->input('user_id');
            $template->site_id = $request->input('site_id');
            $template->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $template->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $template->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $template->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $template->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "template Added Sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getalltemplate(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
                    "site_id" => "required",
                    "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $template = new Template();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('template_id') && $request->input('template_id') != "") {
                $data['template_id'] = $request->input('template_id');
            }
           
            if ($request->has('status') && $request->input('status') != "") {
                $data['status'] = $request->input('status');
            }
            
            if ($request->has('site_id') && $request->input('site_id') != "") {
                $data['site_id'] = $request->input('site_id');
            }
         
            $template = $template->gettemplate($data);

            return response()->json(['status' => 200, 'count' => count($template), 'data' => $template]);
        }

    }

    public function updatetemplate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                 "template_id" => "required",
                 "site_id" => "required",
                 "accesstoken" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $template = new Template();
             $imgpath = null;
             if ($request->hasFile('template_file')) {
                    
                $files = $request->file('template_file');
               
                $filename =$files->getClientOriginalName();
                $imgpath= fileuploads($filename, $files);
               // $query = DB::table('tbl_project_main_certificate')->select('tbl_project_main_certificate.template_file')->where('project_main_certificate_id',$project_main_certificate_id)->get();
              /*   if(count($query)>0)
                {
                    $img=$query[0]->template_file;
                    if($img!=NULL|| $img!="")
                    {
                        if (does_url_exists(mediapath.$img)) {
                            $path = mediapath.$img;
                            unlink($path);
                        } 
                    }
                   
                } */
                }
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['template_id','user_id','accesstoken']);
            
            if ($request->hasFile('template_file')) {
				$newrequest['template_file'] = $imgpath;
		    }
            $result = $template->where('template_id', $request->input('template_id'))->update($newrequest);

            if ($result) {
                
                return response()->json(['status' => 200, 'data' => "template Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }

    }

    public function deletetemplate(Request $request) {

        $valid = Validator::make($request->all(), [
                    "template_id" => "required",
                    "accesstoken" => "required",
                    "site_id" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
             $template = new Template();
            $request->request->add(['status' => 0]);
            $request->request->add(['updated_by' =>  $request->input('user_id')]);
            $request->request->add(['updated_at' =>Carbon::now()->toDateTimeString()]);
            $newrequest = $request->except(['template_id','user_id','accesstoken']);
            $result = $template->where('template_id', $request->input('template_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }

    }

  
}
