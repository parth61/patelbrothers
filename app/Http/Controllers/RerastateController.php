<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Rerastate;
class RerastateController extends Controller
{
    public function addrerastate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [                    
                    "rera_state_name" => "required",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $rerastate = new Rerastate();
            $rerastate->rera_state_name = $request->input('rera_state_name');
            
            $rerastate->created_by = $request->input('created_by');
            $rerastate->updated_by = $request->input('updated_by');
            $rerastate->status = $request->input('status');
            $rerastate->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $rerastate->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $rerastate->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $rerastate->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $rerastate->save();
            if ($result) {
                return response()->json(['status' => 200, 'data' => "Rerastate added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getallrerastate(Request $request) {

        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $rerastate = new Rerastate();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('rera_state_id') && $request->input('rera_state_id') != "") {
                $data['rera_state_id'] = $request->input('rera_state_id');
            }
            $rerastate = $rerastate->getrerastate($data);

            return response()->json(['status' => 200, 'count' => count($rerastate), 'data' => $rerastate]);
        }
    }

    public function updatererastate(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "rera_state_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $rerastate = new Rerastate();
            $newrequest = $request->except(['rera_state_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $rerastate->where('rera_state_id', $request->input('rera_state_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Rerastate updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deletererastate(Request $request) {
        $valid = Validator::make($request->all(), [
                    "rera_state_id" => "required|numeric",
                    
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $rerastate = new Rerastate();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['rera_state_id']);
            $request->request->add(['updated_by' =>  $request->input('updated_by')]);
            $result = $rerastate->where('rera_state_id', $request->input('rera_state_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

}
