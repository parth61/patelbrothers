<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\PaymentType;

class PaymentTypeController extends Controller
{
    
      public function getAllPaymentType(Request $request) {
        
        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $paymenttype = new PaymentType();
            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');
            $role = $paymenttype->getPaymentType($data);
            return response()->json(['status' => 200, 'count' => count($role), 'data' => $role]);
        }
    }
    
    
}
