<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\CustomerOfferClaims;

class CustomerOfferClaimsController extends Controller
{

    public function addCustomerOfferClaims(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "builder_id" => "required",
            "project_id" => "required",
            "unit_no" => "required",
            "unit_type" => "required",
            "mode_of_payment" => "required",
            "transaction_amount" => "required",
            "payment_transaction_details" => "required",
            "booking_date" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $customeroffermodel = new CustomerOfferClaims();
            $customeroffermodel->user_id = $request->input('user_id');
            $customeroffermodel->builder_id = $request->input('builder_id');
            $customeroffermodel->project_id = $request->input('project_id');
            $customeroffermodel->unit_no = $request->input('unit_no');
            $customeroffermodel->unit_type = $request->input('unit_type');
            $customeroffermodel->mode_of_payment = $request->input('mode_of_payment');
            $customeroffermodel->transaction_amount = $request->input('transaction_amount');
            $customeroffermodel->unit_type = $request->input('payment_transaction_details');
            $customeroffermodel->mode_of_payment = $request->input('booking_date');
            $customeroffermodel->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $customeroffermodel->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $customeroffermodel->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $customeroffermodel->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $result = $customeroffermodel->save();

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Customer request added sucessfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function getCustomerOfferClaims(Request $request) {


        $valid = Validator::make($request->all(), [
            "limit" => "required|numeric",
            "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $customeroffermodel = new CustomerOfferClaims();

            $data = array();
            $data['offset'] = $request->input('offset');
            $data['limit'] = $request->input('limit');

            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            if ($request->has('builder_id') && $request->input('builder_id') != "") {
                $data['builder_id'] = $request->input('builder_id');
            }
            if ($request->has('project_id') && $request->input('project_id') != "") {
                $data['project_id'] = $request->input('project_id');
            }
            if ($request->has('mode_of_payment') && $request->input('mode_of_payment') != "") {
                $data['mode_of_payment'] = $request->input('mode_of_payment');
            }
            if ($request->has('unit_type') && $request->input('unit_type') != "") {
                $data['unit_type'] = $request->input('unit_type');
            }

            $customeroffermodeldata = $customeroffermodel->getCustomerOfferClaims($data);

            return response()->json(['status' => 200, 'count' => count($customeroffermodeldata), 'data' => $customeroffermodeldata]);
        }
    }

    public function updateCustomerOfferClaims(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "customer_offer_claim_id" => "required|numeric",
            "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $customerofferclaims = new CustomerOfferClaims();
            $newrequest = $request->except(['customer_offer_claim_id']);
            $result = $customerofferclaims->where('customer_offer_claim_id', $request->input('customer_offer_claim_id'))->update($newrequest);

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Customer request updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function deleteCustomerOfferClaims(Request $request) {
        $valid = Validator::make($request->all(), [
                    "customer_offer_claim_id" => "required|numeric",
                    "updated_by" => "required|numeric"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $contact = new Contact();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['customer_offer_claim_id']);
            $result = $contact->where('customer_offer_claim_id', $request->input('customer_offer_claim_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }
}
