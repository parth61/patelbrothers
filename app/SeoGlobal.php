<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SeoGlobal extends Model
{
    protected $table = 'tbl_seoglobal';
    protected $fillable=['seoglobal_sitename','seoglobal_twitterhandle','seoglobal_facebookurl','seoglobal_googlesiteverification','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address','seoglobal_youtube_url'];

    public static function getseoglobal($data)
    {
        
        $query = DB::table('tbl_seoglobal as seoglobal')->select('seoglobal.*');

         
        if (array_key_exists('seoglobal_id', $data) && isset($data['seoglobal_id'])) {
            $query = $query->where('seoglobal.seoglobal_id', '=' ,$data['seoglobal_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('seoglobal.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('seoglobal.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('seoglobal.site_id', '=' ,$data['site_id']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('seoglobal.'.$data['sortby'], $data['sorttype']);
        }
                    
        $result = $query->get();
                            
        return $result;
    }
}
