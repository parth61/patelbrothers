<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Highlight extends Model
{
    protected $table = 'tbl_highlights';
    protected $fillable=['highlight_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function gethighlight($data)
    {
        
         $query = DB::table('tbl_highlights as h')->select('h.*');
         
         if (array_key_exists('highlight_id', $data) && isset($data['highlight_id'])) {
            $query = $query->where('h.highlight_id', '=' ,$data['highlight_id']);
           }
                          
         $query = $query->where('h.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('h.highlight_id', 'ASC')->get();
                            
         return $result;
    }
}
