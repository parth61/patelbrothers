<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SizeRange extends Model
{
     protected $table = 'tbl_size_range';
    protected $fillable=['size_range_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
      public static function getsizerange($data)
    {
        
         $query = DB::table('tbl_size_range as sr')->select('sr.*');
         
         if (array_key_exists('size_range_id', $data) && isset($data['size_range_id'])) {
            $query = $query->where('sr.size_range_id', '=' ,$data['size_range_id']);
           }
                          
         $query = $query->where('sr.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('sr.size_range_id', 'ASC')->get();
                            
         return $result;
    }
}
