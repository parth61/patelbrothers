<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class LeadTouchPoint extends Model
{
    protected $table = 'tbl_lead_touchpoint';
    protected $fillable=['lead_id','touchpoint_id','created_at','udpated_at','status','browser_name','browser_version','browser_platform','ip_address'];

    public static function gettouchpoints($data)
    {
        
         $result = DB::table('tbl_lead_touchpoint as tltp')
            ->leftJoin('tbl_touchpoint as tp','tp.touchpoint_id','=','tltp.touchpoint_id')
             ->where('tltp.lead_id', '=' ,$data['lead_id'])
            ->pluck('touchpoint_name');
                            
         return $result;
    }
  
  public static function gettouchpointsdata($data)
    {
        
         $result = DB::table('tbl_lead_touchpoint as tltp')->select('tp.touchpoint_name','tltp.*')
     ->leftJoin('tbl_touchpoint as tp','tp.touchpoint_id','=','tltp.touchpoint_id')
     ->where('tltp.lead_id', '=' ,$data['lead_id'])
     ->get();
                            
         return $result;
    }
}
