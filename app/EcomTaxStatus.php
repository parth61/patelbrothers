<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomTaxStatus extends Model
{
    protected $table = 'tbl_ecom_tax_status';
    protected $fillable=['tax_status_name','site_id','created_by','updated_by','status','browser_name','browser_version','browser_platform','ip_address'];

    public  function gettaxstatus($data)
    {
        
        $query = DB::table('tbl_ecom_tax_status as taxstatus')->select('taxstatus.*');
         
        if (array_key_exists('tax_status_id', $data) && isset($data['tax_status_id'])) {
            $query = $query->where('taxstatus.tax_status_id', '=' ,$data['tax_status_id']);
        }
       
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('taxstatus.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('taxstatus.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('taxstatus.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)){
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('taxstatus.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('taxstatus.tax_status_id', 'DESC');
        }
                          
        $result = $query->get();
                            
        return $result;
    }
}
