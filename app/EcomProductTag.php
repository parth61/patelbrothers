<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductTag extends Model
{
    protected $table = 'tbl_ecom_product_tag';
    protected $fillable=['product_tag_id','product_id','tag_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getproducttag($data)
    {
        
        $query = DB::table('tbl_ecom_product_tag as protag')->select('protag.*');
         
        if (array_key_exists('tag_id', $data) && isset($data['tag_id'])) {
            $query = $query->where('protag.tag_id', '=' ,$data['tag_id']);
        }

   

        if (array_key_exists('product_tag_id', $data) && isset($data['product_tag_id'])) {
            $query = $query->where('protag.product_tag_id', '=' ,$data['product_tag_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('protag.product_id', '=' ,$data['product_id']);
        }
        
      
      
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('protag.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('protag.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('protag.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('protag.product_tag_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}