<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Lead extends Model
{
    protected $table = 'tbl_leads';
    protected $fillable=['lead_remarks','user_id','project_id','sales_executive_id','touchpoint_id','source_id','lead_status_primary','lead_status_seconday','lead_status_tertiary','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getlead($data)
    {
        
         $query = DB::table('tbl_leads as lead')->select('lead.*','user.user_firstname','user.user_lastname',
         'user.user_mobile','user.user_email','user.user_whatsapp_number','project.project_name','source.primary_source',
         'source.secondary_source','source.tertiary_source','tp.touchpoint_name','se.sales_executive_firstname','se.sales_executive_lastname')
         ->leftJoin('tbl_user as user','user.user_id','=','lead.user_id')
         ->leftJoin('tbl_project as project','project.project_id','=','lead.project_id')
         ->leftJoin('tbl_sales_executive as se','se.sales_executive_id','=','lead.sales_executive_id')
         ->leftJoin('tbl_source as source','source.source_id','=','lead.source_id')
         ->leftJoin('tbl_touchpoint as tp','tp.touchpoint_id','=','lead.touchpoint_id');

         
          if (array_key_exists('lead_id', $data) && isset($data['lead_id'])) {
            $query = $query->where('lead.lead_id', '=' ,$data['lead_id']);
           }
               
          if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('lead.project_id', '=' ,$data['project_id']);
           }   
          if (array_key_exists('sales_executive_id', $data) && isset($data['sales_executive_id'])) {
            $query = $query->where('lead.sales_executive_id', '=' ,$data['sales_executive_id']);
           }
           if (array_key_exists('source_id', $data) && isset($data['source_id'])) {
            $query = $query->where('lead.source_id', '=' ,$data['source_id']);
           }
           if (array_key_exists('touchpoint_id', $data) && isset($data['touchpoint_id'])) {
            $query = $query->where('lead.touchpoint_id', '=' ,$data['touchpoint_id']);
           }
              
          if (array_key_exists('lead_id', $data) && isset($data['lead_id'])) {
            $query = $query->where('lead.lead_id', '=' ,$data['lead_id']);
           }   

           if (array_key_exists('builder_id', $data) && isset($data['builder_id'])) {
            
            $salesexecutivelist = DB::table('tbl_sales_executive as se')
            ->where('se.status', '=' ,1)
            ->where('se.builder_id', '=' ,$data['builder_id'])
            ->pluck('se.sales_executive_id');
         
            $query = $query->whereIn('se.sales_executive_id', $salesexecutivelist);
           }               

         $query = $query->where('lead.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('lead.lead_id', 'DESC')->get();
                            
         return $result;
    }
}
