<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomTempOrderItemsTax extends Model
{
    protected $table = 'tbl_ecom_temp_order_items_tax';
    protected $fillable=['tbl_ecom_order_items_tax','tax_id','tax_name','tax_percentage','tax_percentage','status','created_at','updated_at','browser_name','browser_version','browser_platform','ip_address'];

    public static function getorderitemtax($data)
    {
        
        $query = DB::table('tbl_ecom_temp_order_items_tax as eoit')->select('eoit.*');
         
        if (array_key_exists('order_item_id', $data) && isset($data['order_item_id'])) {
            $query = $query->where('eoit.order_item_id', '=' ,$data['order_item_id']);
        }

        if (array_key_exists('tax_id', $data) && isset($data['tax_id'])) {
            $query = $query->where('eoit.tax_id', '=' ,$data['tax_id']);
        }
                          
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('eoit.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('eoit.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('eoit.'.$data['sortby'], $data['sorttype']);
        }

                          
        $result = $query->get();
                            
        return $result;
    }
}
