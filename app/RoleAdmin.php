<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class RoleAdmin extends Model
{
    protected $table = 'tbl_role_admin';
    protected $fillable=['role_admin_name','created_by','updated_by','status','browser_name','browser_version','browser_platform','ip_address'];

    public static function getroleadmin($data)
    {
        
         $query = DB::table('tbl_role_admin as ra')->select('ra.*');
         
         if (array_key_exists('role_admin_id', $data) && isset($data['role_admin_id'])) {
            $query = $query->where('ra.role_admin_id', '=' ,$data['role_admin_id']);
           }
                          
        
           if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ra.status', '=' ,$data['status']);
            }
            else
            {
                $query = $query->where('ra.status', '!=' ,0);
            }

          if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
              $query = $query->orderBy('ra.'.$data['sortby'], $data['sorttype']);
          }
                         
          if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
         $result = $query->get();
                            
         return $result;
    }
}
