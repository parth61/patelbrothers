<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Customblocks extends Model
{
    protected  $table = 'tbl_custom_blocks';
    protected $fillable=['custom_blocks_description','custom_blocks_title','post_id','created_by','updated_by','slug_name','browser_name','browser_version','browser_platform','ip_address','site_id','custom_block_guid'];
    
     public static function getcustomblocks($data)
    {
        
         $query = DB::table('tbl_custom_blocks as c')->select('c.*','med.media_file as custom_blocks_media_file')
         ->leftJoin('tbl_media as med','med.media_id','=','c.custom_blocks_featured_img');
         
         if (array_key_exists('custom_blocks_id', $data) && isset($data['custom_blocks_id'])) {
            $query = $query->where('c.custom_blocks_id', '=' ,$data['custom_blocks_id']);
           }
         if (array_key_exists('custom_block_guid', $data) && isset($data['custom_block_guid'])) {
            $query = $query->where('c.custom_block_guid', '=' ,$data['custom_block_guid']);
           }
         if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('c.post_id', '=' ,$data['post_id']);
           }

           if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('c.slug_name', '=' ,$data['slug_name']);
           }

           if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('order_by', $data) && isset($data['order_by'])) {
            $query = $query->orderBy('c.post_id',$data['order_by'] );
        }
        
                          
         $query = $query->where('c.status', '!=' ,0);
                          
         $result = $query->get();
                            
         return $result;
    }
}
