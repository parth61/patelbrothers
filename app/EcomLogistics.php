<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomLogistics extends Model
{
   
    protected $table = 'tbl_ecom_logistics';
    protected $fillable=['logistic_id','logistic_name','logistic_tracking_link','site_id','created_by','updated_by'];

    public  function getecomlogistics($data)
    {
        
        $query = DB::table('tbl_ecom_logistics as logi')->select('logi.*');
         
        if (array_key_exists('logistic_id', $data) && isset($data['logistic_id'])) {
            $query = $query->where('logi.logistic_id', '=' ,$data['logistic_id']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('logi.site_id', '=' ,$data['site_id']);
        }

        $query = $query->where('logi.status', '=' ,1)->offset($data['offset'])->limit($data['limit']);


        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('logi.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->get();
                            
        return $result;
    }

}
