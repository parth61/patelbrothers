<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class PincodeIndia extends Model
{
    protected $table = 'tbl_pincode_india';
    protected $fillable=['country','state','district','pincode',
    'created_by','updated_by','seo_id','browser_name','browser_version','browser_platform','ip_address'];

    public static function getcountry($data)
    {
        
        $query = DB::table('tbl_pincode_india as pi')->select('pi.country');


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pi.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pi.status', '=' ,1);
        }
       
                    
        $result = $query->groupby('pi.country')->distinct()->get();
                            
        return $result;
    }

    
    public static function getstate($data)
    {
        
        $query = DB::table('tbl_pincode_india as pi')->select('pi.state');


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pi.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pi.status', '=' ,1);
        }

        if (array_key_exists('country', $data) && isset($data['country'])) {
            $query = $query->where('pi.country', '=' ,$data['country']);
        }
                    
        $result = $query->groupby('pi.state')->distinct()->get();
                            
        return $result;
    }
    public static function getdistrict($data)
    {
        
        $query = DB::table('tbl_pincode_india as pi')->select('pi.district');


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pi.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pi.status', '=' ,1);
        }

        if (array_key_exists('state', $data) && isset($data['state'])) {
            $query = $query->where('pi.state', '=' ,$data['state']);
        }
                    
        $result = $query->groupby('pi.district')->distinct()->get();
                            
        return $result;
    }

    public static function getpincode($data)
    {
        
        $query = DB::table('tbl_pincode_india as pi')->select('pi.pincode');


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pi.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pi.status', '=' ,1);
        }

        if (array_key_exists('district', $data) && isset($data['district'])) {
            $query = $query->where('pi.district', '=' ,$data['district']);
        }
                    
        $result = $query->groupby('pi.pincode')->distinct()->get();
                            
        return $result;
    }
    public static function getpincodeindia($data)
    {
        
        $query = DB::table('tbl_pincode_india as pi')->select('pi.*');


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pi.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pi.status', '=' ,1);
        }

        if (array_key_exists('district', $data) && isset($data['district'])) {
            $query = $query->where('pi.district', '=' ,$data['district']);
        }

         
        $result = $query->get();
                            
        return $result;
    }
}
