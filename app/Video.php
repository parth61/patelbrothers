<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Video extends Model
{
    protected $table = 'tbl_video';
    protected $fillable=['video_id','video_title','slug_name','video_body','video_featured_img','site_id',
    'videocategory_id','seo_id','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getvideo($data)
    {
        
        $query = DB::table('tbl_video as vi')
        ->select('vi.*','adm.admin_firstname as cb_admin_firstname','adm.admin_lastname as cb_admin_lastname',
        'admin.admin_firstname as ub_admin_firstname','admin.admin_lastname as ub_admin_lastname','admin.admin_slug',
        'med.media_file as video_media_file')
        ->leftJoin('tbl_media as med','med.media_id','=','vi.video_featured_img')
        ->leftJoin('tbl_admin as adm','adm.admin_id','=','vi.created_by')
        ->leftJoin('tbl_admin as admin','admin.admin_id','=','vi.updated_by');
        


        
        
        /* if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('vi.slug_name', '=' ,$data['slug_name']);
        }
        */
       
         
        
        if (array_key_exists('video_id', $data) && isset($data['video_id'])) {
            $query = $query->where('vi.video_id', '=' ,$data['video_id']);
        }
        if (array_key_exists('video_guid', $data) && isset($data['video_guid'])) {
            $query = $query->where('vi.video_guid', '=' ,$data['video_guid']);
        }
       

      


        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('vi.status', '=' ,$data['status']);
        } 
        else
        {
            $query = $query->where('vi.status', '!=' ,0);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('vi.site_id', '=' ,$data['site_id']);
        }

        // if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
        //     $query = $query->orderBy('vi.'.$data['sortby'], $data['sorttype']);
        // }
                        
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('order_by', $data) && isset($data['order_by'])) {
            $query = $query->orderBy('vi.video_id',$data['order_by'] );
        }
        
        $result = $query->get();
                            
        return $result;
    }
  
    
}
