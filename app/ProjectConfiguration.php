<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectConfiguration extends Model
{
    protected $table = 'tbl_project_configuration';
    protected $fillable=['project_id','configuration_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getprojectconfiguration($data)
    {
        
         $query = DB::table('tbl_project_configuration as pc')
			->select('pc.configuration_id','c.*')
			->leftJoin('tbl_configuration as c','c.configuration_id','=','pc.configuration_id');;
         
           if (array_key_exists('project_configuration_id', $data) && isset($data['project_configuration_id'])) {
            $query = $query->where('pc.project_configuration_id', '=' ,$data['project_configuration_id']);
           }
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('pc.project_id', '=' ,$data['project_id']);
           }
           if (array_key_exists('configuration_id', $data) && isset($data['configuration_id'])) {
            $query = $query->where('pc.configuration_id', '=' ,$data['configuration_id']);
           }
                          
         $query = $query->where('pc.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.configuration_id', 'ASC')->get();
                            
         return $result;
    }
}
