<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectSalesExecutive extends Model
{
    protected $table = 'tbl_project_sales_executive';
    protected $fillable=['project_id','sales_executive_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getprojectbysalesexecutiveid($data)
    {
        
         $query = DB::table('tbl_project_sales_executive as pse')
         ->select('pse.project_id');
         
         if (array_key_exists('sales_executive_id', $data) && isset($data['sales_executive_id'])) {
            $query = $query->where('pse.sales_executive_id', '=' ,$data['sales_executive_id']);
           }
                          
         $query = $query->where('pse.status', '=' ,1);
                          
         $result = $query->get();
                            
         return $result;
    }
}
