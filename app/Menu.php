<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Menu extends Model
{
    protected $table = 'tbl_menu';
    protected $fillable=['menu_id','menulocation_id','menu_menuitems','menu_name','status','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getmenu($data)
    {
        
        $query = DB::table('tbl_menu as men')
        ->select('men.*');
         
        if (array_key_exists('menu_id', $data) && isset($data['menu_id'])) {
            $query = $query->where('men.menu_id', '=' ,$data['menu_id']);
        }

        if (array_key_exists('menulocation_id', $data) && isset($data['menulocation_id'])) {
            $query = $query->where('men.menulocation_id', '=' ,$data['menulocation_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('men.status', '=' ,$data['status']);
        } else
        {
            $query = $query->where('men.status', '!=' ,0);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('men.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('men.'.$data['sortby'], $data['sorttype']);
        }
                        
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        $result = $query->get();
                            
        return $result;
    }
}
