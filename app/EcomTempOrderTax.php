<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomTempOrderTax extends Model
{
     protected $table = 'tbl_ecom_temp_order_tax';
    protected $fillable=['order_id','tax_name','tax_percentage','tax_amount','status','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getordertax($data)
    {
        
        $query = DB::table('tbl_ecom_temp_order_tax as ordtx')->select('ordtx.*')
        ->leftJoin('tbl_ecom_temp_order as ord','ord.order_id','=','ordtx.order_id');
         
        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('ordtx.order_id', '=' ,$data['order_id']);
        }

        if (array_key_exists('order_guid', $data) && isset($data['order_guid'])) {
            $query = $query->where('ord.order_guid', '=' ,$data['order_guid']);
        }

        $query = $query->where('ordtx.status', 1);
                          
        $result = $query->get();
                            
        return $result;
    }
}
