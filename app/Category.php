<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Category extends Model
{
    protected  $table = 'tbl_category';
    protected $fillable=['category_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getcategory($data)
    {
        
         $query = DB::table('tbl_category as c')->select('c.*');
         
         if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('c.category_id', '=' ,$data['category_id']);
           }
                          
         $query = $query->where('c.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.category_id', 'ASC')->get();
                            
         return $result;
    }
}
