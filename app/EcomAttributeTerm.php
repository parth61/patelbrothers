<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomAttributeTerm extends Model
{
    protected $table = 'tbl_ecom_attribute_term';
    protected $fillable=['attribute_term_id','slug_name','attribute_term_colorcode','attribute_term','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public function getattributeterm($data)
    {
        
        $query = DB::table('tbl_ecom_attribute_term as attrt')
        ->select('attrt.*','attr.attribute_name','attr.slug_name as attribute_term_slug')
        ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'attrt.attribute_id');
        
        if (array_key_exists('attribute_term_id', $data) && isset($data['attribute_term_id'])) {
            $query = $query->where('attrt.attribute_term_id', '=' ,$data['attribute_term_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('attrt.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('attrt.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('attrt.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('attrt.'.$data['sortby'], $data['sorttype']);
        }

        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('attrt.attribute_id', '=' ,$data['attribute_id']);
        }
                      
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('attrt.slug_name', '=' ,$data['slug_name']);
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
