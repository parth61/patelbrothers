<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Banks extends Model
{
    protected $table = 'tbl_banks';
    protected $fillable=['bank_name','bank_logo','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getbank($data)
    {
        
         $query = DB::table('tbl_banks as tb')->select('tb.*');
         
         if (array_key_exists('bankid', $data) && isset($data['bankid'])) {
            $query = $query->where('tb.bankid', '=' ,$data['bankid']);
           }
                          
         $query = $query->where('tb.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tb.bankid', 'ASC')->get();
                            
         return $result;
    }
}
