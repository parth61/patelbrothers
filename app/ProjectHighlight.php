<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectHighlight extends Model
{
    protected $table = 'tbl_project_highlights';
    protected $fillable=['project_id','highlight_id','project_highlight_alias','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
    public static function getprojecthighlight($data)
    {
        
         $query = DB::table('tbl_project_highlights as ph')
         ->select('ph.*','hi.highlight_name')
         ->leftJoin('tbl_highlights as hi','hi.highlight_id','=','ph.highlight_id');
         
           if (array_key_exists('project_highlights_id', $data) && isset($data['project_highlights_id'])) {
            $query = $query->where('ph.project_highlights_id', '=' ,$data['project_highlights_id']);
           }
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('ph.project_id', '=' ,$data['project_id']);
           }
           if (array_key_exists('highlight_id', $data) && isset($data['highlight_id'])) {
            $query = $query->where('ph.highlight_id', '=' ,$data['highlight_id']);
           }
                          
         $query = $query->where('ph.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('ph.project_highlights_id', 'ASC')->get();
                            
         return $result;
    }
}
