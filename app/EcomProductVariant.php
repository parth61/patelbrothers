<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EcomProduct;
use App\EcomProductTag;
use App\EcomProductCategory;
use App\Slug;
use App\EcomProductGallery;
use App\EcomProductAttribute;
use App\EcomProductAttributeTerm;
use App\EcomProductVariantAttributeTerm;
use Illuminate\Support\Facades\File;
use App\EcomProductVariant;
use App\EcomWishlist;
use DB;

class EcomProductVariant extends Model
{
    protected $table = 'tbl_ecom_product_variant';
    protected $fillable=['product_variant_id','product_id','variant_product_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getproductvariant($data)
    {
        
        $query = DB::table('tbl_ecom_product_variant as provariant')->select('provariant.product_variant_id','provariant.variant_product_id','pro.*','seo.seo_title','seo_description',
        'seo_keywords','seo_image','seo_type','seo_for','seo_for_id','seo_canonical','medi.media_file as seo_media_file')
        ->leftJoin('tbl_ecom_product as pro', 'pro.product_id', '=', 'provariant.variant_product_id')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
         
        if (array_key_exists('variant_product_id', $data) && isset($data['variant_product_id'])) {
            $query = $query->where('provariant.variant_product_id', '=' ,$data['variant_product_id']);
        }

        if (array_key_exists('product_variant_id', $data) && isset($data['product_variant_id'])) {
            $query = $query->where('provariant.product_variant_id', '=' ,$data['product_variant_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('provariant.product_id', '=' ,$data['product_id']);
        }
        
  
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('provariant.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('provariant.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('provariant.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('provariant.product_variant_id', 'DESC');
        }
                          
        $product = $query->offset($data['offset'])->limit($data['limit'])->get();

        $productdata = array();
        $wishlist = new EcomWishlist();
        foreach($product as $item)
        {
            $product_id = $item->variant_product_id;
            if(array_key_exists('user_id', $data) && isset($data['user_id']) && array_key_exists('site_id', $data) && isset($data['site_id'])  ){
               
                $wishlist1=$wishlist->where('user_id','=',$data['user_id'])->where('site_id','=',$data['site_id'])->where('product_id',$product_id)->where('status','=','1')->get();
               
                if(count($wishlist1)>0){
                    $item->is_wishlist=true;
                    
                }else{
                    $item->is_wishlist=false;
                }
            }else{
                $item->is_wishlist=false;
            }
            
            //product tag 
            $tagnamelist = DB::table('tbl_ecom_product_tag as pt')
            ->leftJoin('tbl_ecom_tag as tag', 'pt.tag_id', '=', 'tag.tag_id')
            ->where('pt.status', '=', 1)
            ->where('pt.product_id', '=', $product_id)
            ->orderBy('tag.tag_name', 'ASC')
            ->pluck('tag.tag_name');

            $tagidlist = DB::table('tbl_ecom_product_tag as pt')
            ->where('pt.status', '=', 1)
            ->where('pt.product_id', '=', $product_id)
            ->pluck('pt.tag_id');

          
            //product attribute
            $attributenamelist = DB::table('tbl_ecom_product_attribute as proattr')
            ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'proattr.attribute_id')
            ->where('proattr.status', '=', 1)
            ->where('proattr.product_id', '=', $product_id)
            ->orderBy('attr.attribute_name', 'ASC')
            ->get();

            $attributeidlist = DB::table('tbl_ecom_product_attribute as proattr')
            ->where('proattr.status', '=', 1)
            ->where('proattr.product_id', '=', $product_id)
            ->pluck('proattr.attribute_id');

            $attributetermidlist = DB::table('tbl_ecom_product_variant_attribute_term as proattrterm')
            ->select('proattrterm.*','teat.attribute_term','teat.attribute_term_colorcode','attr.attribute_name')
            ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'proattrterm.attribute_term_id')
            ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'teat.attribute_id')
            ->where('proattrterm.status', '=', 1)
            ->where('teat.status', '=', 1)
            ->where('proattrterm.variant_product_id', '=', $product_id)
            ->get();

            $attributetermnamelist = array();
            foreach($attributetermidlist as $tempitem1){
                array_push($attributetermnamelist, $tempitem1->attribute_term);
            }

            foreach($attributenamelist as $tempitem){
                $attributefinallist = array();
                foreach($attributetermidlist as $tempitem1){
                    if($tempitem1->attribute_id === $tempitem->attribute_id){
                        array_push($attributefinallist, $tempitem1);
                    }
                }
                $tempitem->attributeitemslist = $attributefinallist;
            }

            //product category
            $categorynamelist = array();
            $categoryidlist = array();

            $categorylist = DB::table('tbl_ecom_product_category as pc')
            ->select('category.*')
            ->leftJoin('tbl_ecom_category as category', 'pc.category_id', '=', 'category.category_id')
            ->where('pc.status', '=', 1)
            ->where('pc.product_id', '=', $product_id)
            ->orderBy('category.category_name', 'ASC')
            ->get();

            foreach($categorylist as $catitem){
                array_push($categorynamelist, $catitem->category_name);
                array_push($categoryidlist, $catitem->category_id);
            }

            if($item->seo_media_file==""||$item->seo_media_file==null||$item->seo_media_file=="null")
                {
                    $item->seo_img_path = fixedseofeaturedimage;
                }
                else
                {
                    if(fileuploadtype=="local")
                    { 
                        //code for seo image
                        if (File::exists(baseimagedisplaypath .$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        }
                        
                    }
                    else
                    {
                         //seo image
                        if (does_url_exists(imagedisplaypath.$item->seo_media_file)) {
                            $item->seo_img_path = imagedisplaypath.$item->seo_media_file;
                        } else {
                            $item->seo_img_path = fixedseofeaturedimage;
                        } 
                    }
                }

            //call for getting gallery image with product
            $productgallery = new EcomProductGallery();
            $data['product_id']=$product_id;
            $data['offset'] = 0;
            $data['limit'] = 1000;
            $productgallery = $productgallery->getproductgallery($data);
            $productgallerydata=array();
            foreach ($productgallery as $item1) {
             
                if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                {
                    $item1->product_image_path =fixedproductgalleryimage;
                   
                }
                else
                {
                    if(fileuploadtype=="local")
                    {
                          if (File::exists(baseimagedisplaypath .$item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                            } else {
                                $item1->product_image_path =fixedproductgalleryimage;
                            }
                        
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath. $item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath. $item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }
                }  

               
                array_push($productgallerydata, $item1);
            }
            $item->product_gallery=$productgallerydata;
            $item->tagidlist= $tagidlist;
            $item->tagnamelist = $tagnamelist;
            $item->categoryidlist= $categoryidlist;
            $item->categorynamelist= $categorynamelist;
            $item->categorylist= $categorylist;
            $item->attributenamelist= $attributenamelist;
            $item->attributetermidlist= $attributetermidlist;
            $item->attributeidlist= $attributeidlist;
            $item->attributestring= $attributetermnamelist;
        
            array_push($productdata, $item);
        }
                            
        return $productdata;
    }
}
