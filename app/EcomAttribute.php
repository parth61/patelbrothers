<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class EcomAttribute extends Model
{
    protected $table = 'tbl_ecom_attribute';
    protected $fillable=['attribute_id','slug_name','attribute_name','is_use_in_product_listing','is_searchable','is_comparable','display_layout','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getattribute($data)
    {
        
        $query = DB::table('tbl_ecom_attribute as attr')
        ->select('attr.*','attr.slug_name as attribute_slug');
         
        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('attr.attribute_id', '=' ,$data['attribute_id']);
        }
   
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('attr.slug_name', '=' ,$data['slug_name']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('attr.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('attr.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('attr.status', '=' ,1);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('attr.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('attr.attribute_id', 'DESC');
        }
                         
        $result = $query->get();
                            
        return $result;
    }

    public  function getattributewithcount($data)
    {
        
        $query = DB::table('tbl_ecom_attribute as attr')->select('attr.*');
         
        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('attr.attribute_id', '=' ,$data['attribute_id']);
        }
   
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('attr.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('attr.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('attr.status', '=' ,1);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('attr.'.$data['sortby'], $data['sorttype']);
        }

        else
        {
            $query = $query->orderBy('attr.attribute_id', 'DESC');
        }
                         
        $attributelist = $query->get();

        $variableattributes = DB::table("tbl_ecom_product_variant_attribute_term as vatt")
            ->select("vatt.attribute_id","vatt.attribute_term_id","atterm.attribute_term",DB::raw("count(vatt.attribute_term_id) as total"))
            ->leftJoin('tbl_ecom_attribute_term as atterm', 'atterm.attribute_term_id', '=', 'vatt.attribute_term_id')
            ->leftJoin('tbl_ecom_product as tp', 'tp.product_id', '=', 'vatt.variant_product_id')
            ->where('tp.status', 1)
            ->whereNotNull('tp.product_actual_price')
            ->where('vatt.status', 1)
            ->where('atterm.status', 1)
            ->orderby('vatt.attribute_term_id')
            ->groupby("vatt.attribute_term_id")
            ->get();

        $result['attributelist'] = $attributelist;
        $result['attributetermslist'] = $variableattributes;
                            
        return $result;
    }
}
