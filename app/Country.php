<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Country extends Model
{
    protected  $table = 'tbl_country';
    protected $fillable=['country_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getcountry($data)
    {
        
         $query = DB::table('tbl_country as c')->select('c.*');
         
         if (array_key_exists('country_id', $data) && isset($data['country_id'])) {
            $query = $query->where('c.country_id', '=' ,$data['country_id']);
           }
                          
         $query = $query->where('c.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.country_id', 'ASC')->get();
                            
         return $result;
    }
}
