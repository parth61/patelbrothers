<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomPayments extends Model
{
    protected $table = 'tbl_ecom_payments';
    protected $fillable=['order_id','user_id','site_id','txn_id','amount','user_name','user_email','user_phone','product_info','payment_status','status','created_at','updated_at','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getpayments($data)
    {

        $query = DB::table('tbl_ecom_payments as tep')->select('tep.*','teo.order_guid','us.user_id')
        ->leftJoin('tbl_user as us', 'us.user_id', '=', 'tep.user_id')
        ->leftJoin('tbl_ecom_order as teo', 'teo.order_id', '=', 'tep.order_id');
         
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('tep.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('teo.order_id', '=' ,$data['order_id']);
        }

        if (array_key_exists('order_guid', $data) && isset($data['order_guid'])) {
            $query = $query->where('teo.order_guid', '=' ,$data['order_guid']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('tep.site_id', '=' ,$data['site_id']);
        }
           
        if (array_key_exists('txn_id', $data) && isset($data['txn_id'])) {
            $query = $query->where('tep.txn_id', '=' ,$data['txn_id']);
        } 
        
        if (array_key_exists('payment_status', $data) && isset($data['payment_status']) && $data['payment_status'] != "") {
            $query = $query->where('tep.payment_status', '=' ,$data['payment_status']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('tep.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('tep.status', '=' ,1);
        }

        $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('tep.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('tep.payment_id', 'DESC');
        }

                          
        $result = $query->get();
                            
        return $result;
    }
}
