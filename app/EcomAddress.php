<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomAddress extends Model
{
    protected $table = 'tbl_ecom_address';
    protected $fillable=['address_id','user_id','address_type','city','state','country','pincode','address','landmark','isprimary','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getaddress($data)
    {
        
        $query = DB::table('tbl_ecom_address as ads')->select('ads.*');
         
        if (array_key_exists('address_id', $data) && isset($data['address_id'])) {
            $query = $query->where('ads.address_id', '=' ,$data['address_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ads.status', '=' ,$data['status']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ads.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('ads.user_id', '=' ,$data['user_id']);
        }
        if (array_key_exists('isprimary', $data) && isset($data['isprimary'])) {
            $query = $query->where('ads.isprimary', '=' ,$data['isprimary']);
        }                 
        if (array_key_exists('status', $data) && isset($data['status'])) 
        {
            $query = $query->where('ads.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ads.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) 
        {
            $query = $query->orderBy('ads.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('ads.address_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}