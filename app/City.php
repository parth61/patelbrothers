<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class City extends Model
{
    protected  $table = 'tbl_city';
    protected $fillable=['state_id','country_id','city_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getcity($data)
    {
        
        $query = DB::table('tbl_city as ct')->select('ct.*','s.state_name','c.country_name')
        ->leftJoin('tbl_country as c', 'c.country_id', '=', 'ct.country_id')
        ->leftJoin('tbl_state as s', 's.state_id', '=', 'ct.state_id');
         
        if (array_key_exists('country_id', $data) && isset($data['country_id'])) {
            $query = $query->where('ct.country_id', '=' ,$data['country_id']);
        }

        if (array_key_exists('state_id', $data) && isset($data['state_id'])) {
            $query = $query->where('ct.state_id', '=' ,$data['state_id']);
        }

        if (array_key_exists('city_id', $data) && isset($data['city_id'])) {
            $query = $query->where('ct.city_id', '=' ,$data['city_id']);
        }
                          
        $query = $query->where('ct.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('ct.city_id', 'ASC')->get();
                            
        return $result;
    }
}
