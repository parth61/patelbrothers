<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Comment extends Model
{
    protected $table = 'tbl_comment';
    protected $fillable=['comment_id','message','usource','umedium','ucampaign','ucotent','uterm','iusource','iumedium','iucampaign','iucontent','iuterm','ireferrer','lreferrer','ilandingpage','browser_name','browser_version','browser_platform','ip_address','visits','email','mobile','country_code','country_iso','post_id','status','name','created_at','updated_at','user_id','parent_comment_id'];

    public static function getcomment($data)
    {
        
        $query = DB::table('tbl_comment as tc')->select('tc.*');


         
        if (array_key_exists('comment_id', $data) && isset($data['comment_id'])) {
            $query = $query->where('tc.comment_id', '=' ,$data['comment_id']);
        }
        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('tc.post_id', '=' ,$data['post_id']);
        }
        if (array_key_exists('parent_comment_id', $data) && isset($data['parent_comment_id'])) {
            $query = $query->where('tc.parent_comment_id', '=' ,$data['parent_comment_id']);
        }
        
        

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('tc.site_id', '=' ,$data['site_id']);
        }
         if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('tc.'.$data['sortby'], $data['sorttype']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('tc.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('tc.status', '=' ,1);
        }

       
                    
        $result = $query->get();
                            
        return $result;
    }
    
    public  function getalllcomentwithparentcategory($data)
    {
        $query = DB::table('tbl_comment as tc')->select('tc.parent_comment_id');

        if (array_key_exists('comment_id', $data) && isset($data['comment_id'])) {
            $query = $query->where('tc.comment_id', '=' ,$data['comment_id']);
        }
        if (array_key_exists('post_id', $data) && isset($data['post_id'])) {
            $query = $query->where('tc.post_id', '=' ,$data['post_id']);
        }
        if (array_key_exists('site_id', $data) && isset($data['site_id']) ) {
            $query = $query->where('tc.site_id', '=' ,$data['site_id']);
        }
         if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('tc.'.$data['sortby'], $data['sorttype']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('tc.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('tc.status', '=' ,1);
        }
                          
        $result = $query->groupby('tc.parent_comment_id')->get();
        foreach($result as $item){

            $item->category_data = DB::table('tbl_comment as tc')->select('tc.*')->where('parent_comment_id',$item->parent_comment_id)->get();
        }
                            
        return $result;
    }

}
