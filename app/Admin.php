<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Admin extends Model
{
    protected $table = 'tbl_admin';
    protected $fillable=['admin_firstname','admin_lastname','admin_email','admin_password','role_admin_id','site_id','browser_name','browser_version','browser_platform','ip_address','admin_slug'];

    public static function getadmin($data)
    {
        
        $query = DB::table('tbl_admin as ad')->select('ad.*','ra.role_admin_name')->leftJoin('tbl_role_admin as ra', 'ra.role_admin_id', '=', 'ad.role_admin_id');;

         
        if (array_key_exists('admin_id', $data) && isset($data['admin_id'])) {
            $query = $query->where('ad.admin_id', '=' ,$data['admin_id']);
        }
        if (array_key_exists('admin_slug', $data) && isset($data['admin_slug'])) {
            $query = $query->where('ad.admin_slug', '=' ,$data['admin_slug']);
        }
        

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ad.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ad.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ad.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ad.'.$data['sortby'], $data['sorttype']);
        }
                    
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

}
