<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductAttribute extends Model
{
    protected $table = 'tbl_ecom_product_attribute';
    protected $fillable=['product_attribute_id','is_visible_on_product_page','use_for_variation','product_id','attribute_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getproductattribute($data)
    {
        
        $query = DB::table('tbl_ecom_product_attribute as pattr')
        ->select('pattr.*','attr.attribute_name')
        ->leftJoin('tbl_ecom_attribute as attr', 'attr.attribute_id', '=', 'pattr.attribute_id');
         
        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('pattr.attribute_id', '=' ,$data['attribute_id']);
        }
        
        if (array_key_exists('use_for_variation', $data) && isset($data['use_for_variation'])) {
            $query = $query->where('pattr.use_for_variation', '=' ,$data['use_for_variation']);
        }

        if (array_key_exists('product_attribute_id', $data) && isset($data['product_attribute_id'])) {
            $query = $query->where('pattr.product_attribute_id', '=' ,$data['product_attribute_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('pattr.product_id', '=' ,$data['product_id']);
        }
        
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pattr.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pattr.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('pattr.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('pattr.product_attribute_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
