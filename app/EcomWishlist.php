<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomWishlist extends Model
{
    protected $table = 'tbl_ecom_wishlist';
    protected $fillable=['user_id','product_id','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getwishlist($data)
    {
        
        $query = DB::table('tbl_ecom_wishlist as wlist')
            ->select('wlist.*','product.*','user.user_id','user.user_firstname','user.user_lastname','user.user_email','user.user_whatsapp_number')
            ->leftjoin('tbl_user as user','user.user_id','=','wlist.user_id')
            ->leftjoin('tbl_ecom_product as product','product.product_id','=','wlist.product_id');
         
        if (array_key_exists('wishlist_id', $data) && isset($data['wishlist_id'])) {
            $query = $query->where('wlist.wishlist_id', '=' ,$data['wishlist_id']);
        }

        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('wlist.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('wlist.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('wlist.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('wlist.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('wlist.site_id', '=' ,$data['site_id']);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('wlist.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('wlist.wishlist_id', 'DESC');
        }
                          
        $result = $query->get();
                            
        return $result;
    }

    public static function getwishlistandcartcount($data)
    {
        $result = array();

        $query = DB::table('tbl_ecom_wishlist as wlist')->where('wlist.status', '=' ,1);

        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('wlist.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('wlist.site_id', '=' ,$data['site_id']);
        }
                          
        $result['wishlistcount'] = $query->count();

        $query = DB::table('tbl_ecom_cart as ct')->where('ct.status', '=' ,1);

        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('ct.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ct.site_id', '=' ,$data['site_id']);
        }
                          
        $result['cartlistcount'] = $query->count();
                            
        return $result;
    }
}
