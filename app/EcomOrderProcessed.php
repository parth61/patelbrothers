<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomOrderProcessed extends Model
{
     protected $table = 'tbl_ecom_order_processed';
    protected $fillable=['order_process_id','order_id','order_invoice_id','order_status','status','site_id','created_by','updated_by','created_at','updated_at'];
}
