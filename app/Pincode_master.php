<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Pincode extends Model
{
    //
    protected $table = 'tbl_pincode';
    protected $fillable=['country','state','district','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getpincode($data)
    {
        
         $query = DB::table('tbl_pincode as p')->select('p.*');
         
         if (array_key_exists('pincode_id', $data) && isset($data['pincode_id'])) {
            $query = $query->where('p.pincode_id', '=' ,$data['pincode_id']);
           }
                          
         $query = $query->where('p.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('p.pincode', 'ASC')->get();
                            
         return $result;
    }
    public static function getcountry($data)
    {
        
         $query = DB::table('tbl_pincode as p')->select('p.country as country')->where('p.status', '=' ,1)->distinct('p.country');
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
    public static function getstate($data)
    {
       
        $query = DB::table('tbl_pincode as p')->select('p.state')->where('p.status', '=' ,1)->distinct('state');

        if (array_key_exists('country', $data) && isset($data['country'])) {
            $query = $query ->where('p.country', '=' ,$data['country']);
        }
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
    public static function getdistrict($data)
    {
       
        $query = DB::table('tbl_pincode as p')->select('p.district')->where('p.status', '=' ,1)->distinct('district');

        if (array_key_exists('state', $data) && isset($data['state'])) {
            $query = $query ->where('p.state', '=' ,$data['state']);
        }
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
    public static function getpincodedata($data)
    {
       
        $query = DB::table('tbl_pincode as p')->select('p.pincode')->where('p.status', '=' ,1)->distinct('pincode');

        if (array_key_exists('district', $data) && isset($data['district'])) {
            $query = $query ->where('p.district', '=' ,$data['district']);
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
