<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CustomerOfferClaims extends Model
{
    protected $table = 'tbl_customerofferclaims';
    protected $fillable=['user_id','builder_id','project_id','unit_no','unit_type','mode_of_payment', 'transaction_amount','payment_transaction_details','booking_date','verified_status','status','created_at','updated_at','browser_name','browser_version','browser_platform','ip_address'];

    public static function getCustomerOfferClaims($data)
    {
        
         $query = DB::table('tbl_customerofferclaims as tc')->select('tc.*','tb.builder_name','tb.builder_email','tp.project_name','tu.user_firstname','tu.user_lastname','tu.user_mobile','tu.user_email','tu.user_whatsapp_number')
         ->leftJoin('tbl_builder as tb','tb.builder_id','=','tc.builder_id')
         ->leftJoin('tbl_project as tp','tp.project_id','=','tc.project_id')
         ->leftJoin('tbl_user as tu','tu.user_id','=','tc.user_id');
         
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('tc.user_id', '=' ,$data['user_id']);
        }

        if (array_key_exists('builder_id', $data) && isset($data['builder_id'])) {
            $query = $query->where('tc.builder_id', '=' ,$data['builder_id']);
        }

        if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('tc.project_id', '=' ,$data['project_id']);
        }

        if (array_key_exists('verified_status', $data) && isset($data['verified_status'])) {
            $query = $query->where('tc.verified_status', '=' ,$data['verified_status']);
        }

        if (array_key_exists('unit_type', $data) && isset($data['unit_type'])) {
            $query = $query->where('tc.unit_type', '=' ,$data['unit_type']);
        }
                          
        $query = $query->where('tc.status', '=' , 1);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tc.customer_offer_claim_id', 'DESC')->get();
                            
        return $result;
    }

}
