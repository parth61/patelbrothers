<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProjectPhoto extends Model
{
    protected $table = 'tbl_project_photos';
    protected $fillable=['project_id','project_photo','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    public static function getprojectphoto($data)
    {
        
          $query = DB::table('tbl_project_photos as pc')->select('pc.*','p.project_name')
                   ->leftJoin('tbl_project as p','p.project_id','=','pc.project_id');
         
          if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
              $query = $query->where('pc.project_id', '=' ,$data['project_id']);
          }

          if (array_key_exists('project_photo_id', $data) && isset($data['project_photo_id'])) {
              $query = $query->where('pc.project_photo_id', '=' ,$data['project_photo_id']);
          }

          if (array_key_exists('type', $data) && isset($data['type'])) {
            $query = $query->where('pc.type', '=' ,$data['type']);
        }
                          
         $query = $query->where('pc.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('pc.project_photo_id', 'ASC')->get();
                            
         return $result;
    }
}
