<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class LoginLog extends Model
{
    protected $table = 'tbl_login_log';
    protected $fillable=['admin_id','user_type','login_type','browser_name','browser_version','browser_platform','ip_address'];
    public static function getloginlog($data)
    {
        if($data['user_type'] == "admin"){
	        	$query = DB::table('tbl_login_log as ad')->select('ad.*','ra.admin_firstname','ra.admin_lastname','ra.admin_email')->leftJoin('tbl_admin as ra', 'ra.admin_id', '=', 'ad.user_id');
        }elseif($data['user_type'] == "customer"){
        	$query = DB::table('tbl_login_log as ad')->select('ad.*','ra.user_firstname','ra.user_lastname','ra.user_email')->leftJoin('tbl_user as ra', 'ra.user_id', '=', 'ad.user_id');
        }elseif($data['user_type'] == "salesexecutive"){
        	$query = DB::table('tbl_login_log as ad')->select('ad.*','ra.sales_executive_firstname','ra.sales_executive_lastname','ra.sales_executive_email')->leftJoin('tbl_sales_executive as ra', 'ra.sales_executive_id', '=', 'ad.user_id');
        }
        elseif($data['user_type'] == "builder"){
        	$query = DB::table('tbl_login_log as ad')->select('ad.*','ra.builder_name','ra.builder_email')->leftJoin('tbl_builder as ra', 'ra.builder_id', '=', 'ad.user_id');
        }

         
        if (array_key_exists('login_log_id', $data) && isset($data['login_log_id'])) {
            $query = $query->where('ad.login_log_id', '=' ,$data['login_log_id']);
        }
        if (array_key_exists('user_type', $data) && isset($data['user_type'])) {
            $query = $query->where('ad.user_type', '=' ,$data['user_type']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ad.site_id', '=' ,$data['site_id']);
        }

        
        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ad.'.$data['sortby'], $data['sorttype']);
        }
                    
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

}
