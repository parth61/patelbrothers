<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Subscription extends Model
{
    protected  $table = 'tbl_subscription';
    protected $fillable=['subscription_id','subscription_name','package_id','browser_name','browser_version','browser_platform','ip_address','created_at','updated_at'];
    
     public static function getsubscription($data)
    {
        
        $query = DB::table('tbl_subscription as sp')->select('sp.*');
         
        if (array_key_exists('subscription_id', $data) && isset($data['subscription_id'])) {
            $query = $query->where('sp.subscription_id', '=' ,$data['subscription_id']);
        }
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('sp.user_id', '=' ,$data['user_id']);
        }
        if (array_key_exists('package_id', $data) && isset($data['package_id'])) {
            $query = $query->where('sp.package_id', '=' ,$data['package_id']);
        }

        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('sp.subscription_id', 'ASC')->get();
                            
        return $result;
    }
}
	