<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomTempOrderActivity extends Model
{
   
    protected $table = 'tbl_ecom_temp_order_activity';
    protected $fillable=['order_activity_id','order_activity_type','order_activity_details','order_id','created_by','updated_by'];

    public  function getecomorderactivity($data)
    {
        
        $query = DB::table('tbl_ecom_temp_order_activity as eoa')->select('eoa.*');
         
        if (array_key_exists('order_activity_id', $data) && isset($data['order_activity_id'])) {
            $query = $query->where('eoa.order_activity_id', '=' ,$data['order_activity_id']);
        }

        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('eoa.order_id', '=' ,$data['order_id']);
        }

        $query = $query->where('eoa.status', '=' ,1)->offset($data['offset'])->limit($data['limit']);


        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('eoa.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->get();
                            
        return $result;
    }

}
