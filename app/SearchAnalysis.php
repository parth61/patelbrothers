<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SearchAnalysis extends Model {

    protected $table = 'tbl_search_analysis';
//     public $timestamps = false;
    protected $fillable = ['user_id','category_id','location_id','project_status_id', 'budget_min', 'budget_max', 'status','browser_name','browser_version','browser_platform','ip_address'];


}
