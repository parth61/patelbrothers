<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Units extends Model
{
    protected  $table = 'tbl_units';
    protected $fillable=['unit_id','category_id','unit_type_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getunits($data)
    {
        
        $query = DB::table('tbl_units as s')->select('s.*','c.category_name')
        ->leftJoin('tbl_category as c', 'c.category_id', '=', 's.category_id');
         
        if (array_key_exists('unit_id', $data) && isset($data['unit_id'])) {
            $query = $query->where('s.unit_id', '=' ,$data['unit_id']);
        }
        if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('s.category_id', '=' ,$data['category_id']);
        }

       
                          
        $query = $query->where('s.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('s.unit_id', 'ASC')->get();
                            
        return $result;
    }
}
