<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\UserJourney;
use App\SalesExecutive;
use App\Builder;
use App\Address;
use App\EcomCart;
use App\EcomBusinessSetting;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function adduser(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                   
                 "user_firstname" => "required",
                   "user_lastname" => "required",
                    "user_mobile" =>'required|unique:tbl_user,user_mobile',
                    "site_id" =>'required',
                   "user_email" => "required",
                   // "user_whatsapp_number" => 'required'
                   
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
          
            $user = new User();
            //random number 
            $otp = rand(100000, 999999);

            $user->user_firstname = $request->input('user_firstname');
            $user->user_lastname = $request->input('user_lastname');
            $user->user_password = md5($request->input('user_password'));
            $user->user_mobile = ltrim(str_replace(' ', '',$request->input('user_mobile')), "0");
            $user->user_mobile_code = $request->input('user_mobile_code');
            $user->user_mobile_iso = $request->input('user_mobile_iso');
            $user->user_email = $request->input('user_email');
            $user->user_whatsapp_number = ltrim(str_replace(' ', '',$request->input('user_whatsapp_number')), "0");
            $user->usource = $request->input('USOURCE');
            $user->usubsource = $request->input('USUBSOURCE');
            $user->umedium = $request->input('UMEDIUM');
            $user->ucampaign = $request->input('UCAMPAIGN');
            $user->ucontent = $request->input('UCONTENT');
            $user->uterm = $request->input('UTERM');
            $user->iusource = $request->input('IUSOURCE');
            $user->iumedium = $request->input('IUMEDIUM');
            $user->iucampaign = $request->input('IUCAMPAIGN');
            $user->iucontent = $request->input('IUCONTENT');
            $user->iuterm = $request->input('IUTERM');
            $user->ireferrer = $request->input('IREFERRER');
            $user->ilandpage = $request->input('ILANDPAGE');
            $user->visits = $request->input('VISITS');
            $user->accesstoken = generateAccessToken();
            $user->status =1;
            $user->otp = $otp;
            $user->site_id = $request->input('site_id');
            $user->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $user->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $user->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $user->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            
            $result = $user->save();

            /* Track User Journey */
            $userjourney = new UserJourney();
            $userjourney->user_id = $user->id;
            $userjourney->user_page_visit = "registration";
            $userjourney->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
            $userjourney->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
            $userjourney->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
            $userjourney->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
            $userjourney->save();

            if ($result) {
                 //dynamic entries in tbl_project_amenities
                 if ($request->has('addresslist')) {
                    $addresslist = json_decode($request->input('addresslist'), TRUE);
                    if (count($addresslist) > 0) {
                        foreach ($addresslist as $tmmp) {
                            $Address = new Address();
                          
                                $Address->user_id = $user->id;
                                $Address->address_type = $tmmp['address_type'];
                                $Address->city = $tmmp['city'];
                                $Address->state = $tmmp['state'];
                                $Address->country = $tmmp['country'];
                                $Address->pincode = $tmmp['pincode'];
                                $Address->address = $tmmp['address'];
                                $Address->isprimary = $tmmp['isprimary'];
                                $Address->site_id = $request->input('site_id');
                                $Address->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                                $Address->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                                $Address->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                                $Address->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                                $Address->save();
                            
                        }
                    }
                }

                $businessinfo = new EcomBusinessSetting();
                $userinfo = $user->where('user_id', $user->id)->first();
                $businessinfo = $businessinfo->where('site_id', $request->input('site_id'))->first();

                $content["userinfo"] = $userinfo;
                $content["businessinfo"] = $businessinfo;
                $result = false;
                if($request->has('visitor_tag') && $request->input('visitor_tag') != null){
                    $ecomcartmodel = new EcomCart();
                    $result = $ecomcartmodel->syncCartFromVisitorTag($user->id, $request->input('visitor_tag'));
                }
                $result = registrationEmail($content, $request->input('user_email'));
		if($result){
			  return response()->json(['status' => 200, 'data' => $userinfo, 'otp'=>$otp, 'syncstatus' =>$result]);
	         }else{
			 return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);

                 }

                

                return response()->json(['status' => 200, 'data' => $userinfo, 'otp'=>$otp, 'syncstatus' =>$result]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    
     public function getalluser(Request $request) {
        
        $valid = Validator::make($request->all(), [
                    "limit" => "required|numeric",
                    "offset" => "required|numeric",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $user = new User();
            $data = array();
          
            if ($request->has('offset') && $request->filled('offset')) {
                $data["offset"] = $request->input("offset");
            }

            if ($request->has('limit') && $request->filled('limit')) {
                $data["limit"] = $request->input("limit");
            }
            if ($request->has('user_id') && $request->input('user_id') != "") {
                $data['user_id'] = $request->input('user_id');
            }
            $users = $user->getuser($data);
            foreach($users as $item){
                if(fileuploadtype=="local"){
                    if($item->user_photo==""||$item->user_photo==null||$item->user_photo=="null")
                    {
                        $item->user_photo =fixedpagefeaturedimage;
                    }else{


                        if (File::exists(imagedisplaypath.$item->user_photo)) {
                            $item->user_photo = imagedisplaypath.$item->user_photo;
                        } else {
                            $item->user_photo = fixedpagefeaturedimage;
                        }
                    }
                }
                else
                {
                    if (does_url_exists(imagedisplaypath.$item->user_photo)) {
                        $item->user_photo = imagedisplaypath.$item->user_photo;
                    } else {
                        $item->user_photo =fixedpagefeaturedimage;
                    } 
                }
            }
            return response()->json(['status' => 200, 'count' => count($users), 'data' => $users]);
        }
    }
    
    public function forgotpassword(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_email" => "required",
            
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $guid = generateAccessToken();

            $user = new User();
            $userdata = $user->where('user_email', $request->input('user_email'))->where('status', 1)->get();

            if(count($userdata) > 0){

                /* update accesstoken */
                $user->where('user_email', $request->input('user_email'))->update(
                    array(
                        "accesstoken" => $guid
                    )
                );

                $businessinfo = new EcomBusinessSetting();
                $userinfo = $userdata[0];
                $businessinfo = $businessinfo->where('site_id', $request->input('site_id'))->first();
                $userdata[0]->reset_link = $businessinfo->business_site_url."reset-password?type=customer&guid=".$guid."&user_id=".$userdata[0]->user_id;
                $content['businessinfo'] = $businessinfo;
                $content['userinfo'] = $userinfo;
                forgotPasswordEmail($content,$request->input('user_email'));
                return response()->json(['status' => 200, 'data' =>  'Email sent']);
            }else {
                return response()->json(['status' => 400, 'error' => "No Email Address Found."], 400);
            }

            
        }
    }

    public function updateuser(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
                    "user_id" => "required",
                    "accesstoken" => "required",
                    "updated_by"=> "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $user = new User();
            $newrequest = $request->except(['user_id','site_id','accesstoken','user_photo']);

            if($request->has('user_photo') && $request->filled('user_photo'))
            {
                if(count($request->input('user_photo'))>0){
                    $media_file_base64= $request->input('user_photo');
                    if($media_file_base64!="" && $media_file_base64!=null && $media_file_base64!="null")
                    {
                        if($media_file_base64[0] !="" && $media_file_base64[0] !=null && $media_file_base64[0] !="null")
                        {
                            $responsedata = fileUploadSlim($media_file_base64, "user_photo");
                            $filepath = $responsedata['folderpath'];
                            $newrequest['user_photo'] = $filepath;
                        }
                    }
                }
            }
            /*
            $newrequest['user_mobile_iso'] = $request->input('user_mobile_iso');
            $newrequest['user_mobile_code'] = $request->input('user_mobile_code');
			*/
            $result = $user->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->update($newrequest);

            if ($result) {
                $result1 = $user->where('status', 1)->where('user_id',$request->input('user_id'))->where('site_id',$request->input('site_id'))->get();
                $result1[0]['type']="customer";
                return response()->json(['status' => 200, 'data' =>  $result1]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }
    public function updateuserphoto(Request $request) {
        //validations
        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
                    "user_id" => "required",
                    "accesstoken" => "required",
                    "updated_by"=> "required",
                    "user_photo"=> "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $user = new User();
            $newrequest = $request->except(['user_id','site_id','accesstoken','user_photo']);
            $user_photo= $request->File('user_photo');
                if($user_photo !='' && $user_photo !=null && $user_photo !='null')
                {
                    if ($request->hasFile('user_photo')) {
                        $files = $request->file('user_photo');
                        $filename =$files->getClientOriginalName();
                        $imgpath= fileuploads($filename, $files);
                        
                        $newrequest['user_photo'] = $imgpath;
                        
                    }
                }
            /*
            $newrequest['user_mobile_iso'] = $request->input('user_mobile_iso');
            $newrequest['user_mobile_code'] = $request->input('user_mobile_code');
			*/
            $result = $user->where('site_id', $request->input('site_id'))->where('user_id', $request->input('user_id'))->update($newrequest);

            if ($result) {
                $result1 = $user->where('status', 1)->where('user_id',$request->input('user_id'))->where('site_id', $request->input('site_id'))->get();
                $result1[0]['type']="customer";
                return response()->json(['status' => 200, 'data' =>  $result1]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }
        }
    }

    public function changepassword(Request $request) {
        //validations
        $user = new User();
        $valid = Validator::make($request->all(), [
            "site_id" => "required",
            "user_id" => "required",
            "accesstoken" => "required",
            "updated_by"=> "required",
            "user_password"=> "required",
            "new_password"=> "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $userslist = $user->where('status', 1)->where('user_password',md5($request->input('user_password')))->where('site_id',$request->input('site_id'))->where('user_id',$request->input('user_id'))->get();

            if(count($userslist)>0){

                $newrequest = $request->except(['user_id','site_id','accesstoken','new_password','user_password']);
                $newrequest['user_password'] = md5($request->input('new_password'));
                
                $result = $user->where('user_id', $userslist[0]->user_id)->where('site_id', $request->input('site_id'))->update($newrequest);
                if ($result) {
                    return response()->json(['status' => 200, 'data' => 'Password updated.']);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Old Password Is Incorrect.'], 400);
                }
            }else{
                return response()->json(['status' => 400, 'error' => 'Old Password Is Incorrect.'], 400);
            }
        }
    }
    
     public function deleteuser(Request $request) {
        $valid = Validator::make($request->all(), [
                    "site_id" => "required",
                    "updated_by"=> "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $user = new User();
            $request->request->add(['status' => 0]);
            $newrequest = $request->except(['user_id']);
            $result = $user->where('user_id', $request->input('user_id'))->where('site_id', $request->input('site_id'))->update($newrequest);
            if ($result) {
                return response()->json(['status' => 200, 'data' => $request->all()]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
            }
        }
    }

    public function sendotp(Request $request) {
        $valid = Validator::make($request->all(), [
                "user_mobile" => "required|numeric",
         ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $user = new User();
            $otp = rand(100000, 999999);
            $user_mobile= ltrim(str_replace(' ', '',$request->input('user_mobile')), "0");
            $query = $user->select('*')->where('user_mobile', '=' ,  $user_mobile)->where('status', '=' ,  "1")->get();
            if(count($query)==0)
            {
               /* $user->user_mobile =$user_mobile;
                $result = $user->save();
                $user_id=$user->id;*/
                return response()->json(["status" => 400,"data" =>"Please create your account."], 400);
            }
            else
            {
                $user_id=$query[0]->user_id;
				$result1 = $user->where('user_id', '=', $user_id)->update(['otp' => $otp]);
				if ($result1) {
					$message="Dear User, Your OTP for login to CREDAI365.COM portal is  ".$otp.". Valid for 30 minutes. Please do not share this OTP. Regards, CREDAI Chennai Team";
				//	$status=sendsms($user_mobile,$message);
                     $status=1;
					if($status)
					{
						if($query[0]->user_email != "" || $query[0]->user_email != null){
							verifyOTP($otp, $query[0]->user_email);
						}
						return response()->json(['status' => 200,'data' =>"OTP send successfully"]);
					}
					else
					{
						return response()->json(['status' => 200,'data' =>$otp]);
					}
			  
				}
                
            }
        }
    }

    public function verifyotp(Request $request) {
        $valid = Validator::make($request->all(), [
            "otp" => "required|numeric",
            "user_mobile" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            //return response()->json(['status' => 400, 'error' => $otplist],400);
            if($request->input('otp') !== defaultotp){
                $user = new User();
                $res = $user->select('*')->where('user_mobile', '=' , ltrim(str_replace(' ', '',$request->input('user_mobile')), "0"))
                ->Where('otp', $request->input('otp'))
                ->get();
            }else{
                $user = new User();
                $res = $user->select('*')->where('user_mobile', '=' , ltrim(str_replace(' ', '',$request->input('user_mobile')), "0"))
                ->get();
            }
            
            if(count($res)>0)
            {       
                
                    $user = new User();
                    $res[0]['type']="customer";
                    $otp=null;
                    $user_id= $res[0]->user_id;
                    $result1 = $user->where('user_id', '=', $user_id)->update(['otp' => $otp,'status'=>1]);
					
					$phone = $res[0]->user_mobile;

					//Whatsapp opt in call
					$curl = curl_init();

					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/app/opt/in/CREDAIChennai",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => "user=91".$phone,
						CURLOPT_HTTPHEADER => array(
							"apikey: 5c63e92c5313479ac77c5c5d54f24a9c",
							"cache-control: no-cache",
							"content-type: application/x-www-form-urlencoded",
							"postman-token: 5b77e9f6-f4ed-b422-c4d5-82a0c731f298"
						),
					));
		
					$response = curl_exec($curl);
					$err = curl_error($curl);
					$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
					curl_close($curl);
					
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://api.gupshup.io/sm/api/v1/msg",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "channel=whatsapp&source=917834811114&destination=91$phone&src.name=CREDAIChennai&message=%7B%0A%20%20%20%20%20%20%20%20%22isHSM%22%3A%22true%22%2C%0A%20%20%20%20%20%20%20%20%22type%22%3A%20%22text%22%2C%0A%20%20%20%20%20%20%20%20%22text%22%3A%20%22Thank%20you%20for%20Subscribing%20in%20CREDAI365.com%20%20Welcome%20to%20CREDAI365%20BOT.%22%0A%7D",
					  CURLOPT_HTTPHEADER => array(
						"apikey: 5c63e92c5313479ac77c5c5d54f24a9c",
						"cache-control: no-cache",
						"content-type: application/x-www-form-urlencoded",
						"postman-token: 1fdab451-ef44-ffbc-86a0-0e12a6be0e83"
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);
					$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
					curl_close($curl);
                    
                    /* Track User Journey */
                    $userjourney = new UserJourney();
                    $userjourney->user_id = $res[0]->user_id;
                    $userjourney->user_page_visit = "login";
                    $userjourney->browser_name = $request->has('browser_name')?$request->input('browser_name'):null;
                        $userjourney->browser_version = $request->has('browser_version')?$request->input('browser_version'):null;
                        $userjourney->browser_platform = $request->has('browser_platform')?$request->input('browser_platform'):null;
                        $userjourney->ip_address = $request->has('ip_address')?$request->input('ip_address'):null;
                    $userjourney->save();

                    return response()->json(['status' => 200, 'data' => $res[0], 'response code' =>$httpcode]);
                 
            }
            else
            {
                return response()->json(['status' => 400, 'error' => "Wrong OTP"],400);
            }
            
        }
    }

    public function getcompleteuserslist() {        
        $user = new User();
        $userslist = $user->select('user_email','user_mobile','user_firstname','user_lastname','user_whatsapp_number','created_at')->get();
        return response()->json(['status' => 200, 'count' => count($userslist), 'data' => $userslist]);
    }


    public function changePasswordByGuid(Request $request){

        $valid = Validator::make($request->all(), [
            "user_id" => "required",
            "user_type" => "required",
            "guid" => "required",
            "user_password" => "required",
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $user = new User();

            $user_type = $request->input("user_type");
            $user_id = $request->input("user_id");
            $guid = $request->input("guid");
            $password = $request->input("password");
            $result = false;

            $user_data = $user->where('accesstoken', $request->input('guid'))->where('status', 1)->get();

            if(count($user_data)>0){
                $newrequest = array();
                $newrequest['user_password'] = md5($request->input('user_password'));
                $newrequest['accesstoken'] = $request->input('accesstoken');
                $result = $user->where('user_id', $request->input('user_id'))->update($newrequest);

                if ($result) {
                    return response()->json(['status' => 200, 'data' => 'Password Change Succesfully.']);
                } else {
                    return response()->json(['status' => 400, 'error' => 'Something went wrong.'], 400);
                }
            }else {
                return response()->json(['status' => 400, 'error' => "Your link has been expired."], 400);
            }
        }
    }

    public function changePasswordByid(Request $request){

        $valid = Validator::make($request->all(), [
            "userid" => "required",
            "usertype" => "required",
            "newpassword" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {

            $type = $request->input("usertype");
            $userid = $request->input("userid");
            $result = false;
            $newrequest = array();
           
            if($type=="builder"){
                $builder = new Builder();
                $newrequest['builder_password'] = md5($request->input('newpassword'));
                $result = $builder->where('builder_id',$userid)->update($newrequest);
            }else if($type=="salesexecutive"){
                $salesExecutive = new SalesExecutive();
                $newrequest['sales_executive_password'] = md5($request->input('newpassword'));
                $result = $salesExecutive->where('sales_executive_id', $userid)->update($newrequest);
            }else if($type=="admin"){
                $result = true;
            }else{
                return response()->json(['status' => 400, 'error' => "Please enter valid data."], 400);
            }

            if ($result) {
                return response()->json(['status' => 200, 'data' => "Password Updated succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }

    }
    
    public function addNewMasterDataRequest(Request $request){
        $valid = Validator::make($request->all(), [
            "type" => "required",
            "value" => "required",
            "builder_id" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $builder = new Builder();
            $builderdata = $builder->where('builder_id', $request->input('builder_id'))->first();
            if (isset($builderdata->builder_name)) {
                requestForMasterData($builderdata->builder_name, $request->input('type'), $request->input('value')) ;
                return response()->json(['status' => 200, 'data' => "New Master Data Request Sent succesfully"]);
            } else {
                return response()->json(['status' => 400, 'error' => "Something went wrong."], 400);
            }

        }
    }

    public function checklogin(Request $request) {

        $user=new User(); 
        $valid = Validator::make($request->all(), [
                    "user_email" => "required",
                    "user_password" => "required"
        ]);

        if ($valid->fails()) {
            return response()->json(['status' => 400, 'error' => $valid->errors()], 400);
        } else {
            $result = $user->where('status', 1)->where('user_email',$request->input('user_email'))->where('user_password', md5($request->input('user_password')))->get();
            if (count($result) >= 1) {
               $newresult = $result[0];
                    $user->where('user_id', $newresult->user_id)->update(array('accesstoken' => generateAccessToken()));
                    $newresult = $user->where('user_id', $newresult->user_id)->first(); 
                 
                
                    $request->session()->put('user', $newresult);
                   
                return response()->json(['status' => 200, 'data' => $newresult]);
            } else {
                return response()->json(['status' => 400, 'error' => 'Invalid Username Or Password']);
            }
        }
    }
}
