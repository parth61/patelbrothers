<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Occupation extends Model
{
    protected  $table = 'tbl_occupation';
    protected $fillable=['occupation_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getoccupation($data)
    {
        
         $query = DB::table('tbl_occupation as c')->select('c.*');
         
         if (array_key_exists('occupation_id', $data) && isset($data['occupation_id'])) {
            $query = $query->where('c.occupation_id', '=' ,$data['occupation_id']);
           }
                          
         $query = $query->where('c.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.occupation_id', 'ASC')->get();
                            
         return $result;
    }
}
