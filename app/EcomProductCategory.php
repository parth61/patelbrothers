<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductCategory extends Model
{
   
    protected $table = 'tbl_ecom_product_category';
    protected $fillable=['product_category_id','product_id','category_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getproductcategory($data)
    {
        
        $query = DB::table('tbl_ecom_product_category as procat','promd.media_file  as cat_media_file')
         ->leftJoin('tbl_ecom_category as ec', 'procat.category_id', '=', 'ec.category_id')
         ->leftJoin('tbl_media as promd', 'promd.media_id', '=', 'ec.category_thumbnail');
         
        if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('procat.category_id', '=' ,$data['category_id']);
        }

        if (array_key_exists('product_category_id', $data) && isset($data['product_category_id'])) {
            $query = $query->where('procat.product_category_id', '=' ,$data['product_category_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('procat.product_id', '=' ,$data['product_id']);
        }
        
      
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('procat.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('procat.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('procat.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

    public  function getallcategorywithcount($data)
    {
        
        $query = DB::table('tbl_ecom_product_category as procat')->select('ec.category_name','ec.category_id',DB::raw('count(*) as total'),'ec.parent_category_id','promd.media_file  as cat_media_file')
         ->leftJoin('tbl_ecom_category as ec', 'procat.category_id', '=', 'ec.category_id')
         ->leftJoin('tbl_media as promd', 'promd.media_id', '=', 'ec.category_thumbnail')
         ->leftJoin('tbl_ecom_product as ep', 'ep.product_id', '=', 'procat.product_id');

        $query = $query->where('procat.status', '=' ,1);
        $query = $query->where('ec.status', '=' ,1);
        $query = $query->where('ep.status', '=' ,1);

        if (array_key_exists('is_variation', $data) && isset($data['is_variation'])) 
        {       
            if($data['is_variation']=="1"){
                $query = $query->whereIn('ep.product_type', ['simple','variant']);
            }else{
                $query = $query->whereIn('ep.product_type', ['simple','master']);
            }
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('ec.site_id', '=' ,$data['site_id']);
        }
                          
        $result = $query->groupby('ec.category_id')->get();
                            
        return $result;
    }

    public  function getproductcategorylist($data)
    {
        
        $query = DB::table('tbl_ecom_product_category as procat')
         ->leftJoin('tbl_ecom_category as ec', 'procat.category_id', '=', 'ec.category_id');

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('procat.product_id', '=' ,$data['product_id']);
        }
      
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('procat.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('procat.status', '=' ,1);
        }
                          
        $result = $query->pluck('ec.category_name');
                            
        return $result;
    }
}
