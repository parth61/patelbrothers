<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomRelatedProduct extends Model
{
    protected $table = 'tbl_ecom_related_product';
    protected $fillable=['product_id','related_id','tag_id','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getrelatedproduct($data)
    {
        
        $query = DB::table('tbl_ecom_related_product as rp')
        ->select('rp.*','prorel.*')
       // ->leftJoin('tbl_ecom_product as pro', 'pro.product_id', '=', 'rp.product_id')
        ->leftJoin('tbl_ecom_product as prorel', 'prorel.product_id', '=', 'rp.related_id');
      
        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('rp.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('related_id', $data) && isset($data['related_id'])) {
            $query = $query->where('rp.related_id', '=' ,$data['related_id']);
        }
        
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('rp.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('rp.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('rp.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('rp.related_product_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
