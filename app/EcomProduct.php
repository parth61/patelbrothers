<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use DB;
class EcomProduct extends Model
{
 protected $table = 'tbl_ecom_product';
    protected $fillable=['product_category_id','status','tax_class_id','tax_status_id','slug_name','is_backorders','low_stock_threshold',
    'product_height','product_length','product_weight','product_wide','has_varation','isvisible',
    'views','is_featured','is_variation','is_searchable','height_unit','sale_type','start_date','end_date',
   'is_stockmanagement','is_soldindividual', 'shipping_class_id','length_unit','wide_unit','product_name','product_short_description','product_description','product_stock','product_actual_price','product_sale_price','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public function getproduct($data)
    {
        $query = DB::table('tbl_ecom_product as pro')
        ->select('pro.*','pro.slug_name as product_slug','seo.seo_title','seo_description',
        'seo_keywords','seo_image','seo_type','seo_for','seo_for_id','seo_canonical','medi.media_file as seo_media_file')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
       
        if (array_key_exists('product_id', $data) && isset($data['product_id']))
        {
            $query = $query->where('pro.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('product_guid', $data) && isset($data['product_guid']))
        {
            $query = $query->where('pro.product_guid', '=' ,$data['product_guid']);
        }

        if (array_key_exists('is_variation', $data) && isset($data['is_variation']))
        {
            $query = $query->where('pro.is_variation', '=' ,$data['is_variation']);
        }
     
        //$query = $query->where('pro.is_variation', '=' ,0);

        if (array_key_exists('is_variation', $data) && isset($data['is_variation'])) 
        {
            $query = $query->where('pro.is_variation', '=' ,$data['is_variation']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) 
        {
            $query = $query->where('pro.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('product_category_id', $data) && isset($data['product_category_id'])) 
        {
            $query = $query->where('pro.product_category_id', '=' ,$data['product_category_id']);
        }
        if (array_key_exists('product_name', $data) && isset($data['product_name']))
        {
            $query = $query->where('pro.product_name','like', '%'.$data['product_name'].'%');
        }
        if (array_key_exists('product_type', $data) && count($data['product_type'])>0) 
        {
            $query = $query->whereIn('pro.product_type', $data['product_type']);
        }
        
		
		if (array_key_exists('site_id', $data) && isset($data['site_id'])) 
        {
            $query = $query->where('pro.site_id', $data['site_id']);
        }
           
        if (array_key_exists('status', $data) && isset($data['status'])) 
        {
            $query = $query->where('pro.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pro.status', '!=' ,0);
        }

        $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) 
        {
            $query = $query->orderBy('pro.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('pro.product_id', 'ASC');
        }

        $result = $query->get();
                            
        return $result;
    }   

    public function getproductvariant($data)
    {
        $wishlistold = array();
        $variableproductidlist =array();
        $query = DB::table('tbl_ecom_product as pro')
        ->select('pro.*','pro.slug_name as product_slug','seo.seo_title','seo_description',
        'seo_keywords','seo_image','seo_type','seo_for','seo_for_id','seo_canonical','medi.media_file as seo_media_file')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
        
        if (array_key_exists('product_guid', $data) && isset($data['product_guid']))
        {
            $query = $query->where('pro.product_guid', '=' ,$data['product_guid']);
        }
        if(array_key_exists('product_id', $data) && isset($data['product_id']))
        {
            $product_id=$data['product_id'];
            $variableproductidlist = DB::table('tbl_ecom_product_variant as pv')
            ->where('pv.status', '=', 1)
            ->where('pv.product_id', '=', $product_id)
            ->pluck('pv.variant_product_id');
            $query = $query->whereIn('pro.product_id',$variableproductidlist);
        }


        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) 
        {
            $query = $query->where('pro.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('product_category_id', $data) && isset($data['product_category_id'])) 
        {
            $query = $query->where('pro.product_category_id', '=' ,$data['product_category_id']);
        }

           
        if (array_key_exists('status', $data) && isset($data['status'])) 
        {
            $query = $query->where('pro.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pro.status', '!=' ,0);
        }

        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) 
        {
            $query = $query->orderBy('pro.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('pro.product_id', 'DESC');
        }

        $product = $query->get();

        $productdata = array();
       
        foreach($product as $item)
        {
            $product_id = $item->product_id;
            
            
            // if(array_key_exists('user_id', $data) && isset($data['user_id']) && array_key_exists('site_id', $data) && isset($data['site_id'])  ){
                
            //     $wishlist1=$wishlist->where('user_id','=',$data['user_id'])->where('site_id','=',$data['site_id'])->where('product_id', $item->product_id)->where('status','=','1');
            //     if(count($wishlist1)>0){
            //         $item->is_wishlist=true;
                    
            //     }else{
            //         $item->is_wishlist=false;
            //     }
            // }else{
            //     $item->is_wishlist=false;
            // }
            
            $productgallery = new EcomProductGallery();
            $data['product_id']=$product_id;
            $productgallery = $productgallery->getproductgallery($data);
            $productgallerydata=array();
            foreach ($productgallery as $item1) {
                
                if($item1->product_image_media_file==""||$item1->product_image_media_file==null||$item1->product_image_media_file=="null")
                {
                    $item1->product_image_path =fixedproductgalleryimage;
                    
                }
                else
                {
                    if(fileuploadtype=="local")
                    {
                            if (File::exists(baseimagedisplaypath.$item1->product_image_media_file)) {
                                $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                            } else {
                                $item1->product_image_path =fixedproductgalleryimage;
                            }
                        
                    }
                    else
                    {
                        if (does_url_exists(imagedisplaypath.$item1->product_image_media_file)) {
                            $item1->product_image_path = imagedisplaypath.$item1->product_image_media_file;
                        } else {
                            $item1->product_image_path = fixedproductgalleryimage;
                        }
                    }
                }  

                
                array_push($productgallerydata, $item1);
            }

            
            $item->product_gallery=$productgallerydata;
            $productattribute = DB::table('tbl_ecom_product_attribute as proatt')
                ->select('proatt.*','att.attribute_name')
                ->leftJoin('tbl_ecom_attribute as att', 'att.attribute_id', '=', 'proatt.attribute_id')
                ->where('proatt.status', '=', 1)
                ->where('proatt.product_id', '=',$item->product_id)
                ->get();
            
            $productattributearray=array();

            foreach($productattribute as $item22)
            {
                $selectedattributetermlist = DB::table('tbl_ecom_product_variant_attribute_term as provarattr')
                ->where('provarattr.status', '=', 1)
                ->where('provarattr.variant_product_id', '=',$product_id )
                ->where('provarattr.attribute_id', '=', $item22->attribute_id)
                ->pluck('provarattr.attribute_term_id');

                $attributetermlist = DB::table('tbl_ecom_attribute_term as attrterm')
                ->select('attrterm.attribute_term_id','attrterm.attribute_term')
                ->where('attrterm.status', '=', 1)
                ->where('attrterm.attribute_id', '=', $item22->attribute_id)
                ->get();
                $item22->selectedattributetermlist= $selectedattributetermlist;
                $item22->attributetermlist= $attributetermlist;
                array_push($productattributearray,$item22);
            }

            $item->attributedata=$productattributearray;
        
            array_push($productdata, $item);
            // }
           
        }
                            
        return $productdata;
                            
        /*return $result;*/
    }
    
    public function getproductlist($data)
    {

        $query = DB::table('tbl_ecom_product as pro')
        ->select('pro.product_id','pro.product_code','pro.product_name','pro.product_stock','pro.stock_status','pro.product_actual_price','pro.product_sale_price','pro.product_height','pro.product_length','pro.status','pro.product_weight','pro.product_wide','pro.isvisible','pro.sale_type','pro.start_date','pro.end_date','pro.views','pro.is_featured','pro.is_variation','pro.is_soldindividual','pro.has_variation','pro.is_searchable','pro.stock_status','pro.is_assured','pro.is_stockmanagement','pro.low_stock_threshold','pro.product_guid','pro.slug_name as product_slug','seo.seo_title','seo_description','seo_keywords','seo_image','seo_type','seo_for','seo_for_id','seo_canonical','medi.media_file as seo_media_file')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
       
        if (array_key_exists('product_guid', $data) && isset($data['product_guid']))
        {
            $query = $query->where('pro.product_guid', '=' ,$data['product_guid']);
        }
        if (array_key_exists('product_id', $data) && isset($data['product_id']))
        {
            $query = $query->where('pro.product_id', '=' ,$data['product_id']);
        }

        if (array_key_exists('product_guid', $data) && isset($data['product_guid']))
        {
            $query = $query->where('pro.product_guid', '=' ,$data['product_guid']);
        }

        if (array_key_exists('product_name', $data) && isset($data['product_name']))
        {
            $query = $query->where('pro.product_name','like', '%'.$data['product_name'].'%');
        }
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) 
        {
            $query = $query->where('pro.slug_name', '=' ,$data['slug_name']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('pro.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('is_featured', $data) && isset($data['is_featured'])) 
        {
            $query = $query->where('pro.is_featured', '=' ,$data['is_featured']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) 
        {
            $query = $query->where('pro.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('pro.status', '!=' ,0);
        } 

        if (array_key_exists('is_variation', $data) && isset($data['is_variation'])) 
        {
            //$query = $query->where('pro.is_variation', '=' ,$data['is_variation']);
            if($data['is_variation']=="1"){
                $query = $query->whereIn('pro.product_type', ['simple','variant']);
            }else{
                $query = $query->whereIn('pro.product_type', ['simple','master']);
            }
        }

        if (array_key_exists('stock_status', $data) && isset($data['stock_status'])) 
        {
            $query = $query->where('pro.stock_status', '=' ,$data['stock_status']);
        }

        if (array_key_exists('is_assured', $data) && isset($data['is_assured'])) 
        {
            $query = $query->where('pro.is_assured', '=' ,$data['is_assured']);
        }    

        /*$query = $query->whereNotNull('pro.product_actual_price');*/

        /*if (array_key_exists('category_id', $data) && isset($data['category_id'])) 
        {
            $productlist = DB::table('tbl_ecom_product_category as pro_cat')->where('pro_cat.category_id', '=' ,$data['category_id'])->where('status',1)->pluck('pro_cat.product_id');
            $query = $query->whereIn('pro.product_id', $productlist);
        }*/

        if (array_key_exists('categoryidlist', $data) && isset($data['categoryidlist'])) 
        {
            if(count($data['categoryidlist']) > 0){
                if(isset($data['is_variation']) && $data['is_variation'] == "1"){
                    $productlist = DB::table('tbl_ecom_product_category as pro_cat')
                        ->leftJoin('tbl_ecom_product as tp', 'tp.product_id', '=', 'pro_cat.product_id')
                        ->whereIn('pro_cat.category_id', $data['categoryidlist'])
                        ->where('pro_cat.status',1)
                        ->whereIn('tp.product_type', ['simple','variant'])
                        ->pluck('pro_cat.product_id');
                }else{
                    $productlist = DB::table('tbl_ecom_product_category as pro_cat')
                        ->leftJoin('tbl_ecom_product as tp', 'tp.product_id', '=', 'pro_cat.product_id')
                        ->whereIn('pro_cat.category_id', $data['categoryidlist'])
                        ->where('pro_cat.status',1)
                        ->whereIn('tp.product_type', ['simple','master'])
                        ->pluck('pro_cat.product_id');
                }
                
                $query = $query->whereIn('pro.product_id', $productlist);
            }
        }

        if (array_key_exists('attributetermidlist', $data) && isset($data['attributetermidlist'])) 
        {
            if(count($data['attributetermidlist']) > 0){
                $productlist = DB::table('tbl_ecom_product_attribute_term as pro_attr_term')
                ->whereIn('pro_attr_term.attribute_term_id', $data['attributetermidlist'])->where('pro_attr_term.status',1)->pluck('pro_attr_term.product_id');
                $variantproductlist = DB::table('tbl_ecom_product_variant_attribute_term as pro_attr_term')
                ->whereIn('pro_attr_term.attribute_term_id', $data['attributetermidlist'])->where('pro_attr_term.status',1)->pluck('pro_attr_term.variant_product_id');

                $finallist = array();
                foreach($productlist as $item){
                    array_push($finallist, $item);
                }
                foreach($variantproductlist as $item){
                    array_push($finallist, $item);
                }
                
                $query = $query->whereIn('pro.product_id', $finallist);
            }
        }
        
        if(isset($data['is_variation']) && $data['is_variation']=="1" ){
            if (array_key_exists('minprice', $data) && isset($data['minprice']) && array_key_exists('maxprice', $data) && isset($data['maxprice'])) 
            {
                $query = $query->whereBetween('pro.product_actual_price', [floatval($data['minprice']), floatval($data['maxprice'])]);
            }
        }

        $result['fullcount'] = $query->count();
        
        /*if (array_key_exists('productidlist', $data) && isset($data['productidlist']))
        {
            $query = $query->whereNotIn('pro.product_id', $data['productidlist']);
        }*/
        
        $query = $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby'])) 
        {
            if($data['sortby']==="pricelowtohigh"){
                $query = $query->orderBy('pro.product_actual_price', 'ASC');
                //$query = $query->orderByRaw("CASE WHEN pro.product_sale_price = 'null' THEN pro.product_actual_price ELSE pro.product_sale_price END ASC");
            }else if($data['sortby']==="pricehightolow"){
                $query = $query->orderBy('pro.product_actual_price', 'DESC');
                //$query = $query->orderByRaw("CASE WHEN pro.product_sale_price = 'null' THEN pro.product_actual_price ELSE pro.product_sale_price END DESC");
            }else if($data['sortby']==="nameatoz"){
                $query = $query->orderBy('pro.product_name', 'ASC');
            }else if($data['sortby']==="nameztoa"){
                $query = $query->orderBy('pro.product_name', 'DESC');
            }else{
               /*if(array_key_exists('sorttype', $data) && isset($data['sorttype'])){
                    $query = $query->orderBy('pro.'.$data['sortby'], $data['sorttype']);
                }else{
                    $query = $query->orderBy('pro.'.$data['sortby']);
                }*/
                $query = $query->orderBy('pro.product_id');
            }
        }

        $result['result'] = $query->get();

        return $result;

    }
    
    public function getrelatedproductlist($data)
    {

        $query = DB::table('tbl_ecom_related_product as erp')
        ->select('pro.*', 'pro.slug_name as product_slug','seo.seo_title','seo_description',
        'seo_keywords','seo_image','seo_type','seo_for','seo_for_id','seo_canonical','medi.media_file as seo_media_file')
        ->leftJoin('tbl_ecom_product as pro', 'pro.product_id', '=', 'erp.related_id')
        ->leftJoin('tbl_seo as seo', 'seo.seo_id', '=', 'pro.seo_id')
        ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
       
        if (array_key_exists('product_guid', $data) && isset($data['product_guid']))
        {
            $query = $query->where('pro.product_guid', '=' ,$data['product_guid']);
        }
        if (array_key_exists('product_id', $data) && isset($data['product_id']))
        {
            $query = $query->where('erp.product_id', '=' ,$data['product_id']);
        }
     
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) 
        {
            $query = $query->where('pro.slug_name', '=' ,$data['slug_name']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('pro.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) 
        {
            $query = $query->where('pro.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('pro.status', 1);
        }        

        $query = $query->where('erp.status', '=' ,1)->where('pro.is_variation', '=' ,0)->where('pro.is_searchable', '=' ,1)->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) 
        {
            $query = $query->orderBy('pro.'.$data['sortby'], $data['sorttype']);
        }else
        {
            $query = $query->orderBy('pro.product_id', 'DESC');
        }

        $result = $query->get();
                            
        return $result;

    }

    public function getminmaxforproductid($product_id)
    {
        $productidlist = DB::table('tbl_ecom_product_variant as tepv')
        ->where('tepv.product_id', '=', $product_id)
        ->where('tepv.status', '=', 1)
        ->pluck('tepv.variant_product_id');
        
        $price = DB::table('tbl_ecom_product as tep')
        ->select(DB::raw("MIN(tep.product_actual_price) AS minprice, MAX(tep.product_actual_price) AS maxprice"))
        ->whereIn('tep.product_id', $productidlist)
        ->where('tep.status', '=', 1)
        ->get();

        $data = array();
        $data['maxprice'] = $price[0]->minprice;
        $data['minprice'] = $price[0]->minprice;
        $data['product_id'] = $productidlist;
        return $data;
    }

    public function getattributeandattrtermlistforproductid($product_id)
    {
        $attributelist = DB::table('tbl_ecom_product_variant_attribute_term as tepv')
            ->select('tea.*','tepv.variant_product_id')
            ->leftJoin('tbl_ecom_attribute as tea', 'tea.attribute_id', '=', 'tepv.attribute_id')
            ->where('tepv.variant_product_id', '=', $product_id)
            ->where('tepv.status', '=', 1)
            ->get();

        $attributetermlist = DB::table('tbl_ecom_product_variant_attribute_term as tepv')
            ->select('teat.*')
            ->leftJoin('tbl_ecom_attribute_term as teat', 'teat.attribute_term_id', '=', 'tepv.attribute_term_id')
            ->where('tepv.variant_product_id', '=', $product_id)
            ->where('tepv.status', '=', 1)
            ->get();

        foreach ($attributelist as $item) {
            $temp = array();
            foreach ($attributetermlist as $attrtermitem) {
                if($attrtermitem->attribute_id == $item->attribute_id){
                    array_push($temp, $attrtermitem);
                }
            }
            $item->attrtermlist = $temp;
        }

        return $attributelist;
    }
}
