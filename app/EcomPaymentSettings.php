<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomPaymentSettings extends Model
{
    protected $table = 'tbl_ecom_payment_settings';
    protected $fillable=['ecom_payment_settings_id','site_id','gateway_name','sendbox_url','sendbox_key','sendbox_secret','production_key','production_secret','status','browser_name','browser_version','browser_platform','ip_address','created_at','updated_at','created_by','updated_by'];

    public static function getpaymentsettings($data)
    {
        
        $query = DB::table('tbl_ecom_payment_settings as eps')->select('eps.*');
         
        if (array_key_exists('ecom_payment_settings_id', $data) && isset($data['ecom_payment_settings_id'])) {
            $query = $query->where('eps.ecom_payment_settings_id', '=' ,$data['ecom_payment_settings_id']);
        }

    

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('eps.site_id', '=' ,$data['site_id']);
        }

      
                      
        if (array_key_exists('status', $data) && isset($data['status'])) 
        {
            $query = $query->where('eps.status', '=' ,$data['status']);
        }else{
            $query = $query->where('eps.status', '!=' ,0);
        }
        

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) 
        {
            $query = $query->orderBy('eps.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('eps.ecom_payment_settings_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}