<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductConfigurable extends Model
{
    protected $fillable=['product_configurable_id','product_id','configurable_product_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getproductconfigurable($data)
    {
        
        $query = DB::table('tbl_ecom_product_configurable as proconf')->select('proconf.*');
         
        if (array_key_exists('configurable_product_id', $data) && isset($data['configurable_product_id'])) {
            $query = $query->where('proconf.configurable_product_id', '=' ,$data['configurable_product_id']);
        }

        if (array_key_exists('product_configurable_id', $data) && isset($data['product_configurable_id'])) {
            $query = $query->where('proconf.product_configurable_id', '=' ,$data['product_configurable_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('proconf.product_id', '=' ,$data['product_id']);
        }
        
  
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('proconf.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('proconf.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('proconf.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('proconf.product_configurable_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
