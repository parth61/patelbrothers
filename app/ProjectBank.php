<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProjectBank extends Model
{
    protected $table = 'tbl_project_banks';
    protected $fillable=['project_id','bank_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
    public static function getprojectbank($data)
    {
        
         $query = DB::table('tbl_project_banks as tbp')
         ->select('tbp.*','b.bank_name')
         ->leftJoin('tbl_banks as b','b.bank_id','=','tbp.bank_id');
         
           if (array_key_exists('project_bank_id', $data) && isset($data['project_bank_id'])) {
            $query = $query->where('tbp.project_bank_id', '=' ,$data['project_bank_id']);
           }
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('tbp.project_id', '=' ,$data['project_id']);
           }
           if (array_key_exists('bank_id', $data) && isset($data['bank_id'])) {
            $query = $query->where('tbp.bank_id', '=' ,$data['bank_id']);
           }
                          
         $query = $query->where('tbp.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tbp.pbid', 'ASC')->get();
                            
         return $result;
    }
}
