<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Configuration extends Model
{
      protected $table = 'tbl_configuration';
    protected $fillable=['configuration_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
    public static function getconfiguration($data)
    {
        
         $query = DB::table('tbl_configuration as c')->select('c.*');
         
         if (array_key_exists('configuration_id', $data) && isset($data['configuration_id'])) {
            $query = $query->where('c.configuration_id', '=' ,$data['configuration_id']);
           }
                          
         $query = $query->where('c.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.configuration_id', 'ASC')->get();
                            
         return $result;
    }
}
