<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Shipping extends Model
{
 	protected $table = 'tbl_shipping';
    protected $fillable=['shipping_method','shipping_city','shipping_state','shipping_country','shipping_amount','shipping_pincode','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getshipping($data)
    {
        
        $query = DB::table('tbl_shipping as ship')->select('ship.*');
         
        if (array_key_exists('shipping_id', $data) && isset($data['shipping_id'])) {
            $query = $query->where('ship.shipping_id', '=' ,$data['shipping_id']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ship.site_id', '=' ,$data['site_id']);
        }
                          
        $query = $query->where('ship.status', '=' ,1);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('ship.shipping_id', 'ASC')->get();
                            
        return $result;
    }   
}
