<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomFaqCategory extends Model
{
   
    protected $table = 'tbl_ecom_faq_category';
    protected $fillable=['faq_category_id','faq_category','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getecomfaqcategory($data)
    {
        
        $query = DB::table('tbl_ecom_faq_category as faqcategory')->select('faqcategory.*');
         
        if (array_key_exists('faq_category_id', $data) && isset($data['faq_category_id'])) {
            $query = $query->where('faqcategory.faq_category_id', '=' ,$data['faq_category_id']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('faqcategory.site_id', '=' ,$data['site_id']);
        }

        $query = $query->where('faqcategory.status', '=' ,1)->offset($data['offset'])->limit($data['limit']);


        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('faqcategory.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->get();
                            
        return $result;
    }

    public function getecomfaqcategorywithfaq($data)
    {
        
        $query = DB::table('tbl_ecom_faq_category as faqcategory')->select('faqcategory.*');
         
        if (array_key_exists('faq_category_id', $data) && isset($data['faq_category_id'])) {
            $query = $query->where('faqcategory.faq_category_id', '=' ,$data['faq_category_id']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('faqcategory.site_id', '=' ,$data['site_id']);
        }

        $query = $query->where('faqcategory.status', '=' ,1);


        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('faqcategory.'.$data['sortby'], $data['sorttype']);
        }
                          
        $faqcategorydata = $query->get();

        $query ="";

        $query = DB::table('tbl_ecom_faq as faq')->select('faq.*');

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('faq.site_id', '=' ,$data['site_id']);
        }

        $query = $query->where('faq.status', '=' ,1);

        $faqdata = $query->get();

        $result = array();

        foreach($faqcategorydata as $item){

            $tempfaqdata = array();

            foreach($faqdata as $subitem){
                if($subitem->faq_category_id === $item->faq_category_id){
                    array_push($tempfaqdata, $subitem);
                }
            }
            $result[$item->faq_category] = $tempfaqdata;
        }

                            
        return $result;
    }
}
