<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    protected  $table = 'tbl_state';
    protected $fillable=['country_id','state_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getstate($data)
    {
        
        $query = DB::table('tbl_state as s')->select('s.*','c.country_name')
        ->leftJoin('tbl_country as c', 'c.country_id', '=', 's.country_id');
         
        if (array_key_exists('country_id', $data) && isset($data['country_id'])) {
            $query = $query->where('s.country_id', '=' ,$data['country_id']);
        }

    if (array_key_exists('state_id', $data) && isset($data['state_id'])) {
            $query = $query->where('s.state_id', '=' ,$data['state_id']);
        }
                          
        $query = $query->where('s.status', '!=' ,0);
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }            
        $result = $query->orderBy('s.state_id', 'ASC')->get();
                            
        return $result;
    }
}
