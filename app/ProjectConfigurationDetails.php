<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProjectConfigurationDetails extends Model
{
    protected $table = 'tbl_project_configuration_details';
    protected $fillable=['project_id','configuration_id','budget_min','budget_max','size_range_min','size_range_max','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
    public static function getconfigurationdetail($data)
    {
        
         $query = DB::table('tbl_project_configuration_details as tpcd')
         ->select('tpcd.*','tc.configuration_name')
         ->leftJoin('tbl_configuration as tc','tc.configuration_id','=','tpcd.configuration_id');
         
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('tpcd.project_id', '=' ,$data['project_id']);
           }

           if (array_key_exists('configuration_id', $data) && isset($data['configuration_id'])) {
            $query = $query->where('tpcd.configuration_id', '=' ,$data['configuration_id']);
           }
                          
         $query = $query->where('tpcd.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tpcd.pcdid', 'ASC')->get();
                            
         return $result;
    }
}
