<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TouchPoint extends Model
{
    protected $table = 'tbl_touchpoint';
    protected $fillable=['touchpoint_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function gettouchpoint($data)
    {
        
         $query = DB::table('tbl_touchpoint as touchpoint')->select('touchpoint.*');
         
         if (array_key_exists('touchpoint_id', $data) && isset($data['touchpoint_id'])) {
            $query = $query->where('touchpoint.touchpoint_id', '=' ,$data['touchpoint_id']);
           }
                          
         $query = $query->where('touchpoint.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('touchpoint.touchpoint_id', 'ASC')->get();
                            
         return $result;
    }
}
