<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomCouponCategory extends Model
{
    protected $table = 'tbl_ecom_coupon_category';
    protected $fillable=['coupon_id','category_id','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    
    public  function getcouponcategory($data)
    {
        
        $query = DB::table('tbl_ecom_coupon_category as couponcat')->select('couponcat.*');
         
        if (array_key_exists('coupon_category_id', $data) && isset($data['coupon_category_id'])) {
            $query = $query->where('couponcat.coupon_category_id', '=' ,$data['coupon_category_id']);
        }

        if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('couponcat.category_id', '=' ,$data['category_id']);
        }

        if (array_key_exists('coupon_id', $data) && isset($data['coupon_id'])) {
            $query = $query->where('couponcat.coupon_id', '=' ,$data['coupon_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('couponcat.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('couponcat.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('couponcat.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('couponcat.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('couponcat.coupon_category_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
