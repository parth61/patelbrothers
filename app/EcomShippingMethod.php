<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomShippingMethod extends Model
{
    protected $table = 'tbl_ecom_shipping_methods';
    protected $fillable=['shipping_method','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getshippingmethods($data)
    {
        
        $query = DB::table('tbl_ecom_shipping_methods as sm')->select('sm.*');
         
         
        if (array_key_exists('shipping_method_id', $data) && isset($data['shipping_method_id'])) {
            $query = $query->where('sm.shipping_method_id', '=' ,$data['shipping_method_id']);
        }
        
        $query = $query->where('sm.status', '=' ,1);

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)){
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
    
        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('sm.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('sm.shipping_method_id', 'DESC');
        }
                          
        $result = $query->get();
                            
        return $result;
    }
}
