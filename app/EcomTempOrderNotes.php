<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomTempOrderNotes extends Model
{
    protected $table = 'tbl_ecom_temp_order_notes';
    protected $fillable=['order_id','site_id','order_notes','order_notes_for','status','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getordernotes($data)
    {
        
        $query = DB::table('tbl_ecom_temp_order_notes as ordernotes')
        ->select('ordernotes.*');
         
        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('ordernotes.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('ordernotes.order_id', '=' ,$data['order_id']);
        }
        
        if (array_key_exists('order_notes_for', $data) && isset($data['order_notes_for'])) {
            $query = $query->where('ordernotes.order_notes_for', '=' ,$data['order_notes_for']);
        }
           
        $query = $query->where('ordernotes.status', '=' ,1);

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)) {
            $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ordernotes.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->get();
                            
        return $result;
    }
}
