<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Role extends Model
{
     protected $table = 'tbl_role';
    protected $fillable=['role_name','created_by','updated_by','status','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getrole($data)
    {
        
         $query = DB::table('tbl_role as r')->select('r.*');
         
         if (array_key_exists('role_id', $data) && isset($data['role_id'])) {
            $query = $query->where('r.role_id', '=' ,$data['role_id']);
           }
                          
         $query = $query->where('r.status', '=' ,1);
         if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
          $query = $query->offset($data['offset'])->limit($data['limit']);
          }       
         $result = $query->orderBy('r.role_id', 'DESC')->get();
                            
         return $result;
    }
    //
}
