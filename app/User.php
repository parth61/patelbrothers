<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Model {

    protected $table = 'tbl_user';
//     public $timestamps = false;
    protected $fillable = ['accesstoken','site_id','user_mobile_code','user_mobile_iso','created_by','updated_by', 'user_firstname', 'user_lastname', 'user_mobile', 'user_email', 'user_whatsapp_number', 'user_password','usubsource','browser_name','browser_version','browser_platform','ip_address'];
    public static function getuser($data) {
        $query = DB::table('tbl_user as u')->select('u.*');
               
        
        if (array_key_exists('user_id', $data) && isset($data['user_id'])) {
            $query = $query->where('u.user_id', '=' ,$data['user_id']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('u.site_id', '=' ,$data['site_id']);
        }
           
        $query = $query->where('u.status', '=', 1);
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        $result = $query->orderBy('u.user_id', 'DESC')->get();         
        return $result;
    }
    public function permissions()
    {
        return $this->belongsToMany('App\Permission')->withTimestamps();
    }

}
