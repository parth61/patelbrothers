<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Module extends Model
{
    protected  $table = 'tbl_module';
    protected $fillable=['module_id','module_name','package_id','browser_name','browser_version','browser_platform','ip_address','created_at','updated_at'];
    
     public static function getModule($data)
    {
        
        $query = DB::table('tbl_module as md')->select('md.*');
         
        if (array_key_exists('module_id', $data) && isset($data['module_id'])) {
            $query = $query->where('md.module_id', '=' ,$data['module_id']);
        }
        if (array_key_exists('package_id', $data) && isset($data['package_id'])) {
            $query = $query->where('md.package_id', '=' ,$data['package_id']);
        }

        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('md.module_id', 'ASC')->get();
                            
        return $result;
    }
}
	