<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Seo extends Model
{
    protected $table = 'tbl_seo';
    protected $fillable=['seo_title','seo_description','seo_keywords','seo_image','seo_type','seo_canonical','site_id',
    'created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getseo($data)
    {
        
        $query = DB::table('tbl_seo as seo')->select('seo.*','med.media_file')->leftJoin('tbl_media as med','med.media_id','=','seo.seo_image');

         
        if (array_key_exists('seo_id', $data) && isset($data['seo_id'])) {
            $query = $query->where('seo.seo_id', '=' ,$data['seo_id']);
        }

          
        if (array_key_exists('seo_for', $data) && isset($data['seo_for'])) {
            $query = $query->where('seo.seo_for', '=' ,$data['seo_for']);
        }

          
        if (array_key_exists('seo_for_id', $data) && isset($data['seo_for_id'])) {
            $query = $query->where('seo.seo_for_id', '=' ,$data['seo_for_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('seo.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('seo.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('seo.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('seo.'.$data['sortby'], $data['sorttype']);
        }
                    
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        $result = $query->get();
                            
        return $result;
    }
}
