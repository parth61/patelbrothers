<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomCouponProduct extends Model
{
    protected $table = 'tbl_ecom_coupon_product';
    protected $fillable=['coupon_id','product_id','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

   
    public  function getproducttag($data)
    {
        
        $query = DB::table('tbl_ecom_coupon_product as couponproduct')->select('couponproduct.*');
         
        if (array_key_exists('coupon_product_id', $data) && isset($data['coupon_product_id'])) {
            $query = $query->where('couponproduct.coupon_product_id', '=' ,$data['coupon_product_id']);
        }
   
        if (array_key_exists('coupon_id', $data) && isset($data['coupon_id'])) {
            $query = $query->where('couponcat.coupon_id', '=' ,$data['coupon_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('couponproduct.product_id', '=' ,$data['product_id']);
        }
        
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('couponproduct.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('couponproduct.status', '=' ,1);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('couponproduct.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('couponproduct.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('couponproduct.coupon_product_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
