<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class MenuLocation extends Model
{
    protected $table = 'tbl_menulocation';
    protected $fillable=['menu_id','menulocation_name','status','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getmenulocation($data)
    {
        
        $query = DB::table('tbl_menulocation as menloc')
        ->select('menloc.*');
         
        if (array_key_exists('menu_id', $data) && isset($data['menu_id'])) {
            $query = $query->where('menloc.menu_id', '=' ,$data['menu_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('menloc.status', '=' ,$data['status']);
        } else
        {
            $query = $query->where('menloc.status', '!=' ,0);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('menloc.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('menloc.'.$data['sortby'], $data['sorttype']);
        }
                                      
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
