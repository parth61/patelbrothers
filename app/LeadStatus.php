<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class LeadStatus extends Model
{
    protected $table = 'tbl_lead_status';
    protected $fillable=['primarystatus','secondarystatus','tertiarystatus','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getprimarystatus($data)
    {
        
         $query = DB::table('tbl_lead_status as ls')->select('ls.primarystatus')->where('ls.status', '=' ,1)->distinct('ls.primarystatus');
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
    public static function getsecondarystatus($data)
    {
       
         $query = DB::table('tbl_lead_status as ls')->select('ls.secondarystatus')->where('ls.status', '=' ,1)->distinct('secondarystatus');
                   
         if (array_key_exists('primarystatus', $data) && isset($data['primarystatus'])) {
          $query = $query->where('ls.primarystatus', '=' ,$data['primarystatus']);
         }

         $query = $query->whereNotNull('ls.secondarystatus');

         $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
    public static function gettertiarystatus($data)
    {
       
         $query = DB::table('tbl_lead_status as ls')->select('ls.tertiarystatus')->where('ls.status', '=' ,1)->distinct('tertiarystatus');
         if (array_key_exists('secondarystatus', $data) && isset($data['secondarystatus'])) {
          $query = $query->where('ls.secondarystatus', '=' ,$data['secondarystatus']);
         }

         $query = $query->whereNotNull('ls.tertiarystatus');
         
         $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
         return $result;
    }
}
