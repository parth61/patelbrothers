<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SalesExecutive extends Model
{
    protected $table = 'tbl_sales_executive';
    protected $fillable=['sales_executive_phone_code','sales_executive_phone_iso','sales_executive_whatsapp_code','sales_executive_whatsapp_iso','sales_executive_password','sales_executive_firstname','sales_executive_lastname','created_by','updated_by','sales_executive_photo','sales_executive_email','sales_executive_phone','sales_executive_whatsapp','sales_executive_status_id','ref_builder_id','browser_name','browser_version','browser_platform','ip_address'];

     public static function getsalesexecutive($data)
    {
        
         $query = DB::table('tbl_sales_executive as se')->select('se.*','se.guid as sales_executive_guid','tbl_builder.*','ses.sales_executive_status_name')
                   ->leftJoin('tbl_builder','tbl_builder.builder_id','=','se.builder_id')
                   ->leftJoin('tbl_sales_executive_status as ses','ses.sales_executive_status_id','=','se.sales_executive_status_id');

         if (array_key_exists('sales_executive_id', $data) && isset($data['sales_executive_id'])) {
            $query = $query->where('se.sales_executive_id', '=' ,$data['sales_executive_id']);
          }

          if (array_key_exists('builder_id', $data) && isset($data['builder_id'])) {
            $query = $query->where('se.builder_id', '=' ,$data['builder_id']);
          }
                          
         $query = $query->where('se.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('se.sales_executive_id', 'ASC')->get();

         foreach($result as $item){
            $item->project_list = DB::table('tbl_project_sales_executive as tpse')
                    ->leftJoin('tbl_project as tp','tp.project_id','=','tpse.project_id')
                    ->where('tpse.sales_executive_id', '=',$item->sales_executive_id)
                    ->where('tpse.status', 1)
                    ->pluck('tp.project_name');
         }
                            
         return $result;
    }
    
}
