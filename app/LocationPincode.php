<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class LocationPincode extends Model {

    protected $table = 'tbl_location_pincode';
    protected $fillable = ['location_id', 'pincode_id', 'created_by', 'updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getlocationpincode($data) {

        $query = DB::table('tbl_location_pincode as lp')
                ->select('lp.pincode_id');

        if (array_key_exists('location_pincode_id', $data) && isset($data['location_pincode_id'])) {
            $query = $query->where('lp.location_pincode_id', '=', $data['location_pincode_id']);
        }
        if (array_key_exists('location_id', $data) && isset($data['location_id'])) {
            $query = $query->where('lp.location_id', '=', $data['location_id']);
        }
        if (array_key_exists('pincode_id', $data) && isset($data['pincode_id'])) {
            $query = $query->where('lp.pincode_id', '=', $data['pincode_id']);
        }

        $query = $query->where('lp.status', '=', 1);

        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('lp.location_pincode_id', 'ASC')->get();

        return $result;
    }

    public static function getcustomlocationpincode($data) {

        $query = DB::table('tbl_location_pincode as lp')
                ->leftJoin('tbl_pincode as tp', 'tp.pincode_id', '=', 'lp.pincode_id')
                ->select('lp.pincode_id','tp.pincode');

        if (array_key_exists('location_pincode_id', $data) && isset($data['location_pincode_id'])) {
            $query = $query->where('lp.location_pincode_id', '=', $data['location_pincode_id']);
        }
        if (array_key_exists('location_id', $data) && isset($data['location_id'])) {
            $query = $query->where('lp.location_id', '=', $data['location_id']);
        }
        if (array_key_exists('pincode_id', $data) && isset($data['pincode_id'])) {
            $query = $query->where('lp.pincode_id', '=', $data['pincode_id']);
        }

        $query = $query->where('lp.status', '=', 1);

        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('lp.location_pincode_id', 'ASC')->get();

        return $result;
    }

}
