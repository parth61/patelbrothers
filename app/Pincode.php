<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pincode extends Model
{
    protected  $table = 'tbl_pincode';
    protected $fillable=['pincode','city_id','state_id','country_id','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getpincode($data)
    {
        
        $query = DB::table('tbl_pincode as tp')->select('tp.*','tc.city_name','s.state_name','c.country_name')
        ->leftJoin('tbl_country as c', 'c.country_id', '=', 'tp.country_id')
        ->leftJoin('tbl_state as s', 's.state_id', '=', 'tp.state_id')
        ->leftJoin('tbl_city as tc', 'tc.city_id', '=', 'tp.city_id');
         
        if (array_key_exists('country_id', $data) && isset($data['country_id'])) {
            $query = $query->where('tp.country_id', '=' ,$data['country_id']);
        }

        if (array_key_exists('state_id', $data) && isset($data['state_id'])) {
            $query = $query->where('tp.state_id', '=' ,$data['state_id']);
        }

        if (array_key_exists('city_id', $data) && isset($data['city_id'])) {
            $query = $query->where('tp.city_id', '=' ,$data['city_id']);
        }

        if (array_key_exists('pincode_id', $data) && isset($data['pincode_id'])) {
            $query = $query->where('tp.pincode_id', '=' ,$data['pincode_id']);
        }
                          
        $query = $query->where('tp.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('tp.pincode_id', 'ASC')->get();
                            
        return $result;
    }
}
