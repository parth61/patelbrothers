<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Rerastate extends Model
{
    protected $table = 'tbl_rera_state';
    protected $fillable=['rera_state_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getrerastate($data)
    {
        
         $query = DB::table('tbl_rera_state as b')->select('b.*');
         
         if (array_key_exists('rera_state_id', $data) && isset($data['rera_state_id'])) {
            $query = $query->where('b.rera_state_id', '=' ,$data['rera_state_id']);
           }
                          
         $query = $query->where('b.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.rera_state_id', 'ASC')->get();
                            
         return $result;
    }
}
