<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Project extends Model
{
    protected $table = 'tbl_project';
    protected $fillable=['builder_id','project_name','category_id','project_logo',
    'created_by','updated_by',
    'project_featured_img','budget_id',
    'project_rera_no','project_address',
    'project_website','project_status_id',
    'project_total_units','project_slug','project_latitude','project_longitude','browser_name','browser_version','browser_platform','ip_address'];

    public static function getproject($data)
    {
        
         
        if (array_key_exists('minbudgetval', $data) && isset($data['minbudgetval']) && array_key_exists($data['maxbudgetval']) && isset($data['maxbudgetval'])) {
			
			$projectlist = DB::table('tbl_project_configuration_details as tpcd')
			->where('tpcd.status', '1')
			->where('tpcd.budget_min', '>=', str_replace( ',', '', $data['minbudgetval']))
			->where('tpcd.budget_max', '<=', str_replace( ',', '', $data['maxbudgetval']))
			->distinct()->pluck('tpcd.project_id');
			  
			$query = DB::table('tbl_project as pro')->select('pro.*','tbl_builder.builder_name','pro.created_at as created_at','tbl_project_status.project_status_name','tbl_category.category_name','tbl_category.category_colorcode','tbl_location.location_name','tbl_location.location_latitude','tbl_location.location_longitude')
                   ->leftJoin('tbl_builder','tbl_builder.builder_id','=','pro.builder_id')
                   ->leftJoin('tbl_category','tbl_category.category_id','=','pro.category_id')
                   ->leftJoin('tbl_location','tbl_location.location_id','=','pro.location_id')  
                   ->leftJoin('tbl_project_status','tbl_project_status.project_status_id','=','pro.project_status_id')
				   ->whereIn('pro.project_id', $projectlist);
			 
		}else{
			
			$query = DB::table('tbl_project as pro')->select('pro.*','tbl_builder.builder_name','pro.created_at as created_at','tbl_project_status.project_status_name','tbl_category.category_name','tbl_category.category_colorcode','tbl_location.location_name','tbl_location.location_latitude','tbl_location.location_longitude')
					   ->leftJoin('tbl_builder','tbl_builder.builder_id','=','pro.builder_id')
					   ->leftJoin('tbl_category','tbl_category.category_id','=','pro.category_id')
					   ->leftJoin('tbl_location','tbl_location.location_id','=','pro.location_id')  
					   ->leftJoin('tbl_project_status','tbl_project_status.project_status_id','=','pro.project_status_id');
			
			if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
				$query = $query->where('pro.project_id', '=' ,$data['project_id']);
			}
		}       
        
         if (array_key_exists('builder_id', $data) && isset($data['builder_id'])) {
            $query = $query->where('pro.builder_id', '=' ,$data['builder_id']);
			}
             
         if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('pro.category_id', '=' ,$data['category_id']);
           }
             
          if (array_key_exists('project_status_id', $data) && isset($data['project_status_id'])) {
            $query = $query->where('pro.project_status_id', '=' ,$data['project_status_id']);
           }
		   
		   
		   
          
           if (array_key_exists('location_latitude', $data) && isset($data['location_latitude']) && 
             array_key_exists('location_longitude', $data) && isset($data['location_longitude'])) {
              $location_latitude=$data['location_latitude'];
              $location_longitude=$data['location_longitude'];
              $nearbyprojectkilometer=$data['nearbyprojectkilometer'];
              $locationlist= DB::table('tbl_location')
              ->select('location_id', DB::raw("( 6371 * acos( cos( radians($location_latitude) ) * cos( radians( location_latitude ) )
              * cos( radians( location_longitude ) - radians($location_longitude) ) + sin( radians($location_latitude) ) 
              * sin( radians( location_latitude ) ) ) ) AS distance"))
              ->havingRaw("distance<$nearbyprojectkilometer")
              ->where('location_id', '!=' ,$data['location_id'])
              ->orderBy('distance')
              ->offset($data['offset'])->limit($data['limit'])
              ->pluck('location_id');
         
              $query = $query->whereIn('pro.location_id',$locationlist);
          }else{
            if (array_key_exists('location_id', $data) && isset($data['location_id'])) {
              $query = $query->where('pro.location_id', '=' ,$data['location_id']);
             }
          }
                          
         $query = $query->where('pro.status', '=' ,1);
		 
		 if (array_key_exists('builder_id', $data) || array_key_exists('project_id', $data)) {
			
		 }else{
			$query = $query->where('pro.isvisible', '=' ,1); 
		 }
                          
         $result = $query->inRandomOrder()->offset($data['offset'])->limit($data['limit'])->orderBy('pro.isvisible', 'DESC')->orderBy('pro.ispriority', 'DESC')->get();
                            
         return $result;
    }
	
	public static function getexotelcallerid($projectid)
    {
        $projectdata = DB::table('tbl_project as pro')->where('pro.project_id', $projectid)->first();
		$builderid = $projectdata->builder_id;
		$builderdata = DB::table('tbl_builder as tb')->where('tb.builder_id', $builderid)->first();
		$exotelid = $builderdata->exotel_no;
		return $exotelid;
    }

    
}
