<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Propertytype extends Model
{
    protected $table = 'tbl_property_type';
    protected $fillable=['property_type_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getrerastate($data)
    {
        
         $query = DB::table('tbl_property_type as b')->select('b.*');
         
         if (array_key_exists('property_type_id', $data) && isset($data['property_type_id'])) {
            $query = $query->where('b.property_type_id', '=' ,$data['property_type_id']);
           }
                          
         $query = $query->where('b.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.property_type_id', 'ASC')->get();
                            
         return $result;
    }
}
