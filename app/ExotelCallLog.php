<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ExotelCallLog extends Model
{
    protected $table = 'tbl_exotel_call_log';
    protected $fillable=['lead_id','sid','parentcallsid',
    'accountsid','exotel_to','exotel_from','phonenumbersid','status',
    'starttime','endtime','duration','price','direction','answerdby',
    'forwardedfrom','callername','uri','recordingurl','dateupdated','datecreated','browser_name','browser_version','browser_platform','ip_address'];

}
