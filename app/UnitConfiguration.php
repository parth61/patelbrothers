<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UnitConfiguration extends Model
{
    protected  $table = 'tbl_unit_configuration';
    protected $fillable=['unit_configuration_id','unit_id','category_id','unit_configuration_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getunitconfiguration($data)
    {
        
        $query = DB::table('tbl_unit_configuration as s')->select('s.*','c.category_name','u.unit_type_name')
        ->leftJoin('tbl_units as u', 'u.unit_id', '=', 's.unit_id')
        ->leftJoin('tbl_category as c', 'c.category_id', '=', 's.category_id');
        
        if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('s.category_id', '=' ,$data['category_id']);
        }
         
        if (array_key_exists('unit_id', $data) && isset($data['unit_id'])) {
            $query = $query->where('s.unit_id', '=' ,$data['unit_id']);
        }
        if (array_key_exists('unit_configuration_id', $data) && isset($data['unit_configuration_id'])) {
            $query = $query->where('s.unit_configuration_id', '=' ,$data['unit_configuration_id']);
        }
                          
        $query = $query->where('s.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('s.unit_configuration_id', 'ASC')->get();
                            
        return $result;
    }
}
