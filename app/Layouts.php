<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Layouts extends Model
{
    protected $table = 'tbl_layouts';
    protected $fillable=['layout_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getlayout($data)
    {
        
         $query = DB::table('tbl_layouts as b')->select('b.*');
         
         if (array_key_exists('layout_id', $data) && isset($data['layout_id'])) {
            $query = $query->where('b.layout_id', '=' ,$data['layout_id']);
           }
                          
         $query = $query->where('b.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.layout_id', 'ASC')->get();
                            
         return $result;
    }
}
