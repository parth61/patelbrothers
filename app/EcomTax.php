<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomTax extends Model
{
    protected $table = 'tbl_ecom_tax';
    protected $fillable=['tax_type','shipping_city','tax_ratio','site_id','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public function gettax($data)
    {
        
        $query = DB::table('tbl_ecom_tax as tax')->select('tax.*','tc.tax_class_name')
        ->leftJoin('tbl_ecom_tax_class as tc', 'tc.tax_class_id', '=', 'tax.tax_class_id');
         
        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('tax.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('tax_priority', $data) && isset($data['tax_priority'])) {
            $query = $query->where('tax.tax_priority', '=' ,$data['tax_priority']);
        }

        if (array_key_exists('tax_country', $data) && isset($data['tax_country'])) {
            $query = $query->where('tax.tax_country', '=' ,$data['tax_country']);
        }

        if (array_key_exists('tax_state', $data) && isset($data['tax_state'])) {
            $query = $query->where('tax.tax_state', '=' ,$data['tax_state']);
        }

        if (array_key_exists('tax_city', $data) && isset($data['tax_city'])) {
            $query = $query->where('tax.tax_city', '=' ,$data['tax_city']);
        }

        if (array_key_exists('tax_zipcode', $data) && isset($data['tax_zipcode'])) {
            $query = $query->where('tax.tax_zipcode', '=' ,$data['tax_zipcode']);
        }

        if (array_key_exists('is_applied_to_shipping', $data) && isset($data['is_applied_to_shipping'])) {
            $query = $query->where('tax.is_applied_to_shipping', '=' ,$data['is_applied_to_shipping']);
        }

        if (array_key_exists('tax_class_id', $data) && isset($data['tax_class_id'])) {
            $query = $query->where('tax.tax_class_id', '=' ,$data['tax_class_id']);
        }
                          
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('tax.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('tax.status', '=' ,1);
        }

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)){
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('tax.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('tax.tax_id', 'DESC');
        }
                          
        $result = $query->get();
                            
        return $result;
    }

    public function gettaxbyproductid($product_id, $site_id, $user_id, $userdata, $businessdata)
    {
        $result = array();
        $tax = 0;

        if(isset($product_id) && isset($site_id) && isset($user_id)){

            /* get product info */
            $productdata = DB::table('tbl_ecom_product as tep')->select('tep.*')->where('product_id', $product_id)->where('site_id', $site_id)->where('status', 1)->first();

            if(isset($productdata)){

                if($productdata->tax_status_id==1){
			
                    if($productdata->tax_class_id==1){
	
                        /* Get Taxinfo from class */
			
                        /*$taxpercentagedata = DB::table('tbl_ecom_product_category as tepc')->select('tec.*')
                        ->leftJoin('tbl_ecom_category as tec', 'tec.category_id', '=', 'tepc.category_id')
                        ->where('tepc.product_id', '=', $product_id)->where('tepc.status', 1)
                        ->where('tec.status', 1)->get();*/

                        //$taxpercentage = (count($taxpercentagedata)>0)?$taxpercentagedata[0]->tax_percentage:0;
                        $taxpercentage = (isset($productdata->product_tax))?$productdata->product_tax:0;

                        /* Get Applied Tax */
                        $appliedtax = DB::table('tbl_ecom_tax as tax')->select(DB::raw('min(tax_id) as tax_id'))
                            ->whereIn('tax.tax_country', [$userdata->country,'*'])
                            ->whereIn('tax.tax_state', [$userdata->state,'*'])
                            ->whereIn('tax.tax_city', [$userdata->city,'*'])
                            ->whereIn('tax.tax_zipcode', [$userdata->pincode,'*'])
                            ->where('tax.site_id', $site_id)
                            ->where('tax.tax_class_id', 1)
                            ->where('tax.status', 1)
                            ->groupby('tax.tax_priority')->pluck('tax_id');
//print_r($userdata->country);

                        $taxdata = DB::table('tbl_ecom_tax as tax')->select('tax.*')
                            ->whereIn('tax.tax_id', $appliedtax)
                            ->whereIn('tax.tax_country', [$userdata->country,'*'])
                            ->whereIn('tax.tax_state', [$userdata->state,'*'])
                            ->whereIn('tax.tax_city', [$userdata->city,'*'])
                            ->whereIn('tax.tax_zipcode', [$userdata->pincode,'*'])
                            ->where('tax.site_id', $site_id)
                            ->where('tax.tax_class_id', 1)
                            ->where('tax.status', 1)
                            ->get();

                        $nooftax = count($taxdata);

                        foreach($taxdata as $item){
                            $temp = array();
                            $temp['tax_id'] = $item->tax_id;
                            $temp['tax_name'] = $item->tax_name;
                            $temp['tax_percentage'] = floatval($taxpercentage)/$nooftax;
                            array_push($result, $temp);
                        }

                    }
                    else if($productdata->tax_class_id==2){

                    }else{

                    }

                }

            }else{

            }

            
                                  
        }else{
            $result = array();
        }
        
                            
        return $result;
    }

}
