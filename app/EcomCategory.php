<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomCategory extends Model
{
    protected $table = 'tbl_ecom_category';
    protected $fillable=['category_id','tax_id','slug_name','category_name','category_description',
    'parent_category_id','category_thumbnail','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getcategory($data)
    {
        
        $query = DB::table('tbl_ecom_category as ec')->select('ec.*','md.media_file as cat_media_file','ec.slug_name as category_slug')
        ->leftJoin('tbl_media as md', 'md.media_id', '=', 'ec.category_thumbnail');
        
        if (array_key_exists('category_id', $data) && isset($data['category_id'])) {
            $query = $query->where('ec.category_id', '=' ,$data['category_id']);
        }

        if (array_key_exists('parent_category_id', $data) && isset($data['parent_category_id'])) {
            $query = $query->where('ec.parent_category_id', '=' ,$data['parent_category_id']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('ec.slug_name', '=' ,$data['slug_name']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ec.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('ec.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('ec.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('ec.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

    public  function getmenucategory($data)
    {
        
        $query = DB::table('tbl_ecom_category as ec')->select('ec.*','md.media_file as cat_media_file','ec.slug_name as category_slug')
        ->leftJoin('tbl_media as md', 'md.media_id', '=', 'ec.category_thumbnail');
      
        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('ec.slug_name', '=' ,$data['slug_name']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('ec.site_id', '=' ,$data['site_id']);
        }
         
        $result = $query->where('ec.status', '=' ,1)->get();

        foreach($result as $item){
           /* if($item->parent_category_id == 0){
                array_push($parentlist, $item);
            }else{
                array_push($childlist, $item);
            }*/
            $temp = array();
            $item->subcategory = $temp;
        }

        /*$parentlist = array();
        $childlist = array();
        foreach($result as $item){
            if($item->parent_category_id == 0){
                array_push($parentlist, $item);
            }else{
                array_push($childlist, $item);
            }
        }

        foreach($parentlist as $item){
            $temp = array();
            foreach($childlist as $subitem){
                if($item->category_id == $subitem->parent_category_id){
                    array_push($temp, $subitem);
                }
            }
            $item->subcategory = $temp;
        }*/
        /*
        foreach($parentlist as $item){
            if(count($item->subcategory)>0){
                foreach($item->subcategory as $subitem){
                    $temp = array();
                    foreach($result as $resitem){
                        if($subitem->category_id == $resitem->parent_category_id){
                            array_push($temp, $subitem);
                        }
                    }
                    $subitem->subcategory = $temp;
                }
            }
        }
         */                   
        return $result;
    }
}
