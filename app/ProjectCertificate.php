<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectCertificate extends Model
{
    protected $table = 'tbl_project_certificate';
    protected $fillable=['project_id','project_certificate_name','project_certificate_file','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
   
    public static function getprojectcertificate($data)
    {
        
         $query = DB::table('tbl_project_certificate as pc')->select('pc.*','p.project_name')
                   ->leftJoin('tbl_project as p','p.project_id','=','pc.project_id');
         
         if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('pc.project_id', '=' ,$data['project_id']);
           }

           if (array_key_exists('project_certificate_id', $data) && isset($data['project_certificate_id'])) {
            $query = $query->where('pc.project_certificate_id', '=' ,$data['project_certificate_id']);
           }
                          
         $query = $query->where('pc.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('pc.project_certificate_id', 'ASC')->get();
                            
         return $result;
    }
}
