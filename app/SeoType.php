<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SeoType extends Model
{
    protected $table = 'tbl_seotype';
    protected $fillable=['seo_type','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getseotype($data)
    {
        
        $query = DB::table('tbl_seotype as seotype')->select('seotype.*');

         
        if (array_key_exists('seotype_id', $data) && isset($data['seotype_id'])) {
            $query = $query->where('seotype.seotype_id', '=' ,$data['seotype_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('seotype.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('seotype.status', '=' ,1);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('seotype.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('seotype.'.$data['sortby'], $data['sorttype']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
                    
        $result = $query->get();
                            
        return $result;
    }
}
