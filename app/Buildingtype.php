<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Buildingtype extends Model
{
    protected $table = 'tbl_building_type';
    protected $fillable=['building_type_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getbuilding($data)
    {
        
         $query = DB::table('tbl_building_type as b')->select('b.*');
         
         if (array_key_exists('building_type_id', $data) && isset($data['building_type_id'])) {
            $query = $query->where('b.building_type_id', '=' ,$data['building_type_id']);
           }
                          
         $query = $query->where('b.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.building_type_id', 'ASC')->get();
                            
         return $result;
    }
}
