<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Media extends Model
{
    protected $table = 'tbl_media';
    protected $fillable=['media_title','media_type','media_alt','media_caption','media_description','media_path','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getmedia($data)
    {
        
        $query = DB::table('tbl_media as med')->select('med.*');

         
        if (array_key_exists('media_id', $data) && isset($data['media_id'])) {
            $query = $query->where('med.media_id', '=' ,$data['media_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('med.status', '=' ,$data['status']);
        }else
        {
            $query = $query->where('med.status', '=' ,1);
        }

        if (array_key_exists('media_type', $data) && isset($data['media_type'])) {
            $query = $query->where('med.media_type', '=' ,$data['media_type']);
        }

        if (array_key_exists('month', $data) && isset($data['month'])) {
            $query = $query->whereMonth('med.created_at', '=', $data['month']);
        }
        
        if (array_key_exists('year', $data) && isset($data['year'])) {
            $query = $query->whereYear('med.created_at', '=' ,$data['year']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('med.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('fromdate', $data) && isset($data['fromdate']) && array_key_exists('todate', $data) && isset($data['todate'])) {           
            $start = date("Y-m-d",strtotime($data['fromdate']));
            $end = date("Y-m-d",strtotime($data['todate']."+1 day"));
            $query = $query->whereBetween('med.created_at', array($start, $end));
        } 

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('med.'.$data['sortby'], $data['sorttype']);
        }else{
            $query = $query->orderBy('med.media_id', 'DESC');
        }
                    
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        $result = $query->get();
                            
        return $result;
    }
}
