<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class PostTag extends Model
{
    protected $table = 'tbl_posttag';
    protected $fillable=['posttag_name','slug_id','posttag_description','site_id','created_by','updated_by','seo_id','browser_name','browser_version','browser_platform','ip_address'];

    public static function getposttag($data)
    {
        
        $query = DB::table('tbl_posttag as pt')->select('pt.*',
        'pt.slug_name as posttag_slug','seo.*','medi.media_file as seo_media_file')
            ->leftJoin('tbl_seo as seo','seo.seo_id','=','pt.seo_id') 
            ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');
            
        if (array_key_exists('posttag_id', $data) && isset($data['posttag_id'])) {
            $query = $query->where('pt.posttag_id', '=' ,$data['posttag_id']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('pt.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pt.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pt.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('pt.site_id', '=' ,$data['site_id']);
        }
        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('pt.'.$data['sortby'], $data['sorttype']);
        }
                    
        $result = $query->get();
                            
        return $result;
    }

    
}
