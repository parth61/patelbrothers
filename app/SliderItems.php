<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SliderItems extends Model
{
    protected $table = 'tbl_slider_items';
    protected $fillable=['slider_id','slider_title','slider_subtitle','slider_description','slider_image','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
    public static function getslideritems($data)
    {
        
        $query = DB::table('tbl_slider_items as si')->select('si.*','sr.slider_name')
        ->leftJoin('tbl_slider as sr', 'sr.slider_id', '=', 'si.slider_id');
         
        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('si.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('slider_id', $data) && isset($data['slider_id'])) {
            $query = $query->where('si.slider_id', '=' ,$data['slider_id']);
        }
        
        if (array_key_exists('slider_name', $data) && isset($data['slider_name'])) {
            $query = $query->where('sr.slider_name', '=' ,$data['slider_name']);
        }
              
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('si.status', '=' ,$data['status']);
        }else{
            $query = $query->where('si.status', '!=' ,0);    
        } 
        
        $query = $query->where('sr.status', '!=' ,0);

        if (array_key_exists('offset', $data) && isset($data['offset'])) {
            $query = $query->offset($data['offset']);
        }

        if (array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->limit($data['limit']);
        }
                          
        $result = $query->orderBy('si.slider_item_id', 'ASC')->get();
                            
        return $result;
    }
}
