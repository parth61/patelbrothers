<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomTaxClass extends Model
{
    protected $table = 'tbl_ecom_tax_class';
    protected $fillable=['tax_class_name','site_id','created_by','updated_by','status','browser_name','browser_version','browser_platform','ip_address'];

    public  function gettaxclass($data)
    {
        
        $query = DB::table('tbl_ecom_tax_class as taxclass')->select('taxclass.*');
         
        if (array_key_exists('tax_class_id', $data) && isset($data['tax_class_id'])) {
            $query = $query->where('taxclass.tax_class_id', '=' ,$data['tax_class_id']);
        }

       
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('taxclass.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('taxclass.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('taxclass.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('offset', $data) && array_key_exists('limit', $data)){
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('taxclass.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('taxclass.tax_class_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}


?>