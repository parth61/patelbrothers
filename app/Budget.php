<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Budget extends Model
{
    protected $table = 'tbl_budget';
    protected $fillable=['budget_amount','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getbudget($data)
    {
        
         $query = DB::table('tbl_budget as b')->select('b.*');
         
         if (array_key_exists('budget_id', $data) && isset($data['budget_id'])) {
            $query = $query->where('b.budget_id', '=' ,$data['budget_id']);
           }
                          
         $query = $query->where('b.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('b.budget_id', 'ASC')->get();
                            
         return $result;
    }
}
