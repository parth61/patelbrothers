<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectAmenities extends Model
{
    protected $table = 'tbl_project_amenities';
    protected $fillable=['project_id','amenities_id','project_amenities_alias','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getprojectamenities($data)
    {
        
         $query = DB::table('tbl_project_amenities as pa')
         ->select('pa.*','ame.amenities_name')
         ->leftJoin('tbl_amenities as ame','ame.amenities_id','=','pa.amenities_id');
         
           if (array_key_exists('project_amenities_id', $data) && isset($data['project_amenities_id'])) {
            $query = $query->where('pa.project_amenities_id', '=' ,$data['project_amenities_id']);
           }
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('pa.project_id', '=' ,$data['project_id']);
           }
           if (array_key_exists('amenities_id', $data) && isset($data['amenities_id'])) {
            $query = $query->where('pa.amenities_id', '=' ,$data['amenities_id']);
           }
                          
         $query = $query->where('pa.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('pa.project_amenities_id', 'ASC')->get();
                            
         return $result;
    }
}
