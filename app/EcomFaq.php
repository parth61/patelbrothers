<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomFaq extends Model
{
   
    protected $table = 'tbl_ecom_faq';
    protected $fillable=['faq_category_id','faq_category_id','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getecomfaq($data)
    {
        
        $query = DB::table('tbl_ecom_faq as faq')->select('faq.*');
         
        if (array_key_exists('faq_category_id', $data) && isset($data['faq_category_id'])) {
            $query = $query->where('faq.faq_category_id', '=' ,$data['faq_category_id']);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('faq.site_id', '=' ,$data['site_id']);
        }

        $query = $query->where('faq.status', '=' ,1)->offset($data['offset'])->limit($data['limit']);


        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('faq.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->get();
                            
        return $result;
    }

}
