<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Template extends Model
{
    protected $table = 'tbl_template';
    protected $fillable=['template_id','template_name','template_file','site_id','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function gettemplate($data)
    {
        
        $query = DB::table('tbl_template as temp')
        ->select('temp.*');
         
        if (array_key_exists('template_id', $data) && isset($data['template_id'])) {
            $query = $query->where('temp.template_id', '=' ,$data['template_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('temp.status', '=' ,$data['status']);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('temp.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('temp.'.$data['sortby'], $data['sorttype']);
        }
                                      
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
