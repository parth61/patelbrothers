<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomBusinessSetting extends Model
{
    protected $table = 'tbl_ecom_business_setting';
    protected $fillable=['business_name','business_type',
    'business_category','business_city','business_state','business_country','business_pincode',
    'business_address1','business_address2','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getbusinesssetting($data)
    {
        
        $query = DB::table('tbl_ecom_business_setting as bs')->select('bs.*');
         
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('bs.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('bs.status', '=' ,1);
        }
        
        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('bs.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('bs.'.$data['sortby'], $data['sorttype']);
        }
        
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
