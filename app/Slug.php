<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Slug extends Model
{
    protected $table = 'tbl_slug';
    protected $fillable=['slug_id','slug_name','tag_id','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getslug($data)
    {
        
        $query = DB::table('tbl_slug as slug')->select('slug.*');
         
     
        if (array_key_exists('slug_id', $data) && isset($data['slug_id'])) {
            $query = $query->where('slug.slug_id', '=' ,$data['slug_id']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('slug.slug_name', '=' ,$data['slug_name']);
        }
           
        if (array_key_exists('slug_type', $data) && isset($data['slug_type'])) {
            $query = $query->where('slug.slug_type', '=' ,$data['slug_type']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('slug.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('slug.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('slug.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('slug.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('slug.slug_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

    public  function getDataFromSlug($slug_name, $site_id)
    {
        
        $slug_data = DB::table('tbl_slug as slug')->select('slug.*')
            ->where('slug.slug_name', '=' ,$slug_name)
            ->where('slug.site_id', '=' ,$site_id)
            ->first();

        if(isset($slug_data->slug_type)){
            $result['sluginfo'] = $slug_data;
           
            if($slug_data->slug_type == "page"){
                $result['slugdata'] = DB::table('tbl_page as page')->select('page.*')
                                    ->where('page.slug_id', '=', $slug_data->slug_id)
                                    ->where('page.site_id', '=', $site_id)
                                    ->where('page.status', '=', 1)
                                    ->first();
            }else if($slug_data->slug_type == "productcategory"){
                $result['slugdata'] = DB::table('tbl_ecom_category as cat')->select('cat.*')
                                    ->where('cat.slug_id', '=', $slug_data->slug_id)
                                    ->where('cat.site_id', '=', $site_id)
                                    ->where('cat.status', '=', 1)
                                    ->first();
            }else if($slug_data->slug_type == "productattribute"){
                $result['slugdata'] = DB::table('tbl_ecom_attribute as attr')->select('attr.*')
                                    ->where('attr.slug_id', '=', $slug_data->slug_id)
                                    ->where('attr.site_id', '=', $site_id)
                                    ->where('attr.status', '=', 1)
                                    ->first();
            }else if($slug_data->slug_type == "productattributeterm"){
                $result['slugdata'] = DB::table('tbl_ecom_attribute_term as attrterm')->select('attrterm.*')
                                    ->where('attrterm.slug_id', '=', $slug_data->slug_id)
                                    ->where('attrterm.site_id', '=', $site_id)
                                    ->where('attrterm.status', '=', 1)
                                    ->first();
            }else if($slug_data->slug_type == "product"){
                $result['slugdata'] = DB::table('tbl_ecom_product as prdct')->select('prdct.*')
                                    ->where('prdct.slug_id', '=', $slug_data->slug_id)
                                    ->where('prdct.site_id', '=', $site_id)
                                    ->where('prdct.status', '=', 1)
                                    ->first();
            }else if($slug_data->slug_type == "producttag"){
                $result['slugdata'] = DB::table('tbl_ecom_tag as tag')->select('tag.*')
                                    ->where('tag.slug_id', '=', $slug_data->slug_id)
                                    ->where('tag.site_id', '=', $site_id)
                                    ->where('tag.status', '=', 1)
                                    ->first();
            }else{
                $result['slugdata'] = null;
            }
            return $result;
        }else{
            return null;
        }
                        
    }
}
