<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomProductAttributeTerm extends Model
{
    protected $table = 'tbl_ecom_product_attribute_term';
    protected $fillable=['product_attribute_term_id','attribute_id','product_id','attribute_term_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getproductattributeterm($data)
    {
        
        $query = DB::table('tbl_ecom_product_attribute_term as pattrterm')
        ->select('pattrterm.*');
         
        if (array_key_exists('attribute_term_id', $data) && isset($data['attribute_term_id'])) {
            $query = $query->where('pattrterm.attribute_term_id', '=' ,$data['attribute_term_id']);
        }

        if (array_key_exists('attribute_id', $data) && isset($data['attribute_id'])) {
            $query = $query->where('pattrterm.attribute_id', '=' ,$data['attribute_id']);
        }

        if (array_key_exists('product_attribute_term_id', $data) && isset($data['product_attribute_term_id'])) {
            $query = $query->where('pattrterm.product_attribute_term_id', '=' ,$data['product_attribute_term_id']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('pattrterm.product_id', '=' ,$data['product_id']);
        }
        
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pattrterm.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pattrterm.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('pattrterm.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('pattrterm.product_attribute_term_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
