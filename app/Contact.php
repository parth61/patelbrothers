<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Contact extends Model
{
    protected $table = 'tbl_contact';
    protected $fillable=['first_name','last_name','email','mobile','countrycode','countryiso','browser_name','browser_version','browser_platform','ip_address','type'];

      public static function getcontact($data)
    {
        
         $query = DB::table('tbl_contact as c')->select('c.*');
         
         if (array_key_exists('contact_id', $data) && isset($data['contact_id'])) {
            $query = $query->where('c.contact_id', '=' ,$data['contact_id']);
           }
           if (array_key_exists('type', $data) && isset($data['type'])) {
            $query = $query->where('c.type', '=' ,$data['type']);
           }
                          
         $query = $query->where('c.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('c.contact_id', 'DESC')->get();
                            
         return $result;
    }

}
