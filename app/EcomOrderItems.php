<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomOrderItems extends Model
{
    protected $table = 'tbl_ecom_order_items';
    protected $fillable=['order_id','product_id','tax_id','order_product_quantity','order_product_amount','order_product_tax','order_product_total','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getorderitems($data)
    {
        
        $query = DB::table('tbl_ecom_order_items as orddetail')
        ->select('orddetail.*','pro.product_id','pro.product_short_description','pro.tax_class_id','pro.shipping_class_id','pro.tax_status_id'
        ,'pro.product_code','pro.slug_id','pro.slug_name','pro.product_name','pro.product_actual_price','pro.product_sale_price'
        ,'pro.product_stock')
        ->leftJoin('tbl_ecom_order as ord','ord.order_id','=','orddetail.order_id')
        ->leftJoin('tbl_ecom_product as pro','pro.product_id','=','orddetail.product_id');
         
        if (array_key_exists('order_details_id', $data) && isset($data['order_details_id'])) {
            $query = $query->where('orddetail.order_details_id', '=' ,$data['order_details_id']);
        }

        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('orddetail.order_id', '=' ,$data['order_id']);
        }

        if (array_key_exists('order_guid', $data) && isset($data['order_guid'])) {
            $query = $query->where('ord.order_guid', '=' ,$data['order_guid']);
        }

        if (array_key_exists('product_id', $data) && isset($data['product_id'])) {
            $query = $query->where('orddetail.product_id', '=' ,$data['product_id']);
        }
                          
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('orddetail.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('orddetail.status', '=' ,1);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('orddetail.'.$data['sortby'], $data['sorttype']);
        }

                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }

    public static function getorderitemscount($data)
    {
        $result = array();

        $query = DB::table('tbl_ecom_order_items as orddetail');
         
        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('orddetail.order_id', '=' ,$data['order_id']);
        }
        
        $result['totalproducts'] = $query->where('orddetail.status', '=' ,1)->count();

        $query = DB::table('tbl_ecom_order_items as orddetail')->leftJoin('tbl_ecom_product as pro','pro.product_id','=','orddetail.product_id');
         
        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('orddetail.order_id', '=' ,$data['order_id']);
        }
        
        $result['productlist'] = $query->where('orddetail.status', '=' ,1)->pluck('pro.product_name');
                            
        return $result;
    }
}
