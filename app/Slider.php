<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Slider extends Model
{
    protected $table = 'tbl_slider';
    protected $fillable=['slider_name','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
    public static function getsliders($data)
    {
        
        $query = DB::table('tbl_slider as sr')->select('sr.*');
         
        if (array_key_exists('site_id', $data) && isset($data['site_id'])) {
            $query = $query->where('sr.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('slider_id', $data) && isset($data['slider_id'])) {
            $query = $query->where('sr.slider_id', '=' ,$data['slider_id']);
        }
        
        if (array_key_exists('slider_name', $data) && isset($data['slider_name'])) {
            $query = $query->where('sr.slider_name', '=' ,$data['slider_name']);
        }
                          
        $query = $query->where('sr.status', '=' ,1);

        if (array_key_exists('offset', $data) && isset($data['offset'])) {
            $query = $query->offset($data['offset']);
        }

        if (array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->limit($data['limit']);
        }
                          
        $result = $query->orderBy('sr.slider_id', 'ASC')->get();
                            
        return $result;
    }
}
