<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectSizeRange extends Model
{
    protected $table = 'tbl_project_size_range';
    protected $fillable=['project_id','size_range_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
   
    public static function getprojectsizerange($data)
    {
        
         $query = DB::table('tbl_project_size_range as psr')
         ->select('psr.size_range_id');
         
           if (array_key_exists('project_size_range_id', $data) && isset($data['project_size_range_id'])) {
            $query = $query->where('psr.project_size_range_id', '=' ,$data['project_size_range_id']);
           }
           if (array_key_exists('project_id', $data) && isset($data['project_id'])) {
            $query = $query->where('psr.project_id', '=' ,$data['project_id']);
           }
           if (array_key_exists('size_range_id', $data) && isset($data['size_range_id'])) {
            $query = $query->where('psr.size_range_id', '=' ,$data['size_range_id']);
           }
                          
         $query = $query->where('psr.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('psr.project_size_range_id', 'ASC')->get();
                            
         return $result;
    }
}
