<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SalesExecutiveStatus extends Model
{
    protected $table = 'tbl_sales_executive_status';
    protected $fillable=['sales_executive_status_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getsalesexecutivestatus($data)
    {
        
         $query = DB::table('tbl_sales_executive_status as ses')->select('ses.*');
         
         if (array_key_exists('sales_executive_status_id', $data) && isset($data['sales_executive_status_id'])) {
            $query = $query->where('ses.sales_executive_status_id', '=' ,$data['sales_executive_status_id']);
           }
                          
         $query = $query->where('ses.status', '=' ,1);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('ses.sales_executive_status_id', 'ASC')->get();
                            
         return $result;
    }
}
