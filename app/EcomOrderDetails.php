<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EcomOrderDetails extends Model
{
    protected $table = 'tbl_ecom_order_details';
    protected $fillable=['order_id','user_first_name','user_last_name',
    'shipping_address','shipping_landmark','shipping_city','shipping_state','shipping_country','shipping_pincode',
    'billing_address','billing_landmark','billing_city','billing_state','billing_country','billing_pincode',
    'status','created_at','updated_at','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getorderdetails($data)
    {
        
        $query = DB::table('tbl_ecom_order_details as orddetail')
        ->select('orddetail.*')
        ->leftJoin('tbl_ecom_order as ecomorder','ecomorder.order_id','=','orddetail.order_id');
         
        if (array_key_exists('order_details_id', $data) && isset($data['order_details_id'])) {
            $query = $query->where('orddetail.order_details_id', '=' ,$data['order_details_id']);
        }

        if (array_key_exists('order_id', $data) && isset($data['order_id'])) {
            $query = $query->where('orddetail.order_id', '=' ,$data['order_id']);
        }

        if (array_key_exists('order_guid', $data) && isset($data['order_guid'])) {
            $query = $query->where('ecomorder.order_guid', '=' ,$data['order_guid']);
        }
                          
        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('orddetail.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('orddetail.status', '=' ,1);
        }

        $query->offset($data['offset'])->limit($data['limit']);

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('orddetail.'.$data['sortby'], $data['sorttype']);
        }
                          
        $result = $query->get();
                            
        return $result;
    }
}
