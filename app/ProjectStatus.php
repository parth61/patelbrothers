<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProjectStatus extends Model
{
    protected $table = 'tbl_project_status';
    protected $fillable=['project_status_name','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public static function getprojectstatus($data)
    {
        
         $query = DB::table('tbl_project_status as ps')->select('ps.*');
         
         if (array_key_exists('project_status_id', $data) && isset($data['project_status_id'])) {
            $query = $query->where('ps.project_status_id', '=' ,$data['project_status_id']);
           }
                          
         $query = $query->where('ps.status', '!=' ,0);
                          
         $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('ps.project_status_id', 'ASC')->get();
                            
         return $result;
    }
}
