<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Page extends Model
{
    
    protected $table = 'tbl_page';
    protected $fillable=['page_id','page_title','page_body','page_featured_img','template_id','site_id',
    'seo_id','status','created_by','updated_by','isvisible','browser_name','browser_version','browser_platform','ip_address'];

    public static function getpage($data)
    {
        
        $query = DB::table('tbl_page as pg')
        ->select('pg.*','pg.slug_name as page_slug','temp.template_file','adm.admin_firstname as cb_admin_firstname','adm.admin_lastname as cb_admin_lastname', 'admin.admin_firstname as ub_admin_firstname','admin.admin_lastname as ub_admin_lastname'
        ,'med.media_file as page_media_file','medi.media_file as seo_media_file','seo.seo_description','seo.seo_keywords','seo.seo_image','seo.seo_type','seo.seo_for','seo.seo_for_id','seo.seo_title','seo.seo_canonical')
        ->leftJoin('tbl_media as med','med.media_id','=','pg.page_featured_img')
         ->leftJoin('tbl_seo as seo','seo.seo_id','=','pg.seo_id')
         ->leftJoin('tbl_template as temp','temp.template_id','=','pg.template_id')
         ->leftJoin('tbl_admin as adm','adm.admin_id','=','pg.created_by')
         ->leftJoin('tbl_admin as admin','admin.admin_id','=','pg.created_by')
         ->leftJoin('tbl_media as medi','medi.media_id','=','seo.seo_image');

        if (array_key_exists('page_id', $data) && isset($data['page_id'])) {
            $query = $query->where('pg.page_id', '=' ,$data['page_id']);
        }

        if (array_key_exists('slug_name', $data) && isset($data['slug_name'])) {
            $query = $query->where('pg.slug_name', '=' ,$data['slug_name']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('pg.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('pg.status', '!=' ,0);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('pg.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('offset', $data) && isset($data['offset']) && array_key_exists('limit', $data) && isset($data['limit'])) {
            $query = $query->offset($data['offset'])->limit($data['limit']);
        }
        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) {
            $query = $query->orderBy('pg.'.$data['sortby'], $data['sorttype']);
        }
                                      
        $result = $query->get();
                            
        return $result;
    }
}
