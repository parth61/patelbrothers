<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EcomSetting extends Model
{
    protected $table = 'tbl_ecom_setting';
    protected $fillable=['setting_id','setting_value','setting_key','site_id','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];

    public  function getecomsetting($data)
    {
        
        $query = DB::table('tbl_ecom_setting as setting')->select('setting.*');
         
        if (array_key_exists('setting_key', $data) && isset($data['setting_key'])) {
            $query = $query->where('setting.setting_key', '=' ,$data['setting_key']);
        }
        
        if (array_key_exists('setting_id', $data) && isset($data['setting_id'])) {
            $query = $query->where('setting.setting_id', '=' ,$data['setting_id']);
        }

        if (array_key_exists('status', $data) && isset($data['status'])) {
            $query = $query->where('setting.status', '=' ,$data['status']);
        }
        else
        {
            $query = $query->where('setting.status', '=' ,1);
        }

        if (array_key_exists('site_id', $data) && isset($data['site_id']) && $data['site_id'] != "0") {
            $query = $query->where('setting.site_id', '=' ,$data['site_id']);
        }

        if (array_key_exists('sortby', $data) && isset($data['sortby']) && array_key_exists('sorttype', $data) && isset($data['sorttype'])) 
        {
            $query = $query->orderBy('setting.'.$data['sortby'], $data['sorttype']);
        }
        else
        {
            $query = $query->orderBy('setting.setting_id', 'DESC');
        }
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->get();
                            
        return $result;
    }
}
