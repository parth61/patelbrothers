<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Leadstatustertiary extends Model
{
    protected  $table = 'tbl_lead_status_tertiary';
    protected $fillable=['lead_status_secondary_id','lead_status_primary_id','lead_stauts_tertiary_name','status','created_by','updated_by','browser_name','browser_version','browser_platform','ip_address'];
    
     public static function getleadtertiary($data)
    {
        
        $query = DB::table('tbl_lead_status_tertiary as ct')->select('ct.*','s.lead_status_secondary_name','c.lead_status_primary_name')
      ->leftJoin('tbl_lead_status_primary as c', 'c.lead_status_primary_id', '=', 'ct.lead_status_primary_id')
        ->leftJoin('tbl_lead_status_secondary as s', 's.lead_status_secondary_id', '=', 'ct.lead_status_secondary_id');
         
        if (array_key_exists('lead_status_primary_id', $data) && isset($data['lead_status_primary_id'])) {
            $query = $query->where('ct.lead_status_primary_id', '=' ,$data['lead_status_primary_id']);
        }

        if (array_key_exists('lead_status_secondary_id', $data) && isset($data['lead_status_secondary_id'])) {
            $query = $query->where('ct.lead_status_secondary_id', '=' ,$data['lead_status_secondary_id']);
        }

        if (array_key_exists('lead_status_tertiary_id', $data) && isset($data['lead_status_tertiary_id'])) {
            $query = $query->where('ct.lead_status_tertiary_id', '=' ,$data['lead_status_tertiary_id']);
        }
                          
        $query = $query->where('ct.status', '!=' ,0);
                          
        $result = $query->offset($data['offset'])->limit($data['limit'])->orderBy('ct.lead_status_tertiary_id', 'ASC')->get();
                            
        return $result;
    }
}



