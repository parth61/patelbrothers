<?php
define("fileuploadtype","local");
define("apipath",'http://localhost/hootcms/api');  

//fileupload constants

define("site_url",'http://localhost/hootcms/');
// define("site_url",'https://admin.hoothoot.in/beta/');

//for server
define("imagedisplaypath",'https://d26fi9ryxbztxz.cloudfront.net/beta/media/');
define("mediapath", 'https://d26fi9ryxbztxz.cloudfront.net/beta/media/'); 

define("baseimagedisplaypath", base_path().'/media/');

define('razorpaykeyid','rzp_test_axLcbe3WQupLc5');
define('razorpaykeysecret','ZhrsBgUhtrjFxpLtohyWM8tF');

define("currentmediapath", base_path().'/media/'.date('Y').'/'.date('m'));
define("dashboardnotification",'');
define("bucketpath", '/beta/media/');

define("fixedimagepathbuilderlogo",url('/').'/media/placeholder/builder-logo-placeholder.jpg');
define("fixedimagepathbuilderfeaturedimage",url('/').'/media/placeholder/builder-featured-image-placeholder.jpg');

define("fixedimagepathprojectlogo",url('/').'/media/placeholder/project-logo-placeholder.jpg');
define("fixedimagepathprojectfeaturedimage",url('/').'/media/placeholder/project-featured-image-placeholder.jpg');
define("fixedimagepathprojectgalleryimage",url('/').'/media/placeholder/project-gallery-image-placeholder.jpg');

define("fixedimagepathsalesexecutive",url('/').'/media/placeholder/sales-executive-placeholder.jpg');

define("fixedimagepathlocation",url('/').'/media/placeholder/location-placeholder.jpg');

define("fixedimagepathdocument",url('/').'/media/placeholder/document-placeholder.jpg');

define("exotel_sid"," ");
define("exotel_api_key"," ");
define("exotel_api_token"," ");
define("wtsp_api_key"," ");
define("sms_api_key"," ");
define("maxuploadimagesize",4096000);
define("maximagesizemessage",4);
define("squareratio",'1:1');
define("logosize",'300, 300');
define("featureimagesize",'500, 500');
define("maxuploadsizeinmb",'4');
define("nearbyprojectkilometer",10);
define("brochurelink"," ");
define("projectbrochureprefix"," ");
define("invoiceprefix","hootcms-invoice-");
define("requiredfield",'<span class="text-danger">*</span>');
define("helptexticon",'<span class="svg-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" fill="#000000"/>
                            </g>
                        </svg>
                    </span>');
define("tableloader",'<div class="bouncing-loader"><div></div><div></div><div></div></div>');
define("loader", '<div id="hoot-loader" class="hoot-loader loader-block d-print-none"><div class="ring"></div><span>Loading...</span></div>');
define("defaultotp", '000444');
define("adminemail", 'sagar@themidnight.in');
//define("copyrightlink", '');
define("copyrightlink", 'https://credaichennai.in/?utm_source=email&amp;utm_medium=footerlink&amp;utm_campaign=credai365'); 
define("buttonloader",'<div class="buttoncontainer"><div class="buttonloader"><div class="ball first"></div><div class="ball second"></div><div class="ball third"></div></div></div>');


define("fixedmediaimage",null);
define("fixedpagefeaturedimage",null);
define("fixedpostfeaturedimage",null);
define("fixedtemplatefile",null);
define("fixedseofeaturedimage",null);
//for check string length input type  
define("minlength_inputtext",'1');
define("maxlength_inputtext",'120');

define("minlength_inputtextarea",'1');
define("maxlength_inputtextarea",'260');


//hoot e-commerce  constant
define("fixedcategorythumbnailimage",null);
define("fixedproductgalleryimage",null); 

define("editicon","<i class='flaticon-edit text-warning'></i>");
define("deleteicon","<i class='flaticon-delete text-danger'></i>");
define("settingsicon","<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='24px' height='24px' viewBox='0 0 24 24' version='1.1'><g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'><rect x='0' y='0' width='24' height='24'></rect><path d='M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z' fill='grey'></path></g></svg>");
?>
