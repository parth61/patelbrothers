<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/store','Stores3Controller@store');

//api for slug
Route::post('/addslug','SlugController@addslug');
Route::post('/getallslug','SlugController@getallslug');
Route::post('/updateslug','SlugController@updateslug');
Route::post('/deleteslug','SlugController@deleteslug');
Route::post('/checkslug','SlugController@checkslug');
Route::post('/getdatafromslug','SlugController@getDataFromSlug');

/*Login Related API */
Route::post('/login','LoginController@login');
Route::post('/getallloginhistory','LoginController@getallloginhistory');
Route::post('/sendresetpasswordlink','LoginController@sendResetPasswordLink');
Route::post('/resetpassword','LoginController@resetPassword');
Route::post('/test','TestingController@test');
Route::post('/sendorderemail','TestingController@sendorderemail');
/*Exotel Related API */
Route::post('/exotel','ExotelController@exotel');

/*OTP RELATED API FOR CUSTOMER LOGIN*/
Route::post('/sendotp','UserController@sendotp');
Route::post('/verifyotp','UserController@verifyotp');

/*Amenities Related API */
Route::post('/addamenities','AmenitiesController@addamenities');
Route::post('/getallamenities','AmenitiesController@getallamenities');
Route::post('/updateamenities','AmenitiesController@updateamenities');
Route::post('/deleteamenities','AmenitiesController@deleteamenities');
Route::post('/manageamenities','AmenitiesController@manageamenities');

/*Pincode Related API */
Route::post('/addpincode','PincodeController@addpincode');
Route::post('/getallpincode','PincodeController@getallpincode');
Route::post('/updatepincode','PincodeController@updatepincode');
Route::post('/deletepincode','PincodeController@deletepincode');
Route::post('/managepincode','PincodeController@managepincode');

Route::post('/getallcountry','PincodeController@getallcountry');
Route::post('/getallstate','PincodeController@getallstate');
Route::post('/getalldistrict','PincodeController@getalldistrict');
Route::post('/getallpincodedata','PincodeController@getallpincodedata');

/*Category Related API */
Route::post('/addcategory','CategoryController@addcategory');
Route::post('/getallcategory','CategoryController@getallcategory');
Route::post('/updatecategory','CategoryController@updatecategory');
Route::post('/deletecategory','CategoryController@deletecategory');
Route::post('/managecategory','CategoryController@managecategory');

/*city Master Related API */
Route::post('/addcity','CityController@addcity');
Route::post('/getallcity','CityController@getallcity');
Route::post('/updatecity','CityController@updatecity');
Route::post('/deletecity','CityController@deletecity');


/*country Master Related API */
Route::post('/addmastercountry','CountryController@addmastercountry');
Route::post('/getallmastercountry','CountryController@getallmastercountry');
Route::post('/updatemastercountry','CountryController@updatemastercountry');
Route::post('/deletemastercountry','CountryController@deletemastercountry');



/*LeadstatusprimaryController Master Related API */
Route::post('/addleadstatusprimary','LeadstatusprimaryController@addleadstatusprimary');
Route::post('/getallleadstatusprimary','LeadstatusprimaryController@getallleadstatusprimary');
Route::post('/updateleadstatusprimary','LeadstatusprimaryController@updateleadstatusprimary');
Route::post('/deleteleadstatusprimary','LeadstatusprimaryController@deleteleadstatusprimary');

/*LeadsourceprimaryController Master Related API */
Route::post('/addleadsourceprimary','LeadsourceprimaryController@addleadsourceprimary');
Route::post('/getallleadsourceprimary','LeadsourceprimaryController@getallleadsourceprimary');
Route::post('/updateleadsourceprimary','LeadsourceprimaryController@updateleadsourceprimary');
Route::post('/deleteleadsourceprimary','LeadsourceprimaryController@deleteleadsourceprimary');



/*LeadstatussecondaryController Master Related API */
Route::post('/addleadstatussecondary','LeadstatussecondaryController@addleadstatussecondary');
Route::post('/getallleadstatussecondary','LeadstatussecondaryController@getallleadstatussecondary');
Route::post('/updateleadstatussecondary','LeadstatussecondaryController@updateleadstatussecondary');
Route::post('/deleteleadstatussecondary','LeadstatussecondaryController@deleteleadstatussecondary');


/*LeadsourcetertiaryController Master Related API */
Route::post('/addleadsourcetertiary','LeadsourcetertiaryController@addleadsourcetertiary');
Route::post('/getallleadsourcetertiary','LeadsourcetertiaryController@getallleadsourcetertiary');
Route::post('/updateleadsourcetertiary','LeadsourcetertiaryController@updateleadsourcetertiary');
Route::post('/deleteleadsourcetertiary','LeadsourcetertiaryController@deleteleadsourcetertiary');


/*LeadsourcesecondaryController Master Related API */
Route::post('/addleadsourcesecondary','LeadsourcesecondaryController@addleadsourcesecondary');
Route::post('/getallleadsourcesecondary','LeadsourcesecondaryController@getallleadsourcesecondary');
Route::post('/updateleadsourcesecondary','LeadsourcesecondaryController@updateleadsourcesecondary');
Route::post('/deleteleadsourcesecondary','LeadsourcesecondaryController@deleteleadsourcesecondary');

/*LeadstatustertiaryController Master Related API */
Route::post('/addleadstatustertiary','LeadstatustertiaryController@addleadstatustertiary');
Route::post('/getallleadstatustertiary','LeadstatustertiaryController@getallleadstatustertiary');
Route::post('/updateleadstatustertiary','LeadstatustertiaryController@updateleadstatustertiary');
Route::post('/deleteleadstatustertiary','LeadstatustertiaryController@deleteleadstatustertiary');

/*Occupation Master Related API */
Route::post('/addmasteroccupation','OccupationController@addmasteroccupation');
Route::post('/getallmasteroccupation','OccupationController@getallmasteroccupation');
Route::post('/updatemasteroccupation','OccupationController@updatemasteroccupation');
Route::post('/deletemasteroccupation','OccupationController@deletemasteroccupation');


/*state Master Related API */
Route::post('/addmasterstate','StateController@addmasterstate');
Route::post('/getallmasterstate','StateController@getallmasterstate');
Route::post('/updatemasterstate','StateController@updatemasterstate');
Route::post('/deletemasterstate','StateController@deletemasterstate');


/*Units Master Related API */
Route::post('/addmasterunit','UnitController@addmasterunit');
Route::post('/getallmasterunits','UnitController@getallmasterunits');
Route::post('/updatemasterunit','UnitController@updatemasterunit');
Route::post('/deletemasterunit','UnitController@deletemasterunit');


/*Units Master Related API */
Route::post('/addmasterunitconfiguration','UnitConfigurationController@addmasterunitconfiguration');
Route::post('/getallmasterunitconfiguration','UnitConfigurationController@getallmasterunitconfiguration');
Route::post('/updatemasterunitconfiguration','UnitConfigurationController@updatemasterunitconfiguration');
Route::post('/deletemasterunitconfiguration','UnitConfigurationController@deletemasterunitconfiguration');

/*Size Range Related API */
Route::post('/addsizerange','SizeRangeController@addsizerange');
Route::post('/getallsizerange','SizeRangeController@getallsizerange');
Route::post('/updatesizerange','SizeRangeController@updatesizerange');
Route::post('/deletesizerange','SizeRangeController@deletesizerange');
Route::post('/managesizerange','SizeRangeController@managesizerange');

/*Configuration Related API */
Route::post('/addconfiguration','ConfigurationController@addconfiguration');
Route::post('/getallconfiguration','ConfigurationController@getallconfiguration');
Route::post('/updateconfiguration','ConfigurationController@updateconfiguration');
Route::post('/deleteconfiguration','ConfigurationController@deleteconfiguration');
Route::post('/manageconfiguration','ConfigurationController@manageconfiguration');

/*Highlight Related API */
Route::post('/addhighlight','HighlightController@addhighlight');
Route::post('/getallhighlight','HighlightController@getallhighlight');
Route::post('/updatehighlight','HighlightController@updatehighlight');
Route::post('/deletehighlight','HighlightController@deletehighlight');
Route::post('/managehighlight','HighlightController@managehighlight');

/*Project Status Related API */
Route::post('/addprojectstatus','ProjectStatusController@addprojectstatus');
Route::post('/getallprojectstatus','ProjectStatusController@getallprojectstatus');
Route::post('/updateprojectstatus','ProjectStatusController@updateprojectstatus');
Route::post('/deleteprojectstatus','ProjectStatusController@deleteprojectstatus');
Route::post('/manageprojectstatus','ProjectStatusController@manageprojectstatus');

/*Location Related API */
Route::post('/addlocation','LocationController@addlocation');
Route::post('/getalllocation','LocationController@getalllocation');
Route::post('/updatelocation','LocationController@updatelocation');
Route::post('/deletelocation','LocationController@deletelocation');
Route::post('/getalllocationwithhighestproject','LocationController@getalllocationwithhighestproject');

/*User Related API */
Route::post('/adduser','UserController@adduser');
Route::post('/getalluser','UserController@getalluser');
Route::post('/updateuser','UserController@updateuser');
Route::post('/deleteuser','UserController@deleteuser');
Route::post('/changepasswordbyid','UserController@changePasswordByid');
Route::post('/changepassword','UserController@changepassword');
Route::post('/forgotpassword','UserController@forgotpassword');
Route::post('/changepasswordbyguid','UserController@changePasswordByGuid');
Route::post('/updateuserphoto','UserController@updateuserphoto');

/*Builder Related API */
Route::post('/addbuilder','BuilderController@addbuilder');
Route::post('/getallbuilder','BuilderController@getallbuilder');
Route::post('/updatebuilder','BuilderController@updatebuilder');
Route::post('/deletebuilder','BuilderController@deletebuilder');
Route::post('/updatebuilderlogo','BuilderController@updatebuilderlogo');
Route::post('/updatebuilderfeaturedimage','BuilderController@updatebuilderfeaturedimage');

/*Role Related API */
Route::post('/addrole','RoleController@addrole');
Route::post('/getallrole','RoleController@getallrole');
Route::post('/updaterole','RoleController@updaterole');
Route::post('/deleterole','RoleController@deleterole');


/*Role Admin Related API */
Route::post('/addroleadmin','RoleAdminController@addroleadmin');
Route::post('/getallroleadmin','RoleAdminController@getallroleadmin');
Route::post('/updateroleadmin','RoleAdminController@updateroleadmin');
Route::post('/deleteroleadmin','RoleAdminController@deleteroleadmin');



/*Sales Executive Related API */
Route::post('/addsalesexecutive','SalesExecutiveController@addsalesexecutive');
Route::post('/getallsalesexecutive','SalesExecutiveController@getallsalesexecutive');
Route::post('/updatesalesexecutive','SalesExecutiveController@updatesalesexecutive');
Route::post('/deletesalesexecutive','SalesExecutiveController@deletesalesexecutive');
Route::post('/getsalesexecutivebyid','SalesExecutiveController@getsalesexecutivebyid');
Route::post('/updatesalesexecutivephoto','SalesExecutiveController@updatesalesexecutivephoto');

/*Sales Executive Status Related API */
Route::post('/addsalesexecutivestatus','SalesExecutiveStatusController@addsalesexecutivestatus');
Route::post('/getallsalesexecutivestatus','SalesExecutiveStatusController@getallsalesexecutivestatus');
Route::post('/updatesalesexecutivestatus','SalesExecutiveStatusController@updatesalesexecutivestatus');
Route::post('/deletesalesexecutivestatus','SalesExecutiveStatusController@deletesalesexecutivestatus');

/*Project Sales Executive Related API*/
Route::post('/getallprojectbysalesexecutiveid','ProjectSaleExecutiveController@getallprojectbysalesexecutiveid');

/*Project Related API */
Route::post('/addproject','ProjectController@addproject');
Route::post('/getallproject','ProjectController@getallproject');
Route::post('/updateproject','ProjectController@updateproject');
Route::post('/getallprojectlist','ProjectController@getallprojectlist');
Route::post('/deleteproject','ProjectController@deleteproject');
Route::post('/updateprojectlogo','ProjectController@updateprojectlogo');
Route::post('/updateprojectfeaturedimage','ProjectController@updateprojectfeaturedimage');

/*Project Certificate Related API */
Route::post('/addprojectcertificate','ProjectCertificateController@addprojectcertificate');
Route::post('/getallprojectcertificate','ProjectCertificateController@getallprojectcertificate');
Route::post('/updateprojectcertificate','ProjectCertificateController@updateprojectcertificate');
Route::post('/deleteprojectcertificate','ProjectCertificateController@deleteprojectcertificate');


/*Project Amenities Related API */
Route::post('/addprojectamenities','ProjectAmenitiesController@addprojectamenities');
Route::post('/getallprojectamenities','ProjectAmenitiesController@getallprojectamenities');
Route::post('/updateprojectamenities','ProjectAmenitiesController@updateprojectamenities');
Route::post('/deleteprojectamenities','ProjectAmenitiesController@deleteprojectamenities');

/*Project Budget Related API */
Route::post('/addprojectbudget','ProjectBudgetController@addprojectbudget');
Route::post('/getallprojectbudget','ProjectBudgetController@getallprojectbudget');
Route::post('/getallprojectbudgetid','ProjectBudgetController@getallprojectbudgetid');
Route::post('/updateprojectbudget','ProjectBudgetController@updateprojectbudget');
Route::post('/deleteprojectbudget','ProjectBudgetController@deleteprojectbudget');
Route::post('/manageprojectbudget','ProjectBudgetController@manageprojectbudget');


/*Project Highlight Related API */
Route::post('/addprojecthighlight','ProjectHighlightController@addprojecthighlight');
Route::post('/getallprojecthighlight','ProjectHighlightController@getallprojecthighlight');
Route::post('/updateprojecthighlight','ProjectHighlightController@updateprojecthighlight');
Route::post('/deleteprojecthighlight','ProjectHighlightController@deleteprojecthighlight');

/*Project Size Range Related API */
Route::post('/addprojectsizerange','ProjectSizeRangeController@addprojectsizerange');
Route::post('/getallprojectsizerange','ProjectSizeRangeController@getallprojectsizerange');
Route::post('/updateprojectsizerange','ProjectSizeRangeController@updateprojectsizerange');
Route::post('/deleteprojectsizerange','ProjectSizeRangeController@deleteprojectsizerange');

/*Project Configuration Related API */
Route::post('/addprojectconfiguration','ProjectConfigurationController@addprojectconfiguration');
Route::post('/getallprojectconfiguration','ProjectConfigurationController@getallprojectconfiguration');
Route::post('/updateprojectconfiguration','ProjectConfigurationController@updateprojectconfiguration');
Route::post('/deleteprojectconfiguration','ProjectConfigurationController@deleteprojectconfiguration');

/*Project Budget Related API */
Route::post('/addbudget','BudgetController@addbudget');
Route::post('/getallbudget','BudgetController@getallbudget');
Route::post('/updatebudget','BudgetController@updatebudget');
Route::post('/deletebudget','BudgetController@deletebudget');

/*Project Building Type Related API */
Route::post('/addbuildingtype','BuildingTypeController@addbuildingtype');
Route::post('/getallbuildingtype','BuildingTypeController@getallbuildingtype');
Route::post('/updatebuildingtype','BuildingTypeController@updatebuildingtype');
Route::post('/deletebuildingtype','BuildingTypeController@deletebuildingtype');


/*Project LayoutController Related API */
Route::post('/addlayout','LayoutController@addlayout');
Route::post('/getalllayout','LayoutController@getalllayout');
Route::post('/updatelayout','LayoutController@updatelayout');
Route::post('/deletelayout','LayoutController@deletelayout');



/*Project LegaldocumentController Related API */
Route::post('/addlegaldocument','LegaldocumentController@addlegaldocument');
Route::post('/getalllegaldocument','LegaldocumentController@getalllegaldocument');
Route::post('/updatelegaldocument','LegaldocumentController@updatelegaldocument');
Route::post('/deletelegaldocument','LegaldocumentController@deletelegaldocument');


/*Project RerastateController Related API */
Route::post('/addrerastate','RerastateController@addrerastate');
Route::post('/getallrerastate','RerastateController@getallrerastate');
Route::post('/updatererastate','RerastateController@updatererastate');
Route::post('/deletererastate','RerastateController@deletererastate');


/*Project PropertytypeController Related API */
Route::post('/addpropertytype','PropertytypeController@addpropertytype');
Route::post('/getallpropertytype','PropertytypeController@getallpropertytype');
Route::post('/updatepropertytype','PropertytypeController@updatepropertytype');
Route::post('/deletepropertytype','PropertytypeController@deletepropertytype');

/*Location Pincode Related API */
Route::post('/getalllocationpincode','LocationPincodeController@getalllocationpincode');
Route::post('/getallcustomlocationpincode','LocationPincodeController@getallcustomlocationpincode');

/*Project Photo Related API */
Route::post('/addprojectphoto','ProjectPhotoController@addprojectphoto');
Route::post('/getallprojectphoto','ProjectPhotoController@getallprojectphoto');
Route::post('/updateprojectphoto','ProjectPhotoController@updateprojectphoto');
Route::post('/deleteprojectphoto','ProjectPhotoController@deleteprojectphoto');


/*Source Related API */
Route::post('/getallsource','SourceController@getallsource');
Route::post('/getallprimarysource','SourceController@getallprimarysource');
Route::post('/getallsecondarysource','SourceController@getallsecondarysource');

/*Touch Point Related API */
Route::post('/getalltouchpoint','TouchPointController@getalltouchpoint');

/*Lead Status Related API */
Route::post('/getallprimarystatus','LeadStatusController@getallprimarystatus');
Route::post('/getallsecondarystatus','LeadStatusController@getallsecondarystatus');
Route::post('/getalltertiarystatus','LeadStatusController@getalltertiarystatus');

/*Lead  Related API */
Route::post('/addlead','LeadController@addlead');
Route::post('/getalllead','LeadController@getalllead');
Route::post('/updatelead','LeadController@updatelead');
Route::post('/deletelead','LeadController@deletelead');

Route::post('/useroptin','WhatsappController@userOptIn');
Route::get('/getuseroptinlist','WhatsappController@geUserOptInList');
Route::post('/sendresponse','WhatsappController@sendResponse');


Route::post('/adduserjourney','UserJourneyController@adduserjourney');
Route::get('/getalluserjourney','UserJourneyController@getalluserjourney');
Route::post('/updateuserjourney','UserJourneyController@updateuserjourney');
Route::post('/deleteuserjourney','UserJourneyController@deleteuserjourney');

Route::post('/addcontact','ContactController@addcontact');
Route::get('/getallcontact','ContactController@getallcontact');
Route::post('/updatecontact','ContactController@updatecontact');
Route::post('/deletecontact','ContactController@deletecontact');

Route::post('/addcustomerofferclaims','CustomerOfferClaimsController@addCustomerOfferClaims');
Route::post('/getallcustomerofferclaims','CustomerOfferClaimsController@getCustomerOfferClaims');
Route::post('/updatecustomerofferclaims','CustomerOfferClaimsController@updateCustomerOfferClaims');
Route::post('/deletecustomerofferclaims','CustomerOfferClaimsController@deleteCustomerOfferClaims');

Route::post('/getallpaymenttype','PaymentTypeController@getAllPaymentType');

Route::post('/addnewmasterdatarequest','UserController@addNewMasterDataRequest');

//send mail route

Route::post('/sendmail','SendMailController@sendmail');

// Get All banks


Route::post('/addbank','BankController@addbank');
Route::post('/getallbanks','BankController@getallbanks');
Route::post('/updatebank','BankController@updatebank');
Route::post('/deletebank','BankController@deletebank');

// Get All Regions
Route::post('/addregion','RegionsController@addregion');
Route::post('/getallregions','RegionsController@getallregions');   
Route::post('/updateregion','RegionsController@updateregion');
Route::post('/deleteregion','RegionsController@deleteregion');

//Project Configuration Details Related API
Route::post('/addconfigurationdetail','ProjectConfigurationDetailsController@addconfigurationdetail');
Route::post('/getallconfigurationdetail','ProjectConfigurationDetailsController@getallconfigurationdetail');
Route::post('/updateconfigurationdetail','ProjectConfigurationDetailsController@updateconfigurationdetail');
Route::post('/deleteconfigurationdetail','ProjectConfigurationDetailsController@deleteconfigurationdetail');


//Project Main Certificate Related API
Route::post('/addprojectmaincertificate','ProjectMainCertificateController@addprojectmaincertificate');
Route::post('/getallprojectmaincertificate','ProjectMainCertificateController@getallprojectmaincertificate');
Route::post('/updateprojectmaincertificate','ProjectMainCertificateController@updateprojectmaincertificate');
Route::post('/deleteprojectmaincertificate','ProjectMainCertificateController@deleteprojectmaincertificate');
Route::post('uploadtest','TestingController@uploadImage');
Route::get('/getcompleteuserslist','UserController@getcompleteuserslist');

/* Dashboard API */

Route::post('/getdashboardstats','DashboardController@getDashboardStats');
Route::post('/getprojectwisedata','DashboardController@getProjectwiseData');
Route::post('/getleadstatuswisedata','DashboardController@getLeadstatuswiseData');
Route::post('/getsalesexecutivewisedata','DashboardController@getSalesExecutiveWiseData');
Route::post('/gettouchpointwiseleaddata','DashboardController@getTouchpointWiseLeadData');
Route::post('/getdaywiseleaddata','DashboardController@getDayWiseLeadData');
Route::post('/getleadstatusdata','DashboardController@getDayWiseLeadStatusData');
Route::post('/getbuilderwiseleads','DashboardController@getBuilderwiseLeads');
Route::post('/getsourcewiseleads','DashboardController@getSourcewiseLeads');
Route::post('/getcustomerofferclaimstats','DashboardController@getCustomerOfferClaimStats');
Route::post('/getsalesexecutivedashboardstats','DashboardController@getSalesExecutiveDashboardStats');
Route::post('/getdaywiseleaddataforsalesexecutive','DashboardController@getDayWiseLeadDataforSalesExecutive');
Route::post('/getprojectwisedataforsalesexecutive','DashboardController@getProjectwiseDataforSalesExecutive');
Route::post('/gettouchpointwiseleaddataforsalesexecutive','DashboardController@getTouchpointWiseLeadDataforSalesExecutive');
Route::post('/getleadstatuswisedataforsalesexecutive','DashboardController@getLeadstatuswiseDataForSalesExecutive');

Route::post('/getpdflink','TestingController@getPDFlink');
Route::post('/getnearbylocationlist','TestingController@getNearByLocationList');


Route::post('/testimagepath','TestingController@testimagepath');

Route::post('/generateSequence','ProjectSequenceController@generateSequence');

///////////////////////////////////////////========= API FOR HOOT CMS E-Commerce=============///////////////////

//api for shipping Logistics
Route::post('/addlogistics','EcomLogisticController@addecomlogistic');
Route::post('/getalllogistics','EcomLogisticController@getallecomlogistic');
Route::post('/updatelogistics','EcomLogisticController@updateecomlogistic');
Route::post('/deletelogistics','EcomLogisticController@deleteecomlogistic');

//api for shipping class
Route::post('/addshippingclass','EcomShippingClassController@addshippingclass');
Route::post('/getallshippingclass','EcomShippingClassController@getallshippingclass');
Route::post('/updateshippingclass','EcomShippingClassController@updateshippingclass');
Route::post('/deleteshippingclass','EcomShippingClassController@deleteshippingclass');


//api for Address
Route::post('/addaddress','EcomAddressController@addaddress');
Route::post('/getalladdress','EcomAddressController@getalladdress');
Route::post('/updateaddress','EcomAddressController@updateaddress');
Route::post('/deleteaddress','EcomAddressController@deleteaddress');


//api for Ecompayement settings
Route::post('/addpaymentsettings','EcomPaymentSettingsController@addpaymentsettings');
Route::post('/getallpaymentsettings','EcomPaymentSettingsController@getallpaymentsettings');
Route::post('/updatepaymentsettings','EcomPaymentSettingsController@updatepaymentsettings');
Route::post('/deletepaymentsettings','EcomPaymentSettingsController@deletepaymentsettings');

//api for attribute
Route::post('/addattribute','EcomAttributeController@addattribute');
Route::post('/getallattribute','EcomAttributeController@getallattribute');
Route::post('/updateattribute','EcomAttributeController@updateattribute');
Route::post('/deleteattribute','EcomAttributeController@deleteattribute');
Route::post('/getallattributewithcount','EcomAttributeController@getallattributewithcount');

//api for attribute term
Route::post('/addattributeterm','EcomAttributeTermController@addattributeterm');
Route::post('/getallattributeterm','EcomAttributeTermController@getallattributeterm');
Route::post('/updateattributeterm','EcomAttributeTermController@updateattributeterm');
Route::post('/deleteattributeterm','EcomAttributeTermController@deleteattributeterm');

//api for Business Setting term
Route::post('/addbusinesssetting','EcomBusinessSettingController@addbusinesssetting');
Route::post('/getallbusinesssetting','EcomBusinessSettingController@getallbusinesssetting');
Route::post('/updatebusinesssetting','EcomBusinessSettingController@updatebusinesssetting');
Route::post('/deletebusinesssetting','EcomBusinessSettingController@deletebusinesssetting');

//api for ecom Category term
Route::post('/addecomcategory','EcomCategoryController@addcategory');
Route::post('/getallecomcategory','EcomCategoryController@getallcategory');
Route::post('/getallcategorywithcount','EcomCategoryController@getallcategorywithcount');
Route::post('/getmenucategory','EcomCategoryController@getmenucategory');
Route::post('/updateecomcategory','EcomCategoryController@updatecategory');
Route::post('/deleteecomcategory','EcomCategoryController@deletecategory');

//api for Product Category term
Route::post('/addproductcategory','EcomProductCategoryController@addproductcategory');
Route::post('/getallproductcategory','EcomProductCategoryController@getallproductcategory');
Route::post('/updateproductcategory','EcomProductCategoryController@updateproductcategory');
Route::post('/deleteproductcategory','EcomProductCategoryController@deleteproductcategory');

//api for Product  
Route::post('/addproductexcel','EcomProductController@addproductexcel');
Route::post('/addproduct','EcomProductController@addproduct');
Route::post('/getallproduct','EcomProductController@getallproduct');
Route::post('/updateproduct','EcomProductController@updateproduct');
Route::post('/deleteproduct','EcomProductController@deleteproduct');
Route::post('/getproductlist','EcomProductController@getproductlist');
Route::post('/getproductdetails','EcomProductController@getproductdetails');
Route::post('/getrelatedproductlist','EcomProductController@getrelatedproductlist');
Route::post('/gettrendyproduct','EcomProductController@gettrendyproduct');

//api for product Variant

Route::post('/generateallvariantforproduct','EcomProductController@generateallvariantforproduct');
Route::post('/deletevariantproduct','EcomProductController@deletevariantproduct');
Route::post('/getallproductvariant','EcomProductController@getallproductvariant');
Route::post('/addproductvariant','EcomProductVariantController@addproductvariant');

//api for Related Product  
Route::post('/addrelatedproduct','EcomRelatedProductController@addrelatedproduct');
Route::post('/getallrelatedproduct','EcomRelatedProductController@getallrelatedproduct');
Route::post('/updaterelatedproduct','EcomRelatedProductController@updaterelatedproduct');
Route::post('/deleterelatedproduct','EcomRelatedProductController@deleterelatedproduct');

//api for Product Variant Master
Route::post('/addproductvariantmaster','EcomProductVariantController@addproductvariantmaster');
Route::post('/getallproductvariantmaster','EcomProductVariantController@getallproductvariantmaster');
Route::post('/updateproductvariantmaster','EcomProductVariantController@updateproductvariantmaster');
Route::post('/deleteproductvariantmaster','EcomProductVariantController@deleteproductvariantmaster');


//api for Product Configurable
Route::post('/addproductconfigurable','EcomProductConfigurableController@addproductconfigurable');
Route::post('/getallproductconfigurable','EcomProductConfigurableController@getallproductconfigurable');
Route::post('/updateproductconfigurable','EcomProductConfigurableController@updateproductconfigurable');
Route::post('/deleteproductconfigurable','EcomProductConfigurableController@deleteproductconfigurable');

//api for Product Attribute
Route::post('/addproductattribute','EcomProductAttributeController@addproductattribute');
Route::post('/getallproductattribute','EcomProductAttributeController@getallproductattribute');
Route::post('/updateproductattribute','EcomProductAttributeController@updateproductattribute');
Route::post('/deleteproductattribute','EcomProductAttributeController@deleteproductattribute');


//api for Product Attribute Term
Route::post('/addproductattributeterm','EcomProductAttributeTermController@addproductattributeterm');
Route::post('/getallproductattributeterm','EcomProductAttributeTermController@getallproductattributeterm');
Route::post('/updateproductattributeterm','EcomProductAttributeTermController@updateproductattributeterm');
Route::post('/deleteproductattributerm','EcomProductAttributeTermController@deleteproductattributerm');
Route::post('/manageproductattributeterm','EcomProductAttributeTermController@manageproductattributeterm');

//api for Product  tag
Route::post('/addproducttag','EcomProductTagController@addproducttag');
Route::post('/getallproducttag','EcomProductTagController@getallproducttag');
Route::post('/updateproducttag','EcomProductTagController@updateproducttag');
Route::post('/deleteproducttag','EcomProductTagController@deleteproducttag');

//api for ecom tag
Route::post('/addtag','EcomTagController@addtag');
Route::post('/getalltag','EcomTagController@getalltag');
Route::post('/updatetag','EcomTagController@updatetag');
Route::post('/deletetag','EcomTagController@deletetag');




//api for Product  Gallery
Route::post('/addproductgallery','EcomProductGalleryController@addproductgallery');
Route::post('/getallproductgallery','EcomProductGalleryController@getallproductgallery');
Route::post('/updateproductgallery','EcomProductGalleryController@updateproductgallery');
Route::post('/deleteproductgallery','EcomProductGalleryController@deleteproductgallery');

/*Tax  Related API */
Route::post('/addtax','EcomTaxController@addtax');
Route::post('/getalltax','EcomTaxController@getalltax');
Route::post('/updatetax','EcomTaxController@updatetax');
Route::post('/deletetax','EcomTaxController@deletetax');

/*Tax  Related API */
Route::post('/addtaxstatus','EcomTaxStatusController@addtaxstatus');
Route::post('/getalltaxstatus','EcomTaxStatusController@getalltaxstatus');
Route::post('/updatetaxstatus','EcomTaxStatusController@updatetaxstatus');
Route::post('/deletetaxstatus','EcomTaxStatusController@deletetaxstatus');

/*Tax  Class API */
Route::post('/addtaxclass','EcomTaxClassController@addtaxclass');
Route::post('/getalltaxclass','EcomTaxClassController@getalltaxclass');
Route::post('/updatetaxclass','EcomTaxClassController@updatetaxclass');
Route::post('/deletetaxclass','EcomTaxClassController@deletetaxclass');

/*Order Related API */
Route::post('/getcheckoutsumary','EcomOrderController@getcheckoutsumary');
Route::post('/checkoutandpay','EcomOrderController@checkoutandpay');
Route::post('/checkout','EcomOrderController@checkout');
Route::post('/addorder','EcomOrderController@addorder');
Route::post('/getallorder','EcomOrderController@getallorder');
Route::post('/updateorder','EcomOrderController@updateorder');
Route::post('/deleteorder','EcomOrderController@deleteorder');
Route::post('/getorderdetails','EcomOrderController@getorderdetails');
Route::post('/sendemailinvoice','EcomOrderController@sendEmailInvoice');
Route::post('/updatestatebasedtax','EcomOrderController@updateStateBasedTax');

//api for Order Activity
Route::post('/addecomorderactivity','EcomOrderActivityController@addecomorderactivity');
Route::post('/getallecomorderactivity','EcomOrderActivityController@getallecomorderactivity');
Route::post('/updateecomorderactivity','EcomOrderActivityController@updateecomorderactivity');
Route::post('/deleteecomorderactivity','EcomOrderActivityController@deleteecomorderactivity');

/*Payment Related API */
Route::any('/addpayment','EcomPaymentController@addpayment');
Route::post('/payumoney/response','EcomPaymentController@payumoney_response');
Route::post('/getallpayment','EcomPaymentController@getallpayment');
Route::post('/updatepayment','EcomPaymentController@updatepayment');
Route::post('/getallpaymentprocessinfo','EcomPaymentController@getallpaymentprocessinfo');
Route::post('/updatepaymentbytxnid','EcomPaymentController@updatepaymentbytxnid');
Route::post('/updatepaymentbyrazorpaytxnid','EcomPaymentController@updatepaymentbyrazorpaytxnid');
Route::post('/updatepaymentbypayu','EcomPaymentController@updatepaymentbypayu');


//order detail api
Route::post('/addorderdetail','EcomOrderDetailController@addorderdetail');
Route::post('/getallorderdetail','EcomOrderDetailController@getallorderdetail');
Route::post('/updateorderdetail','EcomOrderDetailController@updateorderdetail');
Route::post('/deleteorderdetail','EcomOrderDetailController@deleteorderdetail');

//order note api
Route::post('/addordernotes','EcomOrderNotesController@addordernotes');
Route::post('/getallordernotes','EcomOrderNotesController@getallordernotes');
Route::post('/updateordernote','EcomOrderNotesController@updateordernote');
Route::post('/deleteordernote','EcomOrderNotesController@deleteordernote');

//ecomsetting related api
Route::post('/addecomsetting','EcomSettingController@addecomsetting');
Route::post('/getallecomsetting','EcomSettingController@getallecomsetting');
Route::post('/updateecomsetting','EcomSettingController@updateecomsetting');
Route::post('/deleteecomsetting','EcomSettingController@deleteecomsetting');
Route::post('/managecomsetting','EcomSettingController@managecomsetting');


//api for cart 
Route::post('/remindcartmail','EcomCartController@ReminderCartMail');
Route::post('/addcart','EcomCartController@addcart');
Route::post('/getallcart','EcomCartController@getallcart');
Route::post('/updatecart','EcomCartController@updatecart');
Route::post('/deletecart','EcomCartController@deletecart');
Route::get('/getcontent/{slug}', 'TestingController@getcontent');
Route::post('/generateinvoice', 'TestingController@generateinvoice');


//coupon related api
Route::post('/addcoupon','EcomCouponController@addcoupon');
Route::post('/getallcoupon','EcomCouponController@getallcoupon');
Route::post('/updatecoupon','EcomCouponController@updatecoupon');
Route::post('/deletecoupon','EcomCouponController@deletecoupon');
Route::post('/applycoupon','EcomCouponController@applycoupon');

//wishlist related api
Route::post('/addwishlist','EcomWishlistController@addwishlist');
Route::post('/getallwishlist','EcomWishlistController@getallwishlist');
Route::post('/updatewishlist','EcomWishlistController@updatewishlist');
Route::post('/deletewishlist','EcomWishlistController@deletewishlist');
Route::post('/deletewishlistbyproductid','EcomWishlistController@deletewishlistbyproductid');
Route::post('/getallwishlistandcartcount','EcomWishlistController@getallwishlistandcartcount');

///////////////////////=====Hoot cms api start here================//////////////////////////

//api for dashboarda data
Route::post('/gethootdashboarddata','DashboardController@gethootdashboarddata');

//api for Admin
Route::post('/addadmin','AdminController@addadmin');
Route::post('/getalladmin','AdminController@getalladmin');
Route::post('/updateadmin','AdminController@updateadmin');
Route::post('/deleteadmin','AdminController@deleteadmin');


//api for Media
Route::post('/addmedia','MediaController@addmedia');
Route::post('/getallmedia','MediaController@getallmedia');
Route::post('/updatemedia','MediaController@updatemedia');
Route::post('/deletemedia','MediaController@deletemedia');


//api for Page
Route::post('/addpage','PageController@addpage');
Route::post('/getallpage','PageController@getallpage');
Route::post('/updatepage','PageController@updatepage');
Route::post('/deletepage','PageController@deletepage');

//api for Post
Route::post('/addpost','PostController@addpost');
Route::post('/getallpost','PostController@getallpost');
Route::post('/updatepost','PostController@updatepost');
Route::post('/deletepost','PostController@deletepost');

//api for Post Tag
Route::post('/addposttag','PostTagController@addposttag');
Route::post('/getallposttag','PostTagController@getallposttag');
Route::post('/updateposttag','PostTagController@updateposttag');
Route::post('/deleteposttag','PostTagController@deleteposttag');

//api for Post Category
Route::post('/addpostcategory','PostCategoryController@addpostcategory');
Route::post('/getallpostcategory','PostCategoryController@getallpostcategory');
Route::post('/updatepostcategory','PostCategoryController@updatepostcategory');
Route::post('/deletepostcategory','PostCategoryController@deletepostcategory');

//api for Template
Route::post('/addtemplate','TemplateController@addtemplate');
Route::post('/getalltemplate','TemplateController@getalltemplate');
Route::post('/updatetemplate','TemplateController@updatetemplate');
Route::post('/deletetemplate','TemplateController@deletetemplate');

//api for Relation Post Category
Route::post('/addrelationpostcategory','RelationPostCategoryController@addrelationpostcategory');
Route::post('/getallrelationpostcategory','RelationPostCategoryController@getallrelationpostcategory');
Route::post('/updaterelationpostcategory','RelationPostCategoryController@updaterelationpostcategory');
Route::post('/deleterelationpostcategory','RelationPostCategoryController@deleterelationpostcategory');

//api for Relation Post Tag
Route::post('/addrelationposttag','RelationPostTagController@addrelationposttag');
Route::post('/getallrelationposttag','RelationPostTagController@getallrelationposttag');
Route::post('/updaterelationposttag','RelationPostTagController@updaterelationposttag');
Route::post('/deleterelationposttag','RelationPostTagController@deleterelationposttag');

//api for Template
Route::post('/manageseo','SeoController@manageseo');
Route::post('/getallseo','SeoController@getallseo');
Route::post('/updateseo','SeoController@updateseo');
Route::post('/deleteseo','SeoController@deleteseo');

//api for seo type
Route::post('/addseotype','SeoTypeController@addseotype');
Route::post('/getallseotype','SeoTypeController@getallseotype');
Route::post('/updateseotype','SeoTypeController@updateseotype');
Route::post('/deleteseotype','SeoTypeController@deleteseotype');

//api for seoglobal
Route::post('/addseoglobal','SeoGlobalController@addseoglobal');
Route::post('/getallseoglobal','SeoGlobalController@getallseoglobal');
Route::post('/updateseoglobal','SeoGlobalController@updateseoglobal');
Route::post('/deleteseoglobal','SeoGlobalController@deleteseoglobal');
Route::post('/manageseoglobal','SeoGlobalController@manageseoglobal');

//api for trackingcode
Route::post('/getalltrackingcode','TrackingCodeController@getAllTrackingCode');
Route::post('/managetrackingcode','TrackingCodeController@manageTrackingCode');

//api for menu
Route::post('/addmenu','MenuController@addmenu');
Route::post('/getallmenu','MenuController@getallmenu');
Route::post('/updatemenu','MenuController@updatemenu');
Route::post('/deletemenu','MenuController@deletemenu');

//api for menu location
Route::post('/addmenulocation','MenuLocationController@addmenulocation');
Route::post('/getallmenulocation','MenuLocationController@getallmenulocation');
Route::post('/updatemenulocation','MenuLocationController@updatemenulocation');
Route::post('/deletemenulocation','MenuLocationController@deletemenulocation');

//api for menu item
Route::post('/addmenuitem','MenuItemController@addmenuitem');
Route::post('/getallmenuitem','MenuItemController@getallmenuitem');
Route::post('/updatemenuitem','MenuItemController@updatemenuitem');
Route::post('/deletemenuitem','MenuItemController@deletemenuitem');

//api for menu item
/*
Route::post('/getallcountry','PincodeIndiaController@getallcountry');
Route::post('/getallstate','PincodeIndiaController@getallstate');
Route::post('/getalldistrict','PincodeIndiaController@getalldistrict');
Route::post('/getallpincode','PincodeIndiaController@getallpincode');
Route::post('/getallpincodeindia','PincodeIndiaController@getallpincodeindia');
*/

//api for menu item
Route::post('/addgeneralsetting','GeneralSettingController@addgeneralsetting');
Route::post('/getallgeneralsetting','GeneralSettingController@getallgeneralsetting');
Route::post('/updategeneralsetting','GeneralSettingController@updategeneralsetting');
Route::post('/deletegeneralsetting','GeneralSettingController@deletegeneralsetting');
Route::post('/managegeneralsetting','GeneralSettingController@managegeneralsetting');

//api for sliders
Route::post('/addslider','SliderController@addslider');
Route::post('/getallslider','SliderController@getallslider');
Route::post('/updateslider','SliderController@updateslider');
Route::post('/deleteslider','SliderController@deleteslider');

//api for slier items
Route::post('/addslideritem','SliderItemsController@addslideritem');
Route::post('/getallslideritem','SliderItemsController@getallslideritem');
Route::post('/updateslideritem','SliderItemsController@updateslideritem');
Route::post('/deleteslideritem','SliderItemsController@deleteslideritem');

//api for ecom Category term
Route::post('/addecomfaqcategory','EcomFaqCategoryController@addecomfaqcategory');
Route::post('/getallecomfaqcategory','EcomFaqCategoryController@getallecomfaqcategory');
Route::post('/getecomfaqcategorywithfaq','EcomFaqCategoryController@getecomfaqcategorywithfaq');
Route::post('/updatecomfaqcategory','EcomFaqCategoryController@updatecomfaqcategory');
Route::post('/deleteecomfaqcategory','EcomFaqCategoryController@deleteecomfaqcategory');

//api for ecom faq
Route::post('/addecomfaq','EcomFaqController@addecomfaq');
Route::post('/getallecomfaq','EcomFaqController@getallecomfaq');
Route::post('/updateecomfaq','EcomFaqController@updateecomfaq');
Route::post('/deleteecomfaq','EcomFaqController@deleteecomfaq');

//api for ecom faq
Route::post('/addshipping','EcomShippingController@addshipping');
Route::post('/updateshipping','EcomShippingController@updateshipping');
Route::post('/getallshipping','EcomShippingController@getallshipping');
Route::post('/deleteshipping','EcomShippingController@deleteshipping');

//api for ecom faq
Route::post('/addshippingmethod','EcomShippingMethodController@addshippingmethod');
Route::post('/getallshippingmethod','EcomShippingMethodController@getallshippingmethod');
Route::post('/updateshippingmethod','EcomShippingMethodController@updateshippingmethod');
Route::post('/deleteshippingmethod','EcomShippingMethodController@deleteshippingmethod');


/* Ecom Dashboard API */

Route::post('/getecomdashboardstats','EcomDashboardController@getDashboardStats');
Route::post('/getdaywiseorderstats','EcomDashboardController@getDaywiseOrderStats');


/* Ecom Dashboard API */

Route::post('/addvisitor','VisitorController@addVisitor');
Route::post('/getallvisitors','VisitorController@getAllVisitors');

/*
Route::post('/getprojectwisedata','DashboardController@getProjectwiseData');
Route::post('/getleadstatuswisedata','DashboardController@getLeadstatuswiseData');
Route::post('/getsalesexecutivewisedata','DashboardController@getSalesExecutiveWiseData');
Route::post('/gettouchpointwiseleaddata','DashboardController@getTouchpointWiseLeadData');
Route::post('/getdaywiseleaddata','DashboardController@getDayWiseLeadData');
Route::post('/getleadstatusdata','DashboardController@getDayWiseLeadStatusData');
Route::post('/getbuilderwiseleads','DashboardController@getBuilderwiseLeads');
Route::post('/getsourcewiseleads','DashboardController@getSourcewiseLeads');
Route::post('/getcustomerofferclaimstats','DashboardController@getCustomerOfferClaimStats');
Route::post('/getsalesexecutivedashboardstats','DashboardController@getSalesExecutiveDashboardStats');
Route::post('/getdaywiseleaddataforsalesexecutive','DashboardController@getDayWiseLeadDataforSalesExecutive');
Route::post('/getprojectwisedataforsalesexecutive','DashboardController@getProjectwiseDataforSalesExecutive');
Route::post('/gettouchpointwiseleaddataforsalesexecutive','DashboardController@getTouchpointWiseLeadDataforSalesExecutive');
Route::post('/getleadstatuswisedataforsalesexecutive','DashboardController@getLeadstatuswiseDataForSalesExecutive');
*/

/* Data Sync Methods */
Route::post('/syncslugtotable','DataSyncController@syncSlugtoTable');

// package
Route::post('/getallpackage','PackageController@getallpackage');
Route::post('/managepackage','PackageController@managepackage');

//modules
Route::post('/getallmodule','ModuleController@getallModule');
Route::post('/managemodule','ModuleController@managemodule');

//subscription
Route::post('/getallsubscription','SubscriptionController@getallsubscription');
Route::post('/managesubscription','SubscriptionController@managesubscription');